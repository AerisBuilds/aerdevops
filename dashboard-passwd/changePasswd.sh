#!/bin/bash
source hostFile

echo "Enter old password for grafanna"
read -s oldpass
echo $oldpass
curl -X PUT -H "Content-Type: application/json" -d '{
  "oldPassword": "'$oldpass'",
  "newPassword": "'$GrafPass'",
  "confirmNew": "'$GrafPass'"
}' -u admin:"$oldpass" http://"$GRAFANA"/api/user/password
echo

id=$(ssh -i "PF-Prod-Internal.pem" ec2-user@$HOST "sudo docker ps | grep nagios | cut -d ' ' -f 1"  2>&1)
echo "Changing password for Nagios"
echo $id
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin $NagPass'"
echo

id=$(ssh -i "PF-Prod-Internal.pem" ec2-user@$RAHOST "sudo docker ps | grep mqtt-master | cut -d ' ' -f 1"  2>&1)
echo "Changing password for RabbitMq Dashboard A"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$RAHOST bash -c "'sudo docker exec -i "$id" rabbitmqctl change_password guest $RabbitPass'"
echo

