#!/bin/bash
source hostFile

echo "Enter old password for grafanna"
read -s oldpass
echo $oldpass
curl -X PUT -H "Content-Type: application/json" -d '{
  "oldPassword": "'$oldpass'",
  "newPassword": "'$GrafPass'",
  "confirmNew": "'$GrafPass'"
}' -u admin:"$oldpass" http://"$GRAFANA"/api/user/password

curl -X POST -H "Content-Type: application/json" -d '{
  "name":"'$GrafUserNmae'",
  "email":"'$Grafemail'",
  "login":"'$Graflogin'",
  "password":"'$Grafuserpass'"
}'   -u admin:"$GrafPass" http://"$GRAFANA"/api/admin/users

echo


########Nagios Admin & New User##########

id=$(ssh -i "PF-Prod-Internal.pem" ec2-user@$HOST "sudo docker ps | grep nagios | cut -d ' ' -f 1"  2>&1)
echo "Changing password for Nagios"
echo $id
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin $NagPass'"
id=$(ssh -i "PF-Prod-Internal.pem" ec2-user@$HOST "sudo docker ps | grep nagios | cut -d ' ' -f 1"  2>&1)
echo "Changing password for Nagios"
echo $id
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin "$NagPass"'"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" htpasswd -b -c /tmp/htpasswd.users nagios $GuestPass'"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo docker cp "$id":/tmp/htpasswd.users /home/ec2-user/nagios-user"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo docker cp "$id":/usr/local/nagios/etc/htpasswd.users /home/ec2-user/"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo chown -R ec2-user:ec2-user /home/ec2-user/htpasswd.users"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo cat /home/ec2-user/nagios-user >> /home/ec2-user/htpasswd.users"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo docker cp /home/ec2-user/htpasswd.users  "$id":/usr/local/nagios/etc/"
ssh -i PF-Prod-Internal.pem ec2-user@$HOST "sudo docker exec -i "$id" chown -R root:root /usr/local/nagios/etc/htpasswd.users"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" sed -i 's/authorized_for_all_services=nagiosadmin/authorized_for_all_services=nagiosadmin,nagios/g' /usr/local/nagios/etc/cgi.cfg '"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" sed -i 's/authorized_for_all_hosts=nagiosadmin/authorized_for_all_hosts=nagiosadmin,nagios/g' /usr/local/nagios/etc/cgi.cfg '"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$HOST bash -c "'sudo docker exec -i "$id" service nagios restart '"
echo
##############################################
id=$(ssh -i "PF-Prod-Internal.pem" ec2-user@$RAHOST "sudo docker ps | grep mqtt-master | cut -d ' ' -f 1"  2>&1)
echo "Changing password for RabbitMq Dashboard A"
ssh -i "PF-Prod-Internal.pem"  ec2-user@$RAHOST bash -c "'sudo docker exec -i "$id" rabbitmqctl change_password guest $RabbitPass'"
echo
