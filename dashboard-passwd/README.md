## Readme

The script changes password of the following dashboard

 * Grafanna
 * Nagios
 * Rabbitmq

Change the hostFile to include the right IPs and password for Grafanna, Nagios and Rabbitmq dashboard.

Clone and run the script

```
chmod u+x changePasswd.sh
./changePasswd.sh
```

