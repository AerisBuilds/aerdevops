#!/bin/sh
/config/s3fs-fuse/src/s3fs -d -f -o endpoint="$REGION" -o use_cache=/tmp/cache -o enable_noobj_cache -o stat_cache_expire=60 -o enable_content_md5 -o use_rrs -o nonempty -o mp_umask=0022 -o allow_other -o bucket="$S3BUCKET" $MOUNTDIR &
sleep 5
/usr/sbin/sshd -D