#!/bin/bash
#aws configure
echo "AWS Access key?"
read  BASH_ACCESS_KEY
echo "AWS region ? ex) us-west-2"
read  BASH_REGION
echo "AWS Secret key?"
read  BASH_SECRET_KEY_TEMP
echo  $BASH_ACCESS_KEY   $BASH_REGION    $BASH_SECRET_KEY_TEMP
BASH_SECRET_KEY=${BASH_SECRET_KEY_TEMP//\//\\/}

aws configure set default.region        $BASH_REGION
aws configure set aws_access_key_id     $BASH_ACCESS_KEY
aws configure set aws_secret_access_key $BASH_SECRET_KEY_TEMP

PARAMETER_FILE="/opt/nginx-conf/parameter_file"
source $PARAMETER_FILE


function CREATE_ELASTIC_IP {

     RESULT_FILE="/opt/nginx-conf/result_file"
     aws ec2 allocate-address > $RESULT_FILE
     ALLOC_ID=`cat $RESULT_FILE |grep AllocationId | awk '{print $2}' |cut -c2- | sed 's/.$//'`
     TEMP_IP=`cat $RESULT_FILE |grep PublicIp | awk '{print $2}' |cut -c2- | sed 's/.$//'`
     ELASTIC_IP_ADDRESS=`echo ${TEMP_IP::-1}`
     echo "create elastic IP address $ALLOC_ID  $ELASTIC_IP_ADDRESS"
     rm -f $RESULT_FILE
}

function FILE_UPDATE {

#--------------------------------- /etc/nginx/nginx.conf

   #NGINX_CONF_FILE="/etc/nginx/nginx.conf"
   NGINX_CONF_FILE="/opt/nginx-conf/nginx.conf"
   NGINX_CONF_FILE_TEMP="/opt/nginx-conf/nginx.conf_temp"
   cp $NGINX_CONF_FILE $NGINX_CONF_FILE_TEMP
   sed -i -e "s/NGINXPLUS_A_DOMAIN_URL/$NGINXPLUS_A_DOMAIN_URL/" $NGINX_CONF_FILE
   sed -i -e "s/NGINXPLUS_B_DOMAIN_URL/$NGINXPLUS_B_DOMAIN_URL/" $NGINX_CONF_FILE   
   sed -i -e "s/NGINXPLUS_C_DOMAIN_URL/$NGINXPLUS_C_DOMAIN_URL/" $NGINX_CONF_FILE   
   sed -i -e "s/NGINXPLUS_PORT/$NGINXPLUS_PORT/" $NGINX_CONF_FILE
   sed -i -e "s/MQTT_A_DOMAIN_URL/$MQTT_A_DOMAIN_URL/" $NGINX_CONF_FILE
   sed -i -e "s/MQTT_PORT/$MQTT_PORT/" $NGINX_CONF_FILE

#--------------------------------- /etc/keepalived/keepalived.conf

   #KEEPALIVED_CONF_FILE="/etc/keepalived/keepalived.conf"
   KEEPALIVED_CONF_FILE="/opt/nginx-conf/keepalived.conf"
   KEEPALIVED_CONF_FILE_TEMP="/opt/nginx-conf/keepalived.conf_temp"
   cp $KEEPALIVED_CONF_FILE $KEEPALIVED_CONF_FILE_TEMP
   sed -i -e "s/MY_IP_ADDRESS/$MY_IP_ADDRESS/" $KEEPALIVED_CONF_FILE
   sed -i -e "s/PEER_IP_ADDRESS/$PEER_IP_ADDRESS/" $KEEPALIVED_CONF_FILE
   echo "elastic IP = $ELASTIC_IP_ADDRESS"
   sed -i -e "s/ELASTIC_IP_ADDRESS/$ELASTIC_IP_ADDRESS/" $KEEPALIVED_CONF_FILE

#------------------------------- /usr/libexec/keepalived/nginx-ha-notify

   #NGINX_HA_NOTIFY_FILE="/usr/libexec/keepalived/nginx-ha-notify"
   NGINX_HA_NOTIFY_FILE="/opt/nginx-conf/nginx-ha-notify"
   NGINX_HA_NOTIFY_FILE_TEMP="/opt/nginx-conf/nginx-ha-notify_temp"
   cp $NGINX_HA_NOTIFY_FILE $NGINX_HA_NOTIFY_FILE_TEMP
   sed -i -e "s/BASH_ACCESS_KEY/$BASH_ACCESS_KEY/" $NGINX_HA_NOTIFY_FILE
   sed -i -e "s/BASH_SECRET_KEY/$BASH_SECRET_KEY/" $NGINX_HA_NOTIFY_FILE
   sed -i -e "s/BASH_REGION/$BASH_REGION/" $NGINX_HA_NOTIFY_FILE
   sed -i -e "s/FIRST_NODE_NAME/$FIRST_NODE_NAME/" $NGINX_HA_NOTIFY_FILE
   sed -i -e "s/SECOND_NODE_NAME/$SECOND_NODE_NAME/" $NGINX_HA_NOTIFY_FILE
   echo "allocation ID = $ALLOC_ID"
   sed -i -e "s/ALLOC_ID/$ALLOC_ID/" $NGINX_HA_NOTIFY_FILE

}


function COPY_ALL_FILES {

     sudo scp -i /opt/$KEY  -o StrictHostKeyChecking=no -o BatchMode=yes $NGINX_CONF_FILE      ec2-user@$MY_IP_ADDRESS:/home/ec2-user/nginx.conf
     sudo scp -i /opt/$KEY  -o StrictHostKeyChecking=no -o BatchMode=yes $KEEPALIVED_CONF_FILE ec2-user@$MY_IP_ADDRESS:/home/ec2-user/keepalived.conf
     sudo scp -i /opt/$KEY  -o StrictHostKeyChecking=no -o BatchMode=yes $NGINX_HA_NOTIFY_FILE ec2-user@$MY_IP_ADDRESS:/home/ec2-user/nginx-ha-notify
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo cp -p /home/ec2-user/nginx.conf /etc/nginx/nginx.conf"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo cp -p /home/ec2-user/keepalived.conf /etc/keepalived/keepalived.conf"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo cp -p /home/ec2-user/nginx-ha-notify /usr/libexec/keepalived/nginx-ha-notify"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo chmod 755 /usr/libexec/keepalived/nginx-ha-notify"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo chmod 755 /usr/libexec/keepalived/nginx-ha-check"

}

function UPDATE_HOSTS_FILE {

     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo rm -f /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo cp /etc/hosts /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo chmod 666 /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo echo $FIRST_LINE   >> /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo echo $SECOND_LINE  >> /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo chmod 644 /home/ec2-user/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo cp /home/ec2-user/hosts /etc/hosts"
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo rm -f /home/ec2-user/hosts"

} 

function START_PROCESSES {
    
     sleep 2
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo service nginx start &"
     sleep 2
     ssh -i /opt/$KEY -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$MY_IP_ADDRESS "sudo service keepalived start &"

}

function CLEAN_TEMP_FILE {

   cp $NGINX_CONF_FILE_TEMP $NGINX_CONF_FILE
   rm -f $NGINX_CONF_FILE_TEMP
   cp $KEEPALIVED_CONF_FILE_TEMP $KEEPALIVED_CONF_FILE
   rm -f $KEEPALIVED_CONF_FILE_TEMP
   cp $NGINX_HA_NOTIFY_FILE_TEMP $NGINX_HA_NOTIFY_FILE
   rm -f  $NGINX_HA_NOTIFY_FILE_TEMP

}

idx=1

while [ $idx -le $TOTAL_NUMBER_OF_NODE ]
do 

   if [ $idx = 1 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_A1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_A1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_A_FIRST_NODE_IP
      PEER_IP_ADDRESS=$ZONE_A_SECOND_NODE_IP
      FIRST_NODE_NAME=$ZONE_A_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_A_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_A_FIRST_NODE_IP $ZONE_A_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_A_SECOND_NODE_IP $ZONE_A_SECOND_NODE_NAME"

      CREATE_ELASTIC_IP
      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   elif [ $idx = 2 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_A1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_A1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_A_SECOND_NODE_IP
      PEER_IP_ADDRESS=$ZONE_A_FIRST_NODE_IP
      FIRST_NODE_NAME=$ZONE_A_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_A_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_A_FIRST_NODE_IP $ZONE_A_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_A_SECOND_NODE_IP $ZONE_A_SECOND_NODE_NAME"

      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   elif [ $idx = 3 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_B1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_B1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_B_FIRST_NODE_IP
      PEER_IP_ADDRESS=$ZONE_B_SECOND_NODE_IP
      FIRST_NODE_NAME=$ZONE_B_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_B_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_A_FIRST_NODE_IP $ZONE_B_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_A_SECOND_NODE_IP $ZONE_B_SECOND_NODE_NAME"

      CREATE_ELASTIC_IP
      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   elif [ $idx = 4 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_B1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_B1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_B_SECOND_NODE_IP
      PEER_IP_ADDRESS=$ZONE_B_FIRST_NODE_IP
      FIRST_NODE_NAME=$ZONE_B_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_B_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_B_FIRST_NODE_IP $ZONE_B_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_B_SECOND_NODE_IP $ZONE_B_SECOND_NODE_NAME"

      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   elif [ $idx = 5 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_C1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_C1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_C_FIRST_NODE_IP
      PEER_IP_ADDRESS=$ZONE_C_SECOND_NODE_IP
      FIRST_NODE_NAME=$ZONE_C_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_C_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_C_FIRST_NODE_IP $ZONE_C_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_C_SECOND_NODE_IP $ZONE_C_SECOND_NODE_NAME"

      CREATE_ELASTIC_IP
      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   elif [ $idx = 6 ]
   then
      NGINXPLUS1_COMMON_DOMAIN_URL=$NGINXPLUS_C1_DOMAIN_URL
      MQTT1_COMMON_DOMAIN_URL=$MQTT_C1_DOMAIN_URL

      MY_IP_ADDRESS=$ZONE_C_SECOND_NODE_IP
      PEER_IP_ADDRESS=$ZONE_C_FIRST_NODE_IP
      FIRST_NODE_NAME=$ZONE_C_FIRST_NODE_NAME
      SECOND_NODE_NAME=$ZONE_C_SECOND_NODE_NAME

      FIRST_LINE="$ZONE_C_FIRST_NODE_IP $ZONE_C_FIRST_NODE_NAME"
      SECOND_LINE="$ZONE_C_SECOND_NODE_IP $ZONE_C_SECOND_NODE_NAME"

      FILE_UPDATE
      COPY_ALL_FILES
      UPDATE_HOSTS_FILE
      START_PROCESSES
      CLEAN_TEMP_FILE

   fi 


let "idx = $idx + 1"

done