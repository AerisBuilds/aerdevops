#!/bin/bash

cd /opt/
mkdir nginx-conf/
cp -rvp automotive-devops/nginx-plus/* nginx-conf/
tar -cvf nginx-conf.tar nginx-conf
read -p "server key:"  Key

for ngx in `cat /opt/nginx-conf-servers`
do

#Installing the software
sudo scp -i $Key  -o StrictHostKeyChecking=no -o BatchMode=yes /opt/nginx-conf.tar ec2-user@$ngx:/home/ec2-user/
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo tar -xvf /home/ec2-user/nginx-conf.tar'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo chown -R ec2-user:ec2-user /home/ec2-user/'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo mkdir -p /etc/ssl/nginx/'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/nginx-repo.crt /etc/ssl/nginx/nginx-repo.crt'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/nginx-repo.key /etc/ssl/nginx/nginx-repo.key'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo yum install -y ca-certificates-2015.2.6-65.0.1.16.amzn1.noarch'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo wget -P /etc/yum.repos.d https://cs.nginx.com/static/files/nginx-plus-amazon.repo'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo yum install -y  nginx-plus-1.13.4-1.amzn1.ngx.x86_64'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo mkdir -p /etc/nginx/ssl'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo yum install -y  keepalived-1.2.13-7.4.amzn1.x86_64 wget-1.18-3.27.amzn1.x86_64'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/certificate.pem  /etc/nginx/ssl/cert.pem'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/key.pem /etc/nginx/ssl/key.pem'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/nginx.conf /etc/nginx/nginx.conf'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/keepalived.conf /etc/keepalived/keepalived.conf'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/nginx-ha-check /usr/libexec/keepalived/nginx-ha-check'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo cp /home/ec2-user/nginx-conf/nginx-ha-notify /usr/libexec/keepalived/nginx-ha-notify'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo rm -rf  nginx-conf.tar'
ssh -i $Key -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo rm -rf  nginx-conf'
done