 #!/bin/bash
bold=$(tput bold)
normal=$(tput sgr0)
rm -f $Jenkins_workspace/git_diff/gitdiff/output
sudo rm -f /var/www/html/output
REPOBRANCH='atsp-core-c2:'$c2_branch' atsp-vsb-a2:'$a2_branch' atsp-owner-portal:'$op_branch' atsp-sync-service:'$sync_branch' atsp-core-bs:'$bs_branch' atsp-config:'$config_branch' atsp-catapi:'$catapi_branch' atsp-core-ecu:'$ecu_branch' atsp-core-em:'$em_branch' atsp-events-streamer:'$es_branch' feed-reporting:'$fr_branch' feed-processor:'$fr_branch' payments:'$pay_branch' product-catalog:'$pcui_branch' productcatalog:'$pc_branch' atsp-core-smh:'$smh_branch' atsp-core-sml:'$sml_branch' atsp-core-vehicle-state:'$vs_branch' atsp-dp-mdsync:'$mdsync_branch' atsp-mts:'$mts_branch' atsp-notification-manager:'$nm_branch' atsp-core-adaptor:'$pa_branch''
echo $REPOBRANCH
for x in $REPOBRANCH
do
    echo $x
    repo=$(echo $x | cut -d ':' -f1)
    branch=$(echo $x | cut -d ':' -f2)
    echo $repo
    echo $branch
    git clone https://$username_password@bitbucket.org/aeriscom/$repo.git
       cd $Jenkins_workspace/git_diff/gitdiff/$repo
    git checkout $branch
echo  "=================Start Comparision For $repo  between master & $branch ===================" >> $Jenkins_workspace/git_diff/gitdiff/output
    git diff --name-status  master..$branch  >> $Jenkins_workspace/git_diff/gitdiff/output
echo  "=================END  Comparision For $repo between master  & $branch ===================" >> $Jenkins_workspace/git_diff/gitdiff/output
    cd $Jenkins_workspace/git_diff/gitdiff
    rm -rf $Jenkins_workspace/git_diff/gitdiff/$repo
    cat $Jenkins_workspace/git_diff/gitdiff/output

done
sudo cp $Jenkins_workspace/git_diff/gitdiff/output /var/www/html/output