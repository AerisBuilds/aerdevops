#!/bin/bash
mkdir -p /opt/elasticsearch
cd /opt/
cp -rvp automotive-devops/elasticsearch/* elasticsearch/
cd /opt/elasticsearch
tar -cvf config.tar config
tar -cvf nginx.tar nginx
read -p "server key:"  pem

for elk in `cat /opt/log-servers`

do

sudo scp -i $pem  -o StrictHostKeyChecking=no -o BatchMode=yes /opt/elasticsearch/config.tar ec2-user@$elk:/home/ec2-user/
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo tar -xvf /home/ec2-user/config.tar'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo mkdir -p /opt/elasticsearch/'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo mv /home/ec2-user/config /opt/elasticsearch/'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo mkdir -p /opt/elasticsearch/config/scripts'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo sysctl -w vm.max_map_count=262144'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$elk 'sudo rm -rf /home/ec2-user/conf.tar'
done

sleep 5

for ngx in `cat /opt/monitoring-servers`
do
sudo scp -i $pem  -o StrictHostKeyChecking=no -o BatchMode=yes /opt/elasticsearch/nginx.tar ec2-user@$ngx:/home/ec2-user/
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo tar -xvf /home/ec2-user/nginx.tar'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo mv /home/ec2-user/nginx /etc/'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo rm -rf /home/ec2-user/nginx.tar'
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo mkdir -p /opt/kibana'
sudo scp -i $pem  -o StrictHostKeyChecking=no -o BatchMode=yes /opt/elasticsearch/kibana/kibana.yml ec2-user@$ngx:/home/ec2-user/
ssh -i $pem -o StrictHostKeyChecking=no -o BatchMode=yes ec2-user@$ngx 'sudo mv /home/ec2-user/kibana.yml /opt/kibana/'
done
