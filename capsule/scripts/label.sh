#!/bin/bash

function GET_NODE_INFO {
  rm -f nodes_info_for_label.list
  rm -f temp.list
  rm -f temp2.list
  kubectl get nodes |grep compute > temp.list

  exec < ./temp.list
       while read  line
         do
           NODE_NAME=`echo $line |awk '{print $1}'`
           IP_ADDRESS=`echo $line |awk '{print $1}' | cut -d "-" -f2,3,4,5 --output-delimiter='.' | rev | cut -c 4- |rev `
           echo $NODE_NAME " " $IP_ADDRESS >> temp2.list
        done

  exec < ./temp2.list
       while read  line
         do
           NODE_NAME=`echo $line |awk '{print $1}'`
           NODE_IP=`echo $line |awk '{print $2}'`
           echo "$NODE_NAME"
           echo "$NODE_IP"
           NODE_AWS_NAME=`aws ec2 describe-instances --filter "Name=private-ip-address,Values=$NODE_IP"  --output text |grep TAGS|grep Name|grep -v autoscaling |cut -f3`
           echo "$NODE_AWS_NAME"
           echo $NODE_NAME " " $NODE_AWS_NAME " " $NODE_IP >> nodes_info_for_label.list
        done
}


function CREATE_LABEL {

  exec < ./label_info.list
       while read line
         do
           whole_line=`echo $line`
           if [ ${whole_line:0:1} = "#" ]
           then
              continue
           fi

           NODE_CATAGORY=`echo $line |awk '{print $1}'`
           NODE_LABEL1=`echo $line |awk '{print $2}'`
           NODE_LABEL2=`echo $line |awk '{print $3}'`
           NODE_LABEL3=`echo $line |awk '{print $4}'`
           NODE_LABEL4=`echo $line |awk '{print $5}'`
           NODE_LABEL5=`echo $line |awk '{print $6}'`
           NODE_LABEL6=`echo $line |awk '{print $7}'`
           echo "NODE_CATAGORY:" $NODE_CATAGORY
           echo "NODE_LABEL1:" $NODE_LABEL1
           echo "NODE_LABEL2:" $NODE_LABEL2
           echo "NODE_LABEL3:" $NODE_LABEL3
           echo "NODE_LABEL4:" $NODE_LABEL4
           echo "NODE_LABEL5:" $NODE_LABEL5
           echo "NODE_LABEL6:" $NODE_LABEL6
 
           NODE_NAME=`cat ./nodes_info_for_label.list |grep $NODE_CATAGORY|awk '{print $1}'`
           echo "NODE_NAME:" $NODE_NAME

           sleep 1
           if [ "$NODE_LABEL1" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL1"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL1
           fi

           if [ "$NODE_LABEL2" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL2"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL2
           fi

           if [ "$NODE_LABEL3" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL3"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL3
           fi

           if [ "$NODE_LABEL4" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL4"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL4
           fi

           if [ "$NODE_LABEL5" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL5"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL5
           fi

           if [ "$NODE_LABEL6" != "" ] 
              then 
                 echo "echo--- kubectl label node  $NODE_NAME  $NODE_LABEL6"  
                 kubectl label node  $NODE_NAME  $NODE_LABEL6
           fi

           echo "-------------------------------------------------------------------------------"

         done

}

GET_NODE_INFO
CREATE_LABEL