#!/bin/bash

function GET_NODE_INFO {
  rm -f nodes_info_for_ebs.list
  rm -f nodes_ipaddress.list
  kubectl get nodes |grep compute | awk '{print$1}' | cut -d "-" -f2,3,4,5 --output-delimiter='.' | rev | cut -c 4- |rev > nodes_ipaddress.list
  exec < ./nodes_ipaddress.list
       while read  line
         do
           NODE_IP=`echo $line`
           echo "$NODE_IP"
           NODE_NAME=`aws ec2 describe-instances --filter "Name=private-ip-address,Values=$NODE_IP"  --output text |grep TAGS|grep Name|grep -v autoscaling |cut -f3`
           echo "$NODE_NAME"
           NODE_ID=`aws ec2 describe-instances --filter "Name=private-ip-address,Values=$NODE_IP"  --output text |grep INSTANCES |cut -f9`
           echo "$NODE_ID"
           echo $NODE_IP " " $NODE_NAME " " $NODE_ID >> nodes_info_for_ebs.list
        done
}

function CREATE_VOL {

  rm -f ebs_info_after_creation.list
  exec < ./ebs_info.list
       while read line
         do
          whole_line=`echo $line`

          if [ ${whole_line:0:1} = "#" ]
          then
              continue
          fi

          VOL_CATAGORY=`echo $line |awk '{print $1}'`
          VOL_NAME=`echo $line |awk '{print $2}'`
          VOL_ENCRYPTED=`echo $line |awk '{print $3}'`
          VOL_TYPE=`echo $line |awk '{print $4}'`
          VOL_SIZE=`echo $line |awk '{print $5}'`
          VOL_MOUNT=`echo $line |awk '{print $6}'`
          VOL_REGION=`echo $line |awk '{print $7}'`
          VOL_ZONE=`echo $line |awk '{print $8}'`
          echo $VOL_NAME " " $VOL_ENCRYPTED

          if [ "$VOL_ENCRYPTED" == "yes" ] || [ "$VOL_ENCRYPTED" == "Yes" ] || [ "$VOL_ENCRYPTED" == "YES" ]
          then 
             VOL_RESULT=`aws ec2 create-volume --encrypted --size $VOL_SIZE --region $VOL_REGION --availability-zone $VOL_ZONE --volume-type $VOL_TYPE --tag-specification 'ResourceType=volume,Tags={Key=Name,Value=$VOL_NAME}' --output text`
             # vol name is just '$VOL_NAME'
             # need to update vol name to match with EC2
             VOL_ID=`echo $VOL_RESULT  |awk '{print $7}'`
             echo $VOL_RESULT
             echo $VOL_ID
             aws ec2 create-tags --resources $VOL_ID --tags Key=Name,Value=$VOL_NAME
 
             echo $VOL_CATAGORY " " $VOL_NAME " " $VOL_ENCRYPTED" "  $VOL_TYPE " "  $VOL_SIZE " " $VOL_MOUNT " " $VOL_REGION " " $VOL_ZONE " " $VOL_ID >> ebs_info_after_creation.list

          else          
             echo "No encrypted volume is not allowed"
          fi

         done
}


function ATTACH_VOL {

  exec < ./ebs_info_after_creation.list
       while read line
         do
           whole_line=`echo $line`
           if [ ${whole_line:0:1} = "#" ]
           then
              continue
           fi

           VOL_CATAGORY=`echo $line |awk '{print $1}'`
           VOL_ID=`echo $line |awk '{print $NF}'`
           echo "VOL_CATAGORY:" $VOL_CATAGORY
           echo "VOL_ID:" $VOL_ID
           NODE_ID=`cat ./nodes_info_for_ebs.list |grep $VOL_CATAGORY|awk '{print $3}'`
           echo "NODE_ID:" $NODE_ID

           aws ec2 attach-volume --volume-id  $VOL_ID   --instance-id $NODE_ID --device /dev/xvdh 

           echo "--------------------"

         done

     echo "wait 7 sec to make sure volume is up"
     sleep 7
}

function FILE_SYSTEM {

  SSH="/root/.ssh/id_rsa"
  exec < ./nodes_info_for_ebs.list
       while read line
         do
           whole_line=`echo $line`
           if [ ${whole_line:0:1} = "#" ]
           then
              continue
           fi

           NODE_IP=`echo $line |awk '{print $1}'`
           echo "NODE_IP:" $NODE_IP
 
           ssh -i $SSH admin@$NODE_IP "hostname;df -kh;sudo mkfs -t ext4 /dev/xvdh;sleep 3; sudo mkdir /EBS;sudo mount -t ext4 -o noatime /dev/xvdh /EBS" < /dev/null
           sleep 1
           ssh -i $SSH admin@$NODE_IP "sudo chmod 666 /etc/fstab; sudo  echo \"/dev/xvdh   /EBS   ext4   defaults,nofail  0 2\" >> /etc/fstab; sudo chmod 644 /etc/fstab;df -kh;hostname" < /dev/null

           echo "-------------------------------------------------------------------------------"
           echo ""
           echo ""
           echo ""

         done

}

GET_NODE_INFO
CREATE_VOL
echo "wait for 10 sec to complete Volume creation"
sleep 10
ATTACH_VOL
FILE_SYSTEM