#
# Aeris Communications, Inc. ("Aeris") CONFIDENTIAL
# Unpublished Copyright 2013-2018 Aeris Communications, Inc., All Rights Reserved.
#
# NOTICE: This source code and the intellectual and technical concepts contained or practiced herein (collectively, the "Aeris IP")
# are proprietary to and exclusively owned by Aeris and its licensors and may be covered by U.S. and Foreign Patents or patent applications. 
# This source code and the Aeris IP are protected by all applicable trade secret and copyright laws.
# Access to this source code and the Aeris IP by any person not employed by Aeris is only provided under written agreement between Aeris and 
# the person or company to whom it is disclosed. No use may be made of this source code or the Aeris IP except to the extent expressly permitted 
# under that agreement, and no implied right or license to use this source code or the Aeris IP is implied. Further use, dissemination, 
# reproduction or publication of this source code or the Aeris IP without the express prior written permission of Aeris is strictly forbidden.
#
# The copyright notice above does not evidence any actual or intended publication or disclosure of this source code or any Aeris IP. 
# Aeris will prosecute any party who violates the terms of any agreement between Aeris and such party relating to this source code and the Aeris IP.    
#

import sys, getopt
import json
import os
import sys
import shutil
import logging
import time
import subprocess
from xml.dom.expatbuilder import Namespaces
#from test.test_xml_etree import namespace

# script version
VERSION = "2.0"

LOGMOD = "CAP-"
LOGPHASEUTL = "UTL-"

#-----------------------------------------
#             Utilities
#-----------------------------------------
def execCommand(command):
    
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()
    return process.returncode
    
# time utility so we get ISO8601 base timestamp
from datetime import datetime, tzinfo, timedelta
class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)


# utility to recreate a file and write to it
def writeToFile(folder, filename, data):
    
    fname = folder + '/' + filename
    f = open(fname, "w")
    f.write(data)
    f.close()
    return fname
    

# utility to slup a file into a buffer and return    
def slurpFile(filename):
    
    data = ''
    with open(filename, 'r') as f:
        data = f.read() #. replace('\n', '')
    return data


# utility to read a json file into a json object
def loadJsonConfiguration(filename):
    
    data = json.loads("{}")
    with open(filename) as f:
        data = json.load(f)
    return data


# re-create a working folder where all produced files will be placed
def createFolder(folder, recreate):
 
    if recreate == True:
        try:
             # request to delete the build folder and wait for completion
             shutil.rmtree(folder)
        except Exception as e:
            pass

    try:
        os.makedirs(folder)
        
        logging.info(LOGMOD + LOGPHASEUTL + "0001" + " Created working folder: " + folder)
    except Exception as e:
        pass
 
    if not os.path.exists(folder):
        logging.error(LOGMOD + LOGPHASEUTL + "0000" + " Cannot create working folder: " + folder)
        sys.exit(1)
        
    return folder


# utility to translate a data buffer with a dictionary
def translate(data, dictionary):
    
    for d in dictionary:
        data = data.replace('{' + d + '}', str(dictionary[d]))
    
    return data



