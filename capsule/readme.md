Aeris Communications, Inc. ("Aeris") CONFIDENTIAL

Unpublished Copyright 2013-2018 Aeris Communications, Inc., All Rights Reserved.
NOTICE: This source code and the intellectual and technical concepts contained or practiced herein (collectively, the "Aeris IP")
are proprietary to and exclusively owned by Aeris and its licensors and may be covered by U.S. and Foreign Patents or patent applications.
This source code and the Aeris IP are protected by all applicable trade secret and copyright laws.
Access to this source code and the Aeris IP by any person not employed by Aeris is only provided under written agreement between Aeris and
the person or company to whom it is disclosed. No use may be made of this source code or the Aeris IP except to the extent expressly permitted
under that agreement, and no implied right or license to use this source code or the Aeris IP is implied. Further use, dissemination,
reproduction or publication of this source code or the Aeris IP without the express prior written permission of Aeris is strictly forbidden.
The copyright notice above does not evidence any actual or intended publication or disclosure of this source code or any Aeris IP.
Aeris will prosecute any party who violates the terms of any agreement between Aeris and such party relating to this source code and the Aeris IP.
Project Title AMP Capsule Infrastruture

Prerequisites Ubuntu kops Version 1.8.1 (git-94ef202) kubectl v1.10.3 helm v2.8.2 aws cli AWS account with user having AmazonEC2FullAccess,AmazonRoute53FullAccess,AmazonS3FullAccess,IAMFullAccess,AmazonVPCFullAccess permissions VPC with 10.147.0.0/16 and Route53 sub domain under the AWS account

Usage: COMMAND: create-capsule, kops-create-cluster, kops-create-instance-group, kops-update, kops-delete helm-install helm-update -c --cloud cloud (minikube, aws, gcp, azure, ...) default aws -p --profile profile name -n --capsule_name cap0, cap1, ... default cap0 -m --market market (us, eu, ch, ...), default us -s --size size of capsule (mini, small, medium, large, xlarge), default medium -d --domain domain/cert -x --namespaces namespaces to confifugre and deploy -r --region cloud region -r --vpc_id id of the vpc -r --vpc_cidr cidr of the vpc --dry_run if included only print --owner the name of the owner of the cluster --team the team name owning the cluster --baselocation where to store the cluster configuration (e.g. s3://...) --azs comma delimited list of availability zones (currently up to 3) --config_aws configure aws? if yes then parameters are <aws_key>,<aws_secret> --charts Helm charts to install or update --helmrepo Helm repo to add -v --version Version of this script -h --help This help menu

e.g : python setup.py create-capsule --helmrepo="devhelmcharts s3://aerisdevhelmchartrepo/charts" --charts="devhelmcharts/capsule --version 0.1.0" --profile dev01 --size=small --domain=aeriskube.net --region us-west-2 --vpc_id vpc-1b744101 --vpc_cidr 10.147.0.0/16 --owner amey --team devops --baselocation s3://clusters.dev01-us.aeriskube.net --azs us-west-2a --config_aws=aaaaaaaaa,sssssssssss &

Authors: Amey Mahajan (amey.mahajan@aeris.net)