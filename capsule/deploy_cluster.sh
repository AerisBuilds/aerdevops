#!/bin/bash
#
#As python command is too long for jenkins. We create this shell script to generate ssh-keys and add the python command here
#
#
rm -f /opt/capsule/environment/automotive-dci/pki/ssh/public/admin/*
mkdir /root/.ssh
ssh-keygen -t rsa -b 4096 -C "" -P "" -f /opt/capsule/environment/automotive-dci/pki/ssh/public/admin/id_rsa
mv /opt/capsule/environment/automotive-dci/pki/ssh/public/admin/id_rsa  /root/.ssh/
mv /opt/capsule/environment/automotive-dci/pki/ssh/public/admin/id_rsa.pub  /root/.ssh/

cd opt/capsule
python setup.py create-capsule --helmrepo="automotive-dci-us-helmcharts s3://automotive-dci-us-helmchartrepo/charts" --charts="automotive-dci-us-helmcharts/capsule --version 0.1.0" --profile automotive-dci --size=automotive-dci --domain=aeriskube.net --market=us --region us-west-2 --vpc_id vpc-1b744762 --vpc_cidr 10.147.0.0/16 --owner preshit --team devops --baselocation s3://clusters.automotive-dci-us.aeriskube.net --azs us-west-2a --config_aws=AKIAJB2CNAK7KDFHSILQ,TPXDW/6758y33tdjXEmx4GhNTmRP27R8xWatWY3a
