#
# Aeris Communications, Inc. ("Aeris") CONFIDENTIAL
# Unpublished Copyright 2013-2018 Aeris Communications, Inc., All Rights Reserved.
#
# NOTICE: This source code and the intellectual and technical concepts contained or practiced herein (collectively, the "Aeris IP")
# are proprietary to and exclusively owned by Aeris and its licensors and may be covered by U.S. and Foreign Patents or patent applications. 
# This source code and the Aeris IP are protected by all applicable trade secret and copyright laws.
# Access to this source code and the Aeris IP by any person not employed by Aeris is only provided under written agreement between Aeris and 
# the person or company to whom it is disclosed. No use may be made of this source code or the Aeris IP except to the extent expressly permitted 
# under that agreement, and no implied right or license to use this source code or the Aeris IP is implied. Further use, dissemination, 
# reproduction or publication of this source code or the Aeris IP without the express prior written permission of Aeris is strictly forbidden.
#
# The copyright notice above does not evidence any actual or intended publication or disclosure of this source code or any Aeris IP. 
# Aeris will prosecute any party who violates the terms of any agreement between Aeris and such party relating to this source code and the Aeris IP.    
#

import sys, getopt
import json
import os
import sys
import shutil
import logging
import time
import commutls
import subprocess
from xml.dom.expatbuilder import Namespaces

# from test.test_xml_etree import namespace

# script version
VERSION = "2.1"
#Updated from v2.0 to make it work with the new 'Capsule' repository structure

LOGMOD = "CAP-"
LOGPHASEUTL = "UTL-"
LOGPHASECMN = "CMN-"
LOGPHASEKOPS = "KOPS-"
LOGPHASEHELM = "HELM-"

# by default do not dry run
dryRun = False
command = ''

# default properties
properties = { 'cloud': 'aws', 'size': 'medium', 'capsule': 'cap0', 'market': 'us', 'az1': '1', 'az2': '2', 'az3': '3', 'team': 'ampdev', 'owner': 'developer' }
instanceGroupsFiles = []
clusterSubnets = {}

#-----------------------------------------
#       Configure KOPS workspace
#-----------------------------------------


# re-create a working folder where all produced files will be placed
def createWorkingFolder():

    return commutls.createFolder('./.kops', True)
   

# get a list of all name spaces folders off of a given size
def getNamespacesFolders(size):
    
    folder = "./environment/" + size
    try:
        # grab all the child folders
        logging.info(LOGMOD + LOGPHASECMN + "0006" + " Retrieving namespaces under " + folder)
        k8_namespaces = next(os.walk(folder))[1]
        
        return k8_namespaces
    
    except Exception as e:
        logging.error(LOGMOD + LOGPHASECMN + "0007" + " Error : ", format(e))
        print ("Error : ", format(e))
        sys.exit(1)


# get the namespaces to configure either explicitly collected in the command line
# or by default all the namespaces folder in the capsule folder
def getNamespacesToConfigure(): 
    
    size = properties['size']
    cloud = properties['cloud']
    
    k8_namespaces = {}
    ns = []
    
    # if no namespaces been defined then configure all namespaces in the template folder
    if not 'namespaces' in properties:        
        # Get all the namespaces folders
        ns = getNamespacesFolders(size);
    else:    
        # parse the namespaces from the command line
        ns = [x.strip() for x in properties.split(',')];
        
    # append the parent folder so we have complete relative path
    folder = './environment/' + size + '/'
    for i, n in enumerate(ns):
        k8_namespaces[n] = folder + n + '/' + cloud
            
    logging.info(LOGMOD + LOGPHASECMN + "0008" + " Namespaces to configure:", k8_namespaces)
        
    return k8_namespaces

    
# return the list of instance groups to prepare
def getInstanceGroups(namespaceFolder, ns):
    
    # read and translate instance groups
    file = namespaceFolder + '/instance-group.json'
    
    instanceGroups = []
    
    try:
        instanceGroups = json.loads(commutls.translate(commutls.translate(commutls.slurpFile(file), properties), { 'namespace': ns }))
        logging.debug("Translated " + file + ": " + json.dumps(instanceGroups))
    except Exception as e:
        pass
    
    if not 'instanceGroups' in instanceGroups:
        return []

    return instanceGroups['instanceGroups']


# return the list of subnets to prepare
def getSubnets(namespaceFolder, ns):
    
     # read and translate subnets, Translate namespace along the way
    file = namespaceFolder + '/subnet.json'
    
    subnets = []
    
    try:
        subnets = json.loads(commutls.translate(commutls.translate(commutls.slurpFile(file), properties), { 'namespace': ns }))
        logging.debug("Translated " + file + ": " + json.dumps(subnets))
    except Exception as e:
        pass
    
    if not 'subnets' in subnets:
        return []

    # add zones that we have previously collected from command line to the subnets objects
    subs = subnets['subnets']
    for i in range(1, 4):
        prop = 'azs.zone.' + str(i)
        if prop in properties:
            subs[i - 1]['zone'] = properties[prop]
            
    logging.debug("Translated " + file + ": " + json.dumps(subs))
    
    return subs


# configure all the properties for a particular instance group, then use the template and 
# write the results to the working folder. 
# Also update the global instanceGroupsFiles so it will be easier to iterate afterwards
def prepareInstanceGroupProperties(workingFolder, k8s_namespace, instanceGroupTemplate, instance, subnets):
    
    properties['instance.name'] = instance['name']
    properties['instance.type'] = instance['instanceType']
    properties['instance.maxSize'] = instance.get('maxSize', 1)  # default to 1
    properties['instance.minSize'] = instance.get('minSize', 1)  # default to 1
    properties['instance.subnet'] = ''
    properties['instance.labels'] = ''
    properties['instance.role'] = instance.get('role', "Node")  # default Node
    
    # add all labels
    if not 'labels' in instance:
        logging.info(LOGMOD + LOGPHASEKOPS + "0010" + " Check " + k8s_namespace + " instance groups, one of the machines is missing at least one label")
        sys.exit(1)
        
    properties['instance.labels'] += ''
    for key, value in instance['labels'].items():
        properties['instance.labels'] += key + ': ' + "\"" +value +  "\"" '\n    '
        
    # add all subnets
    properties['instance.subnet'] += '  '
    # for s in subnets:
    properties['instance.subnet'] += '- ' + instance['subnet'] + '\n    '
    
    instanceGroup = commutls.translate(instanceGroupTemplate, properties)
    logging.debug("Translated template: " + instanceGroup)
    
    # create the instancegroup folder that kops needs
    igf = workingFolder + '/instancegroup'
    commutls.createFolder(igf, False)
    
    # write it to the working folder instance group
    # filename = 'instance-group-' + properties['instance.name']
    filename = properties['instance.name']

    fname = commutls.writeToFile(igf, filename, instanceGroup) 
    logging.debug("Wrote to " + fname)
    
    instanceGroupsFiles.append(fname)
    
    if properties['instance.role'] != 'Node':
        properties['cluster.masterInstanceGroup'] += '- instanceGroup: ' + filename + '\n     '
        properties['cluster.masterInstanceGroup'] += '  name: ' + filename + '\n     '  
    return instanceGroup
    

def prepareNamespace(workingFolder, k8_namespaces):
    
    # read the instance group template file and translate it by substitute properties
    instanceGroupTemplate = commutls.translate(commutls.slurpFile('./kops' + '/instance-group-template.yaml'), properties)

    global clusterSubnets
    clusterSubnets = {}
    properties['cluster.masterInstanceGroup'] = ''
    properties['cluster.masterInstanceGroup'] += ' '
    # for each namespace
    for n in k8_namespaces:
        
        # Get all the instance groups of this namespace
        instanceGroups = getInstanceGroups(k8_namespaces[n], n)
        
        # Get all the subnets that this namespace can have. Translate the {namespace}
        subnets = getSubnets(k8_namespaces[n], n)
        
        # store all subnets (under their name spaces for later we may need them for the cluster specification
        clusterSubnets[n] = subnets

        # for each instance group prepare it, add all the subnets that it shall have
        for ig in instanceGroups:
            prepareInstanceGroupProperties(workingFolder, n, instanceGroupTemplate, ig, subnets)


def getClusterConfiguration():
    
        # read and translate instance groups
    file = './kops' + '/cluster-config.json'
    clusterConfig = json.loads(commutls.translate(commutls.slurpFile(file), properties))
    logging.debug("Translated " + file + ": " + json.dumps(clusterConfig))
    
    if not 'cluster' in clusterConfig:
        return []

    return clusterConfig['cluster']


def prepareClusterSpecProperties(clusterConfig):
    
    properties['cluster.name'] = clusterConfig['name']
    properties['cluster.type'] = clusterConfig['type']
    properties['cluster.dnsZone'] = clusterConfig['dnsZone']
    properties['cluster.dockerVersion'] = clusterConfig.get('dockerVersion') 
    properties['cluster.subnets'] = ''
    properties['cluster.bucketRegion'] = clusterConfig['bucketRegion']
    
    if not 'instanceGroups' in clusterConfig:
        logging.info(LOGMOD + LOGPHASEKOPS + "0011" + " Check cluster configuration instance groups, at least one instance group must be defined")
        sys.exit(1)
        
        
    properties['cluster.subnets'] += '   '
    for ns, subs in clusterSubnets.items():
        for sub in subs:
            properties['cluster.subnets'] += '- cidr: ' + sub['cidr'] + '\n     '
            properties['cluster.subnets'] += '  id: ' + sub['id'] + '\n     '            
            properties['cluster.subnets'] += '  name: ' + sub['name'] + '\n     '
            properties['cluster.subnets'] += '  type: ' + sub.get('type', 'Private') + '\n     '  # use configuration value or default to Private
            properties['cluster.subnets'] += '  zone: ' + sub['zone'] + '\n     '
        

def prepareClusterSpec(workingFolder):
    
    logging.info(LOGMOD + LOGPHASEKOPS + "0012" + " Preparing cluster specification")
    
    clusterConfig = getClusterConfiguration()
    
    prepareClusterSpecProperties(clusterConfig)
    
    clusterSpec = commutls.translate(commutls.slurpFile('./kops' + '/cluster-spec-template.yaml'), properties)
    logging.debug("Translated template: " + clusterSpec)
    
    config = commutls.translate(commutls.slurpFile('./kops' + '/config-template.yaml'), properties)
    logging.debug("Translated template: " + config)
    
    folder = workingFolder 
    commutls.createFolder(folder, False)
    
    # write it to the working folder
    filename = 'cluster.spec'
    fname = commutls.writeToFile(folder, filename, clusterSpec)
    logging.debug("Wrote spec to " + fname)
    
    # write to config
    configFilename = 'config'
    fname = commutls.writeToFile(folder, configFilename, config)
    logging.debug("Wrote config to " + fname)
    
    logging.info(LOGMOD + LOGPHASEKOPS + "0013" + " Cluster specification ready")


#-----------------------------------------
#       KOPS Commands
#-----------------------------------------
def kopsWaitForCluster(waitSec, dryRun): 
    
    if dryRun == True:
        return 0
    
    # compute the end time
    endTime = time.time() + waitSec
    
    ok = False
        
    # wait until we get a positive ak
    while time.time() < endTime:
        logging.info(LOGMOD + LOGPHASEKOPS + "0009" + " checking cluster ... ")
        
        # TODO: here check the cluster using a curl command or something else
        if ok == True:
            return 0
        
        time.sleep(1);
        
    logging.error(LOGMOD + LOGPHASEKOPS + "0010" + " timeout waiting for cluster")
    return -1


# Configuration aws cli
def awsConfigure():
    cmd = "aws configure set aws_access_key_id " + properties["aws_key"]
    res = commutls.execCommand(cmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0002" + " failed to set aws access key")
        sys.exit(-1)
    cmd = "aws configure set aws_secret_access_key " + properties["aws_secret"]
    res = commutls.execCommand(cmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0003" + " failed to set aws secret key")
        sys.exit(-1)    
    cmd = "aws configure set default.region " + properties["region"]
    res = commutls.execCommand(cmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0004" + " failed to set aws default region")
        sys.exit(-1)     
    cmd = "aws configure set default.output json"
    res = commutls.execCommand(cmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0005" + " failed to set aws default output")
        sys.exit(-1) 

def awsCleanup():            
 
    res = commutls.execCommand("aws configure set aws_access_key_id \"\" ")
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0006" + " failed to cleanup aws access key")
        sys.exit(-1)     
    res = commutls.execCommand("aws configure set aws_secret_access_key \"\" ")
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0007" + " failed to cleanup aws secret key")
        sys.exit(-1)     
    res = commutls.execCommand("aws configure set default.region \"\" ")
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0008" + " failed to cleanup default region")
        sys.exit(-1) 

        
# Creating S3 bucket for KOPS state repo            
def awsCreateKopsStateRepo():
    
    res = commutls.execCommand("aws s3 ls " + properties["baselocation"])
    if res != 0:
        
        cmd = "aws s3api create-bucket --bucket " + properties["baselocation"] + " --region " + properties['cluster.bucketRegion']
                                 
        res = commutls.execCommand(cmd)
        if res != 0:
            logging.error(LOGMOD + LOGPHASEKOPS + "0011" + " Error executing: " + cmd)
        else:
            logging.info(LOGMOD + LOGPHASEKOPS + "0012" + " Created: " + cmd)


# Uploading KOPS state to S3 bucket        
    
def awsUpdateKopsStateRepo(workingFolder):
    # copy the working folder to the KOPS repo on s3
    s3bucket = properties['baselocation'] + "/" + properties['cluster.name']
    cpS3Cmd = "aws s3 cp " + workingFolder + " " + s3bucket + " --recursive"
    logging.info(LOGMOD + LOGPHASEKOPS + "0013" + " Executing: " + cpS3Cmd)
    res = commutls.execCommand(cpS3Cmd)
    if res != 0:
            logging.error(LOGMOD + LOGPHASEKOPS + "0015" + " Error executing: " + cpS3Cmd)
    else:
            logging.info(LOGMOD + LOGPHASEKOPS + "0016" + " Created: " + cpS3Cmd)


# Creating ssh PKI for cluster
def kopsCreatePKI():
    #cmd = "cp pki/ssh/public/admin/*  ~/.ssh/id_rsa.pub"
    #res = commutls.execCommand(cmd)
    #if res != 0:
    #    logging.error(LOGMOD + LOGPHASEUTL + "0002" + " failed to copy sshpublickey")
    #    sys.exit(-1)
    cmd = "kops create secret --name " + properties['cluster.name'] + " sshpublickey admin -i ~/.ssh/id_rsa.pub"
    res = commutls.execCommand(cmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEUTL + "0003" + " failed to create sshpublickey")
        sys.exit(-1)    


# Uploading Working Folder with PKI            
def awsUpdateWorkingFolderwithPKI(workingFolder):
    # copy pki to working folder
    cpPKICmd = "cp -R ./environment/" + properties['size'] + "/pki " + workingFolder + "/pki"
    res = commutls.execCommand(cpPKICmd)
    if res != 0:
            logging.error(LOGMOD + LOGPHASEKOPS + "0014" + " Error executing: " + cpPKICmd)


# Updating cluster with available state.            
def kopsUpdateCluster():    
    # Running kops cluster update to create cluster on cloud
    kopsCmd = "kops update cluster --name=" + properties['cluster.name'] + " --state=" + properties['baselocation'] + " --yes"
    res = commutls.execCommand(kopsCmd)
    if res != 0:
            logging.error(LOGMOD + LOGPHASEKOPS + "0017" + " Error executing: " + kopsCmd)
    else:
            logging.info(LOGMOD + LOGPHASEKOPS + "0018" + " Created: " + kopsCmd)


def awsCreateCluster(workingFolder, dryRun):
           
    awsConfigure()
    awsUpdateWorkingFolderwithPKI(workingFolder)
    awsCreateKopsStateRepo()
    awsUpdateKopsStateRepo(workingFolder)
    kopsCreatePKI()   
    kopsUpdateCluster()
    # cleanup the credentials after us so they are not exposed
    awsCleanup()
    kopsWaitForCluster(27, dryRun)
    tillerInstall()
    kopsWaitForCluster(10, dryRun)
    kubeDashboardInstall()
    
          
        
# create the Kubernetes cluster
def kopsCreateCluster(workingFolder, dryRun):
    
    logging.info(LOGMOD + LOGPHASEKOPS + "0001" + " creating kops cluster, dry run is:" + str(dryRun))
    
    dry = "--dry-run" if dryRun == True else ""

    prepareClusterSpec(workingFolder)
    
    res = 0
    
    if properties['cloud'] == "aws":
        
        awsCreateCluster(workingFolder, dryRun)
    
    else:
        
        logging.error(LOGMOD + LOGPHASEKOPS + "0013" + " Create cluster of unsupported cloud: " + properties['cloud'])
        sys.exit(-1)
    
    # if res != 0:
     #   logging.error(LOGMOD + LOGPHASEKOPS + "0014" + " '" + kopsCmd + "' returned " + str(res))
      #  sys.exit(-1)
    
    return 0


# create the Kubernetes instance groups
def kopsCreateInstanceGroups(workingFolder, dryRun):
    
    dry = "--dry-run" if dryRun == True else ""
    
    logging.info(LOGMOD + LOGPHASEKOPS + "0002" + " Creating instance groups, dry run is:" + str(dryRun))
    for igf in instanceGroupsFiles:
       
       kopsCmd = "kops create ig " + dry + " -f " + igf
       logging.info(LOGMOD + LOGPHASEKOPS + "0003" + " Executing: " + kopsCmd)
       
       res = 0
       # res = os.system(kopsCmd)
       if res != 0:
           logging.error(LOGMOD + LOGPHASEKOPS + "0004" + " '" + kopsCmd + "' returned " + str(res))
           sys.exit(-1)
           
    return 0
            
        
# create a complete capsule infrastructure
def kopsCreateCapsule(workingFolder, dryRun):
    
    if kopsCreateCluster(workingFolder, dryRun) != 0:
        sys.exit(-1)

    # wait until we get a live sign from K8s        
    kopsWaitForCluster(5, dryRun)
    
    # if kopsCreateInstanceGroups(workingFolder, dryRun) != 0:
    #    sys.exit(-1)
        
    return 0
    
    
def executeKops(workingFolder, command, dryRun):   
    if command == "kops-create-cluster":
        kopsCreateCluster(workingFolder, dryRun)
    elif command == "kops-create-instance-groups":
        kopsCreateInstanceGroups(workingFolder, dryRun)
    elif command == "create-capsule":
        kopsCreateCapsule(workingFolder, dryRun)
    else:
        logging.error(LOGMOD + LOGPHASEKOPS + "0004" + " Unknown command")
        usage()
        sys.exit(-1)
        
def tillerInstall():
    helmCmd = "helm init"
    logging.info(LOGMOD + LOGPHASEHELM + "0006" + " Executing: " + helmCmd)
    count = 0
    while count < properties['retries']:
        count=count+1
        res=os.system(helmCmd)
        if res != 0:
            logging.error(LOGMOD + LOGPHASEHELM + "0007" + " '" + helmCmd + "' returned " + str(res))
            logging.error(LOGMOD + LOGPHASEHELM + "0008" + "going to sleep 10s before retry")
            time.sleep(10);
        else:
            return 0
        
        
    logging.error(LOGMOD + LOGPHASEKOPS + "0010" + " Giving up tiller installer")
    sys.exit(-1)
    
        
def kubeDashboardInstall():
    kubectlCmd = "kubectl create -f ./k8s_dashboard/kubernetes-dashboard-v1.8.3.yaml"
    logging.info(LOGMOD + LOGPHASEHELM + "0008" + " Executing: " + kubectlCmd)
    res=os.system(kubectlCmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEHELM + "0009" + " '" + kubectlCmd + "' returned " + str(res))
        sys.exit(-1)
#-----------------------------------------
#       HELM Commands
#-----------------------------------------

def helmInstalls3Plugin():
    helmCmd = "helm plugin install https://github.com/hypnoglow/helm-s3.git --version 0.6.1"
    logging.info(LOGMOD + LOGPHASEHELM + "0008" + " Executing: " + helmCmd)
    res=os.system(helmCmd)
    if res not in (0,256):
        logging.error(LOGMOD + LOGPHASEHELM + "0009" + " '" + helmCmd + "' returned " + str(res))
        sys.exit(-1)     
    return 0

def helmAddRepo():
    helmrepo = [x.strip() for x in properties['helmrepo'].split(',')]
    for i, val in enumerate(helmrepo):
    
    	val = val.replace('"','');
    	 
        helmCmd = "helm repo add " + val
        logging.info(LOGMOD + LOGPHASEHELM + "0006" + " Executing: " + helmCmd)
        res = 0
        res = os.system(helmCmd)
        if res != 0:
            logging.error(LOGMOD + LOGPHASEHELM + "0007" + " '" + helmCmd + "' returned " + str(res))
            sys.exit(-1)       
    return 0
    
def helmChartsInstall(dryRun):
    charts = [x.strip() for x in properties['charts'].split(',')]
    for i, val in enumerate(charts):
        dry = "[DRY] " if dryRun == True else ""
        val = val.replace('"',''); 
        helmCmd = dry + "helm install " + val
        logging.info(LOGMOD + LOGPHASEHELM + "0004" + " Executing: " + helmCmd)
        if dryRun == True:
            return 0
        count=0
        res = 0
        while count < properties['retries']:
            count=count+1
            res=os.system(helmCmd)
            if res != 0:
                logging.error(LOGMOD + LOGPHASEHELM + "0007" + " '" + helmCmd + "' returned " + str(res))
                logging.error(LOGMOD + LOGPHASEHELM + "0008" + "going to sleep 10s before retry")
                time.sleep(10);       
            else:
                return 0
                
        logging.error(LOGMOD + LOGPHASEHELM + "0003" + " Giving up on '" + helmCmd)
        sys.exit(-1)
    
    
           

def helmChartsUpdate(dryRun):
    charts = [x.strip() for x in properties['charts'].split(',')]
    for i, val in enumerate(charts):
        dry = "[DRY] " if dryRun == True else ""
        val = val.replace('"',''); 
        helmCmd = dry + "helm update " + val
        logging.info(LOGMOD + LOGPHASEHELM + "0004" + " Executing: " + helmCmd)
        if dryRun == True:
            return 0
        count=0
        res = 0
        while count < properties['retries']:
            count=count+1
            res=os.system(helmCmd)
            if res != 0:
                logging.error(LOGMOD + LOGPHASEHELM + "0007" + " '" + helmCmd + "' returned " + str(res))
                logging.error(LOGMOD + LOGPHASEHELM + "0008" + "going to sleep 10s before retry")
                time.sleep(10);       
            else:
                return 0
                
        logging.error(LOGMOD + LOGPHASEHELM + "0003" + " Giving up on '" + helmCmd)
        sys.exit(-1)

           
           
def executeHelm(workingFolder, command, dryRun): 
    if command in ["helm-install", "create-capsule"]:
    	awsConfigure()
        helmInstalls3Plugin()
        helmAddRepo()
        helmChartsInstall(dryRun)
    	awsCleanup()
    elif command == "helm-update":
        awsConfigure()
        helmInstalls3Plugin()
        helmAddRepo()
        helmChartsUpdate(dryRun)
        awsCleanup()
    else:
        logging.error(LOGMOD + LOGPHASEHELM + "0001" + " Error: unknown command")
        usage()
        sys.exit(-1)
        
def helmS3Plugin(dryRun):
           
    dry = "[DRY] " if dryRun == True else ""
    
    helmS3PluginCmd = "helm plugin install https://github.com/hypnoglow/helm-s3.git --version 0.6.1"
    logging.info(LOGMOD + LOGPHASEHELM + "0002" + " Executing: " + helmS3PluginCmd)
       
    if dryRun == True:
        return 0
    
    res = 0
    # res = os.system(kopsCmd)
    if res != 0:
        logging.error(LOGMOD + LOGPHASEHELM + "0003" + " '" + helmS3PluginCmd + "' returned " + str(res))
        sys.exit(-1)
        
    sys.exit(-1)
        
#-----------------------------------------
#       Capsule Commands
#-----------------------------------------
# create a capsule (either dry run or real deploy
def createCapsule(command, dryRun):        
    
    # create a working folder
    workingFolder = createWorkingFolder()
    
    # determine which namespaces to configure and deploy
    k8_namespaces = getNamespacesToConfigure();
        
    # prepare namespaces under working folder
    prepareNamespace(workingFolder, k8_namespaces)
    
    if command in ["create-capsule", "kops-create-cluster", "kops-create-instance-group", "kops-update", "kops-delete"]:        
        # let KOPS do its job
        logging.info(LOGMOD + LOGPHASEKOPS + "0000" + " Executing kops")
        executeKops(workingFolder, command, dryRun)
    
    if command in ["create-capsule", "helm-install", "helm-update"]:        
        logging.info(LOGMOD + LOGPHASEHELM + "0000" + " Executing helm")
        #executeHelm(workingFolder, command, dryRun)
        sys.exit(0)
        

def usage():
    print "Usage: [COMMAND]" + sys.argv[0] + " [OPTIONS]"
    print "COMMAND: create-capsule, kops-create-cluster, kops-create-instance-group, kops-update, kops-delete helm-install helm-update"
    print "\t-c --cloud        \tcloud (minikube, aws, gcp, azure, ...) default aws" 
    print "\t-p --profile      \tprofile name" 
    print "\t-n --capsule_name \tcap0, cap1, ... default cap0"
    print "\t-m --market       \tmarket (us, eu, ch, ...), default us"
    print "\t-s --size         \tsize of capsule (mini, small, medium, large, xlarge), default medium"
    print "\t-d --domain       \tdomain/cert"
    print "\t-x --namespaces   \tnamespaces to confifugre and deploy"
    print "\t-r --region       \tcloud region"
    print "\t-r --vpc_id       \tid of the vpc"
    print "\t-r --vpc_cidr       \tcidr of the vpc"    
    print "\t--dry_run         \tif included only print"
    print "\t--owner           \tthe name of the owner of the cluster"
    print "\t--team            \tthe team name owning the cluster"
    print "\t--baselocation    \twhere to store the cluster configuration (e.g. s3://...)"
    print "\t--azs             \tcomma delimited list of availability zones (currently up to 3)"
    print "\t--config_aws      \tconfigure aws? if yes then parameters are <aws_key>,<aws_secret>"
    print "\t--charts      	   \tHelm charts to install or update"
    print "\t--helmrepo        \tHelm repo to add"
    print "\t--retries         \tMaximum retries count in case of failure"
    print "\t-v --version      \tVersion of this script" 
    print "\t-h --help         \tThis help menu\n"

    sys.exit(1)


def parseAvailabilityZones():
    
    azs = [x.strip() for x in properties['azs'].split(',')]
    for i, val in enumerate(azs):
        properties['azs.zone.' + str(i + 1)] = val
         

def parseAwsConfig():
    
    aws = [x.strip() for x in properties['config_aws'].split(',')]
    properties['aws_key'] = aws[0]
    properties['aws_secret'] = aws[1]
         

def parseOptions():
    
    if len(sys.argv) < 3:
        usage()
        sys.exit()        
    
    global command
    global dryRun
    command = sys.argv[1]
    
    try:
        opts, args = getopt.getopt(sys.argv[2:], "hc:p:n:m:s:d:x:r:v",
                                   ["help",
                                    "cloud=",
                                    "profile=",
                                    "capsule=",
                                    "market=",
                                    "size=",
                                    "domain=",
                                    "region=",
                                    "namespaces=",
                                    "vpc_id=",
                                    "vpc_cidr=",
                                    "owner=",
                                    "team=",
                                    "baselocation=",
                                    "azs=",
                                    "config_aws=",
                                    "charts=",
                                    "helmrepo=",
                                    "retries=",
                                    "dry_run", 
                                    "version"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        sys.exit(-1)

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-c", "--cloud"):
            properties['cloud'] = a
            
        elif o in ("-p", "--profile"):
            properties['profile'] = a
            
        elif o in ("-n", "--capsule"):
            properties['capsule'] = a
            
        elif o in ("-m", "--market"):
            properties['market'] = a
            
        elif o in ("-s", "--size"):
            properties['size'] = a
            
        elif o in ("-d", "--domain"):
            properties['domain'] = a
            
        elif o in ("-d", "--region"):
            properties['region'] = a
            
        elif o in ("-x", "--namespaces"):
            properties['namespaces'] = a
            
        elif o in ("-r", "--vpc_cidr"):
            properties['vpc_cidr'] = a
            
        elif o in ("-r", "--vpc_id"):
            properties['vpc_id'] = a
            
        elif o in ("--owner"):
            properties['owner'] = a
            
        elif o in ("--team"):
            properties['team'] = a
            
        elif o in ("--baselocation"):
            # set kops storage state environment variable
            os.environ['KOPS_STATE_STORE'] = a
            properties['baselocation'] = a
            
        elif o in ("--azs"):
            properties['azs'] = a
            parseAvailabilityZones()

        elif o in ("--config_aws"):
            properties['config_aws'] = a
            parseAwsConfig()

        elif o in ("--charts"):
            properties['charts'] = a
            
        elif o in ("--helmrepo"):
            properties['helmrepo'] = a
            
        elif o in ("--retries"):
            properties['retries'] = a

        elif o in ("--dry_run"):
            dryRun = True
            
        elif o in ("-v", "--version"):
            print VERSION
            sys.exit(0)
            
        else:
            assert False, "unknown option"
            sys.exit(-1)

    if command not in ["create-capsule", "kops-create-cluster", "kops-create-instance-group", "kops-update", "kops-delete", "helm-install", "helm-update"]:        
        logging.error(LOGMOD + LOGPHASECMN + "0001" + " Error: unknown command: " + command)
        usage()
        sys.exit(-1)
        
   # if (command in ["create-capsule","helm-install"]):
   #	if not properties.get('charts'):
   #     	logging.error(LOGMOD + LOGPHASECMN + "0002" + " Missing mandatory --charts")
   #     	usage()
   #     if not properties.get('helmrepo'):
   #    		logging.error(LOGMOD + LOGPHASECMN + "0003" + " Missing mandatory --charts")
   #    		usage()
        
    if not properties.get('profile'):
        logging.error(LOGMOD + LOGPHASECMN + "0004" + " Missing mandatory --profile")
        usage()
        
    if not properties.get('retries'):
        properties['retries'] = 25
        
    if properties.get('cloud') != "local":
        if not properties.get('domain'):
            logging.error(LOGMOD + LOGPHASECMN + "0005" + " Missing mandatory --domain when using real cloud")
            usage()
        if not properties.get('region'):
            logging.error(LOGMOD + LOGPHASECMN + "0006" + " Missing mandatory --region when using real cloud")
            usage()
        if not properties.get('baselocation'):
            logging.error(LOGMOD + LOGPHASECMN + "0007" + " Missing mandatory --baselocation when using real cloud")
            usage()
        if not properties.get('vpc_id'):
            logging.error(LOGMOD + LOGPHASECMN + "0008" + " Missing mandatory --vpc_id when using real cloud")
            usage()
        if not properties.get('vpc_cidr'):
            logging.error(LOGMOD + LOGPHASECMN + "0008-a" + " Missing mandatory --vpc_cidr when using real cloud")
            usage()
        if not properties.get('azs'):
            logging.error(LOGMOD + LOGPHASECMN + "0009" + " Missing mandatory --azs when using real cloud")
            usage()
        
    # add current timestamp in ISO8061 as property            
    properties['timestamp'] = commutls.datetime.utcnow().replace(tzinfo=commutls.simple_utc()).isoformat()
        
    
def main():
    
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
    parseOptions()
    createCapsule(command, dryRun)


if __name__ == "__main__":


    main()

