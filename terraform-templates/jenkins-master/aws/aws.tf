#------------------------------------------#
# AWS Provider Configuration
#------------------------------------------#
provider "aws" {
    access_key=""
    secret_key=""
    region = "${var.region}"
}
