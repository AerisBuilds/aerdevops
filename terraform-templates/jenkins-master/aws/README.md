# Instructions to run Jenkins Master

  - Include your aws access key and secret key in aws.tf
  - The terraform script pulls the Jenkins image form Nexus repo, to include other plugins modify the plugins.txt from the [jenkins-master-docker](https://bitbucket.org/aeriscom/automotive-devops/src/d451c13bf80f589ad92613b6b454feb4d0851b85/terraform-templates/jenkins-master/jenkins-master-docker/?at=master) repo and then create an image 
  - The first time password for Jenkins dashboard appears in the path "/opt/jenkins_home/secrets/initialAdminPassword"
  - Include the nexus password, private key and other aws properties in the [var file](https://bitbucket.org/aeriscom/automotive-devops/src/d451c13bf80f589ad92613b6b454feb4d0851b85/terraform-templates/jenkins-master/variables.tf?at=master&fileviewer=file-view-default)
  - Configure the JDK home path under Global Tool Configuration, the docker image has "/docker-java-home" as the path
  - The img directory has screenshot of Jenkins configuration on Docker, use it as a reference to configure Jobs
