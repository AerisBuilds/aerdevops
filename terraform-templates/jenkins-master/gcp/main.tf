resource "google_compute_instance" "default" {
  name         = "vm-${var.product_name}-${var.environment}-${var.jenkins_master_purpose}-${var.region}"
  machine_type = "${var.jenkins_master_machine_type}"
  project = "${var.project}"

  boot_disk {
    initialize_params {
      image = "${var.image}"
      size = "${var.jenkins_master_disk_size}"
    }
  }

  zone = "${var.zone}"
  metadata = {
    Product = "${var.product_name}"
    Project = "${var.product_name}"
    Component = "${var.jenkins_master_purpose}"
    Deployment = "${var.environment}"
    docker_image = "${var.jenkins_master_docker_image}"
    bucket = "${var.bucket}"
  }
  metadata_startup_script = "${file("./startup-script.sh")}"

  tags = ["${google_compute_firewall.default.name}"]

  network_interface {
    # A default network is created for all GCP projects
    subnetwork       = "${var.subnetwork}"
    subnetwork_project       = "${var.project}"
    access_config {
    }
  }
  service_account {
    email = "${var.service_account}"
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance_group" "default" {
  name = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project = "${var.project}"
  zone = "${var.zone}"
  instances = [ "${google_compute_instance.default.self_link}" ]
  named_port {
    name = "http"
    port = "${var.jenkins_master_port}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_backend_service" "default" {
  name      = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project   = "${var.project}"
  port_name = "http"
  protocol  = "HTTP"

  backend {
    group = "${google_compute_instance_group.default.self_link}"
  }

  health_checks = [
    "${google_compute_health_check.default.self_link}",
  ]
}

resource "google_compute_health_check" "default" {
  name         = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project      = "${var.project}"
  tcp_health_check {
    port       = "${var.jenkins_master_port}"
  }
}

resource "google_compute_target_https_proxy" "default" {
  name             = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project          = "${var.project}"
  url_map          = "${google_compute_url_map.default.self_link}"
  ssl_certificates = ["projects/${var.project}/global/sslCertificates/${var.ssl_cert_name}"]
}
resource "google_compute_url_map" "default" {
  name            = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project         = "${var.project}"
  default_service = "${google_compute_backend_service.default.self_link}"
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project    = "${var.project}"
  target     = "${google_compute_target_https_proxy.default.self_link}"
  port_range = "443"
  ip_address = "${google_compute_global_address.default.address}"
}

resource "google_compute_global_address" "default" {
  name = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project    = "${var.project}"
}

resource "google_compute_firewall" "default" {
  name    = "hlb-${var.product_name}-${var.jenkins_master_purpose}-${var.region}"
  project = "${var.project}"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["8080","50000"]
  }
}

output "instance_id_jenkins" {
 value = "${google_compute_instance.default.self_link}"
}
