#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_docker() {
    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function disable_startup_scripts() {
    systemctl disable google-shutdown-scripts.service
}

function deploy_jenkins_master() {
    # Create jenkins master directories
    mkdir -p /opt/jenkins_home
    mkdir -p /opt/jenkins_master
    # change owner for directory
    chown 1000 /opt/jenkins_home

    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)
    BUCKET=$(echo ${METADATA} | jq -r .bucket)
    COMPONENT=$(echo ${METADATA} | jq -r .Component)

    # download jenkins.yml JCasC
    sed -e '/plugin_directory/s/^/#/g' -i /etc/boto.cfg
    gsutil cp gs://${BUCKET}/${COMPONENT}/jenkins.yml /opt/jenkins_home/

cat <<END > /opt/jenkins_master/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: jenkins-master
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    ports:
      - 8080:8080
      - 50000:50000
    volumes:
      - /opt/jenkins_home:/var/jenkins_home
    environment:
      - CASC_JENKINS_CONFIG=/var/jenkins_home/jenkins.yml
END

    # Start docker container
    cd /opt/jenkins_master
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_jenkins_master(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_jenkins_master(): Done"
    fi
}

post_deployment_steps() {
    #Install helm gcs plugin on container
    docker exec terraform-server /usr/local/bin/helm init --client-only
    docker exec terraform-server /usr/local/bin/helm plugin install https://github.com/nouney/helm-gcs --version 0.2.0
    #Initialize helm
    /usr/local/bin/helm init --client-only
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

create_routes

configure_ulimits

install_docker

deploy_jenkins_master

#post_deployment_steps
# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
