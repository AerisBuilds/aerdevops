productname = "ampsolutions"
environment = "qualdev"
purpose = "jenkins-master"
machine_type = "n1-standard-2"
region = "us-west1"
zone = "us-west1-a"
image = "amp-solutions-qualdev-201903/aeris-ubuntu-20190313"
disk_size = "32"
network = "vpc-ampsolutions-qualdev-us-west1"
subnetwork = "subnet-ampsolutions-qualdev-bss01-us-west1"
docker_repo = "http://repo.aeriscloud.com:6666"
docker_user = "atspdeployer"
docker_pass = "AtspDeployer20"
docker_image = "repo.aeriscloud.com:6666/jenkins-master:006"
jenkins_user = "admin"
jenkins_password = "admin123"
gcp_project = "amp-solutions-qualdev-201903"
