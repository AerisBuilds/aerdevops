#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file and jenkins.yml file"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file path_to_jenkins.yml"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf vars-product/jenkins.yml"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
echo "Jenkins file path: $2"
source $1
#export PURPOSE=$(grep "^jenkinsm_purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

gsutil cp $2 gs://"${bucket}"/"${jenkins_master_purpose}"/jenkins.yml

terraform init -backend-config=bucket=${bucket} -backend-config=prefix=${jenkins_master_purpose}
terraform apply -var-file=$1 
