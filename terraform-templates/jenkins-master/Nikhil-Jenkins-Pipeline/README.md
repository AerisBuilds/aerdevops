This Jenkinsfile is to create a Parameterized pipeline to spin up the Bootstrap server on AWS for the K8S cluster.

It has 4 stages:

1. Pipeline Initialization Stage: Git clones the main terraform script from the repo. Accepts the input parameters and pushes them into the variables.tf file.
2. Terraform Init and Plan Stage: Setup the terraform environment and prepare the plan.
3. Terraform Build Stage: Terraform apply stage where it deploys the instance to cloud.
4. Terraform Destroy Stage: Terminates all the created instances in the run. Stops the pipeline.

Jenkinsfile can be ran directly by importing it from the SCM. Use 'Pipeline Script with SCM' option in the console to do so.