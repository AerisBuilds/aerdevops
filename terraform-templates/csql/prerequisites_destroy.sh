#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./prerequisites_destroy.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./prerequisites_destroy.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi


export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ZONE=$(grep "^zone=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
./prerequisites/destroy.sh ${BUCKET} ${NETWORK} ${PROJECT} ${ZONE} ${REGION}
