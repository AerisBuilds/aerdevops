#!/bin/bash
cd "$(dirname "$0")"

terraform init -backend-config="bucket=$1" -backend-config="prefix=cloudsql/prerequisites"
terraform destroy -var network="$2" -var project="$3" -var region="$4" -var zone="$5" -auto-approve
