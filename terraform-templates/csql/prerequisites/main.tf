resource "google_project_service" "cloud_sql_api" {
  disable_on_destroy = false
  project = "${var.project}"
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "service_networking_api" {
  disable_on_destroy = false
  project = "${var.project}"
  service = "servicenetworking.googleapis.com"
}



resource "google_compute_global_address" "private_ip-ranges" {
  provider = "google-beta"
  lifecycle {
      ignore_changes = [name]
  }
  project = "${var.project}"
  name          = "cloud-sql-private-ip-address"
  purpose       = "VPC_PEERING"
  address_type = "INTERNAL"
  address = "172.17.0.0" 
  prefix_length = 16
  network       = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
}


resource "null_resource" "vpc_to_services_peering" {
  provisioner "local-exec" {
    command = <<EOF
gcloud beta services vpc-peerings update \
--service=servicenetworking.googleapis.com \
--network=${var.network} \
--ranges=${google_compute_global_address.private_ip-ranges.name} \
--project=${var.project} --force
EOF
  }
}


