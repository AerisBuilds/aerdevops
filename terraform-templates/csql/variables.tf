variable "cloudsql_host" {}
variable "region" {} 
variable "cloudsql_disk_size" {}
variable "project" {}
variable "network" {}
variable "cloudsql_tier" {}
variable "db_replica" {}
variable "zone" {default = "unset"}
variable "db_type" {  default = ["mysql"] }
variable "db_version" { default = "unset"}
variable "master_zone" { default = "unset"}
variable "replica_zone" { default = "unset"}

