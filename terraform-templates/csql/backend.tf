terraform {
 backend "gcs" {
 }
}

provider "google-beta"{
  region = "${var.region}"
  // zone   = "${var.region}-${var.zone}"
  project = "${var.project}"
  version = "~> 2.5"
}