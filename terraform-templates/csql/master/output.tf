output "db_private_ip" {
  value = "${google_sql_database_instance.master.private_ip_address}"
}

output "instance_name" {
  value = "${google_sql_database_instance.master.name}"
}


// output "db_password" {
//   value = "${random_string.mysql_password.result}"
// }