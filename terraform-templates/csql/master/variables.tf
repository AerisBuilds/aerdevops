variable "cloudsql_host" {}
variable "region" {} 
variable "cloudsql_disk_size" {}
variable "project" {}
variable "network" {}
variable "cloudsql_tier" {}
variable "zone" {}
variable "master_zone" { default = "unset"}
variable "db_type" {  default = ["mysql"] }
variable "db_version" { default = "unset"}
