
locals {
  default_database_version = "${var.db_type == "postgres" ? "POSTGRES_9_6" : "MYSQL_5_7"}"
  database_version = "${var.db_version == "unset" ? local.default_database_version : var.db_version}"
  binary_log_enabled = "${var.db_type == "postgres" ? false : true}"
  availability_type = "${var.db_type == "postgres" ? "REGIONAL" : "ZONAL"}"
  master_default_zone = "${var.region}-a"
  master_zone ="${var.master_zone == "unset" ? local.master_default_zone : var.master_zone}" 

}


# Create MySQL Instances
resource "google_sql_database_instance" "master" {
  provider = "google-beta"



  name = "${var.cloudsql_host}-m"
  region = "${var.region}"
  database_version = local.database_version
  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = local.availability_type
    replication_type = "SYNCHRONOUS"
    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    
    }

    location_preference {
      zone = local.master_zone
    }

    backup_configuration {
      enabled = "true"
      start_time = "03:00"
      binary_log_enabled = local.binary_log_enabled
    }
  }
}


# Create MySQL password
resource "random_string" "mysql_password" {
  length = 64
  special = false
}

# Create MySQL user
resource "google_sql_user" "db_user" {
  depends_on = [ "google_sql_database_instance.master" ]
  project = "${var.project}"
  name     = "root"
  instance = "${google_sql_database_instance.master.name}"
  host     = "%"
  password = "${random_string.mysql_password.result}"
}
