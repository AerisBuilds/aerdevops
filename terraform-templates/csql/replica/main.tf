
locals {
  default_database_version = "${var.db_type == "postgres" ? "POSTGRES_9_6" : "MYSQL_5_7"}"
  replica_database_version = "${var.db_version == "unset" ? local.default_database_version : var.db_version}"
  failover_target = "${var.db_type == "postgres" ? false : true}"
  replica_default_zone = "${var.region}-b"
  replica_zone ="${var.replica_zone == "unset" ? local.replica_default_zone : var.replica_zone}" 
}

resource "google_sql_database_instance" "replica" {
  count = "${var.db_replica ? 1 : 0}"
  provider = "google-beta"
  name = "${var.cloudsql_host}-r"
  region = "${var.region}"
  database_version = local.replica_database_version
  master_instance_name = "${var.master_instance_name}"

  replica_configuration {
    connect_retry_interval = 60
    failover_target = local.failover_target
  }

  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = "ZONAL"
    replication_type = "SYNCHRONOUS"
    crash_safe_replication = true
    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    }
    location_preference {
      zone = local.replica_zone
    }
  }
}