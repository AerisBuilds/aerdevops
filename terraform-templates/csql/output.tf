output "db_private_ip" {
  value = "${module.master.db_private_ip}"
}

output "instance_name" {
  value = "${module.master.instance_name}"
}
// output "db_password" {
//   value = "${random_string.mysql_password.result}"
// }