
locals {
  replica_database_version = "${var.db_type == "postgres" ? "POSTGRES_9_6" : "MYSQL_5_7"}"
  failover_target = "${var.db_type == "postgres" ? false : true}"
}

module "master" {
  source = "./master"
  project = "${var.project}"
  region = "${var.region}"
  cloudsql_host = "${var.cloudsql_host}"
  cloudsql_disk_size = "${var.cloudsql_disk_size}"
  network = "${var.network}"
  cloudsql_tier = "${var.cloudsql_tier}"
  zone = "${var.zone}"
  db_type ="${var.db_type}"
  db_version="${var.db_version}"
  master_zone="${var.master_zone}"
}


module "replica" {
  source = "./replica"
  project = "${var.project}"
  region = "${var.region}"
  cloudsql_host = "${var.cloudsql_host}"
  cloudsql_disk_size = "${var.cloudsql_disk_size}"
  network = "${var.network}"
  cloudsql_tier = "${var.cloudsql_tier}"
  master_instance_name = "${module.master.instance_name}"
  db_replica = "${var.db_replica}"
  db_type ="${var.db_type}"
  db_version="${var.db_version}"
  replica_zone="${var.replica_zone}"

}




// # Create MySQL database
// resource "google_sql_database" "database" {
//   depends_on = [ "google_sql_database_instance.master" ]
//   name      = "${var.db_name}"
//   project = "${var.project}"
//   instance  = "${google_sql_database_instance.master.name}"
//   charset   = "latin1"
//   collation = "latin1_swedish_ci"
// }

// # Create MySQL user
// resource "google_sql_user" "db_user" {
//   depends_on = [ "google_sql_database_instance.master" ]
//   project = "${var.project}"
//   name     = "${var.db_user}"
//   instance = "${google_sql_database_instance.master.name}"
//   host     = "%"
//   password = "${random_string.mysql_password.result}"
// }
