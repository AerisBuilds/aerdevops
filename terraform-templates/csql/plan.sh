#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing csql prefix to use in variables"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file vars_prefix"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf keycloak"
    echo -e "\033[0m"
    exit 1
fi

export PREFIX=$2_

grep "^${PREFIX}" $1 | awk -v prefix="$PREFIX" 'BEGIN{FS=prefix;} {print $2}' >> ../${PREFIX}var_file.tf

export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=csql_$2
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
terraform init -backend-config="bucket=${BUCKET}" -backend-config="prefix=cloudsql/${PURPOSE}"
terraform plan -var-file=../${PREFIX}var_file.tf -var network="${NETWORK}" -var project="${PROJECT}"

rm ../${PREFIX}var_file.tf
