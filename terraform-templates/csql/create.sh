#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing csql prefix to use in variables"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file vars_prefix"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf keycloak"
    echo -e "\033[0m"
    exit 1
fi

export PREFIX=$2_

grep "^${PREFIX}" $1 | awk -v prefix="$PREFIX" 'BEGIN{FS=prefix;} {print $2}' >> ../${PREFIX}var_file.tf

export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=csql_$2
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')



out=$(gcloud services vpc-peerings list --network=${NETWORK} --project=${PROJECT} --service=servicenetworking.googleapis.com --format=json | jq '.[] | select(.service=="services/servicenetworking.googleapis.com") and select(.peering=="cloudsql-mysql-googleapis-com")')

if [ "$out" == "true" ]; then
    echo "VPC prerequisites already created"
else
    export ZONE=$(grep "^zone=" ../${PREFIX}var_file.tf |sed 's/"//g' |awk -F'=' '{print $2}')
    export REGION=$(grep "^region=" ../${PREFIX}var_file.tf |sed 's/"//g' |awk -F'=' '{print $2}')
    ./prerequisites/create.sh ${BUCKET} ${NETWORK} ${PROJECT} ${ZONE} ${REGION}
fi

terraform init -backend-config="bucket=${BUCKET}" -backend-config="prefix=cloudsql/${PURPOSE}"


terraform plan -out=$2_master_csql.tfplan -var-file=../${PREFIX}var_file.tf -var network="${NETWORK}" -var project="${PROJECT}" -target=module.master
terraform apply -input=false $2_master_csql.tfplan

terraform plan -out=$2_replica_csql.tfplan -var-file=../${PREFIX}var_file.tf -var network="${NETWORK}" -var project="${PROJECT}" -target=module.replica
terraform apply -input=false $2_replica_csql.tfplan

rm ../${PREFIX}var_file.tf
rm $2_replica_csql.tfplan
rm $2_master_csql.tfplan
rm -rf .terraform