#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./purge_csql.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./purge_csql.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"


export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')



RAW_CSQL_REPLICAS=$(gcloud sql instances list --project "${PROJECT}" --format=json | jq -r --arg NETWORK "$NETWORK" '.[] | select( .masterInstanceName != null  and (.settings.ipConfiguration.privateNetwork!= null) and (.settings.ipConfiguration.privateNetwork | contains($NETWORK))) | .name')
RAW_CSQL_MASTERS=$(gcloud sql instances list --project "${PROJECT}" --format=json | jq -r --arg NETWORK "$NETWORK" '.[] | select( .masterInstanceName == null  and (.settings.ipConfiguration.privateNetwork!= null) and (.settings.ipConfiguration.privateNetwork | contains($NETWORK))) | .name')


SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
CSQL_REPLICAS=($RAW_CSQL_REPLICAS) # split to array $names
CSQL_MASTERS=($RAW_CSQL_MASTERS)

IFS=$SAVEIFS 


for CSQL_REPLICA in "${CSQL_REPLICAS[@]}"
do
   : 
     echo "deleting sql instance: $CSQL_REPLICA"
        gcloud sql instances delete --project "${PROJECT}" ${CSQL_REPLICA} -q

done


for CSQL_MASTER in "${CSQL_MASTERS[@]}"
do
   : 
     echo "deleting sql instance: $CSQL_MASTER"
        gcloud sql instances delete --project "${PROJECT}" ${CSQL_MASTER} -q

done


