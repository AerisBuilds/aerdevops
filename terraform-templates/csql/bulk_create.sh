#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

PREFIX=csql_instances_ids=
CSQL_INSTANCES_RAW=$(grep "^${PREFIX}" $1 | awk -v prefix="$PREFIX" 'BEGIN{FS=prefix;} {print $2}' | cut -d '"' -f 2)
IFS=','
read -ra CSQL_INSTANCES <<< "${CSQL_INSTANCES_RAW}"

for CSQL in "${CSQL_INSTANCES[@]}"; do
    echo "Creating CSQL ${CSQL}"
    ./create.sh $1 ${CSQL}
done