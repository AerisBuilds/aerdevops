#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#

variable "access_key" {}

variable "secret_key" {}

variable "region" {
default = "us-west-2"
description = "AWS region"
}



variable "name_prefix" {
    default     = "k8s-dev-mysql"
    description = "Prefix for all AWS resource names"
}

variable "name_prefix_orcl" {
    default     = "k8s-dev-orcl"
    description = "Prefix for all AWS resource names"
}

variable "subnet_cidrs" {
    default     = ["10.6.51.0/24", "10.6.52.0/24", "10.6.53.0/24"]
    description = "Subnet ranges (requires 3 entries)"
}

variable "subnet_ec2ids" {
    type        = "string"
    default     = "subnet-9774f9e0, subnet-e8513e8d, subnet-96f050cf"
    description = "Subnet ranges (requires 3 entries)"
}


variable "project" {
    default     = "AMP"
    description = "project tag value"
}

variable "owner" {
    default = "abhay.rao@aeris.net"
    description = "Owner tag value, please put your email id here"
}

variable "aws_vpc_id" {
   default = "vpc-1b744762"
   description = "VPC id"
}

variable "vpc_cidr" {
    default     = "10.147.0.0/16"
    description = "Subnet in CIDR format to assign to VPC"
}

#------------------------------------------#
# Database Variables
#------------------------------------------#
variable "db_name" {
    default     = "K8SDEVSQL"
    description = "Name of the RDS DB"
}

variable "db_user" {
    default     = "k8sdevuser"
    description = "Username used to connect to the RDS database"
}

variable "db_pass" {
    default = "amp12345"
    description = "Password used to connect to the RDS database"
}

variable "sg_group" {
    default = "sg-3fde344e"
    description = "Security group for the RDS database"
}

variable "subneta" {
    default = "subnet-620b901b"
    description = "Subnet used to connect to the RDS database"
}

variable "subnetb" {
    default = "subnet-dbd0a381"
    description = "Subnet used to connect to the RDS database"
}

variable "subnetc" {
    default = "subnet-8a8206c1"
    description = "Subnet used to connect to the RDS database"
}

