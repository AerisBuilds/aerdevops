#------------------------------------------#
# RDS Database Configuration
#------------------------------------------#

resource "aws_security_group" "rancher_ha_rds" {
    name        = "${var.name_prefix_orcl}-rds-sg"
    description = "Rancher RDS Ports"
    vpc_id      = "${var.aws_vpc_id}"

    ingress {
        from_port   = "3306"
        to_port     = "3306"
        protocol    = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
        }

    egress {
        from_port   = "0"
        to_port     = "0"
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        }
}


resource "aws_db_subnet_group" "rancher_ha_subnet_group" {
  name       = "${var.name_prefix_orcl}-subnet-group"
  subnet_ids = ["${var.subneta}", "${var.subnetb}", "${var.subnetc}"]
  tags {
    Name = "${var.name_prefix_orcl}-subnet-group"
  }
}

resource "aws_db_instance" "rancher_ha" {
    allocated_storage    = "100"
    storage_type         = "io1"
    iops                 = "5000"
    engine               = "orace-ee"
    engine_version       = "12.1.0.2.v8"
    instance_class       = "db.t2.medium"
    identifier           = "${var.name_prefix_orcl}-db"
    name                 = "${var.db_name}"
    username             = "${var.db_user}"
    password             = "${var.db_pass}"
    port                 = "3306"
    db_subnet_group_name = "${aws_db_subnet_group.rancher_ha_subnet_group.id}"
    multi_az             = "false"
    backup_retention_period = "14"
    skip_final_snapshot  = "true"
    vpc_security_group_ids = ["${var.sg_group}"]


    tags {
    Name = "${var.name_prefix_orcl}"
    Project = "k8s"
    Owner   = "abhay.rao@aeris.net"
    Environment = "Dev"

  }
}
