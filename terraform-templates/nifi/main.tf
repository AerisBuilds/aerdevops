module "nifi" {
  source = "./components/nifi"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  nifi_node_settings = "${var.nifi_node_settings}"
  nifi_service_account = "${var.nifi_service_account}"
  nifi_purpose = "${var.nifi_purpose}"
  nifi_subnetwork = "${var.nifi_subnetwork}"
  nifi_subnetwork_project = "${var.nifi_subnetwork_project}"
  nifi_image = "${var.nifi_image}"
}
