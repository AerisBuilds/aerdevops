variable "project" {}
variable "productname" {}
variable "environment" {}
variable "nifi_service_account" {}
variable "nifi_purpose" {}
variable "nifi_subnetwork" {}
variable "nifi_subnetwork_project" {}
variable "nifi_image" {}
variable "nifi_node_settings" {
  type = "map"
}
