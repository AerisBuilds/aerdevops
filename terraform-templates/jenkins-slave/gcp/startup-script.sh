#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_docker() {
    # Enable Docker service
    echo '{"max-concurrent-uploads": 1}' > /etc/docker/daemon.json
    systemctl enable docker
    systemctl restart docker

    log_message "install_docker(): Done"
}

function disable_startup_scripts() {
    systemctl disable google-shutdown-scripts.service
}

function deploy_jenkins_slave() {
    # Create jenkins slave directories
    mkdir -p /opt/jenkins_home
    mkdir -p /opt/jenkins_slave
    # change owner for directory
    chown 1000 /opt/jenkins_home

    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)
    JENKINS_MASTER_URL=$(echo ${METADATA} | jq -r .jenkins_master_url)
    JENKINS_MASTER_TOKEN=$(echo ${METADATA} | jq -r .jenkins_master_token)
    JENKINS_SLAVE_NAME=$(echo ${METADATA} | jq -r .jenkins_slave_name)

cat <<END > /opt/jenkins_slave/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    command: '-url ${JENKINS_MASTER_URL} ${JENKINS_MASTER_TOKEN} ${JENKINS_SLAVE_NAME}'
    restart: unless-stopped
    container_name: jenkins-slave
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    ports:
      - 8080:8080
      - 50000:50000
    volumes:
      - /opt/jenkins:/var/lib/jenkins
      - /var/run/docker.sock:/var/run/docker.sock
END

    # Start docker container
    cd /opt/jenkins_slave
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_jenkins_slave(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_jenkins_slave(): Done"
    fi
}


post_deployment_steps() {
    #Install helm gcs plugin on container
    docker exec jenkins-slave /usr/local/bin/helm init --client-only
    docker exec jenkins-slave /usr/local/bin/helm plugin install https://github.com/nouney/helm-gcs --version 0.2.0
    #Initialize helm
    /usr/local/bin/helm init --client-only
    #install JQ
    docker exec jenkins-slave yum -y install jq
    #install sw for blackduck backup and cleanup
    docker exec jenkins-slave wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz
    docker exec jenkins-slave tar xzf Python-3.7.3.tgz
    docker exec -w /var/lib/jenkins/Python-3.7.3 jenkins-slave ./configure --enable-optimizations
    docker exec -w /var/lib/jenkins/Python-3.7.3 jenkins-slave make altinstall
    docker exec jenkins-slave pip3.7 install blackduck timestring terminaltables python-dateutil
    docker exec jenkins-slave yum -y install dos2unix
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

configure_ulimits

install_docker

deploy_jenkins_slave

post_deployment_steps
# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
