variable "productname" {}
variable "environment" {}
variable "region" {}
variable "zone" {}
variable "image" {}
variable "subnetwork" {}
variable "project" {}
variable "service_account" {}
variable "jenkinsm_purpose" {}
variable "jenkins_slaves" {type = "list"}

