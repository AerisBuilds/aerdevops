resource "google_compute_instance" "jenkins-slave-1" {
  count        = "${length(var.jenkins_slaves)}"
  name         = "vm-${var.productname}-${var.environment}-${lookup(var.jenkins_slaves[count.index], "slave_name")}-${var.region}"
  machine_type = "${lookup(var.jenkins_slaves[count.index], "machine_type")}"
  project = "${var.project}"

  boot_disk {
    initialize_params {
      image = "${var.image}"
      size  = "${lookup(var.jenkins_slaves[count.index], "disk_size")}"
    }
  }

  zone = "${var.zone}"
  metadata = {
    Product = "${var.productname}"
    Project = "${var.productname}"
    Component = "${var.jenkinsm_purpose}"
    Deployment = "${var.environment}"
    docker_image = "${lookup(var.jenkins_slaves[count.index], "docker_image")}"
    jenkins_master_url = "${lookup(var.jenkins_slaves[count.index], "jenkins_master_url")}"
    jenkins_master_token = "${lookup(var.jenkins_slaves[count.index], "jenkins_master_token")}"
    jenkins_slave_name = "${lookup(var.jenkins_slaves[count.index], "jenkins_slave_name")}"
  }
  
  metadata_startup_script = "${file("./startup-script.sh")}"

  network_interface {
    # A default network is created for all GCP projects
    subnetwork       = "${var.subnetwork}"
    subnetwork_project       = "${var.project}"
    access_config {
    }
  }
  service_account {
    email = "${var.service_account}"
    scopes = ["cloud-platform"]
  }
}
