# Deploy application on GCP using terraform
Code used to deploy terraform infrastructure on GCP

## Deployment from script
1. Edit variables file with the parameters for your environment, example:
    * vi vars-product/product-env.tf
    * Note: remember that you should not leave spaces between the name of the variable and the value, example:
    * gcp_project="project_id"
2. Execute create.sh script passing the path to the configuration file, example:
    * ./create.sh vars-product/product-env.tf
3. When it asks, enter details like username and password of repository where docker image is.
