# Instructions to run Jenkins Master

  - Include your aws access key and secret key in aws.tf
  - Include the env variables to provide master details in the repo [jenkins-slave](https://abhaysr@bitbucket.org/abhaysr/jenkins-slave/) in the .env file 
  - Make sure to have port 50000 open on both  master and slave instances
  - Use the images in the img folder as a reference to configure configurations on Slave
  - ![picture](img/jenkins-slave.PNG)

