module "vminfra" {
  source = "./components/vminfra"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  vminfra_node_settings = "${var.vminfra_node_settings}"
  vminfra_service_account = "${var.vminfra_service_account}"
  vminfra_purpose = "${var.vminfra_purpose}"
  vminfra_subnetwork = "${var.vminfra_subnetwork}"
  vminfra_subnetwork_project = "${var.vminfra_subnetwork_project}"
  vminfra_image = "${var.vminfra_image}"
}
