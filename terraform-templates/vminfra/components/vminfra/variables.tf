variable "project" {}
variable "productname" {}
variable "environment" {}
variable "vminfra_service_account" {}
variable "vminfra_purpose" {}
variable "vminfra_subnetwork" {}
variable "vminfra_subnetwork_project" {}
variable "vminfra_image" {}
variable "vminfra_node_settings" {
  type = "map"
}
