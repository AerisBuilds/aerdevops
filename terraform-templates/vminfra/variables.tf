variable "project" {}
variable "environment" {}
variable "product_name" {}
variable "vminfra_service_account" {}
variable "vminfra_purpose" {}
variable "vminfra_subnetwork" {}
variable "vminfra_subnetwork_project" {}
variable "vminfra_image" {}
variable "vminfra_node_settings" {
  type = "map"
}
