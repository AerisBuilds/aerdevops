variable "project_name" {
  description = "The name for the project"
}
variable "project" {
  description = "If provided, the project uses the given project ID. Mutually exclusive with random_project_id being true."
  default     = ""
}
variable "project_billing_account" {
  description = "The ID of the billing account to associate this project with"
}
variable "project_folder_id" {
  description = "The ID of a folder to host this project"
  default     = ""
}
variable "bucket" {
  description = "name of bucket to populate the new project"
}
