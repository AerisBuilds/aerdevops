provider "google" {
  version = "~> 2.17.0"
}

resource "google_project" "make_project" {
	name = "${var.project_name}"
	project_id = "${var.project}"
	folder_id = "${var.project_folder_id}"
	billing_account = "${var.project_billing_account}"
	auto_create_network = false
}

resource "google_project_services" "project_services" {
	project            = "${var.project}"
	services          = ["iamcredentials.googleapis.com", "compute.googleapis.com","dns.googleapis.com","oslogin.googleapis.com","serviceusage.googleapis.com","containerregistry.googleapis.com","storage-component.googleapis.com","bigquery-json.googleapis.com","pubsub.googleapis.com","cloudresourcemanager.googleapis.com","storage-api.googleapis.com","iam.googleapis.com","container.googleapis.com","serviceusage.googleapis.com","servicenetworking.googleapis.com","monitoring.googleapis.com"]
	disable_on_destroy = true
	depends_on = ["google_project.make_project"]
}

resource "google_storage_bucket" "local_buckets" {
	project            = "${var.project}"
	name	 	   = "${var.bucket}"
	depends_on = ["google_project.make_project"]
        versioning {
            enabled = "true"
        }
}

resource "google_compute_project_metadata" "default" {
	project            = "${var.project}"
	metadata = {
		enable-oslogin  = "TRUE"
  	}
	depends_on = ["google_project.make_project"]
}
