#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=$(grep "^project_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^project_bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

terraform init -backend-config="bucket=${BUCKET}" -backend-config="prefix=${PURPOSE}" -force-copy
terraform plan -out=${PROJECT}_project.tfplan -var-file=$1
terraform apply -input=false ${PROJECT}_project.tfplan
