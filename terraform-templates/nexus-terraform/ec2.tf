#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#
resource "aws_instance" "nexus_instance" {
    ami                         = "${var.ami}"
    iam_instance_profile        = "${var.iam_instance_profile}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
                                #!/bin/bash
                                sudo yum -y update
                                sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
                                sudo yum -y install git
                                sudo chkconfig docker on
                                sudo service docker start
                                sleep 2
                                sudo mkdir -p /var/lib/nexus/data
                                sudo docker login -u atspdeployer -p "${var.nexus_passwd}" http://repo.aeriscloud.com:6666
                                sudo docker run --restart=unless-stopped -d -u 0 -v /var/lib/nexus/data:/nexus-data -p 8081-8089:8081-8089 -e INSTALL4J_ADD_VM_PARAMS="-Xms4g -Xmx4g -XX:MaxDirectMemorySize=6717M" --ulimit nofile=65536:65536 --name nexus repo.aeriscloud.com:6666/nexus:3
                                EOF

    subnet_id                   = "subnet-ac8b53db"

    security_groups = ["${var.aws_security_group}"]

    tags {
        Name = "${var.name_prefix}"
        Project = "${var.name_project}"
        Owner = "${var.owner}"
        Component = "${var.component}"
        Deployment = "${var.deployment}"
    }

    root_block_device {
        volume_type = "gp2"
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }

}
