#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#
variable "region" {
    default     = "us-west-2"
    description = "The region of AWS, for AMI lookups"
}

variable "iam_instance_profile" {
    default     = "TerraformRole"
    description = "IAM Role to attach to EC2 instance"
}

variable "name_prefix" {
    default     = "automotive-nexus"
    description = "Prefix for all AWS resource names"
}

variable "name_project" {
    default     = "AMP"
    description = "Prefix for all AWS resource names"
}

variable "owner" {
    default     = "abhay.rao@aeris.net"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "ami-6b8cef13"
    description = "Instance AMI ID"
}

variable "key_name" {
    default = "ampdeveloper"
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "m5.xlarge" # RAM Requirements >= 8gb
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "200"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "vpc-54907731"
   description = "VPC id"
}

variable "vpc_cidr" {
    default     = "10.6.0.0/16"
    description = "Subnet in CIDR format to assign to VPC"
}

variable "aws_security_group" {
   default = "sg-6a5ad418"
   description = "Security group"
}

variable "component" {
   default = "Artifactory"
}

variable "deployment" {
   default = "Dev/Prod"
   description = "VPC id"
}

variable "nexus_passwd" {
    default     = ""
    description = "Enter repo.aeriscloud.com password"
}
