# Instructions to run Nexus

  - Create an EC2 instance and attach to it the IAM Role "TerraformRole"
  - Include the nexus password, IAM Role, private key and other aws properties in the [var file](https://bitbucket.org/aeriscom/automotive-devops/src/d451c13bf80f589ad92613b6b454feb4d0851b85/terraform-templates/jenkins-master/variables.tf?at=master&fileviewer=file-view-default)
  - Create terraform plan: ```terraform plan -out nexus.plan```
  - Apply terraform plan: ```terraform apply "nexus.plan"```
  - The terraform script pulls the Nexus image form Nexus repo