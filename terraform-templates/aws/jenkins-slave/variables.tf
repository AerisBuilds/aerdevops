#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#
variable "region" {
    default     = "us-west-2"
    description = "The region of AWS, for AMI lookups"
}

variable "count" {
    default     = "1"
    description = "Number of HA servers to deploy"
}

variable "name_prefix" {
    default     = "Automotive-Jenkins-master"
    description = "Prefix for all AWS resource names"
}

variable "name_project" {
    default     = "AMP"
    description = "Prefix for all AWS resource names"
}

variable "owner" {
    default     = "abhay.rao@aeris.net"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "ami-a0cfeed8"
    description = "Instance AMI ID"
}

variable "key_name" {
    default = "ampdeveloper"
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "m4.large" # RAM Requirements >= 8gb
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "200"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "vpc-54907731"
   description = "VPC id"
}

variable "vpc_cidr" {
    default     = "10.6.0.0/16"
    description = "Subnet in CIDR format to assign to VPC"
}


variable "subnet_ec2ids" {
    type        = "string"
    default     = "subnet-ac8b53db,subnet-0b40736f,subnet-10d3e656"
    description = "Subnet ranges (requires 3 entries)"
}

variable "nexus_passwd" {
	default = "AtspDeployer20"
	description = "Nexus repo password"
}

variable "aws_security_group" {
        default = "sg-3b4f6345"
        description = "Nexus repo password"
}

variable "master_url" {
        default = "sg-3b4f6345"
        description = "Jenkins master url"
}

variable "master_user" {
        default = ""
        description = "Jenkins admin user"
}

variable "master_passwd" {
        default = ""
        description = "Password for Jenkins master"
}
