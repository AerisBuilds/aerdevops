variable "project" {}
variable "purpose" {}
variable "product_name" {}
variable "environment" {}
variable "region" {}
variable "additional_zones" { type = "list" }
variable "network" {}
variable "subnetwork" {}
variable "master_ipv4_cidr_block" {}
variable "cluster_secondary_range_name" {}
variable "services_secondary_range_name" {}
variable "service_account" {}
variable "namespaces" {
  type = "list"
}
variable "node_pool_settings" {
  type = "map"
}
