#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=$(grep "^purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export SUBNETWORK=$(grep "^subnetwork=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

#terraform init -backend-config="bucket=\"${BUCKET}\"" -backend-config="prefix=\"${PURPOSE}\"" -backend-config="project=\"${PROJECT}\""
terraform init -backend-config=bucket=${BUCKET} -backend-config=prefix=${PURPOSE} -force-copy
#terraform plan -out=${PROJECT}_cluster.tfplan -var-file=$1 -var purpose="${PURPOSE}" -var subnetwork="${SUBNETWORK}" -target=module.cluster -target=module.node_pool
#terraform apply  -input=false ${PROJECT}_cluster.tfplan
#terraform plan -out=${PROJECT}_cluster.tfplan -var-file=$1 -var purpose="${PURPOSE}" -var subnetwork="${SUBNETWORK}"
terraform plan -out=${PROJECT}_cluster.tfplan -var-file=$1 -var purpose="${PURPOSE}"
terraform apply  -input=false ${PROJECT}_cluster.tfplan

#terraform plan -out=${PROJECT}_loadbalancer.tfplan -var-file=$1 -var purpose="${PURPOSE}" -var subnetwork="${SUBNETWORK}" -target=module.namespaces -target=module.https_load_balancer
#terraform plan -out=${PROJECT}_loadbalancer.tfplan -var-file=$1 -var purpose="${PURPOSE}" -var subnetwork="${SUBNETWORK}" -target=module.namespaces
#terraform apply  -input=false ${PROJECT}_loadbalancer.tfplan

