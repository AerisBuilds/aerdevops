#!/bin/bash
cd "$(dirname "$0")"





function check_return_code() {
    RETURN_CODE=$1
    if [[ "${RETURN_CODE}" -ne 0 ]]; then
        echo -e "gkcluster inventory not found"
        exit 0
    fi
}


if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./clean_cluster.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./clean_cluster.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi


export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENVIRONMENT=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')



gcloud beta container clusters get-credentials ${PRODUCT_NAME}-${ENVIRONMENT}-${REGION} --region ${REGION} --project ${PROJECT}

if [[ "$?" -ne 0 ]]; then
    echo -e "gkcluster not found"
    exit 0
    rm $HOME/.kube/config

else

        RAW_DEPLOYMENTS_INVENTORY=$(helm list | awk '{if (NR!=1) print $1}' )
        SAVEIFS=$IFS   # Save current IFS
        IFS=$'\n'      # Change IFS to new line
        DEPLOYMENTS_INVENTORY=($RAW_DEPLOYMENTS_INVENTORY) # split to array $names
        IFS=$SAVEIFS 


        for DEPLOYMENT in "${DEPLOYMENTS_INVENTORY[@]}"
        do
        : 

            echo "deleting: $DEPLOYMENT"
            helm delete $DEPLOYMENT --purge

        done 


        rm $HOME/.kube/config

fi



 
