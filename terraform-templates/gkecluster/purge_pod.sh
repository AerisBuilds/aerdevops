#!/bin/bash
cd "$(dirname "$0")"


if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./purge_pod.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./purge_pod.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing helm deployment name!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./purge_pod.sh path_to_variables_file helm_deployment_name"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./purge_pod.sh vars-product/product-env.tf mon-filebeat"
    echo -e "\033[0m"
    exit 1
fi


export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENVIRONMENT=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')



gcloud beta container clusters get-credentials ${PRODUCT_NAME}-${ENVIRONMENT}-${REGION} --region ${REGION} --project ${PROJECT}

if [[ "$?" -ne 0 ]]; then
    echo -e "gkcluster not found"
    exit 0
else


    echo "deleting: $2"
    helm delete $2 --purge



fi



 
