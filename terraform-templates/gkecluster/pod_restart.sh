
#!/bin/bash
cd "$(dirname "$0")"


if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./pod_restart.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./pod_restart.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing helm deployment name!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./pod_restart.sh path_to_variables_file helm_deployment_name"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./pod_restart.sh vars-product/product-env.tf mon-filebeat"
    echo -e "\033[0m"
    exit 1
fi



cat <<EOF >>get_deployment_status.py
import sys

helm_status=sys.argv[1]

from_deployment_status=helm_status.split('Deployment')[1]
deployment_status=from_deployment_status.split('==>')[0].splitlines()[2]

print(deployment_status)
EOF

export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENVIRONMENT=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')



gcloud beta container clusters get-credentials ${PRODUCT_NAME}-${ENVIRONMENT}-${REGION} --region ${REGION} --project ${PROJECT}



if [[ "$?" -ne 0 ]]; then
    echo -e "gkcluster not found"
    exit 0
else
    HELM_DEPLOYMENT_NAME=$2
    
    HELM_STATUS=$(helm status $HELM_DEPLOYMENT_NAME -o=json | jq -r '.info.status.resources')
    
    DEPLOYMENT_NAMESPACE=$(helm status $HELM_DEPLOYMENT_NAME -o=json | jq -r '.namespace')

    DEPLOYMENT_STATUS=$(python get_deployment_status.py "$HELM_STATUS")

    POD_REPLICA_VALUE=$(echo $DEPLOYMENT_STATUS | awk '{print $3}')

    DEPLOYMENT_NAME=$(echo $DEPLOYMENT_STATUS | awk '{print $1}')

    kubectl scale deployment $DEPLOYMENT_NAME --replicas=0 -n $DEPLOYMENT_NAMESPACE

    sleep 2

    kubectl scale deployment $DEPLOYMENT_NAME --replicas=$POD_REPLICA_VALUE -n $DEPLOYMENT_NAMESPACE

fi
