#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=$(grep "^purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export SUBNETWORK=$(grep "^subnetwork=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENVIRONMENT=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')




terraform init -backend-config=bucket=${BUCKET} -backend-config=prefix=${PURPOSE}



gcloud beta container clusters get-credentials ${PRODUCT_NAME}-${ENVIRONMENT}-${REGION} --region ${REGION} --project ${PROJECT}

if [[ "$?" -ne 0 ]]; then
    echo -e "gkcluster not found"
    exit 0
    rm $HOME/.kube/config

else
    terraform destroy -var-file=$1 -var purpose="${PURPOSE}" -auto-approve
    rm $HOME/.kube/config
fi


