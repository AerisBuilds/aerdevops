module "cluster" {
  source = "./components/cluster"
  project = "${var.project}"
  region = "${var.region}"
  additional_zones = "${var.additional_zones}"
  name = "${var.product_name}-${var.environment}-${var.region}"
  network = "${var.network}"
  subnetwork = "${var.subnetwork}"
  master_ipv4_cidr_block = "${var.master_ipv4_cidr_block}"
  service_account = "${var.service_account}"
  namespaces = "${var.namespaces}"
  cluster_secondary_range_name = "${var.cluster_secondary_range_name}"
  services_secondary_range_name = "${var.services_secondary_range_name}"
}

module "node_pool" {
  source = "./components/node_pool"
  project = "${var.project}"
  region = "${var.region}"
  cluster_name = "${module.cluster.cluster_name}"
  node_pool = "${var.node_pool_settings}"
  service_account = "${var.service_account}"
}
module "namespaces" {
  source = "./components/namespaces"
  namespaces = "${var.namespaces}"
  endpoint = "${module.cluster.endpoint}"
  cluster_ca_certificate = "${module.cluster.cluster_ca_certificate}"
}
