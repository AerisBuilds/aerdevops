output "cluster_name" {
  value = "${element(google_container_cluster.cluster-sr.*.name,0)}"
}
output "node_pool_urls" {
  value = "${element(google_container_cluster.cluster-sr.*.instance_group_urls,0)}"
}
output "endpoint" {
  value = "${element(google_container_cluster.cluster-sr.*.endpoint,0)}"
}
