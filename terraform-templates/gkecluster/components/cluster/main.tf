data "google_client_config" "current" {}

provider "kubernetes" {
  load_config_file = false
  host = "https://${google_container_cluster.cluster-sr.*.endpoint}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.cluster-sr.*.master_auth.0.cluster_ca_certificate)}"
  token = "${data.google_client_config.current.access_token}"
}

#====================== Google API ======================
resource "google_project_service" "kubernetes_api" {
  project = "${var.project}"
  disable_on_destroy = false
  service = "container.googleapis.com"
}
#====================== Cluster with secondary ranges======================
resource "google_container_cluster" "cluster-sr" {
  depends_on = ["google_project_service.kubernetes_api"]
  project =  "${var.project}"
  region =  "${var.region}"
  additional_zones = "${var.additional_zones}"
  name =  "${var.name}"
  network =  "${var.network}"
  subnetwork = "${var.subnetwork}"
  remove_default_node_pool = true
  initial_node_count = 1
  enable_legacy_abac = "true"
  monitoring_service = "monitoring.googleapis.com/kubernetes"
  logging_service = "logging.googleapis.com/kubernetes"
  private_cluster_config {
    enable_private_endpoint = true
    enable_private_nodes    = true
    //master_ipv4_cidr_block  = "172.16.4.0/28"
    master_ipv4_cidr_block  = "${var.master_ipv4_cidr_block}"
  }
  ip_allocation_policy {
    cluster_secondary_range_name = "${var.cluster_secondary_range_name}"
    services_secondary_range_name = "${var.services_secondary_range_name}"
  }
  master_authorized_networks_config {
    cidr_blocks {
      cidr_block = "172.16.1.0/29"
      display_name = "BSS subnet"
    }
  }
}
output "cluster_ca_certificate" {
  value = "${element(google_container_cluster.cluster-sr.*.master_auth.0.cluster_ca_certificate,0)}"
}
