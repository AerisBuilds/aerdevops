data "google_client_config" "current" {}

provider "kubernetes" {
  load_config_file = true

  host = "https://${var.endpoint}"
  cluster_ca_certificate = "${base64decode(var.cluster_ca_certificate)}"

  token = "${data.google_client_config.current.access_token}"
}

#====================== Namespaces ======================
resource "kubernetes_namespace" "namespace" {
  provider = "kubernetes"
  count = "${length(var.namespaces)}"
  metadata {
    annotations = {
      name = "${element(var.namespaces, count.index)}"
      
    }

    labels = {
      mylabel = "${element(var.namespaces, count.index)}"
    }

    name = "${element(var.namespaces, count.index)}"
  }
}
