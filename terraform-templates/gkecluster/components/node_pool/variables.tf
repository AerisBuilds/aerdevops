variable "project" {}
variable "region" {}
variable "cluster_name" {}
variable "node_pool" { type = "map" }
variable "service_account" {}
