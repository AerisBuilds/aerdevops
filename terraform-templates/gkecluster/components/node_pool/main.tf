resource "google_container_node_pool" "pool" {
 count = "${length(var.node_pool)}"
 project = "${var.project}"
 region = "${var.region}"
 name = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "name" )}"
 cluster = "${var.cluster_name}"
 node_count = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "count" )}"

 node_config {
  machine_type = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "machine_type" )}"
  service_account = "${var.service_account}"
/*
  labels = {
   "vm.type.services/app" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_app", "false" )}"
   "vm.type.services/dp" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_dp", "false" )}"
   "vm.type.services/spc" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_spc", "false" )}"
   "vm.type.services/dap" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_dap", "false" )}"
   "vm.type.services/auth" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_auth", "false" )}"
   "vm.type.services/mon" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_mon", "false" )}"
   "vm.type.services/test" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_test", "false" )}"
   "vm.type.services/ab-services" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ab", "false" )}"
   "vm.type.infra/ibus" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_ibus", "false" )}"
   "vm.type.infra/ebus" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_ebus", "false" )}"
   "vm.type.infra/stream-ibus" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_stream-ibus", "false" )}"
   "vm.type.infra/otabus" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_otabus", "false" )}"
   "vm.type.infra/cache" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_cache", "false" )}"
   "vm.type.infra/umredis" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_umredis", "false" )}"
   "vm.type.infra/hzui" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_hzui", "false" )}"
   "vm.type.infra/hzapi" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_hzapi", "false" )}"
   "vm.type.infra/nifi" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_nifi", "false" )}"
   "vm.type.infra/hz" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_hz", "false" )}"
   "vm.type.infra/infra" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_infra_infra", "false" )}"  
   "vm.type.services/ap-app" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_app", "false" )}"
   //"vm.type.services/ap-app-aeris" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_app_aeris", "false" )}"
   //"vm.type.services/ap-app-sprint" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_app_sprint", "false" )}"
   //"vm.type.services/ap-app-uscc" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_app_uscc", "false" )}"
   //"vm.type.services/ap-app-sim-ui" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_app_simui", "false" )}"
   "vm.type.services/ap-management" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_management", "false" )}"   
   "vm.type.services/ap-reporting" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_reporting", "false" )}"   
   "vm.type.services/ap-partner" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_partner", "false" )}"   
   "vm.type.services/ap-services" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_ap_services", "false" )}"   
   //"vm.type.services/af-services" = "${lookup(var.node_pool[element(keys(var.node_pool), count.index)], "vm_type_services_af_services", "false" )}"   
  }
*/
  oauth_scopes = [
   "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring",
  ]
 }
}
