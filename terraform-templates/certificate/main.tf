resource "google_compute_ssl_certificate" "default" {
  name        = "${var.ssl_cert_name}"
  project     = "${var.project}"
  private_key = "${file("private.key")}"
  certificate = "${file("certificate.pem")}"
  lifecycle {
    create_before_destroy = true
  }
}
