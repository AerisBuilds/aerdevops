variable "project" {
	type = "string"
	description = "GCP Project Name"
}
variable "product_name" {
	type = "string"
	description = "Product Name"
}
variable "environment" {
	type = "string"
	description = "Environment"
}
variable "network" {
  type        = "string"
  description = "Network name"
}
variable "allow_in" {
	type = "list"
	description = "FW rule to allow ingress"
}
variable "allow_out" {
	type = "list"
	description = "FW rule to allow egress"
}
variable "deny_in" {
	type = "list"
	description = "FW rule to deny ingress"
}
variable "deny_out" {
	type = "list"
	description = "FW rule to deny egress"
}
