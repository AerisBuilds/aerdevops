#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./purge_firewallrules.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./purge_firewallrules.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"


export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

RAW_FIREWALL_RULE_NAMES=$(gcloud compute firewall-rules list --project $PROJECT --filter="network=$NETWORK" --format=json | jq -r '.[].name')

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
FIREWALL_RULE_NAMES=($RAW_FIREWALL_RULE_NAMES) # split to array $names
IFS=$SAVEIFS 

for FIREWALL_RULE in "${FIREWALL_RULE_NAMES[@]}"
do
    :

    echo "deleting firewall rule: $FIREWALL_RULE"
    gcloud compute firewall-rules delete $FIREWALL_RULE --project $PROJECT -q

done 