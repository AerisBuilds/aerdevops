resource "google_compute_firewall" "allow_in" {
  count   = "${length(var.allow_in)}"
  name    = "${lookup(var.allow_in[count.index], "name")}-${var.environment}"
  network = "${var.network}"
  project = "${var.project}"

  allow {
    protocol = "${lookup(var.allow_in[count.index], "protocol")}"
    ports    = "${lookup(var.allow_in[count.index], "port")}"
  }

  target_tags   = "${lookup(var.allow_in[count.index], "target_tags")}"
  destination_ranges = "${lookup(var.allow_in[count.index], "destination_ranges")}"
  source_ranges = "${lookup(var.allow_in[count.index], "source_ranges")}"
  source_tags   = "${lookup(var.allow_in[count.index], "source_tags")}"
  priority      = "${lookup(var.allow_in[count.index], "priority")}"
}
resource "google_compute_firewall" "allow_out" {
  count   = "${length(var.allow_out)}"
  name    = "${lookup(var.allow_out[count.index], "name")}-${var.environment}"
  network = "${var.network}"
  project = "${var.project}"

  allow {
    protocol = "${lookup(var.allow_out[count.index], "protocol")}"
    ports    = "${lookup(var.allow_out[count.index], "port")}"
  }

  target_tags   = "${lookup(var.allow_out[count.index], "target_tags")}"
  destination_ranges = "${lookup(var.allow_out[count.index], "destination_ranges")}"
  source_ranges = "${lookup(var.allow_out[count.index], "source_ranges")}"
  source_tags   = "${lookup(var.allow_out[count.index], "source_tags")}"
  priority      = "${lookup(var.allow_out[count.index], "priority")}"
}
resource "google_compute_firewall" "deny_in" {
  count   = "${length(var.deny_in)}"
  name    = "${lookup(var.deny_in[count.index], "name")}-${var.environment}"
  network = "${var.network}"
  project = "${var.project}"

  allow {
    protocol = "${lookup(var.deny_in[count.index], "protocol")}"
    ports    = "${lookup(var.deny_in[count.index], "port")}"
  }

  target_tags   = "${lookup(var.deny_in[count.index], "target_tags")}"
  destination_ranges = "${lookup(var.deny_in[count.index], "destination_ranges")}"
  source_ranges = "${lookup(var.deny_in[count.index], "source_ranges")}"
  source_tags   = "${lookup(var.deny_in[count.index], "source_tags")}"
  priority      = "${lookup(var.deny_in[count.index], "priority")}"
}
resource "google_compute_firewall" "deny_out" {
  count   = "${length(var.deny_out)}"
  name    = "${lookup(var.deny_out[count.index], "name")}-${var.environment}"
  network = "${var.network}"
  project = "${var.project}"

  allow {
    protocol = "${lookup(var.deny_out[count.index], "protocol")}"
    ports    = "${lookup(var.deny_out[count.index], "port")}"
  }

  target_tags   = "${lookup(var.deny_out[count.index], "target_tags")}"
  destination_ranges = "${lookup(var.deny_out[count.index], "destination_ranges")}"
  source_ranges = "${lookup(var.deny_out[count.index], "source_ranges")}"
  source_tags   = "${lookup(var.deny_out[count.index], "source_tags")}"
  priority      = "${lookup(var.deny_out[count.index], "priority")}"
}

