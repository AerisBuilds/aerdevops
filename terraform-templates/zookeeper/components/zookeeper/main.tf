resource "google_compute_instance" "default" {
  count = "${length(var.zookeeper_node_settings)}"
  name         = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "name" )}"
  machine_type = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "machine_type" )}"
  tags = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "tags" )}"
  project = "${var.project}"
  hostname = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "hostname" )}"
  deletion_protection = true

  boot_disk {
    initialize_params {
      image = "${var.zookeeper_image}"
      size = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "disk_size_root" )}"
    }
  }
  attached_disk {
    source = "${element(google_compute_disk.default.*.self_link, count.index)}"
    device_name = "${element(google_compute_disk.default.*.name, count.index)}"
  }

  zone = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "zone" )}"
  metadata = {
    Product = "${var.productname}"
    Project = "${var.productname}"
    Component = "${var.zookeeper_purpose}"
    Deployment = "${var.environment}"
    Role= "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "role" )}"
  }

  //metadata_startup_script = "${file("./startup-script.sh")}"

  //# tags = ["${google_compute_firewall.jenkins-slave-1.name}"]

  network_interface {
    # A default network is created for all GCP projects
    subnetwork       = "${var.zookeeper_subnetwork}"
    subnetwork_project       = "${var.zookeeper_subnetwork_project}"
  }
  service_account {
    email = "${var.zookeeper_service_account}"
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_disk" "default" {
    count = "${length(var.zookeeper_node_settings)}"
    project = "${var.project}"
    name    = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "name" )}-1"
    type    = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "disk_size_type" )}"
    zone    = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "zone" )}"
    size    = "${lookup(var.zookeeper_node_settings[element(keys(var.zookeeper_node_settings), count.index)], "disk_size" )}"
}
