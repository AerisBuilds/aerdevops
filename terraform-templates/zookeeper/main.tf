module "zookeeper" {
  source = "./components/zookeeper"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  zookeeper_node_settings = "${var.zookeeper_node_settings}"
  zookeeper_service_account = "${var.zookeeper_service_account}"
  zookeeper_purpose = "${var.zookeeper_purpose}"
  zookeeper_subnetwork = "${var.zookeeper_subnetwork}"
  zookeeper_subnetwork_project = "${var.zookeeper_subnetwork_project}"
  zookeeper_image = "${var.zookeeper_image}"
}
