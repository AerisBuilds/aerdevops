variable "project" {}
variable "environment" {}
variable "product_name" {}
variable "zookeeper_service_account" {}
variable "zookeeper_purpose" {}
variable "zookeeper_subnetwork" {}
variable "zookeeper_subnetwork_project" {}
variable "zookeeper_image" {}
variable "zookeeper_node_settings" {
  type = "map"
}
