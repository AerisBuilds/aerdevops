variable "project" {}
variable "productname" {}
variable "environment" {}
variable "cloudera_service_account" {}
variable "cloudera_purpose" {}
variable "cloudera_subnetwork" {}
variable "cloudera_subnetwork_project" {}
variable "cloudera_image" {}
variable "cloudera_node_settings" {
  type = "map"
}
