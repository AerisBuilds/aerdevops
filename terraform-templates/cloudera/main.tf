module "cloudera" {
  source = "./components/cloudera"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  cloudera_node_settings = "${var.cloudera_node_settings}"
  cloudera_service_account = "${var.cloudera_service_account}"
  cloudera_purpose = "${var.cloudera_purpose}"
  cloudera_subnetwork = "${var.cloudera_subnetwork}"
  cloudera_subnetwork_project = "${var.cloudera_subnetwork_project}"
  cloudera_image = "${var.cloudera_image}"
}
