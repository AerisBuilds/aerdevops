## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
ssl_cert_name="crt-aerjupiter-com"

## common vars ##
productname="acp-prodtools"
environment="tools"
region="us-west1"
zone="us-west1-a"
image="aeriscomm-hostproj-aer-201903/aeris-ubuntu-latest"
network="vpc-acp-prodtools-us-west1"
subnetwork="subnet-acp-prodtools-infra-us-west1"
docker_repo="http://repo.aeriscloud.com:6666"
tf_bucket="csb-acp-prodtools-tfstate-us-west1"
service_account="iam-acp-prodtools-us-west1@aeriscom-acp-prodtools-201906.iam.gserviceaccount.com"
domain="acp-prodtools.aerjupiter.com"
project="aeriscom-acp-prodtools-201906"
dns_zone_name="acpconnectivity"

## jenkins master vars ###

jenkins_master_purpose="jenkins-master"
jenkins_master_machine_type="n1-standard-2"
jenkins_master_disk_size="40"
jenkins_master_docker_image="repo.aeriscloud.com:6666/jenkins-master:006"
jenkins_master_port="8080"
jenkins_master_subdomain="deploy"


## jenkins slave vars ###
jenkins_slave_purpose="jenkins-slave"
jenkins_slave_machine_type="n1-standard-2"
jenkins_slave_disk_size="100"
jenkins_slave_docker_image="repo.aeriscloud.com:6666/jenkins-slave-gcp:001"
