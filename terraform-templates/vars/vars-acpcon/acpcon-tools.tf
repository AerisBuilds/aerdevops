## common vars ##
productname="acpcon"
environment="tools"
machine_type="n1-standard-2"
region="us-west1"
zone="us-west1-a"
image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190626"
network="vpc-acp-tools-us-west1"
subnetwork="subnet-acp-tools-infra-us-west1"
docker_repo="http://repo.aeriscloud.com:6666"
gcp_project="aeriscom-acp-tools-201905"
docker_user="atspdeployer"
tf_bucket="csb-acp-tools-tfstate-us-west1"
service_account="terraform-seracc@aeriscom-acp-tools-201905.iam.gserviceaccount.com"
domain="acp.aerjupiter.com"


## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
ssl_cert_name="crt-acp-tools-20190606"


dns_zone_name="acpconnectivity"

## sonarqube vars ##
sonarqube_purpose="sonarqube"
sonarqube_machine_type="n1-standard-2"
sonarqube_disk_size="10"
sonarqube_docker_image="repo.aeriscloud.com:6666/sonarqube:001"
sonarqube_port="9000"
cert_location="projects/aeriscom-sol-tools-201903/global/sslCertificates/crt-ampsolutions-tools-20190329"
sonarqube_cloudsql_host="csql-acp-tools-us-west1"
sonarqube_cloudsql_tier="db-n1-standard-1"
sonarqube_db_user="sonar_db_user"
sonarqube_db_name="sonar"
sonarqube_cloudsql_disk_size="100"
sonarqube_subdomain="codequality"
