## common vars ##
bucket="csb-ampsol-dev-tfstate-dci"
project="aeriscom-ampsol-dev-201908"
product_name="ampsol"
environment="dev"
region="us-west1"
network="vpc-ampsol-dev-us-west1"
service_account="iam-ampsol-dev-us-west1@aeriscom-ampsol-dev-201908.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="subnet-ampsol-dev-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    node_pool_id="1"
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_dap="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    node_pool_id="2"
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
}


### vault vars ###

vault_purpose="sec"
machine_type="n1-standard-2"
vault_subnetwork="subnet-ampsolutions-dev-sec01-us-west1"
image="aeriscom-sol-tools-201903/aeris-ubuntu-20190313"
disk_size="10"
docker_repo="http://repo.aeriscloud.com:6666"
docker_image="repo.aeriscloud.com:6666/vault:1.0.2-7"
key_ring="kms-ampsol-dev01-us-west1-ok" # TODO: Change name
crypto_key="key-ampsol-dev01-us-west1-ok" # TODO: Change name
cloudsql_host="csql-ampsol-dev01-us-west1-ok1"
cloudsql_tier="db-n1-standard-1"
vault_db_name="csql-ampsol-dev01-vault"
vault_db_user="dev-vault"
cloudsql_disk_size="10"
service_account_vault="iam-ampsol-dev-us-west1-vault"

################## SQL VARS ######################

#ampsolutions-dev-cdh-sql_cloudsql_host="db-ampsol-dev-solutions-cdh-db-us-west1"
#ampsolutions-dev-cdh-sql_cloudsql_tier="db-n1-standard-2"
#ampsolutions-dev-cdh-sql_cloudsql_disk_size="20"
#ampsolutions-dev-cdh-sql_zone="us-west1-a"
#ampsolutions-dev-cdh-sql_db_replica="true"
#ampsolutions-dev-cdh-sql_region="us-west1"

ampsol-dev-mysql_cloudsql_host="db-ampsol-dev-mysql-db-us-west1"
ampsol-dev-mysql_cloudsql_tier="db-n1-standard-2"
ampsol-dev-mysql_cloudsql_disk_size="20"
ampsol-dev-mysql_zone="us-west1-a"
ampsol-dev-mysql_db_replica="true"
ampsol-dev-mysql_region="us-west1"

### waf vars ###

waf_purpose="waf"
waf_template="vm-ampsol-dev-waf-us-west1"
waf_bucket="csb-ampsol-dev-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-dev-dmz-us-west1"
#waf_image="aeriscomm-hostproj-aer-201903/aeris-ubuntu-latest"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-dev-waf-us-west1"
waf_loadbalancer="hlb-ampsol-dev-waf-us-west1"
waf_healthcheck="hlb-ampsol-dev-waf-us-west1"
waf_certificate="projects/aeriscom-ampsol-dev-201908/global/sslCertificates/crt-aerjupiter-com"
waf_config_path="config/ampsol/ampsol-dev"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"

##cloudera vars ##
cloudera_purpose="cloudera-dev"
cloudera_subnetwork="subnet-ampsol-dev-dss-us-west1"
cloudera_subnetwork_project="aeriscom-ampsol-dev-201908"
cloudera_image="aeriscom-sol-tools-201903/ubuntu14-jdk8-5112"
cloudera_service_account="iam-ampsol-dev-us-west1@aeriscom-ampsol-dev-201908.iam.gserviceaccount.com"

# name - name of the vm
# machine_type - GCP vm type
# disk_size_root - boot volume size
# disk_size - additional disk size
# disk_size_type - disk type
# tags - network tags to apply. required tags- 'dss' & 'private-data'
# role - Role played by this vm. will be set as a instance metadata
# hostname - hostname to set for the vm
# zone - zone
# additional_dns_mapping - additional dns mapping to be created for this vm
cloudera_node_settings={
  node1={
    name="dpp-cdhmaster"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","private-cloudera","dss","private-data"]
    role="cm-manager,master"
    hostname="dpp-cdhmaster.aeris-project.net"
    zone="us-west1-a"
    additional_dns_mapping=""
  }
  node2={
    name="dpp-cdhslave01"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave01.aeris-project.net"
    zone="us-west1-b"
    additional_dns_mapping="dpp-kafka01.aeris-project.net,dpp-zk01.aeris-project.net"
  }
  node3={
    name="dpp-cdhslave02"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave02.aeris-project.net"
    zone="us-west1-c"
    additional_dns_mapping="dpp-kafka02.aeris-project.net,dpp-zk02.aeris-project.net"
  }
   node4={
    name="dpp-cdhslave03"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave03.aeris-project.net"
    zone="us-west1-a"
    additional_dns_mapping="dpp-kafka03.aeris-project.net,dpp-zk03.aeris-project.net"
  }
}

cassandra_purpose="cassandra"
cassandra_subnetwork="projects/aeriscom-ampsol-dev-201908/regions/us-west1/subnetworks/subnet-ampsol-dev-dss-us-west1"
cassandra_subnetwork_project="aeriscom-ampsol-dev-201908"
cassandra_image="aeriscom-ampsol-dev-201908/ampsol-cassandra-nic-2"
cassandra_service_account="iam-ampsol-dev-us-west1@aeriscom-ampsol-dev-201908.iam.gserviceaccount.com"
cassandra_node_settings={
  node1={
    name="ampsol-cassandra-seed-1"
    machine_type="n1-standard-4"
    disk_size_root="30"
    disk_size="100"
    disk_size_type="pd-standard"
	tags=["dss","bss","fw-cassandra"]
    role="node"
    hostname="ampsol-cassandra-seed-1.aeris.project.net"
	zone="us-west1-a"
  }
  node2={
    name="ampsol-cassandra-seed-2"
    machine_type="n1-standard-4"
    disk_size_root="30"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","fw-cassandra"]
    role="node"
    hostname="ampsol-cassandra-seed-2.aeris.project.net"
	zone="us-west1-b"
  }
  node3={
    name="ampsol-cassandra-non-seed-1"
    machine_type="n1-standard-4"
    disk_size_root="30"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","fw-cassandra"]
    role="node"
    hostname="ampsol-cassandra-nonseed-3.aeris.project.net"
	zone="us-west1-a"
  }
  node4={
    name="ampsol-cassandra-non-seed-2"
    machine_type="n1-standard-4"
    disk_size_root="30"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","fw-cassandra"]
    role="node"
    hostname="ampsol-cassandra-nonseed-4.aeris.project.net"
	zone="us-west1-b"
  }
}


### Dummy postgres db example ###

postgres_cloudsql_host="csql-ampsol-test-12-dummy-us-west1"
postgres_region="us-west1"
postgres_cloudsql_disk_size="10"
postgres_cloudsql_tier="db-custom-1-3840"
postgres_db_replica="true"
postgres_zone="us-west1-a"
postgres_db_type="postgres"


mysql_cloudsql_host="csql-ampsol-test-11-dummy-us-west1"
mysql_region="us-west1"
mysql_cloudsql_disk_size="10"
mysql_cloudsql_tier="db-n1-standard-1"
mysql_db_replica="true"
mysql_zone="us-west1-a"

