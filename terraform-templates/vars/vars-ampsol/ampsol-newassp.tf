bucket="csb-ampsol-asia-tfstate1-asia-south1"
project="aeriscom-ampsol-asia-201904"
product_name="ampsol-asia"
environment="prod"
region="asia-south1"
zone="asia-south1-a"
network="vpc-ampsol-asia-asia-south1"
service_account="iam-ampsol-prod-asia-south1@aeriscom-ampsol-asia-201904.iam.gserviceaccount.com"
service_account_id="iam-ampsol-asia-gke-asia-south"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampsol-asia"
project_folder_id="52791446271"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-aer-201903"

subnets = [
  {
    subnet_name           = "subnet-ampsol-asia-k8s-asia-south1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-dss-asia-south1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-dmz-asia-south1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-bss-asia-south1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-sec-asia-south1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-imz-asia-south1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-asia-tst-asia-south1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "asia-south1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampsol-asia-k8s-asia-south1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampsol-asia-dss-asia-south1 = []
  subnet-ampsol-asia-dmz-asia-south1 = []
  subnet-ampsol-asia-bss-asia-south1 = []
  subnet-ampsol-asia-sec-asia-south1 = []
  subnet-ampsol-asia-imz-asia-south1 = []
  subnet-ampsol-asia-tst-asia-south1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "iam-sol-prodtools-asia-south1@aeriscom-sol-prodtools-201904.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-sol-prodtools-201904"
sa_devops_email = "solutions-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer","compute.imageUser"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []

## gkecluster vars ##
purpose="gkecluster"
additional_zones=["asia-south1-a","asia-south1-b"]
subnetwork="subnet-ampsol-asia-k8s-asia-south1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
  dap={
    name="node-pool-dap"
    count=2
    machine_type="n1-standard-16"
    vm_type_services_dap="true"
  }
}

##cloudera vars ##
cloudera_purpose="cloudera"
cloudera_subnetwork="subnet-ampsol-asia-dss-asia-south1"
cloudera_subnetwork_project="aeriscom-ampsol-asia-201904"
cloudera_image="aeriscom-sol-tools-201903/ubuntu14-jdk8-5112-ulimit64k"
cloudera_service_account="iam-ampsol-asia-gke-asia-south@aeriscom-ampsol-asia-201904.iam.gserviceaccount.com"
cloudera_node_settings={
node1={
    name="dpp-cdhmaster"
    machine_type="n1-standard-4"
    disk_size_root="200"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","private-cloudera","dss","private-data"]
    role="cm-manager,master"
    hostname="dpp-cdhmaster.aeris-project.net"
    zone="asia-south1-a"
    additional_dns_mapping=""
  }
  node2={
    name="dpp-cdhslave01"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave01.aeris-project.net"
    zone="asia-south1-b"
    additional_dns_mapping="dpp-kafka01.aeris-project.net,dpp-zk01.aeris-project.net"
  }
  node3={
    name="dpp-cdhslave02"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave02.aeris-project.net"
    zone="asia-south1-c"
    additional_dns_mapping="dpp-kafka02.aeris-project.net,dpp-zk02.aeris-project.net"
  }
   node4={
    name="dpp-cdhslave03"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave03.aeris-project.net"
    zone="asia-south1-a"
    additional_dns_mapping="dpp-kafka03.aeris-project.net,dpp-zk03.aeris-project.net"
  }
  node5={
    name="dpp-cdhslave04"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave04.aeris-project.net"
    zone="asia-south1-b"
    additional_dns_mapping=""
  }
}

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ampsol-newassp-waf"
waf_bucket="csb-ampsol-newassp-waf-asia-southeast1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-asia-dmz-asia-south1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-newassp-waf-asia-southeast1"
waf_loadbalancer="hlb-ampsol-newassp-waf-asia-southeast1"
waf_healthcheck="hlb-ampsol-newassp-waf-asia-southeast1"
waf_certificate=[
  "projects/aeriscom-ampsol-asia-201904/global/sslCertificates/crt-aerjupiter-com",
  "projects/aeriscom-ampsol-asia-201904/global/sslCertificates/aercloudin",
  "projects/aeriscom-ampsol-asia-201904/global/sslCertificates/aeris",
  "projects/aeriscom-ampsol-asia-201904/global/sslCertificates/ncell-axiata",
  "projects/aeriscom-ampsol-asia-201904/global/sslCertificates/okinawascooters"
]
waf_config_path="config/ampsol/ampsol-newassp"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"

