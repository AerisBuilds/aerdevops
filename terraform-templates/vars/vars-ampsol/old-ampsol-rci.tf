## common vars ##
bucket="csb-ampsol-rci-tfstate-us-west1"
project="aeriscom-ampsol-rci-201904"
product_name="ampsol-rci"
environment="rci"
region="us-west1"
logstash_endpoint="logstash:5044"
network="vpc-ampsol-rci-us-west1"
service_account="iam-ampsol-rci-us-west1@aeriscom-ampsol-rci-201904.iam.gserviceaccount.com"



## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain="ampsol-rci.aerjupiter.com"
ssl_cert_name="ampsol-rci"


## gkecluster vars ##

gkecluster_purpose="gkecluster"
additional_zones=["us-west1-a"]
gkecluster_subnetwork="subnet-ampsol-rci-k8s01-us-west1"
add_secondary_ranges="false"
cluster_secondary_range_name=""
services_secondary_range_name=""
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="app"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    node_pool_id="1"
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_dap="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    node_pool_id="2"
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
}
https_lb_settings={
  webapp={
    name="webapp"
    port_protocol="tcp"
    port="31000"
    node_pool_index="1"
    ssl_certificates="projects/ampsol-rci-201904/global/sslCertificates/ampsol-rci"
    port_range="443"
    subdomain="webserver.ampsol-rci.aerjupiter.com"
    managed_zone="ampsol-rci"
  }
}


### waf vars ###

waf_purpose="waf"
waf_template="vm-ampsol-rci-waf-us-west1"
waf_bucket="csb-ampsol-rci-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-rci-dmz01-us-west1"
waf_image="aeriscomm-hostproj-aer-201903/aeris-ubuntu-latest"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-rci-waf-us-west1"
waf_loadbalancer="hlb-ampsol-rci-waf-us-west1"
waf_healthcheck="hlb-ampsol-rci-waf-us-west1"
waf_certificate="projects/aeriscom-ampsol-rci-201904/global/sslCertificates/ampsol-rci"
waf_config_path="config/ampsol/ampsol-rci"
waf_min_instances="1"
waf_max_instances="2"
waf_managed_zone="ampsol-rci"
waf_endpoint="waf-ampsol-rci.aerjupiter.com"
waf_create_dns="false"

### CloudSQL vars ###
ampsol-rci_cloudsql_host="csql-ampsol-rci-1-us-west1"
ampsol-rci_region="us-west1"
ampsol-rci_cloudsql_disk_size="10"
ampsol-rci_cloudsql_tier="db-n1-standard-1"
ampsol-rci_db_replica="true"
ampsol-rci_zone="us-west1-a"
