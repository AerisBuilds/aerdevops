## common vars ##
productname="ampsolutions"
environment="tools"
region="us-west1"
zone="us-west1-a"
image="aeriscomm-hostproj-aer-201903/aeris-bootstrap-latest"
network="vpc-ampsolutions-tools-us-west1"
subnetwork="subnet-ampsolutions-tools-infra-us-west1"
docker_repo="http://repo.aeriscloud.com:6666"
project="aeriscom-sol-tools-201903"
bucket="csb-ampsolutions-tools-tfstate-us-west1"
service_account="terraform@aeriscom-sol-tools-201903.iam.gserviceaccount.com"
domain="ampsolutions.aerjupiter.com"



## sonarqube vars ##
sonarqube_purpose="sonarqube"
machine_type="n1-standard-2"
disk_size="100"
docker_image="repo.aeriscloud.com:6666/sonarqube:001"
gcp_project="aeriscom-sol-tools-201903"
sonarqube_port="9000"
cert_location="projects/aeriscom-sol-tools-201903/global/sslCertificates/crt-ampsolutions-tools-20190329"
domain_prefix="codequality" 
sonarqube_cloudsql_host="csql-ampsolutions-tools-us-west1"
sonarqube_cloudsql_tier="db-n1-standard-1"
sonarqube_db_user="sonar_db_user"
sonarqube_db_name="sonar"
sonarqube_cloudsql_disk_size="10"

## jenkins master vars ###

jenkinsm_purpose="jenkins-master"
jenkinsm_disk_size="40"
jenkinsm_docker_image="repo.aeriscloud.com:6666/jenkins-master:006"
jenkins_version="2.169"
jenkinsm_port="8080"
ssl_cert_name="crt-ampsolutions-tools-20190329"

## jenkins slave vars ###

jenkins_slaves = [
  {
    slave_name           = "jenkins-slave"
    machine_type         = "n1-standard-2"
    disk_size            = "200"
    docker_image         = "gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-slave:latest"
    jenkins_master_url   = "http://10.233.1.41:8080"
    jenkins_master_token = "f63472ef2fc731330f3cdab5497f47eb2ca1576808007d10e831db08231dd9f9"
    jenkins_slave_name   = "jenkins_slave"
  },
  {
    slave_name           = "jenkins-slave2"
    machine_type         = "n1-standard-2"
    disk_size            = "200"
    docker_image         = "gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-slave:latest"
    jenkins_master_url   = "http://10.233.1.41:8080"
    jenkins_master_token = "ae7b9c0645fec65f9f622200d81839ce25ada9020c642fc1ecddcb716503b7c4"
    jenkins_slave_name   = "jenkins_slave2"
  },
]
