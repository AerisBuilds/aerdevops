## common vars ##
bucket="csb-ampsol-rci-tfstate-us-west1"
project="aeriscom-ampsol-rci-201904"
product_name="ampsol-rci"
environment="rci"
region="us-west1"
zone="us-west1-a"
network="vpc-ampsol-rci-us-west1"
service_account_id="iam-ampsol-rci-us-west1"
service_account="iam-ampsol-rci-us-west1@aeriscom-ampsol-rci-201904.iam.gserviceaccount.com"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampsol-rci"
project_folder_id="806199614653"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ampsol-rci-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-rci-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-rci-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-rci-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-rci-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-rci-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampsol-rci-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampsol-rci-dss-us-west1 = []
  subnet-ampsol-rci-dmz-us-west1 = []
  subnet-ampsol-rci-bss-us-west1 = []
  subnet-ampsol-rci-sec-us-west1 = []
  subnet-ampsol-rci-imz-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "terraform@aeriscom-sol-tools-201903.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-sol-tools-201903"
sa_devops_email = "solutions-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ampsol-rci-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth", "test"]
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_dap="true"
    vm_type_services_auth="true"
    vm_type_services_test="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
}


### waf vars ###

waf_purpose="waf"
waf_template="vm-ampsol-rci-waf-us-west1"
waf_bucket="csb-ampsol-rci-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-rci-dmz01-us-west1"
waf_image="aeriscomm-hostproj-aer-201903/aeris-ubuntu-latest"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-rci-waf-us-west1"
waf_loadbalancer="hlb-ampsol-rci-waf-us-west1"
waf_healthcheck="hlb-ampsol-rci-waf-us-west1"
waf_certificate="projects/aeriscom-ampsol-rci-201904/global/sslCertificates/ampsol-rci"
waf_config_path="config/ampsol/ampsol-rci"
waf_min_instances="1"
waf_max_instances="2"
waf_managed_zone="ampsol-rci"
waf_endpoint="waf-ampsol-rci.aerjupiter.com"
waf_create_dns="false"

### CloudSQL vars ###
ampsol-rci_cloudsql_host="csql-ampsol-rci-2-us-west1"
ampsol-rci_region="us-west1"
ampsol-rci_cloudsql_disk_size="10"
ampsol-rci_cloudsql_tier="db-n1-standard-1"
ampsol-rci_db_replica="true"
ampsol-rci_zone="us-west1-a"

##cloudera vars ##
cloudera_purpose="cloudera"
cloudera_subnetwork="subnet-ampsol-rci-dss-us-west1"
cloudera_subnetwork_project="aeriscom-ampsol-rci-201904"
cloudera_image="aeriscom-sol-tools-201903/ubuntu14-jdk8-5112"
cloudera_service_account="iam-ampsol-rci-us-west1@aeriscom-ampsol-rci-201904.iam.gserviceaccount.com"
cloudera_node_settings={
  node1={
    name="dpp-cdhmaster"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","private-cloudera","dss","private-data"]
    role="cm-manager,master"
    hostname="dpp-cdhmaster.aeris-project.net"
    zone="us-west1-a"
    additional_dns_mapping=""
  }
  node2={
    name="dpp-cdhslave01"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave01.aeris-project.net"
    zone="us-west1-b"
    additional_dns_mapping="dpp-kafka01.aeris-project.net,dpp-zk01.aeris-project.net"
  }
  node3={
    name="dpp-cdhslave02"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave02.aeris-project.net"
    zone="us-west1-c"
    additional_dns_mapping="dpp-kafka02.aeris-project.net,dpp-zk02.aeris-project.net"
  }
   node4={
    name="dpp-cdhslave03"
    machine_type="n1-standard-4"
    disk_size_root="80"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave03.aeris-project.net"
    zone="us-west1-a"
    additional_dns_mapping="dpp-kafka03.aeris-project.net,dpp-zk03.aeris-project.net"
  }
}
