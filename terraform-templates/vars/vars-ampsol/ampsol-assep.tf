## common vars ##
bucket="csb-ampsol-prod-tfstate-asia-southeast1-1"
project="aeriscom-ampsol-asiase-201907"
product_name="ampsol-assep"
environment="prod"
region="asia-southeast1"
network="vpc-ampsol-prod-asia-southeast1"
service_account="iam-ampsol-prod-asia-se1@aeriscom-ampsol-asiase-201907.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##
purpose="gkecluster"
additional_zones=["asia-southeast1-a","asia-southeast1-b"]
subnetwork="subnet-ampsol-prod-k8s-asia-southeast1"
add_secondary_ranges="false"
cluster_secondary_range_name="na"
services_secondary_range_name="na"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="app"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_dap="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
  jdap={
    name="node-pool-dap"
    count=4
    machine_type="n1-standard-4"
    vm_type_services_dap="true"
  }
}

###### SQL Vars ######
csql_instances_ids="ampsol-assep-umslds-db,ampsol-assep-keycloak-db,ampsol-assep-kong-db"

ampsol-assep-umslds-db_cloudsql_host="db-ampsol-assep-umslds"
ampsol-assep-umslds-db_cloudsql_tier="db-n1-standard-2"
ampsol-assep-umslds-db_cloudsql_disk_size="10"
ampsol-assep-umslds-db_zone="asia-southeast1-a"
ampsol-assep-umslds-db_db_replica="true"
ampsol-assep-umslds-db_region="asia-southeast1"

ampsol-assep-keycloak-db_cloudsql_host="db-ampsol-assep-keycloak"
ampsol-assep-keycloak-db_cloudsql_tier="db-n1-standard-2"
ampsol-assep-keycloak-db_cloudsql_disk_size="10"
ampsol-assep-keycloak-db_zone="asia-southeast1-a"
ampsol-assep-keycloak-db_db_replica="true"
ampsol-assep-keycloak-db_region="asia-southeast1"

ampsol-assep-kong-db_cloudsql_host="psql-ampsol-assp-asia-southeast1-a"
ampsol-assep-kong-db_cloudsql_tier="db-custom-1-3840"
ampsol-assep-kong-db_cloudsql_disk_size="10"
ampsol-assep-kong-db_zone="asia-southeast1-a"
ampsol-assep-kong-db_db_replica="true"
ampsol-assep-kong-db_region="asia-southeast1"
ampsol-assep-kong-db_db_type="postgres"

##cloudera vars ##
cloudera_purpose="cloudera"
cloudera_subnetwork="subnet-ampsol-prod-dss-asia-southeast1"
cloudera_subnetwork_project="aeriscom-ampsol-asiase-201907"
cloudera_image="aeriscom-sol-tools-201903/ubuntu14-jdk8-5112-ulimit64k"
cloudera_service_account="iam-ampsol-prod-asia-se1@aeriscom-ampsol-asiase-201907.iam.gserviceaccount.com"
cloudera_node_settings={
node1={
    name="dpp-cdhmaster"
    machine_type="n1-standard-4"
    disk_size_root="200"
    disk_size="200"
    disk_size_type="pd-standard"
    tags=["hadoop","private-cloudera","dss","private-data"]
    role="cm-manager,master"
    hostname="dpp-cdhmaster.aeris-project.net"
    zone="asia-southeast1-a"
    additional_dns_mapping=""
  }
  node2={
    name="dpp-cdhslave01"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave01.aeris-project.net"
    zone="asia-southeast1-b"
    additional_dns_mapping="dpp-kafka01.aeris-project.net,dpp-zk01.aeris-project.net"
  }
  node3={
    name="dpp-cdhslave02"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave02.aeris-project.net"
    zone="asia-southeast1-c"
    additional_dns_mapping="dpp-kafka02.aeris-project.net,dpp-zk02.aeris-project.net"
  }
   node4={
    name="dpp-cdhslave03"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave03.aeris-project.net"
    zone="asia-southeast1-a"
    additional_dns_mapping="dpp-kafka03.aeris-project.net,dpp-zk03.aeris-project.net"
  }
  node5={
    name="dpp-cdhslave04"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1000"
    disk_size_type="pd-standard"
    tags=["hadoop","dss","private-data"]
    role="slave"
    hostname="dpp-cdhslave04.aeris-project.net"
    zone="asia-southeast1-b"
    additional_dns_mapping=""
  }
}

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ampsol-assep-waf-asia-southeast1"
waf_bucket="csb-ampsol-assep-waf-asia-southeast1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-prod-dmz-asia-southeast1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-assep-waf-asia-southeast1"
waf_loadbalancer="hlb-ampsol-assep-waf-asia-southeast1"
waf_healthcheck="hlb-ampsol-assep-waf-asia-southeast1"
waf_certificate=["projects/aeriscom-ampsol-asiase-201907/global/sslCertificates/crt-aerjupiter-com","projects/aeriscom-ampsol-asiase-201907/global/sslCertificates/aercloud-aeris-com","projects/aeriscom-ampsol-asiase-201907/global/sslCertificates/aeris-com","projects/aeriscom-ampsol-asiase-201907/global/sslCertificates/aeriscloud-com","projects/aeriscom-ampsol-asiase-201907/global/sslCertificates/fleetech"]
waf_config_path="config/ampsol/ampsol-assep"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"

