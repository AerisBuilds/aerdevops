## common vars ##
bucket="csb-ampsol-dci-tfstate-us-west1"
project="aeriscom-ampsol-dci-201905"
product_name="ampsol"
environment="dci"
region="us-west1"
zone="us-west1-a"
network="vpc-ampsol-dci-us-west1"
service_account="iam-ampsol-dci-us-west1@aeriscom-ampsol-dci-201905.iam.gserviceaccount.com"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampsol-dci"
project_folder_id="1031638290754"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ampsol-dci-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-dci-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-dci-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-dci-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-dci-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampsol-dci-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampsol-dci-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampsol-dci-dss-us-west1 = []
  subnet-ampsol-dci-dmz-us-west1 = []
  subnet-ampsol-dci-bss-us-west1 = []
  subnet-ampsol-dci-sec-us-west1 = []
  subnet-ampsol-dci-imz-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer"]
sa_tools = "terraform@aeriscom-sol-tools-201903.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-sol-tools-201903"
sa_devops_email = "solutions-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]


## gkecluster vars ##
purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="subnet-ampsol-dci-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
//docker_server="repo.aeriscloud.com"
//docker_email="software@aeris.net"
//registry_name="dockerrepokey"
//registry_namespace="app"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_dap="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_infra_ibus="true"
    vm_type_infra_cache="true"
  }
}

### vault vars ###

vault_purpose="sec"
machine_type="n1-standard-2"
vault_subnetwork="subnet-ampsol-dci-sec01-us-west1"
image="aeriscom-sol-tools-201903/aeris-ubuntu-20190313"
disk_size="10"
docker_repo="http://repo.aeriscloud.com:6666"
docker_image="repo.aeriscloud.com:6666/vault:1.0.2-7"
key_ring="kms-ampsol-dci01-us-west1" # TODO: Change name
crypto_key="key-ampsol-dci01-us-west1" # TODO: Change name
cloudsql_host="csql-ampsol-dci01-us-west1-ok1"
cloudsql_tier="db-n1-standard-1"
vault_db_name="csql-ampsol-dci01-vault"
vault_db_user="dci-vault"
cloudsql_disk_size="10"
service_account_vault="iam-ampsol-dci-us-west1-vault"

### waf vars ###

waf_purpose="waf"
waf_template="vm-ampsol-dci-waf-us-west1"
waf_bucket="csb-ampsol-dci-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-dci-dmz01-us-west1"
waf_image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190611"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-dci-waf-us-west1"
waf_loadbalancer="hlb-ampsol-dci-waf-us-west1"
waf_healthcheck="hlb-ampsol-dci-waf-us-west1"
waf_certificate="projects/aeriscom-ampsol-dci-201905/global/sslCertificates/crt-ampsol-dci-20190503"
waf_config_path="config/ampsol/ampsol-dci"
waf_min_instances="1"
waf_max_instances="2"
waf_managed_zone="ampsol-dci"
waf_endpoint="waf.ampsol-dci.aerjupiter.com"
waf_create_dns="false"

###cloudera cluster vars ###

cloudera_purpose="cloudera"
cloudera_subnetwork="subnet-ampsol-dci-dss01-us-west1"
cloudera_subnetwork_project="aeriscom-ampsol-dci-201905"
cloudera_image="aeriscom-sol-tools-201903/ubuntu14-jdk8-5112"
cloudera_service_account="iam-ampsol-dci-us-west1@aeriscom-ampsol-dci-201905.iam.gserviceaccount.com"
cloudera_node_settings={
  node1={
    name="cdh-dci-managernode"
    machine_type="n1-standard-4"
    disk_size_root="200"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["hadoop","private-cloudera"]
    role="cm-manager"
    hostname="dci-dpp-cdhmanager.internal.aeris.com"
  }
  node2={
    name="cdh-dci-masternode1"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="master"
    hostname="dci-dpp-cdhmaster01.internal.aeris.com"
  }
  node3={
    name="cdh-dci-masternode2"
    machine_type="n1-standard-8"
    disk_size_root="200"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhmaster02.internal.aeris.com"
  }
  node4={
    name="cdh-dci-slave-node1"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave01.internal.aeris.com"
  }
  node5={
    name="cdh-dci-slave-node2"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave02.internal.aeris.com"
  }
   node6={
    name="cdh-dci-slave-node3"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave03.internal.aeris.com"
  }
   node7={
    name="cdh-dci-slave-node4"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave04.internal.aeris.com"
  }
   node8={
    name="cdh-dci-slave-node5"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave05.internal.aeris.com"
  }
   node9={
    name="cdh-dci-slave-node6"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave06.internal.aeris.com"
  }
   node10={
    name="cdh-dci-slave-node7"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave07.internal.aeris.com"
  }
  node11={
    name="cdh-dci-slave-node8"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave08.internal.aeris.com"
  }
  node12={
    name="cdh-dci-slave-node9"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1024"
    disk_size_type="pd-standard"
    tags=["hadoop"]
    role="slave"
    hostname="dci-dpp-cdhslave09.internal.aeris.com"
  }
}
