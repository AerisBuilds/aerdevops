## common vars ##
bucket="csb-ampsol-prod-tfstate-asia-south1"
project="aeriscom-ampsol-asia-201904"
product_name="ampsol-asia"
environment="prod"
region="asia-south1"
logstash_endpoint="logstash:5044"
network="vpc-ampsol-prod-asia-south1"
service_account="iam-ampsol-prod-asia-south1@aeriscom-ampsol-asia-201904.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain_name="ampsol-assp.aerjupiter.com"
ssl_cert_name="cert-ampsol-assp.aerjupiter.com"

## gkecluster vars ##

gkecluster_purpose="gkecluster"
additional_zones=["asia-south1-b"]
gkecluster_subnetwork="subnet-ampsol-prod-k8s01-asia-south1"
namespaces=["infra", "mon", "dp", "spc", "app", "dap", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="app"
node_pool_settings= {
  app = {
    name = "node-pool-app"
    count = 1
    machine_type = "n1-standard-16"
    vm_type_services_app = "true"
    vm_type_services_dp = "true"
    vm_type_services_spc = "true"
    vm_type_services_dap = "true"
    vm_type_services_auth = "true"
  }
  infra = {
    name = "node-pool-infra"
    count = 1
    machine_type = "n1-standard-8"
    vm_type_services_mon = "true"
    vm_type_infra_ibus = "true"
    vm_type_infra_cache = "true"
  }
}

https_lb_settings={
}

### waf vars ###
docker_repo="http://repo.aeriscloud.com:6666"
zone="a"

waf_purpose="waf"
waf_template="vm-ampsol-asia-prod-waf-asia-south1"
waf_bucket="csb-ampsol-asia-prod-waf-asia-south1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampsol-prod-dmz01-asia-south1"
waf_image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190419"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampsol-asia-prod-waf-asia-south1"
waf_loadbalancer="hlb-ampsol-asia-prod-waf-asia-south1"
waf_healthcheck="hlb-ampsol-asia-prod-waf-asia-south1"
waf_certificate="projects/aeriscom-ampsol-asia-201904/global/sslCertificates/aeris"
waf_endpoint="hlb-waf-webapp-assp.aeris.com"
waf_upstream_endpoint="hlb-webapp-assp.aeris.com"
waf_config_path="config/ampsol/ampsol-asia-south"
waf_min_instances="1"
waf_max_instances="2"
waf_managed_zone="ampsol-assp"
waf_endpoint="null"
dns_create_record="false"