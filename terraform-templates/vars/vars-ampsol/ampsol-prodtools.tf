## common vars ##
tf_bucket_bucket="csb-ampsol-prodtools-tfstate-asia-south1"
project="aeriscom-sol-prodtools-201904"
productname="ampsol-prodtools"
environment="tools"
machine_type="n1-standard-2"
region="asia-south1"
zone="asia-south1-a"
image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190419"
network="vpc-ampsol-prodtools-asia-south1"
subnetwork="subnet-ampsol-prodtools-infra-asia-south1"
docker_repo="http://repo.aeriscloud.com:6666"
gcp_project="aeriscom-sol-prodtools-201904"
service_account="iam-sol-prodtools-asia-south1@aeriscom-sol-prodtools-201904.iam.gserviceaccount.com"


## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain_name="ampsol-prodtools.aerjupiter.com"
ssl_cert_name="crt-ampsol-prodtools-20190329"


## jenkins master vars ##

jenkinsm_purpose="jenkins-master"
jenkinsm_disk_size="40"
jenkinsm_docker_image="repo.aeriscloud.com:6666/jenkins-master:006"
jenkins_version="2.169"
port="8080"
ssl_cert_name="crt-ampsol-prodtools-20190329"
domain="ampsol-prodtools.aerjupiter.com"
subdomain="deploy"

## jenkins slave vars ##

jenkinss_purpose="jenkins-slave"
jenkinss_disk_size="100"
jenkinss_docker_image="repo.aeriscloud.com:6666/jenkins-slave-gcp:001"
