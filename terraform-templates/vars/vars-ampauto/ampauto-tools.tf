## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
ssl_cert_name="ampautomotive-20190610"

## common vars ##
productname="ampauto"
environment="tools"
region="us-west1"
zone="us-west1-a"
image="aeriscomm-hostproj-aer-201903/aeris-bootstrap-latest"
network="vpc-automotive-tools-us-west1"
subnetwork="subnet-ampautomotive-tools-infra-us-west1"
docker_repo="http://repo.aeriscloud.com:6666"
tf_bucket="csb-ampauto-tools-tfstate-us-west1"
service_account="terraform@aeriscom-auto-tools-201905.iam.gserviceaccount.com"
domain="ampautomotive.aerjupiter.com"
project="aeriscom-auto-tools-201905"
dns_zone_name="ampautomotive"

## sonarqube vars ##
sonarqube_purpose="sonarqube"
sonarqube_machine_type="n1-standard-2"
sonarqube_disk_size="100"
sonarqube_docker_image="repo.aeriscloud.com:6666/sonarqube:001"
sonarqube_port="9000"
sonarqube_subdomain="codequality"
sonarqube_cloudsql_host="csql1-ampautomotive-tools-us-west1"
sonarqube_cloudsql_tier="db-n1-standard-1"
sonarqube_db_user="sonar_db_user"
sonarqube_db_name="sonar"
sonarqube_cloudsql_disk_size="10"

## jenkins master vars ###

jenkins_master_purpose="jenkins-master"
jenkins_master_machine_type="n1-standard-2"
jenkins_master_disk_size="40"
jenkins_master_docker_image="repo.aeriscloud.com:6666/jenkins-master:006"
jenkins_master_port="8080"
jenkins_master_subdomain="build"


## jenkins slave vars ###

jenkins_slaves = [
  {
    slave_name           = "jenkins-slave"
    machine_type         = "n1-standard-2"
    disk_size            = "200"
    docker_image         = "gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-slave:latest"
    jenkins_master_url   = "http://172.30.113.12:8080"
    jenkins_master_token = "857286933191aba8da4fc256c4f0f968b32009680bd6d6d8153b7363f2917033"
    jenkins_slave_name   = "jenkins_slave"
  },
  {
    slave_name           = "jenkins-slave2"
    machine_type         = "n1-standard-2"
    disk_size            = "200"
    docker_image         = "gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-slave:latest"
    jenkins_master_url   = "http://172.30.113.12:8080"
    jenkins_master_token = "89aa8956b3dcecf8df7b23d49a097688cefc2e6b4dd7405bb50cf61145fd31f2"
    jenkins_slave_name   = "jenkins_slave2"
  },
]