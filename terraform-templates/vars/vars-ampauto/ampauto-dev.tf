## common vars ##
bucket="csb-ampauto-dev-tfstate-us-west1"
project="aeriscom-ampauto-dev-201908"
product_name="ampauto"
environment="dev"
region="us-west1"
network="vpc-ampauto-dev-us-west1"
service_account="iam-ampauto-dev-gke-us-west1@aeriscom-ampauto-dev-201908.iam.gserviceaccount.com"

## certificate vars ##
ssl_cert_name="crt-aerjupiter-com"


## project vars ##
project_name="ampauto-dev"
project_folder_id="194325965410"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ampauto-dev-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-dev-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-dev-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-dev-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-dev-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-dev-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampauto-dev-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampauto-dev-dss-us-west1 = []
  subnet-ampauto-dev-dmz-us-west1 = []
  subnet-ampauto-dev-bss-us-west1 = []
  subnet-ampauto-dev-sec-us-west1 = []
  subnet-ampauto-dev-imz-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer"]
sa_tools = "terraform@aeriscom-auto-tools-201905.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-auto-tools-201905"
sa_devops_email = "automotive-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

## gkecluster vars ##
purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="subnet-ampauto-dev-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "test", "auth"]
 
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_app="true"
    vm_type_services_dp="true"
    vm_type_services_spc="true"
    vm_type_services_auth="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_infra="true"
  }
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
  }
  test={
    name="node-pool-test"
    count=1
    machine_type="n1-standard-1"
    vm_type_services_test="true"
  }
}

###### SQL Vars ######
ampauto-dev-vsb-db_cloudsql_host="db-ampauto-dev-vsb"
ampauto-dev-vsb-db_cloudsql_tier="db-n1-standard-2"
ampauto-dev-vsb-db_cloudsql_disk_size="100"
ampauto-dev-vsb-db_zone="us-west1-a"
ampauto-dev-vsb-db_db_replica="false"
ampauto-dev-vsb-db_region="us-west1"

ampauto-dev-kong-db_db_type="postgres"
ampauto-dev-kong-db_cloudsql_host="db-ampauto-dev-kong"
ampauto-dev-kong-db_cloudsql_tier="db-f1-micro"
ampauto-dev-kong-db_cloudsql_disk_size="10"
ampauto-dev-kong-db_zone="us-west1-a"
ampauto-dev-kong-db_db_replica="false"
ampauto-dev-kong-db_region="us-west1"
