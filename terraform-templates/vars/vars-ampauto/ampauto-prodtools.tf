## common vars ##
product_name="ampauto"
environment="prodtools"
region="us-west1"
zone="us-west1-a"
image="aeriscomm-hostproj-aer-201903/aeris-bootstrap-latest"
network="vpc-ampauto-prodtools-us-west1"
subnetwork="subnet-ampauto-prodtools-infra-us-west1"
docker_repo="http://repo.aeriscloud.com:6666"
bucket="csb-ampauto-prodtools-tfstate-us-west1"
service_account="iam-ampauto-prodtools-us-west1@aeriscom-auto-prodtools-201909.iam.gserviceaccount.com"
service_account_id="iam-ampauto-prodtools-us-west1"
domain="aerjupiter.com"
project="aeriscom-auto-prodtools-201909"
dns_zone_name="ampautomotive"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampauto-prodtools"
project_folder_id="894408676283"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-aer-201903"

subnets = [
  {
    subnet_name           = "subnet-ampauto-prodtools-infra-us-west1"
    subnet_ip             = "172.31.116.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampauto-prodtools-infra-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "storage.admin", "iam.serviceAccountUser"]
sa_tools = "terraform@aeriscom-auto-prodtools-201909.iam.gserviceaccount.com"
sa_tools_roles = []
sa_tools_project = "aeriscom-auto-prodtools-201909"
sa_devops_email = "automotive-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []


## jenkins master vars ###

jenkins_master_purpose="jenkins-master"
jenkins_master_machine_type="n1-standard-2"
jenkins_master_disk_size="40"
jenkins_master_docker_image="gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-master:latest"
jenkins_master_port="8080"

## jenkins slave vars ###
jenkins_slave_purpose="jenkins-slave"
jenkins_slave_machine_type="n1-standard-2"
jenkins_slave_disk_size="200"
jenkins_slave_docker_image="gcr.io/aeriscomm-hostproj-aer-201903/aeris-jenkins-slave:latest"
