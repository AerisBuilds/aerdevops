## common vars ##
bucket="csb-ampauto-pace-tfstate-us-west1"
project="aeriscom-ampauto-pace-201909"
product_name="ampauto"
environment="pace"
region="us-west1"
zone="us-west1-a"
network="vpc-ampauto-pace-us-west1"
service_account="iam-ampauto-pace-gke-us-west1@aeriscom-ampauto-pace-201909.iam.gserviceaccount.com"
service_account_id="iam-ampauto-pace-gke-us-west1"

## certificate vars ##

ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampauto-pace"
project_folder_id="995166630061"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ampauto-pace-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pace-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampauto-pace-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampauto-pace-dss-us-west1 = []
  subnet-ampauto-pace-dmz-us-west1 = []
  subnet-ampauto-pace-bss-us-west1 = []
  subnet-ampauto-pace-sec-us-west1 = []
  subnet-ampauto-pace-imz-us-west1 = []
  subnet-ampauto-pace-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "terraform@aeriscom-auto-tools-201905.iam.gserviceaccount.com"
sa_tools_project = "aeriscom-auto-tools-201905"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin","cloudbuild.builds.builder","iam.serviceAccountAdmin","iam.serviceAccountUser","viewer","compute.viewer","compute.admin","compute.instanceAdmin","compute.instanceAdmin.v1","compute.networkAdmin"]
sa_devops_email = "automotive-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ampauto-pace-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "test", "auth", "spr", "dpbase"]
node_pool_settings={
  nde1={
    name="node-pool-nde1"
    count=3
    machine_type="n1-standard-16"
  }
  nde2={
    name="node-pool-nde2"
    count=1
    machine_type="n1-standard-4"
  } 
}

node_pool_tags=["node-pool-nde1:vm.type.services/test=true;vm.type.services/mon=true;vm.type.services/infra=true;vm.type.services/dp=true;vm.type.services/spc=true;vm.type.services/spr=true;vm.type.services/auth=true;vm.type.services/app=true","node-pool-nde2:vm.type.services/dpbase=true"]

###### SQL Vars ######
ampauto-pace-msqlspc_db_type="mysql"
ampauto-pace-msqlspc_cloudsql_host="msql-ampauto-pace-sp"
ampauto-pace-msqlspc_cloudsql_tier="db-n1-standard-2"
ampauto-pace-msqlspc_cloudsql_disk_size="100"
ampauto-pace-msqlspc_zone="us-west1-a"
ampauto-pace-msqlspc_db_replica="true"
ampauto-pace-msqlspc_region="us-west1"

ampauto-pace-msqlcmn_db_type="mysql"
ampauto-pace-msqlcmn_cloudsql_host="msql-ampauto-pace-cmmn"
ampauto-pace-msqlcmn_cloudsql_tier="db-n1-standard-2"
ampauto-pace-msqlcmn_cloudsql_disk_size="100"
ampauto-pace-msqlcmn_zone="us-west1-a"
ampauto-pace-msqlcmn_db_replica="true"
ampauto-pace-msqlcmn_region="us-west1"


ampauto-pace-psql_db_type="postgres"
ampauto-pace-psql_cloudsql_host="psql-ampauto-pace-kong"
ampauto-pace-psql_cloudsql_tier="db-f1-micro"
ampauto-pace-psql_cloudsql_disk_size="10"
ampauto-pace-psql_zone="us-west1-a"
ampauto-pace-psql_db_replica="true"
ampauto-pace-psql_region="us-west1"
