## common vars ##
bucket="csb-ampauto-pre-tfstate-us-west1"
project="aeriscom-ampauto-pre-201909"
product_name="ampauto"
environment="pre"
region="us-west1"
zone="us-west1-a"
network="vpc-ampauto-pre-us-west1"
service_account="iam-ampauto-pre-gke-us-west1@aeriscom-ampauto-pre-201909.iam.gserviceaccount.com"
service_account_id="iam-ampauto-pre-gke-us-west1"

## certificate vars ##

ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampauto-pre"
project_folder_id="931015214975"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-aer-201903"

subnets = [
  {
    subnet_name           = "subnet-ampauto-pre-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-pre-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampauto-pre-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampauto-pre-dss-us-west1 = []
  subnet-ampauto-pre-dmz-us-west1 = []
  subnet-ampauto-pre-bss-us-west1 = []
  subnet-ampauto-pre-sec-us-west1 = []
  subnet-ampauto-pre-imz-us-west1 = []
  subnet-ampauto-pre-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["dns.admin","cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "iam-ampauto-prodtools-us-west1@aeriscom-auto-prodtools-201909.iam.gserviceaccount.com"
sa_tools_project = "aeriscom-auto-prodtools-201905"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin","cloudbuild.builds.builder","iam.serviceAccountAdmin","iam.serviceAccountUser","viewer","compute.viewer","compute.admin","compute.instanceAdmin","compute.instanceAdmin.v1","compute.networkAdmin"]
sa_devops_email = "automotive-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ampauto-pre-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["infra", "mon", "dp", "spc", "app", "test", "auth", "spr", "dpbase"]
node_pool_settings={
  nde1={
    name="node-pool-nde1"
    count=3
    machine_type="n1-standard-16"
  }
  nde2={
    name="node-pool-nde2"
    count=1
    machine_type="n1-standard-4"
  }
}

node_pool_tags=["node-pool-nde1:vm.type.services/test=true;vm.type.services/mon=true;vm.type.services/infra=true;vm.type.services/dp=true;vm.type.services/spc=true;vm.type.services/spr=true;vm.type.services/auth=true;vm.type.services/app=true","node-pool-nde2:vm.type.services/dpbase=true"]

###### SQL Vars ######
ampauto-pre-psql_db_type="postgres"
ampauto-pre-psql_cloudsql_host="psql1-ampauto-pre-kong-db"
ampauto-pre-psql_cloudsql_tier="db-f1-micro"
ampauto-pre-psql_cloudsql_disk_size="10"
ampauto-pre-psql_db_replica="true"
ampauto-pre-psql_region="us-west1"
ampauto-pre-psql_db_version="POSTGRES_9_6"
ampauto-pre-psql_master_zone="us-west1-a"
ampauto-pre-psql_replica_zone="us-west1-b"

## WAF VARS ##

waf_purpose="waf"
waf_template="vm-ampauto-us-pre-waf-us-west1"
waf_bucket="csb-ampauto-pre-waf-us-west1"
waf_ssl_bucket="csb-ampauto-pre-wafssl-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ampauto-pre-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_healthcheck="hlb-amaputo-us-pre-waf-us-west1"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ampauto-us-pre-waf-us-west1"
waf_config_path="config/ampauto/ampauto-pre"
waf_min_instances="2"
waf_max_instances="5"
