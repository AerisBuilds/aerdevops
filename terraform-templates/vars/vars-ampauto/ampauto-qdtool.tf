## common vars ##
bucket="csb-ampauto-qdtool-tfstate-us-west1-a"
project="aeriscom-ampauto-qdtool-201908"
product_name="ampauto"
environment="qdtool"
region="us-west1"
zone="us-west1-a"
network="vpc-ampauto-qdtool-us-west1"
service_account="iam-ampauto-qdtool-us-west1@aeriscom-ampauto-qdtool-201908.iam.gserviceaccount.com"
service_account_id="iam-ampauto-qdtool-us-west1"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="ampauto-qdtool"
project_folder_id="140799695791"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ampauto-qdtool-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ampauto-qdtool-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ampauto-qdtool-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ampauto-qdtool-dss-us-west1 = []
  subnet-ampauto-qdtool-dmz-us-west1 = []
  subnet-ampauto-qdtool-bss-us-west1 = []
  subnet-ampauto-qdtool-sec-us-west1 = []
  subnet-ampauto-qdtool-imz-us-west1 = []
  subnet-ampauto-qdtool-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "terraform@aeriscom-auto-tools-201905.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-auto-tools-201905"
sa_devops_email = "automotive-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []

