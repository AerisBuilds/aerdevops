## common vars ##
bucket="csb-ampauto-dci-tfstate-us-west1"
project="aeriscom-ampauto-dci-201905"
product_name="ampauto"
environment="dci"
region="us-west1"
logstash_endpoint="logstash:5044"
network="vpc-ampauto-dci-cap-us-west1"
service_account="iam-ampauto-dci-gke-us-west1@aeriscom-ampauto-dci-201905.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain="ampauto-dci.aerjupiter.com"
ssl_cert_name="crt-ampauto-dci-20190607"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="subnet-ampauto-dci-k8s01-us-west1"
add_secondary_ranges="false"
cluster_secondary_range_name=""
services_secondary_range_name=""
namespaces=["infra", "mon", "dp", "spc", "app", "test", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="app"
node_pool_settings={
  node1={
    name="node-pool-mon"
    count=1
    node_pool_id="1"
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
  }
  node2={
    name="node-pool-infra"
    count=1
    node_pool_id="2"
    machine_type="n1-standard-4"
    vm_type_infra_ibus="true"
    vm_type_infra_stream-ibus="true"
    vm_type_infra_otabus="true"
    vm_type_infra_cache="true"
    vm_type_infra_umredis="true"
  }
  node3={
    name="node-pool-app-auth"
    count=1
    node_pool_id="3"
    machine_type="n1-standard-2"
    vm_type_services_app="true"
    vm_type_services_auth="true"
  }
  node4={
    name="node-pool-dp"
    count=1
    node_pool_id="4"
    machine_type="n1-standard-8"
    vm_type_services_dp="true"
  }
  node5={
    name="node-pool-spc-test"
    count=1
    node_pool_id="5"
    machine_type="n1-standard-8"
    vm_type_services_spc="true"
    vm_type_services_test="true"
  }

}
###### SQL Vars ######
ampauto-dci_cloudsql_host="db-ampauto-dci-sql-us-west1-a"
ampauto-dci_cloudsql_tier="db-n1-standard-2"
ampauto-dci_cloudsql_disk_size="100"
ampauto-dci_zone="us-west1-a"
ampauto-dci_db_replica="true"
ampauto-dci_region="us-west1"

