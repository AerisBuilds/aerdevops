## common vars ##
bucket="csb-ap-prod-tfstate-us-west1"
project="aeriscom-ap-prod-201907"
product_name="acpap"
environment="prod"
region="us-west1"
network="vpc-ap-prod-us-west1"
docker_username="atspdeployer"
docker_password="AtspDeployer20"
service_account="iam-ap-prod-gke-us-west1@aeriscom-ap-prod-201907.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

################## SQL VARS ######################

ap-prod-fusekeycloak-sql_cloudsql_host="db-ap-prod-fuse-keycloak-us-west1-2"
ap-prod-fusekeycloak-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-fusekeycloak-sql_cloudsql_disk_size="10"
ap-prod-fusekeycloak-sql_zone="us-west1-a"
ap-prod-fusekeycloak-sql_db_replica="true"
ap-prod-fusekeycloak-sql_region="us-west1"

ap-prod-uscckeycloak-sql_cloudsql_host="db-ap-prod-uscc-keycloak-us-west1-2"
ap-prod-uscckeycloak-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-uscckeycloak-sql_cloudsql_disk_size="10"
ap-prod-uscckeycloak-sql_zone="us-west1-a"
ap-prod-uscckeycloak-sql_db_replica="true"
ap-prod-uscckeycloak-sql_region="us-west1"

ap-prod-fuseums-sql_cloudsql_host="db-ap-prod-fuse-user-management-us-west1-2"
ap-prod-fuseums-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-fuseums-sql_cloudsql_disk_size="10"
ap-prod-fuseums-sql_zone="us-west1-a"
ap-prod-fuseums-sql_db_replica="true"
ap-prod-fuseums-sql_region="us-west1"

ap-prod-usccums-sql_cloudsql_host="db-ap-prod-uscc-user-management-us-west1-2"
ap-prod-usccums-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-usccums-sql_cloudsql_disk_size="10"
ap-prod-usccums-sql_zone="us-west1-a"
ap-prod-usccums-sql_db_replica="true"
ap-prod-usccums-sql_region="us-west1"

ap-prod-staticip-sql_cloudsql_host="db-ap-prod-staticip-mgmt-us-west1-1"
ap-prod-staticip-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-staticip-sql_cloudsql_disk_size="10"
ap-prod-staticip-sql_zone="us-west1-a"
ap-prod-staticip-sql_db_replica="true"
ap-prod-staticip-sql_region="us-west1"

ap-prod-fuseliferay-sql_cloudsql_host="db-ap-prod-fuse-liferay-us-west1-b"
ap-prod-fuseliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-fuseliferay-sql_cloudsql_disk_size="10"
ap-prod-fuseliferay-sql_zone="us-west1-b"
ap-prod-fuseliferay-sql_db_replica="true"
ap-prod-fuseliferay-sql_db_version="MYSQL_5_6"
ap-prod-fuseliferay-sql_region="us-west1"

ap-prod-sprintliferay-sql_cloudsql_host="db-ap-prod-sprint-liferay-us-west1-b"
ap-prod-sprintliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-sprintliferay-sql_cloudsql_disk_size="10"
ap-prod-sprintliferay-sql_zone="us-west1-b"
ap-prod-sprintliferay-sql_db_replica="true"
ap-prod-sprintliferay-sql_region="us-west1"

ap-prod-usccliferay-sql_cloudsql_host="db-ap-prod-uscc-liferay-us-west1-b"
ap-prod-usccliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-usccliferay-sql_cloudsql_disk_size="10"
ap-prod-usccliferay-sql_zone="us-west1-b"
ap-prod-usccliferay-sql_db_replica="true"
ap-prod-usccliferay-sql_db_version="MYSQL_5_6"
ap-prod-usccliferay-sql_region="us-west1"

ap-prod-neoliferay-sql_cloudsql_host="db-ap-prod-neo-liferay-us-west1-b"
ap-prod-neoliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-neoliferay-sql_cloudsql_disk_size="10"
ap-prod-neoliferay-sql_zone="us-west1-b"
ap-prod-neoliferay-sql_db_replica="true"
ap-prod-neoliferay-sql_region="us-west1"

ap-prod-neoliferay56-sql_cloudsql_host="db-ap-prod-neo-liferay-us-west1-b-5-6"
ap-prod-neoliferay56-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-neoliferay56-sql_cloudsql_disk_size="10"
ap-prod-neoliferay56-sql_zone="us-west1-b"
ap-prod-neoliferay56-sql_db_replica="true"
ap-prod-neoliferay56-sql_db_version="MYSQL_5_6"
ap-prod-neoliferay56-sql_region="us-west1"

ap-prod-aerportutility-sql_cloudsql_host="db-ap-prod-aerport-utility-us-west1-b"
ap-prod-aerportutility-sql_cloudsql_tier="db-n1-standard-2"
ap-prod-aerportutility-sql_cloudsql_disk_size="20"
ap-prod-aerportutility-sql_zone="us-west1-b"
ap-prod-aerportutility-sql_db_replica="true"
ap-prod-aerportutility-sql_region="us-west1"

ap-prod-swvpn-psql_cloudsql_host="db-ap-prod-sw-vpn-us-west1-2"
ap-prod-swvpn-psql_cloudsql_tier="db-n1-standard-2"
ap-prod-swvpn-psql_cloudsql_disk_size="10"
ap-prod-swvpn-psql_zone="us-west1-a"
ap-prod-swvpn-psql_db_replica="false"
ap-prod-swvpn-psql_region="us-west1"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-prod-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "infra", "ap-app", "ap-reporting", "ap-partner","ap-services","ap-management", "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-64"
    vm_type_services_ap_app="true"
    vm_type_services_ap_services="true"
    vm_type_services_ap_partner="true"
    vm_type_services_ap_app_aeris="true"
    vm_type_services_ap_app_sprint="true"
    vm_type_services_ap_app_uscc="true"
    vm_type_services_ap_app_simui="true"
  }
  management={
    name="node-pool-management"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_ap_management="true"
    vm_type_services_ap_reporting="true"
  }

  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_ap_management="true"
    vm_type_services_ap_reporting="true"
  }

}

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ap-prod-waf-us-west1"
waf_bucket="csb-ap-prod-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ap-prod-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ap-prod-waf-us-west1"
waf_loadbalancer="hlb-ap-prod-waf-us-west1"
waf_healthcheck="hlb-ap-prod-waf-us-west1"
waf_certificate=[
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeris-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeris-net",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeriscloud-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/commandcenter-sprint-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/connecthq-uscellular-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/crt-aerisapis-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/crt-aerjupiter-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/m2mweb-net"
]
waf_config_path="config/ap/ap-prod"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"
waf_ruleset="aerport.rules"

# Memory Store
CLOUD_MEMORY_STORE_IDS="dpp,solutions"

UMSFUSE_CLOUD_MEMORY_STORE_NAME="umsfuse-redis"
UMSFUSE_CLOUD_MEMORY_STORE_SIZE="1"
UMSFUSE_CLOUD_MEMORY_STORE_REGION="us-west1"
UMSFUSE_CLOUD_MEMORY_STORE_ZONE="us-west1-a"
UMSFUSE_CLOUD_MEMORY_STORE_VERSION="redis_4_0"
UMSFUSE_CLOUD_MEMORY_STORE_NETWORK="vpc-ampsol-pace-us-west1"
