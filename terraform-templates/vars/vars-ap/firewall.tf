## firewall rules ##

allow_in = [
  {
    name           = "fwr-project-allow-in-highports-from-all-project"
    protocol       = "tcp"
    port           = ["1024-65535","80"]
    target_tags    = []
    source_ranges  = ["172.16.0.0/16","10.60.0.0/14","10.64.0.0/20"]
    source_tags    = []
    priority       = "1000"
  },
  {
    name           = "fwr-project-allow-in-icmp-from-all-project"
    protocol       = "icmp"
    port           = []
    target_tags    = []
    source_ranges  = ["172.16.0.0/16","10.60.0.0/14","10.64.0.0/20"]
    source_tags    = []
    priority       = "1000"
  },
  {
    name           = "fwr-project-allow-in-ssh-from-bss"
    protocol       = "tcp"
    port           = ["22"]
    target_tags    = []
    source_ranges  = []
    source_tags    = ["bss"]
    priority       = "1000"
  },
  {
    name           = "fwr-project-allow-in-dns-from-project"
    protocol       = "udp"
    port           = ["53"]
    target_tags    = ["dnsproxy"]
    source_ranges  = ["172.16.0.0/16","10.60.0.0/14","10.64.0.0/20"]
    source_tags    = []
    priority       = "1000"
  },
]
allow_out = []
deny_in = []
deny_out = []
