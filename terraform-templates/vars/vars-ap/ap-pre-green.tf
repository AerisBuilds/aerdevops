## common vars ##
bucket="csb-ap-pre-green-tfstate-us-west1"
project="aeriscom-ap-pre-201907"
product_name="acpap"
environment="pre-green"
region="us-west1"
network="vpc-ap-pre-us-west1"
service_account="iam-ap-pre-gke-us-west1@aeriscom-ap-pre-201907.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-pre-k8s-us-west1"
master_ipv4_cidr_block = "172.16.4.32/28"
add_secondary_ranges="true"
cluster_secondary_range_name="podsgreen"
services_secondary_range_name="servicesgreen"
namespaces=["test", "mon", "ap-services" ,"ap-app" , "ap-partner" , "ap-reporting" , "ap-management" , "infra" , "auth"]
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-64"
  }
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
  }
  management={
    name="node-pool-management"
    count=1
    machine_type="n1-standard-16"
  }

  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
  }
}
node_pool_tags=["node-pool-app:vm.type.services/ap_app=true;vm.type.services/ap_services=true;vm.type.services/ap_partner=true;vm.type.services/ap_app_aeris=true;vm.type.services/ap_app_sprint=true;vm.type.services/ap_app_uscc=true;vm.type.services/ap_app_simui=true","node-pool-mon:vm.type.services/mon=true;vm.type.services/test=true","node-pool-management:vm.type.services/ap_management=true;vm.type.services/ap_reporting=true","node-pool-infra:vm.type.services/ap_management=true;vm.type.services/ap_reporting=true"]
