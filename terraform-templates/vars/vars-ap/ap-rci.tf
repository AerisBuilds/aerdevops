## common vars ##
bucket="csb-ap-rci-tfstate-us-west1"
project="aeriscom-ap-rci-201908"
product_name="acpap"
environment="rci"
region="us-west1"
zone="us-west1-a"
network="vpc-ap-rci-us-west1"
service_account="iam-ap-rci-gke-us-west1@aeriscom-ap-rci-201908.iam.gserviceaccount.com"
service_account_id="iam-ap-rci-gke-us-west1"

## certificate vars ##

ssl_cert_name="crt-aerjupiter-com"

##################VM NGINX REVERSE PROXY BOX########################

vminfra_purpose="nginxreverseproxy"
vminfra_subnetwork="projects/aeriscom-ap-rci-201908/regions/us-west1/subnetworks/subnet-ap-rci-dss-us-west1"
vminfra_subnetwork_project="aeriscom-ap-rci-201908"
vminfra_image="aeriscom-acp-tools-201905/ubuntu-1804-lts"
vminfra_service_account="iam-ap-rci-gke-us-west1@aeriscom-ap-rci-201908.iam.gserviceaccount.com"
vminfra_node_settings={
node1={
    name="ap-rci-ngxreverseproxy-us-west1-a"
    machine_type="n1-standard-8"
    disk_size_root="20"
    disk_size="1"
    disk_size_type="pd-standard"
	tags=["dss","bss"]
    role="node"
    hostname="ap-rci-ngxreverseproxy-us-west1-a.internal.aeris.com"
	zone="us-west1-a"

################## SQL VARS ######################

ap-rci-fuseliferay-sql_cloudsql_host="db-ap-rci-fuse-liferray-us-west1-2"
ap-rci-fuseliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-rci-fuseliferay-sql_cloudsql_disk_size="10"
ap-rci-fuseliferay-sql_zone="us-west1-a"
ap-rci-fuseliferay-sql_db_replica="false"
ap-rci-fuseliferay-sql_region="us-west1"

ap-rci-sprintliferay-sql_cloudsql_host="db-ap-rci-sprint-liferray-us-west1-2"
ap-rci-sprintliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-rci-sprintliferay-sql_cloudsql_disk_size="10"
ap-rci-sprintliferay-sql_zone="us-west1-a"
ap-rci-sprintliferay-sql_db_replica="false"
ap-rci-sprintliferay-sql_region="us-west1"

ap-rci-usccliferay-sql_cloudsql_host="db-ap-rci-uscc-liferray-us-west1-2"
ap-rci-usccliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-rci-usccliferay-sql_cloudsql_disk_size="10"
ap-rci-usccliferay-sql_zone="us-west1-a"
ap-rci-usccliferay-sql_db_replica="false"
ap-rci-usccliferay-sql_region="us-west1"

ap-rci-sim-management-sql_cloudsql_host="db-ap-rci-sim-management-us-west1-2"
ap-rci-sim-management-sql_cloudsql_tier="db-n1-standard-2"
ap-rci-sim-management-sql_cloudsql_disk_size="10"
ap-rci-sim-management-sql_zone="us-west1-a"
ap-rci-sim-management-sql_db_replica="false"
ap-rci-sim-management-sql_region="us-west1"

ap-rci-psql_db_type="postgres"
ap-rci-psql_cloudsql_host="db-ap-rci-kong-us-west1-2"
ap-rci-psql_cloudsql_tier="db-n1-standard-2"
ap-rci-psql_cloudsql_disk_size="20"
ap-rci-psql_zone="us-west1-a"
ap-rci-psql_db_replica="false"
ap-rci-psql_region="us-west1"

ap-rci-swvpn-sql_cloudsql_host="db-ap-rci-sw-vpn-us-west1-2"
ap-rci-swvpn-sql_cloudsql_tier="db-n1-standard-2"
ap-rci-swvpn-sql_cloudsql_disk_size="10"
ap-rci-swvpn-sql_zone="us-west1-a"
ap-rci-swvpn-sql_db_replica="false"
ap-rci-swvpn-sql_region="us-west1"

## project vars ##
project_name="ap-rci"
project_folder_id="500348824782"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-ap-rci-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-ap-rci-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-ap-rci-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-ap-rci-dss-us-west1 = []
  subnet-ap-rci-dmz-us-west1 = []
  subnet-ap-rci-bss-us-west1 = []
  subnet-ap-rci-sec-us-west1 = []
  subnet-ap-rci-imz-us-west1 = []
  subnet-ap-rci-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "terraform-seracc@aeriscom-acp-tools-201905.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin","cloudbuild.builds.builder","iam.serviceAccountAdmin","iam.serviceAccountUser","viewer","compute.viewer","compute.admin","compute.instanceAdmin","compute.instanceAdmin.v1","compute.networkAdmin"]
sa_tools_project = "aeriscom-acp-tools-201905"
sa_devops_email = "connectivity-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-rci-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "infra", "ap-app", "ap-reporting", "ap-partner","ap-services","ap-management", "auth"]
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-64"
    vm_type_services_ap_app="true"
    vm_type_services_ap_services="true"
    vm_type_services_ap_partner="true"
    vm_type_services_ap_reporting="true"
    vm_type_services_ap_management="true"
  }
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_ap_management="true"
    vm_type_services_ap_reporting="true"
  }
}

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ap-rci-waf-us-west1"
waf_bucket="csb-ap-rci-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ap-rci-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ap-rci-waf-us-west1"
waf_loadbalancer="hlb-ap-rci-waf-us-west1"
waf_healthcheck="hlb-ap-rci-waf-us-west1"
waf_certificate=["projects/aeriscom-ap-rci-201908/global/sslCertificates/crt-aerjupiter-com"]
waf_config_path="config/ap/ap-rci"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"
waf_ruleset="aerport.rules"

###########SQL soft-vpn vars###############
ap-rci-soft-vpn-db_cloudsql_host="csqldb-sofware-vpn-db-rci"
ap-rci-soft-vpn-db_cloudsql_tier="db-n1-standard-1"
ap-rci-soft-vpn-db_cloudsql_disk_size="10"
ap-rci-soft-vpn-db_zone="us-west1-a"
ap-rci-soft-vpn-db_db_replica="false"
ap-rci-soft-vpn-db_region="us-west1"
###########################################



