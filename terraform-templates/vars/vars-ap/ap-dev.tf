## common vars ##
bucket="csb-ap-dev-2-tfstate-us-west1"
project="aeriscom-ap-dev-201907-2"
product_name="acpap"
environment="dev"
region="us-west1"
network="vpc-ap-dev-us-west1"
service_account="iam-ap-dev-gke-us-west1@aeriscom-ap-dev-201907-2.iam.gserviceaccount.com"

## certificate vars ##
certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-dev-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "infra", "ap-app", "ap-reporting", "ap-partner","ap-services","ap-management", "auth","ap-app-aeris","ap-app-sprint" ,"ap-app-uscc", "ap-app-sim-ui"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-16"
    vm_type_infra_infra="true"
    vm_type_services_auth="true"
    vm_type_services_ap_app="true"
    vm_type_services_ap_management="true"
    vm_type_services_ap_management="true"
	vm_type_services_ap_app_aeris="true"
	vm_type_services_ap_app_sprint="true"
	vm_type_services_ap_app_uscc="true"
	vm_type_services_ap_app_simui="true"
  }
  ap-services={
    name="node-pool-ap-services"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_ap_reporting="true"
    vm_type_services_ap_partner="true"
    vm_type_services_ap_services="true"
    
  }
}

###### SQL Vars ######
ap-dev-keycloak-db_cloudsql_host="test-2837123123"
ap-dev-keycloak-db_cloudsql_tier="db-n1-standard-2"
ap-dev-keycloak-db_cloudsql_disk_size="10"
ap-dev-keycloak-db_zone="us-west1-a"
ap-dev-keycloak-db_db_replica="true"
ap-dev-keycloak-db_region="us-west1"

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ap-dev-waf-us-west1"
waf_bucket="csb-ap-dev-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ap-dev-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ap-dev-waf-us-west1"
waf_loadbalancer="hlb-ap-dev-waf-us-west1"
waf_healthcheck="hlb-ap-dev-waf-us-west1"
waf_certificate=["projects/aeriscom-ap-dev-201907-2/global/sslCertificates/crt-aerjupiter-com"]
waf_config_path="config/ap/ap-dev"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"
waf_ruleset="aerport.rules"


###########SQL soft-vpn vars###############
ap-dev-soft-vpn-db_cloudsql_host="sofware-vpn-db"
ap-dev-soft-vpn-db_cloudsql_tier="db-n1-standard-1"
ap-dev-soft-vpn-db_cloudsql_disk_size="10"
ap-dev-soft-vpn-db_zone="us-west1-a"
ap-dev-soft-vpn-db_db_replica="false"
ap-dev-soft-vpn-db_region="us-west1"