## common vars ##
bucket="csb-ap-pre-tfstate-us-west1"
project="aeriscom-ap-pre-201907"
product_name="acpap"
environment="pre"
region="us-west1"
network="vpc-ap-pre-us-west1"
service_account="iam-ap-pre-gke-us-west1@aeriscom-ap-pre-201907.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

################## SQL VARS ######################

ap-pre-fusekeycloak-sql_cloudsql_host="db-ap-pre-fuse-keycloak-us-west1-3"
ap-pre-fusekeycloak-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-fusekeycloak-sql_cloudsql_disk_size="10"
ap-pre-fusekeycloak-sql_zone="us-west1-a"
ap-pre-fusekeycloak-sql_db_replica="true"
ap-pre-fusekeycloak-sql_region="us-west1"

ap-pre-uscckeycloak-sql_cloudsql_host="db-ap-pre-uscc-keycloak-us-west1-1"
ap-pre-uscckeycloak-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-uscckeycloak-sql_cloudsql_disk_size="10"
ap-pre-uscckeycloak-sql_zone="us-west1-a"
ap-pre-uscckeycloak-sql_db_replica="true"
ap-pre-uscckeycloak-sql_region="us-west1"

ap-pre-fuseums-sql_cloudsql_host="db-ap-pre-fuse-user-management-us-west1-3"
ap-pre-fuseums-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-fuseums-sql_cloudsql_disk_size="10"
ap-pre-fuseums-sql_zone="us-west1-a"
ap-pre-fuseums-sql_db_replica="true"
ap-pre-fuseums-sql_region="us-west1"

ap-pre-usccums-sql_cloudsql_host="db-ap-pre-uscc-user-management-us-west1-3"
ap-pre-usccums-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-usccums-sql_cloudsql_disk_size="10"
ap-pre-usccums-sql_zone="us-west1-a"
ap-pre-usccums-sql_db_replica="true"
ap-pre-usccums-sql_region="us-west1"

ap-pre-staticip-sql_cloudsql_host="db-ap-pre-staticip-mgmt-us-west1-3"
ap-pre-staticip-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-staticip-sql_cloudsql_disk_size="10"
ap-pre-staticip-sql_zone="us-west1-a"
ap-pre-staticip-sql_db_replica="true"
ap-pre-staticip-sql_region="us-west1"

ap-pre-fuseliferay-sql_cloudsql_host="db-ap-pre-fuse-liferray-us-west1-2"
ap-pre-fuseliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-fuseliferay-sql_cloudsql_disk_size="10"
ap-pre-fuseliferay-sql_zone="us-west1-a"
ap-pre-fuseliferay-sql_db_replica="true"
ap-pre-fuseliferay-sql_region="us-west1"

ap-pre-sprintliferay-sql_cloudsql_host="db-ap-pre-sprint-liferray-us-west1-2"
ap-pre-sprintliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-sprintliferay-sql_cloudsql_disk_size="10"
ap-pre-sprintliferay-sql_zone="us-west1-a"
ap-pre-sprintliferay-sql_db_replica="true"
ap-pre-sprintliferay-sql_region="us-west1"

ap-pre-usccliferay-sql_cloudsql_host="db-ap-pre-uscc-liferray-us-west1-2"
ap-pre-usccliferay-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-usccliferay-sql_cloudsql_disk_size="10"
ap-pre-usccliferay-sql_zone="us-west1-a"
ap-pre-usccliferay-sql_db_replica="true"
ap-pre-usccliferay-sql_region="us-west1"

ap-pre-sim-management-sql_cloudsql_host="db-ap-pre-sim-management-us-west1-2"
ap-pre-sim-management-sql_cloudsql_tier="db-n1-standard-2"
ap-pre-sim-management-sql_cloudsql_disk_size="10"
ap-pre-sim-management-sql_zone="us-west1-a"
ap-pre-sim-management-sql_db_replica="true"
ap-pre-sim-management-sql_region="us-west1"

ap-pre-kong-psql_db_db_type="postgres"
ap-pre-kong-psql_cloudsql_host="db-ap-pre-kong-us-west1-3"
ap-pre-kong-psql_cloudsql_tier="db-n1-standard-2"
ap-pre-kong-psql_cloudsql_disk_size="20"
ap-pre-kong-psql_zone="us-west1-a"
ap-pre-kong-psql_db_replica="false"
ap-pre-kong-psql_region="us-west1"

ap-pre-swvpn-psql_cloudsql_host="db-ap-pre-sw-vpn-us-west1-2"
ap-pre-swvpn-psql_cloudsql_tier="db-n1-standard-2"
ap-pre-swvpn-psql_cloudsql_disk_size="10"
ap-pre-swvpn-psql_zone="us-west1-a"
ap-pre-swvpn-psql_db_replica="false"
ap-pre-swvpn-psql_region="us-west1"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-pre-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "ap-services" ,"ap-app" , "ap-partner" , "ap-reporting" , "ap-management" , "infra" , "auth"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-64"
    vm_type_services_ap_app="true"
    vm_type_services_ap_services="true"
    vm_type_services_ap_partner="true"
    vm_type_services_ap_app_aeris="true"
    vm_type_services_ap_app_sprint="true"
    vm_type_services_ap_app_uscc="true"
    vm_type_services_ap_app_simui="true"
  }
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  management={
    name="node-pool-management"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_ap_management="true"
    vm_type_services_ap_reporting="true"
  }

  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_ap_management="true"
    vm_type_services_ap_reporting="true"
  }

}

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-ap-pre-waf-us-west1"
waf_bucket="csb-ap-pre-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-ap-pre-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-ap-pre-waf-us-west1"
waf_loadbalancer="hlb-ap-pre-waf-us-west1"
waf_healthcheck="hlb-ap-pre-waf-us-west1"
waf_certificate=[
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeris-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeris-net",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/aeriscloud-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/commandcenter-sprint-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/connecthq-uscellular-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/crt-aerisapis-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/crt-aerjupiter-com",
  "projects/aeriscom-ap-pre-201907/global/sslCertificates/m2mweb-net"
]
waf_config_path="config/ap/ap-pre"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"
waf_ruleset="aerport.rules"
