## common vars ##
bucket="csb-ap-dci-tfstate-us-west1"
project="aeriscom-ap-dci-201906"
product_name="acpap"
environment="dci"
region="us-west1"
network="vpc-ap-dci-us-west1"
service_account="iam-ap-dci-gke-us-west1@aeriscom-ap-dci-201906.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-ap-dci-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "infra", "ap-app", "ap-reporting", "ap-partner","ap-services","ap-management", "auth","ap-app-aeris","ap-app-sprint" ,"ap-app-uscc", "ap-app-sim-ui"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-16"
    vm_type_infra_infra="true"
    vm_type_services_auth="true"
    vm_type_services_ap_app="true"
    vm_type_services_ap_management="true"
    vm_type_services_ap_app_aeris="true"
	vm_type_services_ap_app_sprint="true"
	vm_type_services_ap_app_uscc="true"
	vm_type_services_ap_app_simui="true"
  }
  ap-services={
    name="node-pool-ap-services"
    count=1
    machine_type="n1-standard-16"
    vm_type_services_ap_reporting="true"
    vm_type_services_ap_partner="true"
    vm_type_services_ap_services="true"
    
  }
}
