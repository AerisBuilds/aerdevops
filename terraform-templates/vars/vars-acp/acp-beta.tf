## common vars ##
bucket="csb-acp-beta-tfstate-us-west1"
project="aeriscom-acp-beta-201910"
product_name="acp"
environment="beta"
region="us-west1"
zone="us-west1-a"
network="vpc-acp-beta-us-west1"
service_account="iam-acp-beta-gke-us-west1@aeriscom-acp-beta-201910.iam.gserviceaccount.com"
service_account_id="iam-acp-beta-gke-us-west1"

## certificate vars ##
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="acp-beta"
project_folder_id="510011582329"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##
shared_vpc_project_id="aeriscomm-hostproj-aer-201903"
subnets = [
  {
    subnet_name           = "subnet-acp-beta-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-acp-beta-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-acp-beta-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-acp-beta-dss-us-west1 = []
  subnet-acp-beta-dmz-us-west1 = []
  subnet-acp-beta-bss-us-west1 = []
  subnet-acp-beta-sec-us-west1 = []
  subnet-acp-beta-imz-us-west1 = []
  subnet-acp-beta-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "iam-acp-prodtools-us-west1@aeriscom-acp-prodtools-201906.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin", "cloudbuild.builds.builder", "iam.serviceAccountAdmin", "iam.serviceAccountUser", "viewer", "compute.viewer", "compute.admin", "compute.instanceAdmin", "compute.instanceAdmin.v1", "compute.networkAdmin"]
sa_tools_project = "aeriscom-acp-prodtools-201906"
sa_devops_email = "connectivity-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]
sa_additional_accounts = []
sa_additional_roles = []
