## common vars ##
bucket="csb-ab-dci-tfstate-us-west1"
project="aeriscom-ab-dci-201905"
product_name="acpab"
environment="dci"
region="us-west1"
zone="us-west1-a"
network="projects/aeriscomm-hostproj-np-201903/global/networks/host-nonpod-subnet-vpc"
service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"




## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain="ab-dci.aerjupiter.com"
ssl_cert_name="ab-dci-20190614"



## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-k8s01-us-west1-25"
add_secondary_ranges="true"
cluster_secondary_range_name="subnet-ab-dci-k8s01-pods-us-west1-pods"
services_secondary_range_name="subnet-ab-dci-k8s01-us-west1-services"
namespaces=["test", "mon", "infra", "ab-services"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    node_pool_id="1"
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_services_test="true"
    vm_type_services_aerboss="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    node_pool_id="2"
    machine_type="n1-standard-16"
  }
}
https_lb_settings={}

################## SQL VARS ######################

ab-dci-cdh-sql_cloudsql_host="db-ab-dci-ngb-db-us-west1"
ab-dci-cdh-sql_cloudsql_tier="db-n1-standard-1"
ab-dci-cdh-sql_cloudsql_disk_size="20"
ab-dci-cdh-sql_zone="us-west1-a"
ab-dci-cdh-sql_db_replica="false"
ab-dci-cdh-sql_region="us-west1"

###storm vars ###

storm_purpose="storm"
storm_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
storm_subnetwork_project="aeriscomm-hostproj-np-201903"
storm_image="aeriscom-acp-tools-201905/ubuntu-storm-image-20190613"
storm_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
storm_node_settings={
  node1={
    name="storm-ab-dci-nrt-node01-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","private-cloudera","private-ssh"]
    role="node"
    hostname="storm-ab-dci-nrt-node01-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
  }
  node2={
    name="storm-ab-dci-nrt-node02-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","private-cloudera","private-ssh"]
    role="node"
    hostname="storm-ab-dci-nrt-node02-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
 }
 
  node3={
    name="storm-ab-dci-nrt-node03-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="100"
    disk_size_type="pd-standard"
    tags=["dss","bss","private-cloudera","private-ssh"]
    role="node"
    hostname="storm-ab-dci-nrt-node03-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
 }
}


###cloudera cluster vars ###

cloudera_purpose="test-cloudera"
cloudera_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
cloudera_subnetwork_project="aeriscomm-hostproj-np-201903"
cloudera_image="aeriscom-acp-tools-201905/cloudera-jdk8-152"
cloudera_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
cloudera_node_settings={
  node1={
    name="cdh-test-dev-masternode"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["private-cloudera","dss","bss"]
    role="cm-manager,master"
    hostname="cdh-test-dev-masternode.internal.aeris.com"

  }
  node2={
    name="cdh-test-dev-slave1"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["private-cloudera","dss","bss"]
    role="slave"
    hostname="cdh-test-dev-slave1.internal.aeris.com"
 }
  node3={
    name="cdh-test-dev-slave2"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["private-cloudera","dss","bss"]
    role="slave"
    hostname="cdh-test-dev-slave2.internal.aeris.com"
  }
  node4={
    name="cdh-test-dev-slave3"
    machine_type="n1-standard-8"
    disk_size_root="100"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["private-cloudera","dss","bss"]
    role="slave"
    hostname="cdh-test-dev-slave3.internal.aeris.com"
  }
}



###cassandra cluster vars ###

cassandra_purpose="cassandra"
cassandra_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
cassandra_subnetwork_project="aeriscomm-hostproj-np-201903"
cassandra_image="aeriscom-acp-tools-201905/ubuntu-cassandra-image-20190613"
cassandra_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
email="acp-devops@aeris.net"
cassandra_node_settings={
  node1={
    name="cassandra-dci-node1"
    cassandra_node_id="1"         
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
  node2={                 
    name="cassandra-dci-node2"
    cassandra_node_id="2"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
  node3={
    name="cassandra-dci-node3"
    cassandra_node_id="3"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
   node4={
    name="cassandra-dci-node4"
    cassandra_node_id="4"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
   node5={
    name="cassandra-dci-node5"
    cassandra_node_id="5"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
   node6={
    name="cassandra-dci-node6"
    cassandra_node_id="6"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
   node7={
    name="cassandra-dci-node7"
    cassandra_node_id="7"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
   node8={
    name="cassandra-dci-node8"
    cassandra_node_id="8"
    machine_type="n1-highmem-8"
    disk_size_root="50"
    disk_size="500"
    disk_size_type="pd-standard"
  }
}

###vminfra cluster vars ###

vminfra_purpose="vminfra"
vminfra_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
vminfra_subnetwork_project="aeriscomm-hostproj-np-201903"
vminfra_image="aeriscom-acp-tools-201905/ubuntu-storm-image-20190613"
vminfra_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
vminfra_node_settings={
  node1={
    name="hazelcast-nrt-dci-node1"
	vminfra_node_id="1"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="250"
    disk_size_type="pd-standard"
  }
  node2={
    name="hazelcast-nrt-dci-node2"
	vminfra_node_id="2"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="250"
    disk_size_type="pd-standard"
  }
   node3={
    name="rmq-nrt-dci-node1"
    vminfra_node_id="3"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
  node4={
    name="rmq-nrt-dci-node2"
    vminfra_node_id="4"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
  node5={
    name="kafka-nrt-dci-node1"
    vminfra_node_id="5"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
  node6={
    name="kafka-nrt-dci-node2"
    vminfra_node_id="6"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
    node7={
    name="nifi-nrt-dci-node1"
    vminfra_node_id="7"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
  node8={
    name="nifi-nrt-dci-node2"
    vminfra_node_id="8"
	machine_type="n1-standard-2"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
  }
}

###rabbitmq cluster vars ###

rabbitmq_purpose="test-rabbitmq"
rabbitmq_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
rabbitmq_subnetwork_project="aeriscomm-hostproj-np-201903"
rabbitmq_image="aeriscom-acp-tools-201905/ubuntu-rmq-20190722"
rabbitmq_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
rabbitmq_node_settings={
  node1={
    name="rmq-ab-dci-nrt-node01-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["dss","bss","private-cloudera","private-ssh"]
    role="node"
    hostname="rmq-ab-dci-nrt-node01-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
  }
  node2={
    name="rmq-ab-dci-nrt-node02-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="50"
    disk_size="1"
    disk_size_type="pd-standard"
    tags=["dss","bss","private-cloudera","private-ssh"]
    role="node"
    hostname="rmq-ab-dci-nrt-node02-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
 }
}

####ZOOKEEPER VARS ####

zookeeper_purpose="zookeeper"
zookeeper_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnet-ab-dci-dss01-us-west1-26"
zookeeper_subnetwork_project="aeriscomm-hostproj-np-201903"
zookeeper_image="aeriscom-acp-tools-201905/ubuntu-storm-image-20190613"
zookeeper_service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"
zookeeper_node_settings={
	node1={
    name="zk-ab-dci-nrt-node01-us-west1-a"
    machine_type="n1-standard-4"
    disk_size_root="30"
    disk_size="20"
    disk_size_type="pd-standard"
	tags=["dss","bss"]
	role="master"
	hostname="zk-ab-dci-nrt-node01-us-west1-a.internal.aeris.com"
	zone="us-west1-a"
  }
 }

