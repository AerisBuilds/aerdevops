## common vars ##
bucket="csb-ab-dci-tfstate-us-west1"
project="aeriscom-ab-dci-201905"
product_name="acpab"
environment="dci"
region="us-west1"

network="projects/aeriscomm-hostproj-np-201903/global/networks/host-nonpod-subnet-vpc"
service_account="iam-ab-dci-gke-us-west1@aeriscom-ab-dci-201905.iam.gserviceaccount.com"




## certificate vars ##

certificate_purpose="certificate"
certbot_email="aeris-devops@aeris.net"
domain_name="ab-dci.aerjupiter.com"
ssl_cert_name="crt-ab-dci-20190521"



## gkecluster vars ##

gkecluster_purpose="gkecluster"
additional_zones=["us-west1-a"]
gkecluster_subnetwork="projects/aeriscomm-hostproj-np-201903/regions/us-west1/subnetworks/subnetk84"
add_secondary_ranges="true"
cluster_secondary_range_name="tier1"
services_secondary_range_name="tier2"
namespaces=["test", "mon", "infra", "aerboss"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="app"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    node_pool_id="1"
    machine_type="n1-standard-8"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    node_pool_id="2"
    machine_type="n1-standard-16"
    vm_type_infra_aerboss="true"
  }
}
https_lb_settings={
/*  webapp={
    name="webapp"
    port_protocol="tcp"
    port="31000"
    node_pool_index="1"
    ssl_certificates="projects/aeriscom-ampsol-dci-201905/global/sslCertificates/crt-ampsol-dci-20190503"
    port_range="443"
    subdomain="webserver.ampsol-dci.aerjupiter.com"
    managed_zone="ampsol-dci"
  }
  keycloak={                                                                                             
    name="keycloak"                                                                                      
    port_protocol="tcp"                                                                                  
    port="32000"                                                                                         
    node_pool_index="1"                                                                                  
    ssl_certificates="projects/aeriscom-ampsol-dci-201905/global/sslCertificates/crt-ampsol-dci-20190503"
    port_range="443"                                                                                     
    subdomain="auth.ampsol-dci.aerjupiter.com"                                                           
    managed_zone="ampsol-dci"                                                                            
  }
*/
}



### vault vars ###

vault_purpose="sec"
machine_type="n1-standard-2"
zone="a"
vault_subnetwork="subnet-ab-dci-sec01-us-west1"
image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190419"
disk_size="10"
docker_repo="http://repo.aeriscloud.com:6666"
docker_image="repo.aeriscloud.com:6666/vault:1.0.2-7"
key_ring="kms-ab-dci01-us-west1-ok" # TODO: Change name
crypto_key="key-ab-dci01-us-west1-ok" # TODO: Change name
cloudsql_host="csql-ab-dci01-us-west1-ok1"
cloudsql_tier="db-n1-standard-1"
vault_db_name="csql-ab-dci01-vault"
vault_db_user="dci-vault"
cloudsql_disk_size="10"
service_account_vault="iam-ab-dci-us-west1-vault"
