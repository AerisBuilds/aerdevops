## common vars ##
bucket="csb-ab-pre-tfstate-us-west1"
project="aeriscom-ab-pre-201907"
product_name="acpab"
environment="pre"
region="us-west1"
network="vpc-ab-pre-us-west1"
service_account="iam-ab-pre-gke-us-west1@aeriscom-ab-dev-201907.iam.gserviceaccount.com"

## certificate vars ##

certificate_purpose="certificate"
ssl_cert_name="crt-aerjupiter-com"

## gkecluster vars ##

purpose="gkecluster"
additional_zones=["us-west1-a""us-west1-b"]
subnetwork="subnet-ab-pre-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["test", "mon", "ab-services"]
docker_server="repo.aeriscloud.com"
docker_email="software@aeris.net"
registry_name="dockerrepokey"
registry_namespace="mon"
node_pool_settings={
  app={
    name="node-pool-app"
    count=1
    machine_type="n1-standard-8"
    vm_type_services_ab="true"
  }
  infra={
    name="node-pool-infra"
    count=1
    machine_type="n1-standard-4"
    vm_type_services_mon="true"
    vm_type_services_test="true"
  }
}
