## common vars ##
bucket="csb-af-pre-tfstate-us-west1-a"
project="aeriscom-af-pre-201909"
product_name="af"
environment="pre"
region="us-west1"
zone="us-west1-a"
network="vpc-af-pre-us-west1"
service_account="iam-af-pre-gke-us-west1@aeriscom-af-pre-201909.iam.gserviceaccount.com"
service_account_id="iam-af-pre-gke-us-west1"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="af-pre"
project_folder_id="477118579300"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-aer-201903"

subnets = [
  {
    subnet_name           = "subnet-af-pre-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-af-pre-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-af-pre-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-af-pre-dss-us-west1 = []
  subnet-af-pre-dmz-us-west1 = []
  subnet-af-pre-bss-us-west1 = []
  subnet-af-pre-sec-us-west1 = []
  subnet-af-pre-imz-us-west1 = []
  subnet-af-pre-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "iam-acp-prodtools-us-west1@aeriscom-acp-prodtools-201906.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin", "cloudbuild.builds.builder", "iam.serviceAccountAdmin", "iam.serviceAccountUser", "viewer", "compute.viewer", "compute.admin", "compute.instanceAdmin", "compute.instanceAdmin.v1", "compute.networkAdmin"]
sa_tools_project = "aeriscom-acp-prodtools-201906"
sa_devops_email = "connectivity-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "iam.serviceAccountKeyAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]
sa_additional_accounts = []
sa_additional_roles = []

## gkecluster vars ##
purpose="gkecluster"
additional_zones=["us-west1-a","us-west1-b"]
subnetwork="subnet-af-pre-k8s-us-west1"
add_secondary_ranges="true"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["mon", "af-services", "test"]
node_pool_settings={
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-4"
  }
  svc={
    name="node-pool-af-services"
    count=1
    machine_type="n1-standard-4"
  }
  infra={
    name="node-pool-test"
    count=1
    machine_type="n1-standard-2"
  }
}

node_pool_tags=["node-pool-af-services:vm.type.services/af-services=true","node-pool-test:vm.type.services/test","node-pool-mon:vm.type.services/mon=true"]

### WAF Vars ###
waf_purpose="waf"
waf_template="vm-af-pre-waf-us-west1"
waf_bucket="csb-af-pre-waf-us-west1"
waf_machine_type="n1-standard-1"
waf_subnetwork="subnet-af-pre-dmz-us-west1"
waf_image="ubuntu-os-cloud/ubuntu-1804-lts"
waf_disk_size="32"
waf_docker_image="dmgnx/nginx-naxsi"
waf_instance_group="vm-af-pre-waf-us-west1"
waf_loadbalancer="hlb-af-pre-waf-us-west1"
waf_healthcheck="hlb-af-pre-waf-us-west1"
waf_certificate=[
  "projects/aeriscom-af-pre-201909/global/sslCertificates/crt-aerjupiter-com",
  "projects/aeriscom-af-pre-201909/global/sslCertificates/ctr-aerframe-aeris-com"
]
waf_config_path="config/af/af-pre"
waf_min_instances="2"
waf_max_instances="4"
waf_managed_zone="none"
waf_endpoint="none"
waf_create_dns="false"
waf_ruleset="aerframe.rules"