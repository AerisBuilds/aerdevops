## common vars ##
bucket="csb-project-dev-tfstate-us-west1"
project="aeriscom-project-dev-201908"
product_name="project"
environment="dev"
region="us-west1"
zone="us-west1-a"
network="vpc-project-dev-us-west1"
service_account="iam-project-dev-gke-us-west1@aeriscom-project-dev-201908.iam.gserviceaccount.com"
service_account_id="iam-project-dev-gke-us-west1"
ssl_cert_name="crt-aerjupiter-com"

## project vars ##
project_name="project-dev"
project_folder_id="194325965410"
project_billing_account="0183C7-9A2A07-7BD8B6"
project_bucket="aeris-projects"

## network vars ##

shared_vpc_project_id="aeriscomm-hostproj-np-201903"

subnets = [
  {
    subnet_name           = "subnet-project-dev-k8s-us-west1"
    subnet_ip             = "172.16.2.0/24"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-dss-us-west1"
    subnet_ip             = "172.16.1.128/25"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-dmz-us-west1"
    subnet_ip             = "172.16.1.32/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-bss-us-west1"
    subnet_ip             = "172.16.1.0/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-sec-us-west1"
    subnet_ip             = "172.16.1.8/29"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-imz-us-west1"
    subnet_ip             = "172.16.1.64/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
  {
    subnet_name           = "subnet-project-dev-tst-us-west1"
    subnet_ip             = "172.16.3.0/27"
    subnet_region         = "us-west1"
    subnet_private_access = "true"
  },
]
secondary_ranges = {
  subnet-project-dev-k8s-us-west1 = [
  {
    range_name = "services"
    ip_cidr_range = "10.64.0.0/20"
  },
  {
    range_name = "pods"
    ip_cidr_range = "10.60.0.0/14"
  },
  ]
  subnet-project-dev-dss-us-west1 = []
  subnet-project-dev-dmz-us-west1 = []
  subnet-project-dev-bss-us-west1 = []
  subnet-project-dev-sec-us-west1 = []
  subnet-project-dev-imz-us-west1 = []
  subnet-project-dev-tst-us-west1 = []
}

## service account vars ##
sa_roles = ["cloudsql.admin", "compute.admin", "compute.networkAdmin", "container.admin", "container.clusterAdmin", "iam.serviceAccountUser", "redis.admin", "serviceusage.serviceUsageAdmin", "storage.admin", "viewer", "clouddebugger.agent", "clouddebugger.user", "cloudtrace.admin", "logging.admin", "monitoring.admin", "monitoring.metricWriter", "stackdriver.accounts.editor", "stackdriver.resourceMaintenanceWindow.editor", "stackdriver.resourceMetadata.writer"]
sa_tools = "terraform@aeriscom-sol-tools-201903.iam.gserviceaccount.com"
sa_tools_roles = ["container.admin", "iam.serviceAccountAdmin", "resourcemanager.projectIamAdmin", "servicemanagement.admin", "serviceusage.serviceUsageAdmin"]
sa_tools_project = "aeriscom-sol-tools-201903"
sa_devops_email = "solutions-devops@aeris.net"
sa_devops_roles = ["compute.admin", "storage.objectAdmin", "viewer"]
sa_gcr_roles = ["storage.objectCreator", "storage.objectViewer"]
sa_images_project = "aeriscomm-hostproj-aer-201903"
sa_images_roles = ["compute.imageUser"]
sa_network_roles = ["compute.networkUser"]

##  additional service accounts ##
sa_additional_accounts = []
sa_additional_roles = []

## gkecluster vars ##
purpose="gkecluster"
additional_zones=["us-west1-a"]
subnetwork="subnet-project-dev-k8s-us-west1"
master_ipv4_cidr_block = "172.16.4.0/28"
cluster_secondary_range_name="pods"
services_secondary_range_name="services"
namespaces=["mon", "test"]
node_pool_settings={
  mon={
    name="node-pool-mon"
    count=1
    machine_type="n1-standard-8"
  }
  test={
    name="node-pool-test"
    count=1
    machine_type="n1-standard-8"
  }
}
node_pool_tags=["node-pool-mon:vm.type.services/mon=true","node-pool-test:vm.type.services/test=true"]
### waf vars ###

// waf_purpose="waf"
// waf_template="vm-ampsol-dci-waf-us-west1"
// waf_bucket="csb-ampsol-dci-waf-us-west1"
// waf_machine_type="n1-standard-1"
// waf_subnetwork="subnet-ampsol-dci-dmz01-us-west1"
// waf_image="aeriscomm-hostproj-aer-201903/ubuntu-image-20190611"
// waf_disk_size="32"
// waf_docker_image="dmgnx/nginx-naxsi"
// waf_instance_group="vm-ampsol-dci-waf-us-west1"
// waf_loadbalancer="hlb-ampsol-dci-waf-us-west1"
// waf_healthcheck="hlb-ampsol-dci-waf-us-west1"
// waf_certificate="projects/aeriscom-ampsol-dci-201905/global/sslCertificates/crt-ampsol-dci-20190503"
// waf_config_path="config/ampsol/ampsol-dci"
// waf_min_instances="1"
// waf_max_instances="2"
// waf_managed_zone="ampsol-dci"
// waf_endpoint="waf.ampsol-dci.aerjupiter.com"
// waf_create_dns="false"
