#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./purge_addresses.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./purge_addresses.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')


RAW_SUBNETS=$(gcloud compute networks subnets list --project=$PROJECT --network=$NETWORK |gcloud compute networks subnets list --project=$PROJECT --network=$NETWORK | awk '{if (NR!=1) print $1}')

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
SUBNETS=($RAW_SUBNETS) # split to array $names
IFS=$SAVEIFS 

for SUBNET in "${SUBNETS[@]}"
do
   : 

    RAW_ADDRESSES=$(gcloud compute addresses list --project=$PROJECT --format=json | jq --arg SUBNET "$SUBNET" '.[]? |select(.subnetwork != null) |select(.subnetwork | contains($SUBNET)) | .name' | cut -d'"' -f2)
    SAVEIFS=$IFS   # Save current IFS
    IFS=$'\n'      # Change IFS to new line
    ADDRESSES=($RAW_ADDRESSES) # split to array $names
    IFS=$SAVEIFS 

    for ADDRESS in "${ADDRESSES[@]}"
    do
    : 


        echo "ADDRESS: $ADDRESS"
        REGION=$(gcloud compute addresses list --filter="$ADDRESS" --project=$PROJECT --format=json | jq --arg SUBNET "$SUBNET" '.[]? |select(.region != null) | .region'  | cut -d '"' -f 2 | awk -F/ '{print $NF}')
        
            echo "Deleting address: $ADDRESS"

        if [[ "${REGION}" != "" ]]; then
            gcloud compute addresses delete $ADDRESS --project $PROJECT --region $REGION -q                            

        else
            gcloud compute addresses delete --project $PROJECT --global $ADDRESS -q                         

        fi 


    done 


done 



