/******************************************
        VPC configuration
 *****************************************/
resource "google_compute_network" "network" {
  name                    = "${var.network}"
  auto_create_subnetworks = "false"
  routing_mode            = "${var.routing_mode}"
  project                 = "${var.project}"
}

/******************************************
        Shared VPC
 *****************************************/
resource "google_compute_shared_vpc_host_project" "shared_vpc_host" {
  count   = "${var.shared_vpc_host == "true" ? 1 : 0}"
  project = "${var.project}"
}

/******************************************
        Subnet configuration
 *****************************************/
resource "google_compute_subnetwork" "subnetwork" {
  count = "${length(var.subnets)}"

  name                     = "${lookup(var.subnets[count.index], "subnet_name")}"
  ip_cidr_range            = "${lookup(var.subnets[count.index], "subnet_ip")}"
  region                   = "${lookup(var.subnets[count.index], "subnet_region")}"
  private_ip_google_access = "${lookup(var.subnets[count.index], "subnet_private_access", "false")}"
  enable_flow_logs         = "${lookup(var.subnets[count.index], "subnet_flow_logs", "false")}"
  network                  = "${google_compute_network.network.name}"
  project                  = "${var.project}"

  secondary_ip_range = "${var.secondary_ranges[lookup(var.subnets[count.index], "subnet_name")]}"
}
/*
data "google_compute_subnetwork" "created_subnets" {
  count = "${length(var.subnets)}"

  name    = "${element(google_compute_subnetwork.subnetwork.*.name, count.index)}"
  region  = "${element(google_compute_subnetwork.subnetwork.*.region, count.index)}"
  project = "${var.project}"
}
*/
/******************************************
        Routes
 *****************************************/
resource "google_compute_route" "route" {
  count   = "${length(var.routes)}"
  project = "${var.project}"
  network = "${var.network}"

  name                   = "${lookup(var.routes[count.index], "name", format("%s-%s-%d", lower(var.network), "route",count.index))}"
  description            = "${lookup(var.routes[count.index], "description","")}"
  tags                   = "${compact(split(",",lookup(var.routes[count.index], "tags","")))}"
  dest_range             = "${lookup(var.routes[count.index], "destination_range","")}"
  next_hop_gateway       = "${lookup(var.routes[count.index], "next_hop_internet","") == "true" ? "default-internet-gateway":""}"
  next_hop_ip            = "${lookup(var.routes[count.index], "next_hop_ip","")}"
  next_hop_instance      = "${lookup(var.routes[count.index], "next_hop_instance","")}"
  next_hop_instance_zone = "${lookup(var.routes[count.index], "next_hop_instance_zone","")}"
  next_hop_vpn_tunnel    = "${lookup(var.routes[count.index], "next_hop_vpn_tunnel","")}"
  priority               = "${lookup(var.routes[count.index], "priority", "1000")}"

  depends_on = [
    "google_compute_network.network",
    "google_compute_subnetwork.subnetwork",
  ]
}

resource "null_resource" "delete_default_internet_gateway_routes" {
  count = "${var.delete_default_internet_gateway_routes ? 1 : 0}"

  provisioner "local-exec" {
    command = "${path.module}/scripts/delete-default-gateway-routes.sh ${var.project} ${var.network}"
  }

  triggers = {
    number_of_routes = "${length(var.routes)}"
  }

  depends_on = [
    "google_compute_network.network",
    "google_compute_subnetwork.subnetwork",
    "google_compute_route.route",
  ]
}

resource "google_compute_shared_vpc_service_project" "default" {
  host_project    = "${var.shared_vpc_project_id}"
  service_project = "${var.project}"
}

resource "null_resource" "delay" {
  provisioner "local-exec" {
    command = "sleep 120"
  }
  triggers = {
    before = "${google_compute_network.network.id}"
  }
}

resource "google_compute_router" "default" {
  depends_on = ["null_resource.delay"]
  name       = "natrtr-${var.product_name}-${var.environment}-${var.region}"
  project    = "${var.project}"
  region     = "${var.region}"
  network    = "${var.network}"
}
resource "google_compute_router_nat" "default" {
  name                               = "nat-${var.product_name}-${var.environment}-${var.region}"
  project                            = "${var.project}"
  router                             = "${google_compute_router.default.name}"
  region                             = "${var.region}"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

