variable "project" {
  description = "The ID of the project where this VPC will be created"
}
variable "project_name" {
  description = "The name of the project"
}
variable "product_name" {
  description = "The name of the product name"
}
variable "environment" {
  description = "The name of the environment"
}
variable "region" {
  description = "The region"
}

variable "network" {
  description = "The name of the network being created"
}

variable "shared_vpc_project_id" {
  description = "The project id for shared VPC host project"
}

variable "routing_mode" {
  type        = "string"
  default     = "GLOBAL"
  description = "The network routing mode (default 'GLOBAL')"
}

variable "shared_vpc_host" {
  type        = "string"
  description = "Makes this project a Shared VPC host if 'true' (default 'false')"
  default     = "false"
}

variable "subnets" {
  type        = "list"
  description = "The list of subnets being created"
}

variable "secondary_ranges" {
  type        = "map"
  description = "Secondary ranges that will be used in some of the subnets"
}

variable "routes" {
  type        = "list"
  description = "List of routes being created in this VPC"
  default     = []
}

variable "delete_default_internet_gateway_routes" {
  description = "If set, ensure that all routes within the network specified whose names begin with 'default-route' and with a next hop of 'default-internet-gateway' are deleted"
  default     = "false"
}

