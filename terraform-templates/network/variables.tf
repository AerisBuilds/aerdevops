variable "subnets" {
	type = "list"
	description = "full data for making subnets to populate VPC"
}
variable "network" {
	type = "string"
	description = "Name of VPC"
}
variable "project" {
	type = "string"
	description = "GCP Project ID"
}
variable "project_name" {
	type = "string"
	description = "GCP Project Name"
}
variable "product_name" {
	type = "string"
	description = "Product name"
}
variable "environment" {
	type = "string"
	description = "Environment"
}
variable "region" {
	type = "string"
	description = "Region"
}
variable "secondary_ranges" {
  type        = map(list(object({ range_name = string, ip_cidr_range = string })))
  description = "Secondary ranges that will be used in some of the subnets"
}
variable "shared_vpc_project_id" {
	type = "string"
	description = "Shared VPC Project ID to attach to our project"
}
