module "vpc" {
   source  = "./components/vpc_subnets"
   project = "${var.project}"
   project_name = "${var.project_name}"
   product_name = "${var.product_name}"
   environment = "${var.environment}"
   network = "${var.network}"
   region  = "${var.region}"
   shared_vpc_project_id = "${var.shared_vpc_project_id}"
   shared_vpc_host = "false"
   subnets = "${var.subnets}"
   secondary_ranges = "${var.secondary_ranges}"

   routes = [
       // The following example shows you how to set up an internet egress rule
       /*
       {
           name                   = "egress-internet"
           description            = "route through IGW to access internet"
           destination_range      = "0.0.0.0/0"
           tags                   = "egress-inet"
           next_hop_internet      = "true"
       }
       /*
       // The following is an example to configure a route for a custom proxy
       /*
       {
           name                   = "app-proxy"
           description            = "route through proxy to reach app"
           destination_range      = "10.50.10.0/24"
           tags                   = "app-proxy"
           next_hop_instance      = "app-proxy-instance"
           next_hop_instance_zone = "us-west1-a"
       },*/
   ]
}

