#! /bin/bash
for HOST in `cat HOSTLIST`; do
echo "*** $HOST"
scp -i /home/kapil_khakholary/nrt-test.pem -o StrictHostKeyChecking=no -p \
${GITREPO}conf.sh* \
${GITREPO}config.sh* \
${GITREPO}hostlist.sh* \
${GITREPO}nimbus.sh* \
${GITREPO}nimbus_start.sh* \
${GITREPO}storm.yaml \
${GITREPO}supervisor.sh* \
${GITREPO}supervisor_start.sh* \
${GITREPO}zdk_start.sh* \
${GITREPO}zkserver.sh* \
$HOST:
done
