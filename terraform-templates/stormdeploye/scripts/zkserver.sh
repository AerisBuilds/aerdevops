#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#List of Zookeeper server
zdk=10.4.18.26,10.4.18.4,10.4.18.58
USER=ubuntu
ssh -i key/nrt-test.pem -o StrictHostKeyChecking=no -t $USER@$zdk "sudo bash $BOPT ./zdk_start.sh"
done
#
#