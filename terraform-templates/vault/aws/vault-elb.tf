#------------------------------------------#
# ELB Configuration
#------------------------------------------#

resource "aws_elb" "vault_http" {
    name     = "${var.name_prefix}-elb"
    internal = "${var.internal_elb}"
#   availability_zones = ["us-west-2a", "us-west-2b"]
    subnets = ["subnet-01a11b2e97b413d5b", "subnet-01a11b2e97b413d5b"]
    security_groups = ["${var.aws_security_group}"]
    instances       = ["${aws_instance.vault.*.id}"]

    idle_timeout                = 400
    cross_zone_load_balancing   = true
    connection_draining         = true
    connection_draining_timeout = 400

    listener {
        instance_port     = 8200
        instance_protocol = "tcp"
        lb_port           = 443
        lb_protocol       = "ssl"
        ssl_certificate_id = "arn:aws:iam::248971149884:server-certificate/${var.certificate_name}"
    }

    listener {
        instance_port     = 8200
        instance_protocol = "tcp"
        lb_port           = 80
        lb_protocol       = "tcp"
    }

    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = 3
        target              = "HTTP:8200/ui/vault/auth?with=token"
        interval            = 30
    }

    tags {
        Name = "${var.name_prefix}-elb-http"
        Project = "${var.project}"
        Component = "${var.component}"
        Deployment = "${var.deployment}"
        Owner = "${var.owner}"
        Product = "${var.product}"
    }
}

resource "aws_proxy_protocol_policy" "vault_http_proxy_policy" {
    load_balancer  = "${aws_elb.vault_http.name}"
    instance_ports = ["80"]
}
