#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#
provider "aws" {
 region = "${var.region}"
}

resource "aws_instance" "vault" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
                                 #!/bin/bash
                                 sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
                                 sudo service docker start
                                 sleep 1
                                 sudo docker login -u ${var.nexus_username} -p ${var.nexus_password} nexus-solutions.devaeris.com:8084
                                 sudo docker pull nexus-solutions.devaeris.com:8084/vault:1.0.2-7
                                 sleep 1
                                 yum install -y mysql
                                 sleep 1
                                 sudo echo "create database ${var.schema_name};" > ./sql-create-file.sql
                                 sudo echo "create user '${var.username_rds}'@'%' identified by '${var.password_rds}';" >> ./sql-create-file.sql
                                 sudo echo "grant select,insert,update,delete,create,alter,drop on ${var.schema_name}.* to '${var.username_rds}'@'%';" >> ./sql-create-file.sql
                                 sudo mysql -h ${var.db_hostname} -u ${var.db_admin} -p${var.db_admin_password} < "sql-create-file.sql"
                                 sleep 1
                                 ip="$(ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
                                 cd /
                                 sudo mkdir vault
                                 sudo chmod 777 vault
                                 cd /vault
                                 sudo mkdir config
                                 sudo chmod 755 config
                                 cd config
                                 echo "{                                                                        " > ./local.json
                                 echo "       \"listener\": [{                                                  " >> ./local.json
                                 echo "                \"tcp\": {                                               " >> ./local.json
                                 echo "                       \"address\": \"0.0.0.0:8200\",                    " >> ./local.json
                                 echo "                       \"tls_disable\": 1                                " >> ./local.json
                                 echo "                         }                                               " >> ./local.json
                                 echo "                     }],                                                 " >> ./local.json
                                 echo "       \"api_addr\": \"http://$ip:8200\",                                " >> ./local.json
                                 echo "        \"storage\": {                                                   " >> ./local.json
                                 echo "                \"mysql\": {                                             " >> ./local.json
                                 echo "                        \"username\": \"${var.username_rds}\",           " >> ./local.json
                                 echo "                        \"password\": \"${var.password_rds}\",           " >> ./local.json
                                 echo "                        \"address\": \"${var.db_hostname}:3306\",        " >> ./local.json
                                 echo "                        \"database\": \"${var.schema_name}\"             " >> ./local.json
                                 echo "  }                                                                      " >> ./local.json
                                 echo "        },                                                               " >> ./local.json
                                 echo "        \"max_lease_ttl\": \"10h\",                                      " >> ./local.json
                                 echo "       \"default_lease_ttl\": \"10h\",                                   " >> ./local.json
                                 echo "        \"ui\": true                                                     " >> ./local.json
                                 echo "}                                                                        " >> ./local.json
                                 sudo docker run -d -v /vault:/vault -p 8200:8200 --cap-add=IPC_LOCK nexus-solutions.devaeris.com:8084/vault:1.0.2-7 server
                                 EOF
    subnet_id                   = "${element(split(",", var.subnet_ec2ids), count.index)}"
    vpc_security_group_ids = ["${var.aws_security_group}"]

    tags {
        Name = "${var.name_prefix}-${count.index}"
        Project = "${var.project}"
        Component = "${var.component}"
        Deployment = "${var.deployment}"
        Owner = "${var.owner}"
        Product = "${var.product}"
    }

    root_block_device {
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }
}
