#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#

variable "region" {
default = ""
description = "AWS region"
}

variable "zone_id" {
default = ""
description = "Zone ID of route 53 hosted zone where A record will be created"
}

variable "elb_zone_id" {
default = ""
description = "Zone ID of elb hosted zone where route 53 record point to elb dns"
}

variable "record_name" {
default = ""
description = "URL for vault server"
}

variable "certificate_name" {
default = ""
description = "Name of the certificate, it should be same as in aws"
}

variable "count" {
     default     = ""
    description = "Number of EC2 instances to deploy"
}

variable "name_prefix" {
    default     = ""
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = ""
    description = "Instance AMI ID"
}

variable "key_name" {
default = ""
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = ""
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = ""
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = ""
   description = "VPC id"
}

variable "subnet_ec2ids" {
    type        = "string"
    default     = ""
    description = "Subnet range"
}

variable "availability_zones" {
    default     = ""
    description = "Availability zones to place subnets"
}

variable "internal_elb" {
    default     = ""
    description = "Force the ELB to be internal only"
}

variable "deployment" {
    default     = ""
    description = "Deployment tag value"
}

variable "component" {
    default     = ""
    description = "component tag value"
}

variable "project" {
    default     = ""
    description = "project tag value"
}

variable "owner" {
default = ""
    description = "fagun.tripathi@aeris.net"
}

variable "product" {
default = ""
    description = "product tag value"
}

variable "aws_security_group" {
        default = ""
        description = "Security Group"
}

variable "nexus_username" {
default = ""
description = "Nexus Administrator Username"
}

variable "nexus_password" {
default = ""
description = "Nexus Administrator Password"
}

variable "schema_name" {
default = ""
description = "Schema RDS for the Vault- Do not use '-' in schema name"
}

variable "username_rds" {
default = ""
description = "Schema Username for Vault"
}

variable "password_rds" {
default = ""
description = "Schema Password for the Vault"
}

variable "db_hostname" {
default = ""
description = "Database Name"
}

variable "db_admin" {
default = ""
description = "Admin username for the RDS Database"
}

variable "db_admin_password" {
default = ""
description = "Admin Password for the RDS Database"
}
