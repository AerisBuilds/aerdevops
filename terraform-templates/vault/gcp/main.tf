#====================== Provider config ======================
provider "google" {
  region = "${var.region}"
  zone   = "${var.region}-${var.zone}"
  project = "${var.project}"
  version = "~> 2.4"
}

provider "google-beta"{
  region = "${var.region}"
  zone   = "${var.region}-${var.zone}"
  project = "${var.project}"
  version = "~> 2.5"
}

provider "random" {
  version = "~> 2.1"
}

#====================== Cloud SQL ======================
# Enable required APIs
resource "google_project_service" "cloud_sql_api" {
  disable_on_destroy = false
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "service_networking_api" {
  disable_on_destroy = false
  service = "servicenetworking.googleapis.com"
}

# Create MySQL Instances
resource "google_sql_database_instance" "master" {
  provider = "google-beta"
  depends_on = [
    "google_project_service.cloud_sql_api",
    "google_project_service.service_networking_api"
  ]

  name = "${var.cloudsql_host}-a"
  region = "${var.region}"
  database_version = "MYSQL_5_7"

  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = "ZONAL"
    replication_type = "SYNCHRONOUS"
    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    }

    location_preference {
      zone = "${var.region}-a"
    }

    backup_configuration {
      binary_log_enabled = true
      enabled = "true"
      start_time = "03:00"
    }
  }
}

resource "google_sql_database_instance" "replica" {
  provider = "google-beta"
  depends_on = [
    "google_project_service.cloud_sql_api",
    "google_project_service.service_networking_api",
    "google_sql_database_instance.master",
    "google_sql_database.vault",
    "google_sql_user.vault"
  ]

  name = "${var.cloudsql_host}-b"
  region = "${var.region}"
  database_version = "MYSQL_5_7"
  master_instance_name = "${google_sql_database_instance.master.name}"

  replica_configuration {
    connect_retry_interval = 60
    failover_target = true
  }

  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = "ZONAL"
    replication_type = "SYNCHRONOUS"
    crash_safe_replication = true

    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    }

    location_preference {
      zone = "${var.region}-b"
    }
  }
}

# Create MySQL password
resource "random_string" "mysql_password" {
  length = 64
  special = false
}

# Create MySQL database
resource "google_sql_database" "vault" {
  depends_on = [ "google_sql_database_instance.master" ]
  name      = "${var.vault_db_name}"
  instance  = "${google_sql_database_instance.master.name}"
  charset   = "latin1"
  collation = "latin1_swedish_ci"
}

# Create MySQL user
resource "google_sql_user" "vault" {
  depends_on = [ "google_sql_database_instance.master" ]
  name     = "${var.vault_db_user}"
  instance = "${google_sql_database_instance.master.name}"
  host     = "%"
  password = "${random_string.mysql_password.result}"
}

#====================== Vault Service ======================
resource "google_service_account" "vault" {
  account_id   = "${var.service_account_vault}"
  display_name = "Vault account for ${var.project}"
}

#====================== KMS ======================
# https://learn.hashicorp.com/vault/operations/autounseal-gcp-kms
# Enable KMS API
resource "google_project_service" "cloudkms_api" {
  disable_on_destroy = false
  service = "cloudkms.googleapis.com"
}

# Create a KMS key ring
resource "google_kms_key_ring" "vault" {
  depends_on = ["google_project_service.cloudkms_api"]
  project = "${var.project}"
  name     = "${var.key_ring}"
  location = "${var.region}"
}

# Create a crypto key for the key ring
resource "google_kms_crypto_key" "vault" {
  name            = "${var.crypto_key}"
  key_ring        = "${google_kms_key_ring.vault.self_link}"
  rotation_period = "86400s"
}

# Add the service account to the Keyring
resource "google_kms_key_ring_iam_binding" "vault_iam_kms_binding" {
  depends_on = [
    "google_service_account.vault",
    "google_kms_key_ring.vault"
  ]
   key_ring_id = "${google_kms_key_ring.vault.id}"
   role = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

   members = [
     "serviceAccount:${google_service_account.vault.email}",
   ]
}


#====================== Vault VM ======================
resource "google_compute_firewall" "vault" {
  name    = "vault"
  network = "${var.network}"
  project = "${var.project}"

  allow {
    protocol = "tcp"
    ports    = ["8200"]
  }
}

resource "google_compute_instance" "vm" {
  depends_on = [
    "google_compute_firewall.vault",
    "google_kms_key_ring.vault",
    "google_kms_crypto_key.vault",
    "google_sql_database_instance.master",
    "google_sql_database.vault",
    "google_sql_user.vault"
  ]

  name = "vm-${var.product_name}-${var.environment}-${var.purpose}-${var.region}-${var.zone}"
  machine_type = "${var.machine_type}"
  count = "${var.count}"
  project = "${var.project}"

  boot_disk {
    initialize_params {
      image = "${var.image}"
    }
  }

  service_account {
    email = "${google_service_account.vault.email}"
    scopes = ["cloud-platform"]
  }

  zone = "${var.region}-${var.zone}"
  metadata = {
    Product = "${var.product_name}"
    Project = "${var.product_name}"
    Component = "${var.purpose}"
    Deployment = "${var.environment}"
    docker_repo = "${var.docker_repo}"
    docker_username = "${var.docker_username}"
    docker_password = "${var.docker_password}"
    docker_image = "${var.docker_image}"
    vault_db_user = "${var.vault_db_user}"
    vault_db_pass = "${random_string.mysql_password.result}"
    vault_db_host = "${google_sql_database_instance.master.private_ip_address}"
    vault_db_name = "${var.vault_db_name}"
    region = "${var.region}"
    key_ring = "${google_kms_key_ring.vault.name}"
    crypto_key = "${google_kms_crypto_key.vault.name}"
  }

  metadata_startup_script = "${file("./startup-script.sh")}"

  tags = ["${google_compute_firewall.vault.name}"]

  network_interface {
    subnetwork       = "${var.subnetwork}"
    subnetwork_project       = "${var.project}"
    access_config = {
    }
  }
}
