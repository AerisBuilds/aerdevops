#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function upgrade_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get upgrade -y
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            apt-get autoremove -y
            log_message "upgrade_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "upgrade_packages(): Error"
            exit 1
        done

        log_message "upgrade_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "upgrade_packages(): Done"
}

function install_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get install -y ${*}
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            log_message "install_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "install_packages(): Error"
            exit 1
        done

        log_message "install_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "install_packages(): Done"
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_dependencies() {
    install_packages \
        jq \
        rsync \
        git \
        python \
        python-pip \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    log_message "install_dependencies(): Done"
}
function install_docker() {
    # Install docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    install_packages docker-ce docker-ce-cli containerd.io

    pip install docker-compose
    if [[ $? -ne 0 ]]; then
        log_message "install_docker(): Error trying to install docker-compose"
        exit 1
    fi

    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function configure_docker_repo() {
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_USERNAME=$(echo ${METADATA} | jq -r .docker_username)
    DOCKER_PASSWORD=$(echo ${METADATA} | jq -r .docker_password)
    DOCKER_REPO=$(echo ${METADATA} | jq -r .docker_repo)
    docker login -u "${DOCKER_USERNAME}" -p "${DOCKER_PASSWORD}" "${DOCKER_REPO}"
    if [[ $? -ne 0 ]]; then
        log_message "configure_docker_repo(): Error"
        exit 1
    else
        log_message "configure_docker_repo(): Done"
    fi
}

function disable_startup_scripts() {
    systemctl disable google-startup-scripts.service
    systemctl disable google-shutdown-scripts.service
}

function deploy_vault() {
    # Create docker directory
    mkdir /opt/vault

    # Get instance metadata
    IP=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip")
    PROJECT=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/project/project-id")
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)
    VAULT_DB_USER=$(echo ${METADATA} | jq -r .vault_db_user)
    VAULT_DB_PASS=$(echo ${METADATA} | jq -r .vault_db_pass)
    VAULT_DB_HOST=$(echo ${METADATA} | jq -r .vault_db_host)
    VAULT_DB_NAME=$(echo ${METADATA} | jq -r .vault_db_name)
    REGION=$(echo ${METADATA} | jq -r .region)
    KEY_RING=$(echo ${METADATA} | jq -r .key_ring)
    CRYPTO_KEY=$(echo ${METADATA} | jq -r .crypto_key)

cat <<END > /opt/vault/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: vault-server
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    ports:
      - 8200:8200
    volumes:
      - /vault:/vault
    command:
      - server
    environment:
      - VAULT_ADDR=http://127.0.0.1:8200
    cap_add:
      - IPC_LOCK
END

    # Create Vault directory
    mkdir /vault

    # Create vault config
    mkdir /vault/config
cat <<END > /vault/config/config.hcl
listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}

storage "mysql" {
  username = "${VAULT_DB_USER}"
  password = "${VAULT_DB_PASS}"
  address = "${VAULT_DB_HOST}"
  database = "${VAULT_DB_NAME}"
  ha_enabled = "true"
}

api_addr = "http://${IP}:8200"

seal "gcpckms" {
  project     = "${PROJECT}"
  region      = "${REGION}"
  key_ring    = "${KEY_RING}"
  crypto_key  = "${CRYPTO_KEY}"
}

max_lease_tt = "10h"
default_lease_ttl = "10h"
ui = "true"
END

    # Start docker container
    cd /opt/vault
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_terraform(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_terraform(): Done"
    fi
    log_message "deploy_vault(): Done"

    # Initialize Vault
    sleep 30
    docker exec vault-server vault operator init
    log_message "vault(): Done"
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

upgrade_packages

install_dependencies

configure_ulimits

install_docker

configure_docker_repo

deploy_vault

# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
