variable "project" {}
variable "product_name" {}
variable "environment" {}
variable "purpose" {}
variable "machine_type" {}
variable "region" {}
variable "zone" {}
variable "count" { default = "1" }
variable "image" {}
variable "network" {}
variable "subnetwork" {}
variable "docker_username" {}
variable "docker_password" {}
variable "docker_repo" {}
variable "docker_image" {}
variable "key_ring" {}
variable "crypto_key" {}

# CloudSQL
variable "cloudsql_host" {}
variable "cloudsql_tier" {}
variable "vault_db_name" {}
variable "vault_db_user" {}
variable "cloudsql_disk_size" {}
variable "service_account_vault" {}