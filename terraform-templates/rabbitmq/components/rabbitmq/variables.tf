variable "project" {}
variable "productname" {}
variable "environment" {}
variable "rabbitmq_service_account" {}
variable "rabbitmq_purpose" {}
variable "rabbitmq_subnetwork" {}
variable "rabbitmq_subnetwork_project" {}
variable "rabbitmq_image" {}
variable "rabbitmq_node_settings" {
  type = "map"
}
