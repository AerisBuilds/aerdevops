module "rabbitmq" {
  source = "./components/rabbitmq"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  rabbitmq_node_settings = "${var.rabbitmq_node_settings}"
  rabbitmq_service_account = "${var.rabbitmq_service_account}"
  rabbitmq_purpose = "${var.rabbitmq_purpose}"
  rabbitmq_subnetwork = "${var.rabbitmq_subnetwork}"
  rabbitmq_subnetwork_project = "${var.rabbitmq_subnetwork_project}"
  rabbitmq_image = "${var.rabbitmq_image}"
}
