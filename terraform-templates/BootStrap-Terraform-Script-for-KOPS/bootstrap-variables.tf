#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#

variable "region" {
default = "us-west-2"
description = "AWS region"
}

variable "count" {
     default     = "1"
    description = "Number of EC2 instances to deploy"
}

variable "name_prefix" {
    default     = "xxxxxxxxx"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "ami-08d7305a73c22d760"
    description = "Instance AMI ID"
}

variable "key_name" {
default = "xxxxxxxx"
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "t2.large"
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "100"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "vpc-xxxxxx"
   description = "VPC id"
}


variable "subnet_ec2ids" {
    type        = "string"
    default     = "subnet-xxxxxxxxxxx"
    description = "Subnet range"
}

variable "availability_zones" {
    default     = "us-west-2a"
    description = "Availability zones to place subnets"
}

variable "deployment" {
    default     = "DEV"
    description = "Deployment tag value"
}

variable "component" {
    default     = "Orchestration"
    description = "component tag value"
}

variable "project" {
    default     = "AMP"
    description = "project tag value"
}

variable "owner" {
default = "Fagun Tripathi"
    description = "fagun.tripathi@aeris.net"
}

variable "product" {
default = "Automotive"
    description = "product tag value"
}

variable "aws_security_group" {
        default = "sg-xxxxxxxxxx"
        description = "Security Group"
}

variable "username" {
        default = "xxxxxxx"
        description = "Git Username"
}

variable "password" {
        default = "xxxxxxx"
        description = "Git Password"
}
