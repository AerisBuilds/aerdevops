#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#

provider "aws" {
 region = "${var.region}"
}

resource "aws_instance" "bootstrap" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
                                 #!/bin/bash
                                 sudo yum -y install docker
                                 sleep 5
                                 sudo service docker start
                                 sleep 5
                                 sudo yum install git -y
                                 sleep 2
                                 sudo mkdir -p /opt/k8s; sudo chmod 777 /opt/k8s
                                 sleep 2
                                 cd /opt/k8s
                                 sudo git clone https://${var.username}:${var.password}@bitbucket.org/aeriscom/amp-capsule-infrastructure.git
                                 sleep 2
                                 cd /opt/k8s/amp-capsule-infrastructure/boostrap
                                 sudo docker build . -t k8s_kops_env:1.0
                                 sleep 5
                                 sudo docker run -it -d k8s_kops_env:1.0
                                 EOF
    subnet_id                   = "${element(split(",", var.subnet_ec2ids), count.index)}"
    vpc_security_group_ids = ["${var.aws_security_group}"]

    tags {
        Name = "${var.name_prefix}-${count.index}"
        Project = "${var.project}"
        Component = "${var.component}"
        Deployment = "${var.deployment}"
        Owner = "${var.owner}"
        Product = "${var.product}"
    }

    root_block_device {
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }
}