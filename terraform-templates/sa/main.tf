resource "google_service_account" "default" {
  account_id   = "${var.service_account_id}"
  display_name = "Default service account"
  project = "${var.project}"
}

resource "google_service_account_key" "default" {
  service_account_id = "${google_service_account.default.name}"
  public_key_type = "TYPE_X509_PEM_FILE"
}

resource "null_resource" "delay" {
  provisioner "local-exec" {
    command = "sleep 120"
  }
  triggers = {
    before = "${google_service_account.default.id}"
  }
}

resource "google_project_iam_member" "default" {
  depends_on = ["null_resource.delay"]
  count      = "${length(var.sa_roles)}"
  project    = "${var.project}"
  role       = "roles/${element(var.sa_roles, count.index)}"
  member     = "serviceAccount:${var.service_account}"
}

resource "google_project_iam_member" "default_tools" {
  count      = "${length(var.sa_tools_roles)}"
  project    = "${var.project}"
  role       = "roles/${element(var.sa_tools_roles, count.index)}"
  member     = "serviceAccount:${var.sa_tools}"
}

resource "google_project_iam_member" "default_devops" {
  count      = "${length(var.sa_devops_roles)}"
  project    = "${var.project}"
  role       = "roles/${element(var.sa_devops_roles, count.index)}"
  member     = "group:${var.sa_devops_email}"
}

resource "google_project_iam_member" "default_gcr" {
  count      = "${length(var.sa_gcr_roles)}"
  project    = "${var.sa_images_project}"
  role       = "roles/${element(var.sa_gcr_roles, count.index)}"
  member     = "serviceAccount:${var.service_account}"
}

resource "google_project_iam_member" "default_images" {
  count      = "${length(var.sa_images_roles)}"
  project    = "${var.sa_images_project}"
  role       = "roles/${element(var.sa_images_roles, count.index)}"
  member     = "serviceAccount:${var.service_account}"
}

resource "google_project_iam_member" "default_bucket" {
  count      = "${length(var.sa_gcr_roles)}"
  project    = "${var.sa_tools_project}"
  role       = "roles/${element(var.sa_gcr_roles, count.index)}"
  member     = "serviceAccount:${var.service_account}"
}

resource "google_project_iam_member" "default_network" {
  count      = "${length(var.sa_network_roles)}"
  project    = "${var.shared_vpc_project_id}"
  role       = "roles/${element(var.sa_network_roles, count.index)}"
  member     = "serviceAccount:${var.service_account}"
}

output "service_account_credentials" {
  value = "${base64decode(google_service_account_key.default.private_key)}"
  sensitive = "true"
}

resource "google_service_account" "additional_sa" {
  count    = "${length(var.sa_additional_accounts)}"
  account_id   = "${element(var.sa_additional_accounts, count.index)}"
  display_name = "${element(var.sa_additional_accounts, count.index)}"
  project = "${var.project}"
}

resource "null_resource" "delay_additional_sa" {
  count = "${length(var.sa_additional_accounts)}"
  provisioner "local-exec" {
    command = "sleep 60"
  }
  triggers = {
    before = "${google_service_account.additional_sa[count.index].id}"
  }
}

resource "google_project_iam_member" "additional_sa_roles" {
  count      = "${length(var.sa_additional_roles)}"
  project    = "${var.project}"
  role       = "roles/${lookup(var.sa_additional_roles[count.index], "role")}"
  member     = "serviceAccount:${lookup(var.sa_additional_roles[count.index], "sa")}@${var.project}.iam.gserviceaccount.com"
}
