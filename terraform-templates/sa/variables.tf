variable "project" {
	type = "string"
	description = "GCP Project Name"
}
variable "project_name" {
	type = "string"
	description = "GCP Project Name"
}
variable "region" {
	type = "string"
	description = "Region name"
}
variable "service_account" {
	type = "string"
	description = "Service Account Name"
}
variable "service_account_id" {
	type = "string"
	description = "Service Account ID"
}

variable "sa_roles" {
  type        = "list"
  description = "list of roles to be assigned to service account"
}
variable "sa_tools" {
  type        = "string"
  description = "Service account from tools project"
}
variable "sa_tools_project" {
  type        = "string"
  description = "Project id for tools project"
}
variable "sa_tools_roles" {
  type        = "list"
  description = "list of roles to be assigned to service account from tools project"
}
variable "sa_devops_email" {
  type        = "string"
  description = "Email of the devops group"
}
variable "sa_devops_roles" {
  type        = "list"
  description = "list of roles to be assigned to devops group"
}
variable "sa_gcr_roles" {
  type        = "list"
  description = "list of roles to be assigned to allow pull docker images from GCR on tools project"
}
variable "sa_images_project" {
  type        = "string"
  description = "Project id where we have our images to use"
}
variable "sa_images_roles" {
  type        = "list"
  description = "list of roles to be assigned to allow pull OS images"
}
variable "sa_network_roles" {
  type        = "list"
  description = "list of roles to be assigned to allow use shared VPC"
}
variable "shared_vpc_project_id" {
  type        = "string"
  description = "project id where shared vpc resides to grant correct permissions"
}
variable "sa_additional_accounts" {
  type        = "list"
  description = "list of additional service accounts"
}
variable "sa_additional_roles" {
  type        = "list"
  description = "list of roles to be assigned to additional service accounts"
}
