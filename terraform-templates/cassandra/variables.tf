variable "project" {}
variable "environment" {}
variable "product_name" {}
variable "cassandra_service_account" {}
variable "cassandra_purpose" {}
variable "cassandra_subnetwork" {}
variable "cassandra_subnetwork_project" {}
variable "cassandra_image" {}
variable "cassandra_node_settings" {
  type = "map"
}
