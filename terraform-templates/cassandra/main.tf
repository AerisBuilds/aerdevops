module "cassandra" {
  source = "./components/cassandra"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  cassandra_node_settings = "${var.cassandra_node_settings}"
  cassandra_service_account = "${var.cassandra_service_account}"
  cassandra_purpose = "${var.cassandra_purpose}"
  cassandra_subnetwork = "${var.cassandra_subnetwork}"
  cassandra_subnetwork_project = "${var.cassandra_subnetwork_project}"
  cassandra_image = "${var.cassandra_image}"
}
