/*
 * Singapore TCP load balancer script - 3/7/2019, jeff.howe@aeris.net
 * Deploys a TCP load-balancer
 *
 * README - edit the project name into the 'project', 'firewall', the
 * correct region and zone values, and
 * firewall rule tag name in 'target_tag'm, and port. 
 * alternatively, comment out the 'default' lines for these variables if
 * you would like the script to prompt for input.
 */


provider "google" {
        credentials = "${file("aerismainaccount.json")}"
        project = "amp-solutions-qualdev-201903"
        region  = "us-west1"
        zone    = "us-west1-a"
}

variable project {
  description = "The project to deploy to, if not set the default provider project is used."
  default     = "amp-solutions-qualdev-201903"
}

variable region {
  description = "Region for cloud resources."
  default     = "us-west1"
}

variable network {
  description = "Name of the network to create resources in."
  default     = "default"   
}

variable firewall_project {
  description = "Name of the project to create the firewall rule in. Useful for shared VPC. Default is var.project."
  default     = "amp-solutions-qualdev-201903"
}

variable name {
  description = "Name for the forwarding rule and prefix for supporting resources."
  default     = ""
}

variable service_port {
  description = "TCP port your service is listening on."
  default     = "41234"
}

variable target_tags {
  description = "List of target tags to allow traffic using firewall rule."
  type        = ""
  default     = ""
}

variable session_affinity {
  description = "How to distribute load. Options are `NONE`, `CLIENT_IP` and `CLIENT_IP_PROTO`"
  default     = "NONE"
}

variable protocol {
  description = "transport layer protocol, TCP or UDP"
  default     = "TCP"
}

variable source_ranges {
  description = "Source request IP range, ex 0.0.0.0/0"
  default     = "0.0.0.0/0"
}
