Creates a generic TCP load balancer, firewall rule, and health check.

Make sure to make the necessary changes to the variables.tf file.  This includes the project name ,
region, zone, firewall target tag, and port with the deployment requirements.

The file aerismainaccount.json file must be included and referenced in the variables.tf file. 
