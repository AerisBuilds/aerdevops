resource "google_compute_instance" "default" {
  name         = "vm-${var.productname}-${var.environment}-${var.sonarqube_purpose}-${var.region}-1"
  machine_type = "${var.sonarqube_machine_type}"
  project = "${var.project}"
  zone = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.image}"
      size = "${var.sonarqube_disk_size}"
    }
  }

  
  metadata = {
    Product = "${var.productname}"
    Project = "${var.productname}"
    Component = "${var.sonarqube_purpose}"
    Deployment = "${var.environment}"
    docker_repo = "${var.docker_repo}"
    docker_user = "${var.docker_user}"
    docker_password = "${var.docker_password}"
    docker_image = "${var.sonarqube_docker_image}"
    sonar_db_name = "${var.sonarqube_db_name}"
    sonar_user = "${var.sonarqube_db_user}"
    sonar_pass = "${module.sonarqube_db.db_password}"
    sonar_db_url = "${module.sonarqube_db.db_private_ip}"
  }
  
  metadata_startup_script = "${file("./startup-script.sh")}"

  tags = ["${google_compute_firewall.default.name}"]

  network_interface {
    # A default network is created for all GCP projects
    subnetwork       = "${var.subnetwork}"
    subnetwork_project       = "${var.project}"
    access_config {
    }
  }
}

resource "google_compute_instance_group" "default" {
  name = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project = "${var.project}"
  zone = "${var.zone}"
  instances = [ "${google_compute_instance.default.self_link}" ]
  named_port {
    name = "http"
    port = "${var.sonarqube_port}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_backend_service" "default" {
  name      = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project   = "${var.project}"
  port_name = "http"
  protocol  = "HTTP"

  backend {
    group = "${google_compute_instance_group.default.self_link}"
  }

  health_checks = [
    "${google_compute_health_check.default.self_link}",
  ]
}

resource "google_compute_health_check" "default" {
  name         = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project      = "${var.project}"
  tcp_health_check {
    port       = "${var.sonarqube_port}"
  }
}

resource "google_compute_target_https_proxy" "default" {
  name             = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project          = "${var.project}"
  url_map          = "${google_compute_url_map.default.self_link}"
  ssl_certificates = ["projects/${var.project}/global/sslCertificates/${var.ssl_cert_name}"]
}


resource "google_compute_url_map" "default" {
  name            = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project         = "${var.project}"
  default_service = "${google_compute_backend_service.default.self_link}"
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project    = "${var.project}"
  target     = "${google_compute_target_https_proxy.default.self_link}"
  port_range = "443"
  ip_address = "${google_compute_global_address.default.address}"
}

resource "google_compute_global_address" "default" {
  name = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project    = "${var.project}"
}

resource "google_dns_record_set" "default" {
  name = "${var.sonarqube_subdomain}.${var.domain}."
  project    = "${var.project}"
  type = "A"
  ttl  = 300
  managed_zone = "${var.dns_zone_name}"
  rrdatas = ["${google_compute_global_address.default.address}"]
}

resource "google_compute_firewall" "default" {
  name    = "hlb-${var.productname}-${var.sonarqube_purpose}-${var.region}"
  project = "${var.project}"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["9000","9002"]
  }
}

provider "google-beta"{
  region = "${var.region}"
  zone   = "${var.region}-${var.zone}"
  project = "${var.project}"
  version = "~> 2.5"
}

module "sonarqube_db" {
  source = "./sql"
  cloudsql_host = "${var.sonarqube_cloudsql_host}"
  region = "${var.region}"
  cloudsql_disk_size = "${var.sonarqube_cloudsql_disk_size}"
  project = "${var.project}"
  network = "${var.network}"
  cloudsql_tier = "${var.sonarqube_cloudsql_tier}"
  db_name = "${var.sonarqube_db_name}"
  db_user = "${var.sonarqube_db_user}"
}

output "instance_id" {
 value = "${google_compute_instance.default.self_link}"
}
