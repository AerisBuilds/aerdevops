#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1
export PROJECT=$(grep "^gcp_project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=$(grep "^sonarqube_purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^tf_bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')


out=$(gcloud services vpc-peerings list --network=${NETWORK} --project=${PROJECT} --service=servicenetworking.googleapis.com --format=json | jq '.[] | select(.service=="services/servicenetworking.googleapis.com") and select(.peering=="cloudsql-mysql-googleapis-com")')

if [ "$out" == "true" ]; then
    echo "VPC prerequisites already created"
else
    export ZONE=$(grep "^zone=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
    export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
   
    ./prerequisites/create.sh ${BUCKET} ${NETWORK} ${PROJECT} ${ZONE} ${REGION}
fi




terraform init -backend-config="bucket=${BUCKET}" -backend-config="prefix=${PURPOSE}"

terraform apply -var-file=$1 -var project="${PROJECT}"
