#!/bin/bash
#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function upgrade_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get upgrade -y
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            apt-get autoremove -y
            log_message "upgrade_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "upgrade_packages(): Error"
            exit 1
        done

        log_message "upgrade_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "upgrade_packages(): Done"
}

function install_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get install -y ${*}
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            log_message "install_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "install_packages(): Error"
            exit 1
        done

        log_message "install_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "install_packages(): Done"
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_dependencies() {
    install_packages \
        jq \
        rsync \
        git \
        python \
        python-pip \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    log_message "install_dependencies(): Done"
}
function install_docker() {
    # Install docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    install_packages docker-ce docker-ce-cli containerd.io

    pip install docker-compose
    if [[ $? -ne 0 ]]; then
        log_message "install_docker(): Error trying to install docker-compose"
        exit 1
    fi

    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function configure_docker_repo() {
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_USER=$(echo ${METADATA} | jq -r .docker_user)
    DOCKER_PASSWORD=$(echo ${METADATA} | jq -r .docker_password)
    DOCKER_REPO=$(echo ${METADATA} | jq -r .docker_repo)
    docker login -u "${DOCKER_USER}" -p "${DOCKER_PASSWORD}" "${DOCKER_REPO}"
    if [[ $? -ne 0 ]]; then
        log_message "configure_docker_repo(): Error"
        exit 1
    else
        log_message "configure_docker_repo(): Done"
    fi
}

function disable_startup_scripts() {
    systemctl disable google-startup-scripts.service
    systemctl disable google-shutdown-scripts.service
}



function deploy_sonarqube() {
    # Create sonarqube directories
    mkdir -p /opt/sonarqube
    mkdir -p /opt/sonarqube_db
    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)
    SONAR_DB_NAME=$(echo ${METADATA} | jq -r .sonar_db_name)
    SONAR_USER=$(echo ${METADATA} | jq -r .sonar_user)
    SONAR_PASS=$(echo ${METADATA} | jq -r .sonar_pass)
    SONAR_DB_URL=$(echo ${METADATA} | jq -r .sonar_db_url)

cat <<END > /opt/sonarqube/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: sonarqube
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    ports:
      - 9000:9000
      - 9092:9092
    environment:      
      - SONARQUBE_JDBC_URL=jdbc:mysql://${SONAR_DB_URL}:3306/${SONAR_DB_NAME}?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance&useSSL=false
      - SONARQUBE_JDBC_USERNAME=${SONAR_USER}
      - SONARQUBE_JDBC_PASSWORD=${SONAR_PASS}

END
    # Start docker container
    cd /opt/sonarqube
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_sonarqube(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_sonarqube(): Done"
    fi
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

upgrade_packages

install_dependencies

configure_ulimits

install_docker

configure_docker_repo

deploy_sonarqube

# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
