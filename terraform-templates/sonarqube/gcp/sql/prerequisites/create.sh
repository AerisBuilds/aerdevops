#!/bin/bash
cd "$(dirname "$0")"

terraform init -backend-config="bucket=$1" -backend-config="prefix=cloudsql/prerequisites"
terraform plan -out=csql_prerequisites.tfplan -var network="$2" -var project="$3" -var region="$4" -var zone="$5"
terraform apply -input=false csql_prerequisites.tfplan
rm csql_prerequisites.tfplan