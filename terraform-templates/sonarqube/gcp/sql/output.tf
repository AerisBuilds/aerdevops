output "db_private_ip" {
//  value = "${google_container_cluster.cluster.instance_group_urls}"
  value = "${google_sql_database_instance.master.private_ip_address}"
}

output "db_password" {
//  value = "${google_container_cluster.cluster.instance_group_urls}"
  value = "${random_string.mysql_password.result}"
}