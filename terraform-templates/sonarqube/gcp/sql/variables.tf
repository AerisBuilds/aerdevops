
variable "cloudsql_host" {}
variable "region" {} 
variable "cloudsql_disk_size" {}
variable "project" {}
variable "network" {}
variable "cloudsql_tier" {}
variable "db_name" {}
variable "db_user" {}
