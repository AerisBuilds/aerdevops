







# Create MySQL Instances
resource "google_sql_database_instance" "master" {
  provider = "google-beta"


  name = "${var.cloudsql_host}-a"
  region = "${var.region}"
  database_version = "MYSQL_5_7"

  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = "ZONAL"
    replication_type = "SYNCHRONOUS"
    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    
    }

    location_preference {
      zone = "${var.region}-a"
    }

    backup_configuration {
      enabled = "true"
      start_time = "03:00"
      binary_log_enabled = true
    }
  }
}

resource "google_sql_database_instance" "replica" {
  provider = "google-beta"
  depends_on = [
    "google_sql_database_instance.master",
    "google_sql_database.database",
    "google_sql_user.db_user"
  ]

  name = "${var.cloudsql_host}-b"
  region = "${var.region}"
  database_version = "MYSQL_5_7"
  master_instance_name = "${google_sql_database_instance.master.name}"

  replica_configuration {
    connect_retry_interval = 60
    failover_target = true
  }

  settings {
    tier = "${var.cloudsql_tier}"
    disk_size = "${var.cloudsql_disk_size}"
    disk_type = "PD_SSD"
    availability_type = "ZONAL"
    replication_type = "SYNCHRONOUS"
    crash_safe_replication = true

    ip_configuration {
      ipv4_enabled = false
      private_network = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/${var.network}"
    }

    location_preference {
      zone = "${var.region}-b"
    }
  }
}

# Create MySQL password
resource "random_string" "mysql_password" {
  length = 64
  special = false
}

# Create MySQL database
resource "google_sql_database" "database" {
  depends_on = [ "google_sql_database_instance.master" ]
  name      = "${var.db_name}"
  project = "${var.project}"
  instance  = "${google_sql_database_instance.master.name}"
  charset   = "latin1"
  collation = "latin1_swedish_ci"
}

# Create MySQL user
resource "google_sql_user" "db_user" {
  depends_on = [ "google_sql_database_instance.master" ]
  project = "${var.project}"
  name     = "${var.db_user}"
  instance = "${google_sql_database_instance.master.name}"
  host     = "%"
  password = "${random_string.mysql_password.result}"
}
