#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#
resource "aws_instance" "sonar_instance" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
				 #!/bin/bash
                    		 sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
				 sudo yum install git
                    		 sudo service docker start
                    		 sleep 2
				 sudo docker login -u atspdeployer -p "${var.nexus_passwd}" http://repo.aeriscloud.com:6666
				 sudo docker pull repo.aeriscloud.com:6666/sonarqube:001
				 sudo docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 \
			         -e SONARQUBE_JDBC_USERNAME="${var.db_user}" \
                                 -e SONARQUBE_JDBC_PASSWORD="${var.db_passwd}" \
                                 -e SONARQUBE_JDBC_URL=jdbc:postgresql://"${var.db_endpoint}" \
                                 sonarqube
				 EOF

    subnet_id                   = "subnet-ac8b53db"

    vpc_security_group_ids = ["${var.aws_security_group}"]

    tags {
        Name = "${var.name_prefix}"
        Deployment = "Dev"
        Component = "Codequality"
        Project = "AMP"
    }

    root_block_device {
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }
}
