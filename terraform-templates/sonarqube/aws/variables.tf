#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#
variable "region" {
    default     = "us-west-2"
    description = "The region of AWS, for AMI lookups"
}

variable "count" {
    default     = "1"
    description = "Number of HA servers to deploy"
}

variable "name_prefix" {
    default     = "automotive-sonarqube"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "ami-5ec1673e"
    description = "Instance AMI ID"
}

variable "key_name" {
    default = "ampdeveloper"
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "t2.medium" # RAM Requirements >= 8gb
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "50"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "vpc-54907731"
   description = "VPC id"
}

variable "aws_security_group" {
    default     = "sg-f737028d"
    description = "Subnet in CIDR format to assign to VPC"
}

variable "vpc_cidr" {
    default     = "10.6.0.0/16"
    description = "Subnet in CIDR format to assign to VPC"
}

variable "db_user" {
    default     = ""
    description = "Username for database"
}
variable "db_passwd" {
    default     = ""
    description = "database password"
}

variable "db_endpoint" {
    default     = ""
    description = "Include the port number and database name"
}


variable "subnet_ec2ids" {
    type        = "string"
    default     = "subnet-ac8b53db,subnet-0b40736f,subnet-10d3e656"
    description = "Subnet ranges (requires 3 entries)"
}

variable "nexus_passwd" {
	default = "AtspDeployer20"
	description = "Nexus repo password"
}
