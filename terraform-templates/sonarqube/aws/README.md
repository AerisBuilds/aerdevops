# Instructions to run Sonarqube Master

  - Include your aws access key and secret key in aws.tf
  - The terraform script pulls the Sonarqube image form Nexus repo.
  - Include the nexus password, private key and other aws properties in the [var file](https://bitbucket.org/aeriscom/automotive-devops/src/d451c13bf80f589ad92613b6b454feb4d0851b85/terraform-templates/sonarqube/variables.tf?at=master&fileviewer=file-view-default)
