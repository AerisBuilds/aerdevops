#!/bin/bash
cd "$(dirname "$0")"

function check_return_code() {
    RETURN_CODE=$1
    if [[ "${RETURN_CODE}" -ne 0 ]]; then
        echo -e "\e[31mError:\e[0m The last command finished with error."
        exit 1
    fi
}

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ${0} path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ${0} ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PURPOSE=$(grep "^waf_purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

terraform init -backend-config=bucket=${BUCKET} -backend-config=prefix=${PURPOSE}
check_return_code $?

terraform plan -out=${PROJECT}_waf.tfplan -var-file=$1 -var purpose="${PURPOSE}"
check_return_code $?