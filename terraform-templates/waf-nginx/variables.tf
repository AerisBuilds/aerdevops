variable "project" {}
variable "product_name" {}
variable "environment" {}
variable "purpose" {}
variable "waf_machine_type" {}
variable "region" {}
variable "network" {}
variable "bucket" {}
variable "waf_bucket" {}
variable "waf_disk_size" {}

variable "waf_template" {}

variable "waf_image" {}

variable "waf_docker_image" {}

variable "waf_subnetwork" {}

variable "waf_instance_group" {}

variable "waf_healthcheck" {}

variable "waf_loadbalancer" {}

variable "waf_min_instances" {}

variable "waf_max_instances" {}

variable "waf_config_path" {}

variable "waf_ruleset" {
  default = "core.rules"
}
variable "waf_certificate" {
  type = "list"
}

variable "waf_endpoint" {}

variable "waf_managed_zone" {}

variable "waf_create_dns" {
  default = "true"
}

# WAF List maximum size 100 element(s)
# Spamhaus DROP List 2019/06/17 - (c) 2019 The Spamhaus Project
# https://www.spamhaus.org/drop/drop.txt
# Last-Modified: Sun, 16 Jun 2019 18:40:03 GMT
variable "waf_blacklist_ips" {
  type = "list"
  default = [
    "1.10.16.0/20",
    "1.19.0.0/16",
    "1.32.128.0/18",
    "2.56.255.0/24",
    "2.58.92.0/22",
    "2.59.16.0/22",
    "2.59.151.0/24",
    "2.59.248.0/22",
    "2.59.252.0/22",
    "5.8.37.0/24",
    "5.101.221.0/24",
    "5.134.128.0/19",
    "5.182.184.0/22",
    "5.182.192.0/22",
  ]
}