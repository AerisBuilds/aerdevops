######################################## Timeouts
proxy_connect_timeout   300;
proxy_send_timeout      300;
proxy_read_timeout      300;
send_timeout            300;

######################################## NGINX configs
underscores_in_headers on;
client_max_body_size 0;

# Allow big server_name has tables
# https://nginx.org/en/docs/http/server_names.html
server_names_hash_bucket_size 128;
server_names_hash_max_size 4096;

######################################## Aerframe WS
server {
  listen 80;
  listen [::]:80;

  server_name api.aerframe.aeris.com api1.aerframe.aeris.com api2.aerframe.aeris.com afwsapi-af-prod.aerjupiter.com;

  location /_GCPWAF/ {
    access_log off;
    log_not_found off;
    root   /usr/share/nginx/html;
    index  index.html;
  }

  location / {
    proxy_pass http://afwsapi-backend-af-prod.aerjupiter.com:31080;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_max_temp_file_size 0;

    #enable naxsi
    SecRulesEnabled;
    #LearningMode;

    LibInjectionSql; #enable libinjection support for SQLI
    LibInjectionXss; #enable libinjection support for XSS

    #the location where naxsi will redirect the request when it is blocked
    DeniedUrl "/_GCPWAF/RequestDenied/index.html";
    CheckRule "$SQL >= 20" BLOCK;

    # Whitelist Rules
    BasicRule wl:11 "mz:$URL_X:^/registration/v2/|BODY" "msg: SRE-469 - Allow unknown content-type";
    BasicRule wl:11 "mz:$URL_X:^/smsmessaging/v2/|BODY" "msg: SRE-469 - Allow unknown content-type";
    BasicRule wl:1008 "mz:$URL_X:^/smsmessaging/v2/|BODY" "msg: SRE-469 - Allow semicolon";

    # Whitelist required for JSON documents
    BasicRule wl:1001,1003,1007,1009,1010,1011,1015,1017 "msg:JUP-1342,SRE-297 - permit JSON files";

    # Allow invalid JSON
    BasicRule wl:15 "msg: Allow invalid JSON";

    # Allow Empty body
    BasicRule wl:16 "msg: Empty body";

    # Allow big files
    BasicRule wl:2 "msg:JUP-1377,JUP-1342,SRE-297 - allow big files";
  }
}

######################################## Aerframe Longpoll
server {
  listen 80;
  listen [::]:80;

  server_name longpoll.aerframe.aeris.com longpoll1.aerframe.aeris.com longpoll2.aerframe.aeris.com aflpapi-af-prod.aerjupiter.com;

  location /_GCPWAF/ {
    access_log off;
    log_not_found off;
    root   /usr/share/nginx/html;
    index  index.html;
  }

  location / {
    proxy_pass http://aflpapi-backend-af-prod.aerjupiter.com:31090;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_max_temp_file_size 0;

    #enable naxsi
    SecRulesEnabled;
    #LearningMode;

    LibInjectionSql; #enable libinjection support for SQLI
    LibInjectionXss; #enable libinjection support for XSS

    #the location where naxsi will redirect the request when it is blocked
    DeniedUrl "/_GCPWAF/RequestDenied/index.html";
    CheckRule "$SQL >= 20" BLOCK;

    # Whitelist Rules

    # Whitelist required for JSON documents
    BasicRule wl:1001,1003,1007,1009,1010,1011,1015,1017 "msg:JUP-1342,SRE-297 - permit JSON files";

    # Allow invalid JSON
    BasicRule wl:15 "msg: Allow invalid JSON";

    # Allow Empty body
    BasicRule wl:16 "msg: Empty body";

    # Allow big files
    BasicRule wl:2 "msg:JUP-1377,JUP-1342,SRE-297 - allow big files";
  }
}
