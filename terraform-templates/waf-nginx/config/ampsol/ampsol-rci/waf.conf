######################################## Timeouts
proxy_connect_timeout   300;
proxy_send_timeout      300;
proxy_read_timeout      300;
send_timeout            300;

######################################## Whitelist
geo $limit {
    default 1;
    13.233.112.96/32 0; # India-Website-Monitor
    52.76.106.165/32 0; # Singapore-Website-Monitor
    35.154.122.224/32 0; # production GKE Cluster
    35.200.246.125/32 0; # production GKE Cluster
    35.200.149.203/32 0; # production GKE Cluster
    10.26.1.49/32 0; # AWS Internal range
    34.83.131.196/32 0; # ampsol-rci GKE cluster
    34.83.48.157/32 0; # ampsol-rci GKE cluster
}

map $limit $limit_key {
    0 "";
    1 $http_x_forwarded_for;
}

######################################## NGINX configs
underscores_in_headers on;

######################################## Webapp
limit_req_zone $limit_key zone=webapp:10m rate=500000r/m;

server {
  listen 80;
  listen [::]:80;

  server_name webserver.ampsol-rci.aerjupiter.com ktprj.ampsol-rci.aerjupiter.com xlfleet.ampsol-rci.aerjupiter.com okinawa.ampsol-rci.aerjupiter.com ncell.ampsol-rci.aerjupiter.com;

  location /_GCPWAF/ {
    access_log off;
    log_not_found off;
    root   /usr/share/nginx/html;
    index  index.html;
  }

  location / {
    # Ratelimit
    limit_req zone=webapp burst=30;

    proxy_pass https://hlb-webapp.ampsol-rci.aerjupiter.com:443;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_max_temp_file_size 0;

    #enable naxsi
    SecRulesEnabled;
    #LearningMode;

    LibInjectionSql; #enable libinjection support for SQLI
    LibInjectionXss; #enable libinjection support for XSS

    #the location where naxsi will redirect the request when it is blocked
    DeniedUrl "/_GCPWAF/RequestDenied/index.html";
    CheckRule "$SQL >= 16" BLOCK;#the action to take when the $SQL score is superior or equal to 8
    CheckRule "$RFI >= 12" BLOCK;
    CheckRule "$TRAVERSAL >= 5" BLOCK;
    CheckRule "$UPLOAD >= 5" BLOCK;
    CheckRule "$XSS >= 8" BLOCK;
    CheckRule "$EVADE >= 4" BLOCK;

    # UnWantedAccess
    CheckRule "$UWA >= 8" BLOCK;

    # Identified Attacks
    CheckRule "$ATTACK >= 8" BLOCK;

    # Whitelist Rules
    BasicRule wl:1002,1007 "mz:$URL_X:^/api/things/accounts/create|$HEADERS_VAR_X:^cookie$" "msg: Account creation";
    BasicRule wl:1001,1015,1205 "mz:$URL_X:^/api/things/accounts/create|BODY" "msg: Account creation";
    BasicRule wl:1001,1002,1015,1205,1310,1311 "mz:$URL_X:^/api/fleet/places|BODY" "msg: Places creation";
    BasicRule wl:1002 "mz:$URL_X:^/api/fleet/places|$HEADERS_VAR_X:^cookie$" "msg: Places creation";
    BasicRule wl:1002,1007 "mz:$URL_X:^/api/fleet/assets/|$HEADERS_VAR_X:^cookie$" "msg: /api/fleet/assets";
    BasicRule wl:1001,1007,1013 "mz:$URL_X:^/api/fleet/assets/|BODY" "msg: /api/fleet/assets";
    BasicRule wl:17,1010 "mz:$URL_X:^/login-app|$ARGS_VAR_X:^x$" "msg: /login-app";
    BasicRule wl:1002,1007 "mz:$URL_X:^/api/v2/users|$HEADERS_VAR_X:^cookie$" "msg: /api/v2/users";
    BasicRule wl:1001,1013 "mz:$URL_X:^/api/v2/users|BODY" "msg: /api/v2/users";

  }
}

######################################## aertrak-api
limit_req_zone $limit_key zone=aertrak-api:10m rate=500000r/m;

server {
  listen 80;
  listen [::]:80;

  server_name pp-api-aertrak.aerisdev.com aertrak-api-backend.ampsol-rci.aerjupiter.com;

  location /_GCPWAF/ {
    access_log off;
    log_not_found off;
    root   /usr/share/nginx/html;
    index  index.html;
  }

  location / {
    # Ratelimit
    limit_req zone=aertrak-api burst=30;

    proxy_pass https://aertrak-api-backend.ampsol-rci.aerjupiter.com:443;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_max_temp_file_size 0;

    #enable naxsi
    SecRulesEnabled;
    #LearningMode;

    LibInjectionSql; #enable libinjection support for SQLI
    LibInjectionXss; #enable libinjection support for XSS

    #the location where naxsi will redirect the request when it is blocked
    DeniedUrl "/_GCPWAF/RequestDenied/index.html";
    CheckRule "$SQL >= 16" BLOCK;#the action to take when the $SQL score is superior or equal to 8
    CheckRule "$RFI >= 12" BLOCK;
    CheckRule "$TRAVERSAL >= 5" BLOCK;
    CheckRule "$UPLOAD >= 5" BLOCK;
    CheckRule "$XSS >= 8" BLOCK;
    CheckRule "$EVADE >= 4" BLOCK;

    # UnWantedAccess
    CheckRule "$UWA >= 8" BLOCK;

    # Identified Attacks
    CheckRule "$ATTACK >= 8" BLOCK;

    # Whitelist Rules

  }
}

######################################## Keycloak
limit_req_zone $limit_key zone=keycloak:10m rate=500000r/m;

server {
  listen 80;
  listen [::]:80;

  server_name auth.ampsol-rci.aerjupiter.com auth-ktprj.ampsol-rci.aerjupiter.com auth-xlfleet.ampsol-rci.aerjupiter.com auth-okinawa.ampsol-rci.aerjupiter.com auth-ncell.ampsol-rci.aerjupiter.com;

  location /_GCPWAF/ {
    access_log off;
    log_not_found off;
    root   /usr/share/nginx/html;
    index  index.html;
  }

  location / {
    # Ratelimit
    limit_req zone=keycloak burst=30;

    proxy_pass https://hlb-keycloak.ampsol-rci.aerjupiter.com:443;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_max_temp_file_size 0;



    #enable naxsi
    SecRulesEnabled;
    #LearningMode;

    LibInjectionSql; #enable libinjection support for SQLI
    LibInjectionXss; #enable libinjection support for XSS

    #the location where naxsi will redirect the request when it is blocked
    DeniedUrl "/_GCPWAF/RequestDenied/index.html";
    CheckRule "$SQL >= 20" BLOCK;#the action to take when the $SQL score is superior or equal to 8
    CheckRule "$RFI >= 10" BLOCK;
    CheckRule "$TRAVERSAL >= 5" BLOCK;
    CheckRule "$UPLOAD >= 5" BLOCK;
    CheckRule "$XSS >= 8" BLOCK;
    CheckRule "$EVADE >= 4" BLOCK;

    # UnWantedAccess
    CheckRule "$UWA >= 8" BLOCK;

    # Identified Attacks
    CheckRule "$ATTACK >= 8" BLOCK;

    # Whitelist Rules
    BasicRule wl:1001,1015,1101,1205 "mz:$URL_X:^/auth/admin/realms/|BODY" "msg: Account creation";
    BasicRule wl:1100,1101,1315 "mz:$URL_X:^/auth/realms/[a-zA-Z1-9-_]+/protocol/openid-connect/auth|$ARGS_VAR_X:^redirect_uri$" "msg: Keycloak login";
    BasicRule wl:1002 "mz:$URL_X:^/auth/realms/[a-zA-Z1-9-_]+/protocol/openid-connect/auth|$HEADERS_VAR_X:^cookie$" "msg: Keycloak login";
  }
}