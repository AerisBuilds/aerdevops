output "waf_ip" {
  value = google_compute_global_address.default.address
}

output "waf_endpoint" {
  value = var.waf_endpoint
}
