terraform {
  required_version = ">= 0.12.0"
}

#====================== Provider config ======================
provider "google" {
  region = var.region
  project = var.project
  version = "~> 2.4"
}

provider "google-beta"{
  region = var.region
  project = var.project
  version = "~> 2.5"
}

provider "template" {
  version = "~> 2.1"
}

#====================== Google API ======================
# Enable required APIs
resource "google_project_service" "logging_api" {
  disable_on_destroy = false
  service = "logging.googleapis.com"
}

#====================== Cloud Storage ======================
resource "google_storage_bucket" "waf_bucket" {
  name = var.waf_bucket
  location = var.region
  storage_class = "REGIONAL"
  force_destroy = "true"

  versioning {
    enabled = "true"
  }
}

#====================== NGINX Config Files ======================
data "template_file" "nginx_conf" {
  template = file("${path.module}/config/nginx/nginx.conf")
}

resource "google_storage_bucket_object" "nginx_conf" {
  name = "nginx.conf"
  content = data.template_file.nginx_conf.rendered
  bucket = google_storage_bucket.waf_bucket.name
}

data "template_file" "waf_conf" {
  template = file("${path.module}/${var.waf_config_path}/waf.conf")
}

resource "google_storage_bucket_object" "waf_conf" {
  name = "conf.d/waf.conf"
  content = data.template_file.waf_conf.rendered
  bucket = google_storage_bucket.waf_bucket.name
}

data "template_file" "naxsi_core_rules" {
  template = file("${path.module}/config/nginx/naxsi.d/${var.waf_ruleset}")
}

resource "google_storage_bucket_object" "naxsi_core_rules" {
  name = "naxsi.d/${var.waf_ruleset}"
  content = data.template_file.naxsi_core_rules.rendered
  bucket = google_storage_bucket.waf_bucket.name
}

#====================== Firewall rules ======================
resource "google_compute_firewall" "default" {
  name = var.waf_healthcheck
  network = var.network
  priority = "1500"
  target_tags = ["waf-endpoint"]
  disabled = "false"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}
#====================== Instance template ======================
resource "google_compute_instance_template" "waf_template" {
  name_prefix = "${var.waf_template}-"
  machine_type = var.waf_machine_type
  region = var.region

  // boot disk
  disk {
    boot = "true"
    disk_size_gb = var.waf_disk_size
    source_image = var.waf_image
  }

  metadata = {
    docker_image = var.waf_docker_image
    waf_bucket = var.waf_bucket
    product_name = var.product_name
    environment = var.environment
  }

  metadata_startup_script = file("${path.module}/startup-script.sh")

  tags = ["waf-endpoint"]

  // networking
  network_interface {
    subnetwork = var.waf_subnetwork

    access_config {
    }
  }

  service_account {
    scopes = ["logging-write", "userinfo-email", "compute-ro", "storage-ro"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

#====================== Instance Group ======================
resource "google_compute_health_check" "default" {
  name = var.waf_healthcheck
  check_interval_sec = "10"
  timeout_sec = 5
  healthy_threshold = "2"
  unhealthy_threshold = "3"

  http_health_check {
    request_path = "/_GCPWAF/healthcheck/"
    port = 80
  }
}

resource "google_compute_region_instance_group_manager" "default" {
  provider = "google-beta"
  name = var.waf_instance_group
  base_instance_name = var.waf_instance_group
  region = var.region
  distribution_policy_zones = ["${var.region}-a", "${var.region}-b"]
  target_size = var.waf_min_instances

  version {
    instance_template = google_compute_instance_template.waf_template.self_link
    name = "waf"
  }

  named_port {
    name = "waf-endpoint"
    port = 80
  }

  auto_healing_policies {
    health_check      = google_compute_health_check.default.self_link
    initial_delay_sec = 200
  }

}

#====================== Autoscaler ======================

resource "google_compute_region_autoscaler" "foobar" {
  provider = "google-beta"
  name = var.waf_instance_group
  target = google_compute_region_instance_group_manager.default.self_link
  region = var.region

  autoscaling_policy {
    max_replicas = var.waf_max_instances
    min_replicas = var.waf_min_instances
    cooldown_period = 200

    cpu_utilization {
      target = 0.7
    }
  }
}

#====================== Load balancer ======================
resource "google_compute_security_policy" "waf-blacklist" {
  name = "waf-blacklist"

  dynamic "rule" {
    for_each = var.waf_blacklist_ips
    content {
      action = "deny(403)"
      priority = rule.key
      match {
        versioned_expr = "SRC_IPS_V1"
        config {
          src_ip_ranges = [rule.value]
        }
      }
      description = "Deny access to IPs in blacklist"
    }
  }
  rule {
    action   = "allow"
    priority = "2147483647"
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = ["*"]
      }
    }
    description = "Default rule, higher priority overrides it	"
  }
}

#====================== Load balancer ======================
resource "google_compute_url_map" "default" {
  name = var.waf_loadbalancer
  default_service = google_compute_backend_service.default.self_link
}

resource "google_compute_target_https_proxy" "default" {
  name = var.waf_loadbalancer
  url_map = google_compute_url_map.default.self_link
  ssl_certificates = var.waf_certificate
}

resource "google_compute_backend_service" "default" {
  name = var.waf_loadbalancer
  port_name = "waf-endpoint"
  protocol = "HTTP"
  timeout_sec = 120
  health_checks = [google_compute_health_check.default.self_link]
  security_policy = google_compute_security_policy.waf-blacklist.self_link

  backend {
    balancing_mode = "UTILIZATION"
    capacity_scaler = "1"
    max_utilization = "0.7"
    group = google_compute_region_instance_group_manager.default.instance_group
  }
}

resource "google_compute_global_forwarding_rule" "default" {
  name = var.waf_loadbalancer
  target = google_compute_target_https_proxy.default.self_link
  port_range = "443"
  ip_address = google_compute_global_address.default.address
}

resource "google_compute_global_address" "default" {
  name = var.waf_loadbalancer
}

resource "google_dns_record_set" "default" {
  count = "${var.waf_create_dns ? 1 : 0}"
  name = "${var.waf_endpoint}."
  type = "A"
  ttl = 60
  managed_zone = var.waf_managed_zone
  rrdatas      = [google_compute_global_address.default.address]
}