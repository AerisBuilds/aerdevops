module "storm" {
  source = "./components/storm"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  storm_node_settings = "${var.storm_node_settings}"
  storm_service_account = "${var.storm_service_account}"
  storm_purpose = "${var.storm_purpose}"
  storm_subnetwork = "${var.storm_subnetwork}"
  storm_subnetwork_project = "${var.storm_subnetwork_project}"
  storm_image = "${var.storm_image}"
}
