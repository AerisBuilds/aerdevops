resource "google_compute_instance" "default" {
  count = "${length(var.storm_node_settings)}"
  name         = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "name" )}"
  machine_type = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "machine_type" )}"
  tags = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "tags" )}"
  project = "${var.project}"
  hostname = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "hostname" )}"
  deletion_protection = true

  boot_disk {
    initialize_params {
      image = "${var.storm_image}"
      size = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "disk_size_root" )}"
    }
  }
  attached_disk {
    source = "${element(google_compute_disk.default.*.self_link, count.index)}"
    device_name = "${element(google_compute_disk.default.*.name, count.index)}"
  }

  zone = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "zone" )}"
  metadata = {
    Product = "${var.productname}"
    Project = "${var.productname}"
    Component = "${var.storm_purpose}"
    Deployment = "${var.environment}"
    Role= "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "role" )}"
  }

  //metadata_startup_script = "${file("./startup-script.sh")}"

  //# tags = ["${google_compute_firewall.jenkins-slave-1.name}"]

  network_interface {
    # A default network is created for all GCP projects
    subnetwork       = "${var.storm_subnetwork}"
    subnetwork_project       = "${var.storm_subnetwork_project}"
  }
  service_account {
    email = "${var.storm_service_account}"
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_disk" "default" {
    count   = "${length(var.storm_node_settings)}"
    project = "${var.project}"
    name    = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "name" )}-1"
    type    = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "disk_size_type" )}"
    zone    = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "zone" )}"
    size    = "${lookup(var.storm_node_settings[element(keys(var.storm_node_settings), count.index)], "disk_size" )}"
}
