variable "project" {}
variable "productname" {}
variable "environment" {}
variable "storm_service_account" {}
variable "storm_purpose" {}
variable "storm_subnetwork" {}
variable "storm_subnetwork_project" {}
variable "storm_image" {}
variable "storm_node_settings" {
  type = "map"
}
