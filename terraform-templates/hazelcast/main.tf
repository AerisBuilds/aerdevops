module "hazelcast" {
  source = "./components/hazelcast"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  hazelcast_node_settings = "${var.hazelcast_node_settings}"
  hazelcast_service_account = "${var.hazelcast_service_account}"
  hazelcast_purpose = "${var.hazelcast_purpose}"
  hazelcast_subnetwork = "${var.hazelcast_subnetwork}"
  hazelcast_subnetwork_project = "${var.hazelcast_subnetwork_project}"
  hazelcast_image = "${var.hazelcast_image}"
}
