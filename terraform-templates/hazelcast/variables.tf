variable "project" {}
variable "environment" {}
variable "product_name" {}
variable "hazelcast_service_account" {}
variable "hazelcast_purpose" {}
variable "hazelcast_subnetwork" {}
variable "hazelcast_subnetwork_project" {}
variable "hazelcast_image" {}
variable "hazelcast_node_settings" {
  type = "map"
}
