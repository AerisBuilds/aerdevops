resource "google_dns_record_set" "frontend" {
  name = "${var.record_name}"
  type = "${var.record_type}"
  ttl  = "${var.record_ttl}"

  managed_zone = "${var.zone_name}"

  rrdatas = "${var.record_data}"
}


variable "record_name" { default = "aertrak.ampsolutions-qualdev.aerjupiter.com"}
variable "record_type" { default = "A" }
variable "record_ttl" { default = 300 }
variable "record_data" { type = "list", default = ["8.8.4.4"] }
variable "zone_name" { default = "ampsolutions-qualdev" }
variable "dns_name" { default = "ampsolutions-qualdev.aerjupiter.com." }