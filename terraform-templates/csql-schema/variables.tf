variable "cloudsql_host" {}
variable "project" {}
variable "db_user" {}
variable "db_name" {}
variable "db_password" {}
variable "zone" {}
variable "region" {}
variable "db_type" { default = ["mysql"] }

