locals {
  charset = "${var.db_type == "postgres" ? "UTF8" : "latin1"}"
  collation = "${var.db_type == "postgres" ? "en_US.UTF8" : "latin1_swedish_ci"}"
}



# Create MySQL database
resource "google_sql_database" "database" {
  name      = "${var.db_name}"
  project = "${var.project}"
  instance  = "${var.cloudsql_host}-a"
  // charset   = "latin1"
  // collation = "latin1_swedish_ci"
  // charset   = "utf8"
  // collation = "utf8_general_ci"
  charset   = local.charset
  collation = local.collation
}

# Create MySQL user
resource "google_sql_user" "db_user" {
  project = "${var.project}"
  name     = "${var.db_user}"
  instance = "${var.cloudsql_host}-a"
  host     = "%"
  password = "${var.db_password}"
}
