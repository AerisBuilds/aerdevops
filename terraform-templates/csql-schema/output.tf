output "db_user" {
  value = "${google_sql_user.db_user.name}"
}

output "db_name" {
  value = "${google_sql_database.database.name}"
}
