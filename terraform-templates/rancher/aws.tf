#------------------------------------------#
# AWS Provider Configuration
#------------------------------------------#
provider "aws" {
    access_key="<Your aws access key>"
    secret_key="Your aws secret key"
    region = "${var.region}"
}
