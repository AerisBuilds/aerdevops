#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#
resource "aws_instance" "rancher_ha" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
				 #!/bin/bash
                    		 sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
                    		 sudo service docker start
                    		 sleep 2
                    		 sudo docker run -d --restart=unless-stopped \
                      		 -p 8080:8080 -p 9345:9345 \
                      		 rancher/server:"${var.rancher_version}" \
                      		 --db-host "${aws_db_instance.rancher_ha.endpoint}" \
                      		 --db-name "${aws_db_instance.rancher_ha.name}" \
                      		 --db-port "${aws_db_instance.rancher_ha.port}" \
                      		 --db-user "${var.db_user}" \
                      		 --db-pass "${var.db_pass}" \
	                         --advertise-address $(ip route get 8.8.8.8 | awk '{print $NF;exit}')
                		 EOF

    subnet_id                   = "${element(split(",", var.subnet_ec2ids), count.index)}"

    vpc_security_group_ids = ["${aws_security_group.rancher_ha.id}"]

    tags {
        Name = "${var.name_prefix}-${count.index}"
    }

    root_block_device {
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }
    depends_on = ["aws_db_instance.rancher_ha"]
}


resource "aws_security_group" "rancher_ha" {
    name        = "${var.name_prefix}-server"
    description = "Rancher HA Server Ports"
    vpc_id      = "${var.aws_vpc_id}"

    ingress {
        from_port = 0
        to_port   = 65535
        protocol  = "tcp"
        self      = true
    }

    ingress {
        from_port = 0
        to_port   = 65535
        protocol  = "udp"
        self      = true
    }

    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 9345
        to_port     = 9345
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
