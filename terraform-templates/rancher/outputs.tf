#------------------------------------------#
# AWS Outputs
#------------------------------------------#
output "elb_http_dns" {
    value = "${aws_elb.rancher_ha_http.dns_name}"
}

