#------------------------------------------#
# RDS Database Configuration
#------------------------------------------#

resource "aws_security_group" "rancher_ha_rds" {
    name        = "${var.name_prefix}-rds-sg"
    description = "Rancher RDS Ports"
    vpc_id      = "${var.aws_vpc_id}"

    ingress {
        from_port   = "3306"
        to_port     = "3306"
        protocol    = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
        }

    egress {
        from_port   = "0"
        to_port     = "0"
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        }
}


resource "aws_db_subnet_group" "rancher_ha_subnet_group" {
  name       = "${var.name_prefix}-subnet-group"
  subnet_ids = ["subnet-9774f9e0", "subnet-e8513e8d", "subnet-96f050cf"]
  tags {
    Name = "${var.name_prefix}-subnet-group"
  }
}

resource "aws_db_instance" "rancher_ha" {
    allocated_storage    = "100"
    storage_type         = "io1"
    iops                 = "5000"
    engine               = "mysql"
    engine_version       = "5.7.19"
    instance_class       = "db.r3.large"
    identifier           = "${var.name_prefix}-db"
    name                 = "${var.db_name}"
    username             = "${var.db_user}"
    password             = "${var.db_pass}"
    port                 = "3306"
    db_subnet_group_name = "${aws_db_subnet_group.rancher_ha_subnet_group.id}"
    parameter_group_name = "default.mysql5.7"
    instance_class       = "db.r3.large"
    multi_az             = "true"
    skip_final_snapshot  = "true"
    vpc_security_group_ids = ["${aws_security_group.rancher_ha_rds.id}"]
}
