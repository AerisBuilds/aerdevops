#------------------------------------------#
# AWS EC2 Configuration
#------------------------------------------#
resource "aws_instance" "<rancher ec2 name>" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_type}"
    key_name                    = "${var.key_name}"
    user_data                   = <<-EOF
				 #!/bin/bash
                    		 sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
                    		 sudo service docker start
                    		 sleep 2
                    		 sudo docker run -d --restart=unless-stopped \
                      		 -p 8080:8080 -p 9345:9345 \
                      		 rancher/server:"${var.rancher_version}" \
                                 --db-host "${aws_rds_cluster.solutions_rancher_ha.endpoint}" \
                                 --db-name "${aws_rds_cluster.solutions_rancher_ha.database_name}" \
                                 --db-port "${aws_rds_cluster.solutions_rancher_ha.port}" \
                                 --db-user "${var.db_user}" \
                                 --db-pass "${var.db_pass}" \
	                         --advertise-address $(ip route get 8.8.8.8 | awk '{print $NF;exit}')
                		 EOF

    subnet_id                   = "${element(split(",", var.subnet_ec2ids), count.index)}"

    vpc_security_group_ids = ["${aws_security_group.solutions_rancher_ha.id}"]

    tags {
        Name = "${var.name_prefix}-${count.index}"
	Component = "Rancher"
	Deployment = "Production"
	Project = "Solutions"
    }

    root_block_device {
        volume_size = "${var.root_volume_size}"
        delete_on_termination = true
    }
    depends_on = ["aws_rds_cluster_instance.solutions_rancher_ha"]
}


resource "aws_security_group" "<new-security-group-name>" {
    name        = "${var.name_prefix}-server"
    description = "Rancher HA Server Ports"
    vpc_id      = "${var.aws_vpc_id}"

    ingress {
        from_port = 0
        to_port   = 65535
        protocol  = "tcp"
        self      = true
    }

    ingress {
        from_port = 0
        to_port   = 65535
        protocol  = "udp"
        self      = true
    }

    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
    }

    ingress {
        from_port   = 9345
        to_port     = 9345
        protocol    = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
