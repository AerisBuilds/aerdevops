#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#
variable "region" {
    default     = "<aws-region-name>"
    description = "The region of AWS, for AMI lookups"
}

variable "count" {
    default     = "2"
    description = "Number of HA servers to deploy"
}

variable "name_prefix" {
    default     = "<resource-prefiX>>"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "<ami-id>"
    description = "Instance AMI ID"
}

variable "key_name" {
    default	= "solutions-india"
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "m4.large" # RAM Requirements >= 8gb
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "60"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "<aws-vpc-id>"
   description = "VPC id in the region for the deployment"
}

variable "vpc_cidr" {
    default     = "<example: 10.26.0.0/16>"
    description = "Subnet in CIDR format to assign to VPC"
}

variable "subnet_cidrs" {
    default     = [<"example: 10.26.4.0/24", "10.26.5.0/24>"]
    description = "Subnet ranges (requires 2 entries)"
}

variable "subnet_ec2ids" {
    type        = "string"
    default     = "<subnet-#1, subnet-@2>"
    description = "Subnet ranges for HA Rancher (requires 2 entries)"
}


variable "availability_zones" {
    default     = [<"ap-south-1b", "ap-south-1a">]
    description = "Availability zones to place subnets"
}

variable "internal_elb" {
    default     = "false"
    description = "Force the ELB to be internal only"
}

#------------------------------------------#
# Database Variables
#------------------------------------------#
variable "db_name" {
    default     = "<databasee-name>>"
    description = "Name of the RDS DB"
}

variable "db_user" {
    default     = "<db username>"
    description = "Username used to connect to the RDS database"
}

variable "db_pass" {
    default	= "<db password>"
    description = "Password used to connect to the RDS database"
}

#------------------------------------------#
# SSL Variables
#------------------------------------------#
variable "enable_https" {
    default     = false
    description = "Enable HTTPS termination on the loadbalancer"
}

variable "cert_body" {
    default = ""
}

variable "cert_private_key" {
    default = ""
}

variable "cert_chain" {
    default = ""
}

#------------------------------------------#
# Rancher Variables
#------------------------------------------#
variable "rancher_version" {
    default     = "stable"
    description = "Rancher version to deploy"
}
