#------------------------------------------#
# AWS Provider Configuration
#------------------------------------------#
provider "aws" {
    access_key="<AWS_IAM_ACCESS_KEY>"
    secret_key="<AWS_IAM_SECRET_KEY>"
    region = "${var.region}"
}

