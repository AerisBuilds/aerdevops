variable "project" {}
variable "productname" {}
variable "environment" {}
variable "kafka_service_account" {}
variable "kafka_purpose" {}
variable "kafka_subnetwork" {}
variable "kafka_subnetwork_project" {}
variable "kafka_image" {}
variable "kafka_node_settings" {
  type = "map"
}
