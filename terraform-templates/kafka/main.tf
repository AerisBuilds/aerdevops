module "kafka" {
  source = "./components/kafka"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  kafka_node_settings = "${var.kafka_node_settings}"
  kafka_service_account = "${var.kafka_service_account}"
  kafka_purpose = "${var.kafka_purpose}"
  kafka_subnetwork = "${var.kafka_subnetwork}"
  kafka_subnetwork_project = "${var.kafka_subnetwork_project}"
  kafka_image = "${var.kafka_image}"
}
