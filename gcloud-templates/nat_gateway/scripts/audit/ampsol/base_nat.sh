Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         10.4.1.1        0.0.0.0         UG    100    0        0 ens4
10.0.0.0        172.16.1.1      255.255.255.0   UG    0      0        0 ens5
10.0.0.0        10.4.1.1        255.0.0.0       UG    0      0        0 ens4
10.3.0.0        10.4.1.1        255.255.0.0     UG    0      0        0 ens4
10.4.1.0        10.4.1.1        255.255.255.0   UG    100    0        0 ens4
10.4.1.1        0.0.0.0         255.255.255.255 UH    100    0        0 ens4
10.5.0.0        10.4.1.1        255.255.0.0     UG    0      0        0 ens4
10.60.0.0       172.16.1.1      255.252.0.0     UG    0      0        0 ens5
10.64.0.0       172.16.1.1      255.255.240.0   UG    0      0        0 ens5
10.166.0.0      172.16.1.1      255.255.0.0     UG    0      0        0 ens5
172.16.0.0      172.16.1.1      255.255.0.0     UG    0      0        0 ens5
172.16.1.0      172.16.1.1      255.255.255.248 UG    0      0        0 ens5
172.16.1.1      0.0.0.0         255.255.255.255 UH    0      0        0 ens5