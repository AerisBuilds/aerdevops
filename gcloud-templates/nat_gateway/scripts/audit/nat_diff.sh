#!/bin/bash


sudo route -n  > nat

diff_output=$(diff --minimal -u -w -b -B -E -Z $1 nat)

if [ $? -eq 0 ]; then
    echo "PASSED AUDIT:  NAT Gateway Routes are correct"
else
    echo "WARNING:  Your NAT Gateway Routes are not upto standards - these are the discrepencies"
    eho "$diff_output"
fi


rm nat


