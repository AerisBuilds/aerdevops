#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing port"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./mytcpdump.sh port_number"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./mytcpdump.sh 6379"
    echo -e "\033[0m"
    exit 1
fi

sudo tcpdump -i ens4 -nn -s0 -v port $1