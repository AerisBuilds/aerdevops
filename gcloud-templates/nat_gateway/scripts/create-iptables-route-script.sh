#!/bin/bash

#################### FUNCTIONS ####################
setup_nat_gateway_route_script() {
    echo "Setup custom nat-gateway-route.sh script"
    NAT_GATEWAY_IPTABLES_ROUTES=$1
    SHARED_VPC_INTERNAL_IP_ADDRESS=$2

    IFS=','
    read -ra IPTABLE_ROUTE <<< "${NAT_GATEWAY_IPTABLES_ROUTES}"

    for i in "${IPTABLE_ROUTE[@]}"; do
        # Build iptables route rules
        IPTABLES_ROUTES+="sudo iptables -A POSTROUTING -t nat -p tcp -d ${i} -j SNAT --to-source ${SHARED_VPC_INTERNAL_IP_ADDRESS}"$'\n'
    done

cat <<END > /tmp/nat-gateway-route.sh
#!/bin/bash
${IPTABLES_ROUTES}
END
}

#################### MAIN ####################

NAT_GATEWAY_IPTABLES_ROUTES=$1
SHARED_VPC_INTERNAL_IP_ADDRESS=$2

if [[ -z "${NAT_GATEWAY_IPTABLES_ROUTES}" || -z "${SHARED_VPC_INTERNAL_IP_ADDRESS}" ]];
then
    echo -e "\033[31m"
    echo -e "\033[31m Missing parameters!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create-iptables-route-script.sh NAT_GATEWAY_IPTABLES_ROUTES SHARED_VPC_INTERNAL_IP_ADDRESS"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create-iptables-route-script.sh 10.3.0.0/16 172.16.1.4"
    echo -e "\033[0m"
    exit 1
fi

setup_nat_gateway_route_script ${NAT_GATEWAY_IPTABLES_ROUTES} ${SHARED_VPC_INTERNAL_IP_ADDRESS}
