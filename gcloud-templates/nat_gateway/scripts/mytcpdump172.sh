#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing port"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./mytcpdump172.sh port_number"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./mytcpdump172.sh 6379"
    echo -e "\033[0m"
    exit 1
fi

sudo tcpdump -i ens5 -nn -s 0 -v port $1