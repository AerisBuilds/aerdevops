#!/bin/bash

#################### MAIN ####################
if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./iptables-updater.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./iptables-updater.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

IFS=','
read -ra LB_ID <<< "${INTERNAL_LOAD_BALANCER_IDS}"

for ILB in "${LB_ID[@]}"; do
  LOAD_BALANCER_NAME="${ILB^^}_ILB_NAME"
  LOAD_BALANCER_NODE_PORT="${ILB^^}_ILB_NODE_PORT"
  IP_LOAD_BALANCER="${ILB^^}_IP_LOAD_BALANCER"

  read -ra SP <<< "${!LOAD_BALANCER_NODE_PORT}"

  for SERVICE_PORT in "${SP[@]}"; do
    if [[ -z "${!LOAD_BALANCER_NAME}" || -z "${IP_LOAD_BALANCER}" || -z "${SERVICE_PORT}" || -z "${INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM}" || -z "${INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM}" ]] ; then
        echo -e "\033[31m"
        echo -e "\033[31m Missing parameters to update IPTABLES rules!"
        echo -e "\033[31m"
        echo -e "\033[0m"
        exit 1
    fi

    IPTABLES_PREROUTING="sudo iptables -A PREROUTING -t nat -p tcp -d ${INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM} --dport ${SERVICE_PORT} -j DNAT --to-destination ${!IP_LOAD_BALANCER}:${SERVICE_PORT}"
    IPTABLES_POSTROUTING="sudo iptables -A POSTROUTING -t nat -p tcp -d ${!IP_LOAD_BALANCER} --dport ${SERVICE_PORT} -j SNAT --to-source ${INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM}"

    IPTABLES_SERVICE_PREROUTING="$(grep -n "${SERVICE_PORT}" /home/ubuntu/nat-gateway-route.sh | grep "PREROUTING" | cut -d : -f1 | sed -e 1b -e '$!d')"
    IPTABLES_SERVICE_POSTROUTING="$(grep -n "${SERVICE_PORT}" /home/ubuntu/nat-gateway-route.sh | grep "POSTROUTING" | cut -d : -f1 | sed -e 1b -e '$!d')"

    if [[ -n "${IPTABLES_SERVICE_PREROUTING}" && -n "${IPTABLES_SERVICE_POSTROUTING}" ]];
    then
      echo "Replace current IPTABLES entries for ${SERVICE_PORT}"
      sed -i "${IPTABLES_SERVICE_PREROUTING}s/.*/${IPTABLES_PREROUTING}/" /home/ubuntu/nat-gateway-route.sh
      sed -i "${IPTABLES_SERVICE_POSTROUTING}s/.*/${IPTABLES_POSTROUTING}/" /home/ubuntu/nat-gateway-route.sh
    else
      echo "Adding IPTABLES entries for ${SERVICE_PORT}"
      IPTABLES_ROUTES=""
      IPTABLES_ROUTES+="# ${!LOAD_BALANCER_NAME} ${SERVICE_PORT}"$'\n'
      IPTABLES_ROUTES+="${IPTABLES_PREROUTING}"$'\n'
      IPTABLES_ROUTES+="${IPTABLES_POSTROUTING}"$'\n'

      echo "$IPTABLES_ROUTES" >> /home/ubuntu/nat-gateway-route.sh
    fi

    echo "nat-gateway-route.sh updated with ${!LOAD_BALANCER_NAME} ${SERVICE_PORT} entries"
  done
done
