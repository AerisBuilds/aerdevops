#!/bin/bash

#################### FUNCTIONS ####################
setup_ip_routes_script() {
    echo "Setup custom add-ip-routes.sh script"
    NAT_GATEWAY_IP_ROUTES=$1
    NAT_GATEWAY_SUBNET_GATEWAY_IP=$2
    NAT_GATEWAY_ISLAND_SUBNET_GATEWAY_IP=$3

    IFS=','
    read -ra IP_ROUTE <<< "${NAT_GATEWAY_IP_ROUTES}"

    for j in "${IP_ROUTE[@]}"; do
        # Build ip route rules for K8s connectivity
        IP_ROUTES+="sudo route add -v -net ${j} gw ${NAT_GATEWAY_SUBNET_GATEWAY_IP} dev ens5"$'\n'
    done

    set -f; IFS='.'
    set -- ${NAT_GATEWAY_SUBNET_GATEWAY_IP}
    first=$1; second=$2;
    set +f; unset IFS

    IP_ROUTES+="sudo route add -v -net ${first}.${second}.0.0/16 gw ${NAT_GATEWAY_SUBNET_GATEWAY_IP} dev ens5"$'\n'

    # Make the Project VPC our default route
    IP_ROUTES+="#sudo route add -v -net 0.0.0.0/0 gw ${NAT_GATEWAY_SUBNET_GATEWAY_IP} dev ens5"$'\n'
    # Remove the Host VPC as the default route
    IP_ROUTES+="#sudo route del -v -net 0.0.0.0/0 gw ${NAT_GATEWAY_ISLAND_SUBNET_GATEWAY_IP} dev ens4"$'\n'
    # Add a route to the Host VPC for full 10.0.0.0/8 network
    IP_ROUTES+="#sudo route add -v -net 10.0.0.0/8 gw ${NAT_GATEWAY_ISLAND_SUBNET_GATEWAY_IP} dev ens4"$'\n'

cat <<END > /tmp/add-ip-routes.sh
#!/bin/bash
${IP_ROUTES}
END
}

#################### MAIN ####################

K8S_ADDRESS_RANGES=$1
SUBNET_GATEWAY_IP=$2
ISLAND_SUBNET_GATEWAY_IP=$3

if [[ -z "${K8S_ADDRESS_RANGES}" || -z "${SUBNET_GATEWAY_IP}" || -z "${ISLAND_SUBNET_GATEWAY_IP}" ]];
then
    echo -e "\033[31m"
    echo -e "\033[31m Missing parameters!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create-ip-route-script.sh K8S_ADDRESS_RANGES SUBNET_GATEWAY_IP ISLAND_SUBNET_GATEWAY_IP"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create-ip-route-script.sh 10.60.0.0/14,10.64.0.0/20 172.16.1.1 10.4.1.1"
    echo -e "\033[0m"
    exit 1
fi

setup_ip_routes_script "${K8S_ADDRESS_RANGES}" "${SUBNET_GATEWAY_IP}" "${ISLAND_SUBNET_GATEWAY_IP}"
