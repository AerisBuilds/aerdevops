#!/bin/bash

#################### FUNCTIONS ####################
delete_static_internal_ip_address() {
    echo "Delete static internal ip"
    gcloud compute --project=${PROJECT} addresses delete ${NAT_GATEWAY_VM_NAME} \
        --region ${NAT_GATEWAY_REGION} -q
}

delete_nat_gateway_vm() {
    echo "Delete NAT Gateway VM"
    gcloud compute --project=${PROJECT} instances delete ${NAT_GATEWAY_VM_NAME} \
        --zone=${NAT_GATEWAY_ZONE} -q
}

delete_route() {
    echo "Delete network route"
    IP_RANGE=$1

    set -f; IFS='.'
    set -- $IP_RANGE
    first=$1; second=$2; third=$3
    set +f; unset IFS

    gcloud compute --project=${PROJECT} routes delete "route-project-vpc-to-aws-${first}-${second}" -q

}


#################### MAIN ####################
if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_nat_gateway.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_nat_gateway.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

# STATIC_INTERNAL_IP_ADDRESS="$(gcloud compute --project=${PROJECT} addresses list | grep ${NAT_GATEWAY_VM_NAME} | awk '{print $2}')"

# Delete VM instance
delete_nat_gateway_vm

# Delete a static internal IP address
delete_static_internal_ip_address


IFS=','
read -ra IPTABLE_ROUTE <<< "${NAT_GATEWAY_IPTABLES_ROUTES}"

for i in "${IPTABLE_ROUTE[@]}"; do
    # Create network route
    delete_route ${i}
done

echo "Nat gateway deleted: ${NAT_GATEWAY_VM_NAME}"
