#!/bin/bash

#################### FUNCTIONS ####################
create_static_internal_ip_address() {
    echo "Create static internal ip"
    gcloud compute --project="${PROJECT}" addresses create "${NAT_GATEWAY_VM_NAME}" \
        --region "${NAT_GATEWAY_REGION}" --subnet "${NAT_GATEWAY_SUBNET}"
}

create_nat_gateway_vm() {
    echo "Create NAT Gateway VM"
    gcloud compute --project="${PROJECT}" instances create "${NAT_GATEWAY_VM_NAME}" \
        --zone="${NAT_GATEWAY_ZONE}" \
        --machine-type="${NAT_GATEWAY_MACHINE_TYPE}" \
        --can-ip-forward \
        --network-interface subnet=projects/"${NAT_GATEWAY_SHARED_VPC_PROJECT}"/regions/"${NAT_GATEWAY_SHARED_VPC_REGION}"/subnetworks/"${NAT_GATEWAY_SHARED_VPC_SUBNET}",no-address \
        --network-interface subnet=projects/"${PROJECT}"/regions/"${NAT_GATEWAY_REGION}"/subnetworks/"${NAT_GATEWAY_SUBNET}",private-network-ip="${STATIC_INTERNAL_IP_ADDRESS}",no-address \
        --image="${NAT_GATEWAY_IMAGE}" \
        --image-project="${NAT_GATEWAY_IMAGE_PROJECT}" \
        --boot-disk-size="${NAT_GATEWAY_BOOT_DISK_SIZE}" \
        --boot-disk-type=pd-standard \
        --boot-disk-device-name="${NAT_GATEWAY_VM_NAME}" \
        --tags="${NAT_GATEWAY_NETWORK_TAGS}"
}

create_route() {
    echo "Create network route"
    IP_RANGE=$1

    set -f; IFS='.'
    set -- $IP_RANGE
    first=$1; second=$2;
    set +f; unset IFS

    gcloud compute --project="${PROJECT}" routes create "route-project-vpc-to-aws-${first}-${second}" \
            --destination-range="${IP_RANGE}" \
            --next-hop-address="${STATIC_INTERNAL_IP_ADDRESS}" \
            --network="${NAT_GATEWAY_NETWORK}"
}

add_startup_script_to_vm() {
    echo "Add startup script metadata to Nat Gateway VM"

cat <<END > /tmp/startup-script.sh
#!/bin/bash
/home/ubuntu/enableforward.sh
/home/ubuntu/nat-gateway-route.sh
/home/ubuntu/add-ip-routes.sh
END

    gcloud compute --project="${PROJECT}" instances add-metadata "${NAT_GATEWAY_VM_NAME}" \
        --metadata-from-file startup-script=/tmp/startup-script.sh \
        --zone="${NAT_GATEWAY_ZONE}"
}


#################### MAIN ####################
if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_nat_gateway.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_nat_gateway.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source "$1"

# Create a static internal IP address
create_static_internal_ip_address

STATIC_INTERNAL_IP_ADDRESS="$(gcloud compute --project="${PROJECT}" addresses list | grep "${NAT_GATEWAY_VM_NAME}" | awk '{print $2}')"

# Create VM instance
create_nat_gateway_vm

IFS=','
read -ra IPTABLE_ROUTE <<< "${NAT_GATEWAY_IPTABLES_ROUTES}"

for i in "${IPTABLE_ROUTE[@]}"; do
    # Create network route
    create_route "${i}"
done

# Add a startup script to enable ip forwarding and add the iptable routes and ip routes to the host
add_startup_script_to_vm
