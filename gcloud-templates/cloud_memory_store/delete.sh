#!/bin/bash



#################### FUNCTIONS ####################
delete_cloud_memory_store() {
    gcloud beta redis instances delete ${!CLOUD_MEMORY_STORE_NAME} \
        --project=${PROJECT} \
        --region=${!CLOUD_MEMORY_STORE_REGION} -q
}








if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1


# Delete cloud memory store

IFS=','
read -ra CMS_ID <<< "${CLOUD_MEMORY_STORE_IDS}"

for i in "${CMS_ID[@]}"; do

    CLOUD_MEMORY_STORE_NAME="${i^^}_CLOUD_MEMORY_STORE_NAME"
    CLOUD_MEMORY_STORE_REGION="${i^^}_CLOUD_MEMORY_STORE_REGION"

    delete_cloud_memory_store
    echo "Delete ${i} cloud memory store"

done

