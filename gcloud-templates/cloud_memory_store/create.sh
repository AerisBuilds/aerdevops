#!/bin/bash



#################### FUNCTIONS ####################
create_cloud_memory_store() {


    if [[ "${!CLOUD_MEMORY_STORE_TIER}" == "" ]]; then
        TIER="standard"
    else
        TIER=${!CLOUD_MEMORY_STORE_TIER}
    fi


    gcloud beta redis instances create ${!CLOUD_MEMORY_STORE_NAME} \
        --project=${PROJECT} \
        --size=${!CLOUD_MEMORY_STORE_SIZE} \
        --region=${!CLOUD_MEMORY_STORE_REGION} \
        --zone=${!CLOUD_MEMORY_STORE_ZONE} \
        --redis-version=${!CLOUD_MEMORY_STORE_VERSION} \
        --network=${!CLOUD_MEMORY_STORE_NETWORK} \
        --reserved-ip-range=${!CLOUD_MEMORY_STORE_IP_RANGE} \
        --tier=${TIER}
}






if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1



API_ENABLE=$(gcloud services list --project $PROJECT --format=json | jq '.[] | select(.name | contains("redis.googleapis.com"))')

    if [[ "${API_ENABLE}" == "" ]]; then
        gcloud services enable --project $PROJECT redis.googleapis.com    
    fi



IFS=','
read -ra CMS_ID <<< "${CLOUD_MEMORY_STORE_IDS}"

for i in "${CMS_ID[@]}"; do
    echo "Creating ${i} cloud memory store"

    CLOUD_MEMORY_STORE_NAME="${i^^}_CLOUD_MEMORY_STORE_NAME"
    CLOUD_MEMORY_STORE_SIZE="${i^^}_CLOUD_MEMORY_STORE_SIZE"
    CLOUD_MEMORY_STORE_REGION="${i^^}_CLOUD_MEMORY_STORE_REGION"
    CLOUD_MEMORY_STORE_ZONE="${i^^}_CLOUD_MEMORY_STORE_ZONE"
    CLOUD_MEMORY_STORE_VERSION="${i^^}_CLOUD_MEMORY_STORE_VERSION"
    CLOUD_MEMORY_STORE_NETWORK="${i^^}_CLOUD_MEMORY_STORE_NETWORK"
    CLOUD_MEMORY_STORE_IP_RANGE="${i^^}_CLOUD_MEMORY_STORE_IP_RANGE"
    CLOUD_MEMORY_STORE_TIER="${i^^}_CLOUD_MEMORY_STORE_TIER"

    create_cloud_memory_store

done

