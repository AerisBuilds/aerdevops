#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_dns_zone.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_dns_zone vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

environment=$2

templatename=$(dirname $1)/dns.template
echo "Template file path: $templatename"

setupstage() {
	HOST_PROJECT='aeriscomm-hostproj-np-201903'
	HOST_ZONE='aerjupiter-com'
	domain_name='aerjupiter.com'
	PRIVATE_ZONE="aeris-private-net"
	private_domain_name="aeris-private.net."
}

create_private_zone() {
	zone_details=`gcloud dns managed-zones describe $PRIVATE_ZONE --format json | jq -r '.dnsName'`
	if [[ "$zone_details" == "$private_domain_name" ]] ; then
		echo "PRIVATE ZONE: $PRIVATE_ZONE exists"
	else
		echo "Creating Private Zone"
		gcloud dns managed-zones create "${PRIVATE_ZONE}" \
		--project="${PROJECT}" \
		--dns-name="${private_domain_name}" \
		--description="${private_domain_name}" \
		--networks="${NAT_GATEWAY_NETWORK}" \
		--visibility=private
		
	fi
}

add_private_dns_records() {
	IFS=$'\n'
	domain_list=($(awk '/PrivateDnsTemplatesFileStarts/{flag=1; next} /PrivateDnsTemplatesFileEnds/{flag=0} flag' ${templatename}))
	ip_address_list=$(gcloud compute forwarding-rules list --project=${PROJECT} )
	echo "ip_address_list is : $ip_address_list"
	echo "domain_list is : $domain_list[@]"
	
	echo "Start Transaction"
	gcloud dns record-sets transaction start --project=${PROJECT} --zone=${PRIVATE_ZONE}

	for i in ${domain_list[@]}
	do
		url=$(echo $i | awk '{print $1}'| sed -E "s|<domain_name>|$private_domain_name|g" | sed -E "s|<env>|$environment|g")
		record_type=$(echo $i | awk '{print $2}')
		src_name=$(echo $i | awk '{print $3}'| sed -E "s|<env>|$environment|g" | sed -E "s|<region>|${REGION}|g")
		echo "Source is: $src_name for Domain Name: $url and Record Type is: $record_type"

		if [[ "${record_type^^}" == "LB" ]]; then
			ip_address=$(echo "$ip_address_list" | grep "$src_name" | rev | awk '{print $3}' |rev)
		elif [[ "${record_type^^}" == "VA" ]]; then
			ip_address="${!src_name}"
		elif [[ "${record_type^^}" == "A" ]]; then
			ip_address="${src_name}"
		else
			echo "Record Type Not Supported:: ${record_type}"
			exit 1
		fi

		if [[ `gcloud dns record-sets list --zone="${PRIVATE_ZONE}" --project=${PROJECT} | grep -e ^$url` ]] ; then 
			echo "Record for $url exists: Removing old record"
			record_details=`gcloud dns record-sets list --zone="${PRIVATE_ZONE}" --project=${PROJECT} | grep $url`
			ttl=`echo $record_details|awk '{print $3}'`
			ip=`echo $record_details|awk '{print $4}'`
			name=`echo $record_details|awk '{print $1}'`
			gcloud dns record-sets transaction remove "${ip}" \
			  --name="${name}" \
			  --ttl="${ttl}" \
			  --type="A" \
			  --zone="${PRIVATE_ZONE}" \
			  --project=${PROJECT}
		fi
		
		echo "Adding A Record For Domain Name: $url And Value: $ip_address"
		gcloud dns record-sets transaction add ${ip_address} --name=${url} --ttl=300 --type=A --zone=${PRIVATE_ZONE} --project=${PROJECT}
	done
	
	echo "Publishing The Records"
	gcloud dns --project=${PROJECT} record-sets transaction execute --zone=${PRIVATE_ZONE} || rm -rf transaction.yaml
}

setupstage
## Create private zone in project if it doesnot exist
create_private_zone
##Add Private dns records
add_private_dns_records