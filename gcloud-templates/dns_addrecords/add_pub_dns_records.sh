#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_dns_zone.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_dns_zone vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

environment=$2

templatename=$(dirname $1)/dns.template
echo "Template file path: $templatename"

setupstage() {
	HOST_PROJECT='aeriscomm-hostproj-np-201903'
	HOST_ZONE='aerjupiter-com'
	domain_name='aerjupiter.com'
	PRIVATE_ZONE="aeris-private-net"
	private_domain_name="aeris-private.net."
}

add_dns_records() {
	IFS=$'\n'
	domain_list=($(awk '/PublicDnsTemplatesFileStarts/{flag=1; next} /PublicDnsTemplatesFileEnds/{flag=0} flag' ${templatename}))
	ip_address_list=$(gcloud compute forwarding-rules list --project=${PROJECT} )
	echo "ip_address_list is : $ip_address_list"
	echo "domain_list is : $domain_list[@]"
	
	echo "Start Transaction"
	gcloud dns record-sets transaction start --project=${HOST_PROJECT} --zone=${HOST_ZONE}
	
	for i in ${domain_list[@]}
	do 
		url=$(echo $i | awk '{print $1}'| sed -E "s|<domain_name>|$domain_name|g" | sed -E "s|<env>|$environment|g")
		record_type=$(echo $i | awk '{print $2}')
		src_name=$(echo $i | awk '{print $3}'| sed -E "s|<env>|$environment|g" | sed -E "s|<region>|${REGION}|g")
		echo "Source is: $src_name for Domain Name: $url and Record Type is: $record_type"

		if [[ "${record_type^^}" == "LB" ]]; then
		ip_address=$(echo "$ip_address_list" | grep "$src_name" | rev | awk '{print $3}' |rev)
		elif [[ "${record_type^^}" == "VA" ]]; then
		ip_address="${!src_name}"
		elif [[ "${record_type^^}" == "A" ]]; then
		ip_address="${src_name}"
		else
			echo "Type Not Supported:: ${record_type}"
			exit 1
		fi

		if [[ `gcloud dns record-sets list --zone="${HOST_ZONE}" --project=${HOST_PROJECT} | grep -e ^$url` ]] ; then 
			echo "Record for $url exists: Removing old record"
			record_details=`gcloud dns record-sets list --zone="${HOST_ZONE}" --project=${HOST_PROJECT} | grep $url`
			ttl=`echo $record_details|awk '{print $3}'`
			ip=`echo $record_details|awk '{print $4}'`
			name=`echo $record_details|awk '{print $1}'`
			gcloud dns record-sets transaction remove "${ip}" \
			  --name="${name}" \
			  --ttl="${ttl}" \
			  --type="A" \
			  --zone="${HOST_ZONE}" \
			  --project=${HOST_PROJECT}
		fi

		echo "Adding A Record For Domain Name: $url And Value: $ip_address"
		gcloud dns record-sets transaction add ${ip_address} --name=${url} --ttl=300 --type=A --zone=${HOST_ZONE} --project=${HOST_PROJECT}
	done
	
	echo "Publish The Records"
	gcloud dns --project=${HOST_PROJECT} record-sets transaction execute --zone=${HOST_ZONE} || rm -rf transaction.yaml
}

setupstage
add_dns_records