# DNS Zone creation

## Deployment from script
1. Install gcloud in the local environment:
    * [Installing Google Cloud SDK](https://cloud.google.com/sdk/install)
2. Configure gcloud:
    * gcloud auth login
3. Edit **vars.sh** file and set the correct project settings.
4. Execute **create_bootstrap.sh** script
5. Verify that the VM instance was created in on GCP Console.
