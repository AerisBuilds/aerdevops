# Google Container Registry
Google Container Registry (GCR) is a single place to manage Docker images, perform vulnerability analysis and access control.

# Required permissions
- gcloud.services.enable

# Deployment
1. Install gcloud in the local environment:
    * [Installing Google Cloud SDK](https://cloud.google.com/sdk/install)
2. Configure gcloud:
    * gcloud auth login
3. Edit **vars.sh** file and set the correct project settings.
4. Execute **create_gcr.sh** script

# Validation
To verify that the GCR API is enabled, use the following procedure:

1. Pull a known docker image:
    * `docker pull nginx:latest`
2. Tag the docker image, replace "project-id" with the correct project:
    * `docker tag nginx:latest gcr.io/project-id/nginx:latest`
3. Push the docker image to GCR:
    * `docker push gcr.io/project-id/nginx:latest`
4. Verify that the image is in GCR using GCP Console or the gcloud command: 
    * `gcloud container images  list`
5. Remove the image from GCR:
    * `gcloud container images delete --quiet gcr.io/amp-solutions-qualdev-201903/nginx:latest`
    
# More information
https://cloud.google.com/container-registry/docs/pushing-and-pulling