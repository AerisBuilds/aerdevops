###################################################################################################
# Author         : arun joshi
# Purpose        : Create dataproc cluster and execute nessessary initialization scripts from here.
# Name           : trigger_install.sh
# Type           : Script

#
# Mod Log
# Date                  By                Jira                    Description
# ----------    -----------------       ----------              ---------------
# 2019-08-08      arun joshi             jup-1128                  Inception
####################################################################################################

#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bootstrap.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bootstrap.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi
## Load variables from vars file
# Load variables from vars file

echo "Variables file path: $1"

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $1

#creating required gs buckets to install dataproc
gsutil mb -l "${REGION}" gs://"${DATAPROC_STAGING_BUCKET}"
gsutil mb -l "${REGION}" gs://"${DATAPROC_WAREHOUSE_BUCKET}"
#gsutil mb -l "${REGION}" gs://"${DATAPROC_ASSETS_BUCKET}"


#echo "${DATAPROC_MY_SQL_INSATANCE_NAME}"
#gcloud --project "${PROJECT}" beta sql instances create "${DATAPROC_MY_SQL_INSATANCE_NAME}" \
#--database-version="${DATAPROC_MYSQL_VERSION}" \
#--activation-policy=ALWAYS \
#--zone "${ZONE}" \
#--no-assign-ip \
#--network=${DATAPROC_NETWORK}

gcloud dataproc clusters create "${DATAPROC_CLUSTER_NAME}" \
--scopes=cloud-platform \
--initialization-actions "gs://"${DATAPROC_ASSETS_BUCKET}"/dataproc-init-scripts/presto/presto-aeris-v224.sh","gs://"${DATAPROC_ASSETS_BUCKET}"/dataproc-init-scripts/cloud-sql-proxy/cloud-sql-proxy-aeris.sh","gs://"${DATAPROC_ASSETS_BUCKET}"/dataproc-init-scripts/hbase/hbase.sh","gs://"${DATAPROC_ASSETS_BUCKET}"/dataproc-init-scripts/sqoop/sqoop-import-aeris.sh","gs://"${DATAPROC_ASSETS_BUCKET}"/dataproc-init-scripts/general/general-tasks.sh" \
--bucket "${DATAPROC_STAGING_BUCKET}" \
--subnet "${DATAPROC_SUBNET}" \
--region "${REGION}" \
--zone "${ZONE}" \
--num-masters "${DATAPROC_NUM_MASTER_NODE}" --master-machine-type "${DATAPROC_MASTER_MACHINE_TYPE}" --master-boot-disk-size "${DATAPROC_MASTER_BOOT_DISK_SIZE}" \
--num-workers "${DATAPROC_NUM_WORKER_NODE}" --worker-machine-type "${DATAPROC_WORKER_MACHINE_TYPE}" --worker-boot-disk-size "${DATAPROC_WORKER_BOOT_DISK_SIZE}" \
--image-version "${DATAPROC_OS_IMG_VERSION}" \
--no-address \
--metadata hive-metastore-instance="${PROJECT}":"${REGION}":"${DATAPROC_MY_SQL_INSATANCE_NAME}" \
--metadata use-cloud-sql-private-ip=true \
--metadata ngrs-dataproc-assets-bucket=gs://"${DATAPROC_ASSETS_BUCKET}" \
--metadata ngrs-dataproc-warehouse-bucket-name="${DATAPROC_WAREHOUSE_BUCKET}" \
--metadata mounted-bucket-folder="${DATAPROC_MOUNTED_BUCKET_FOLDER}" \
--metadata project-zone="${ZONE}" \
--metadata deployment-environment="${DATAPROC_DEPLOYMENT_ENV}" \
--properties hive:hive.metastore.warehouse.dir=gs://"${DATAPROC_WAREHOUSE_BUCKET}"/datasets \
--properties 'hadoop-env:HADOOP_CLASSPATH=${HADOOP_CLASSPATH}:/etc/tez/conf:/usr/lib/tez/*:/usr/lib/tez/lib/*' \
--service-account="${DATAPROC_SERVICE_ACCOUNT}" \
--tags="${DATAPROC_NETWORK_TAGS}" \
--project="${PROJECT}"
