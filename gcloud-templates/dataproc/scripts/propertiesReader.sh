#!/bin/bash

########################################################################################################################
# Author         : arun joshi
# Purpose        : Property reader, to read and provide dataproc related properties based on the environment provided
# Name           : propertiesReader.sh
# Type           : Script

#
# Mod Log
# Date                  By                Jira                    Description
# ----------    -----------------       ----------              ---------------
# 2019-08-08      arun joshi             jup-1128                  Inception
#########################################################################################################################

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

env=${1}
PROP_FILE=$THIS_DIR/../conf/$env/config.properties

DATAPROC_ASSETS_BUCKET=`cat ${PROP_FILE} | grep -w DATAPROC_ASSETS_BUCKET | cut -d '=' -f2`
echo `date` "DATAPROC_ASSETS_BUCKET=${DATAPROC_ASSETS_BUCKET}"

DATAPROC_STAGING_BUCKET=`cat ${PROP_FILE} | grep -w DATAPROC_STAGING_BUCKET | cut -d '=' -f2`
echo `date` "DATAPROC_STAGING_BUCKET=${DATAPROC_STAGING_BUCKET}"

DATAPROC_WAREHOUSE_BUCKET=`cat ${PROP_FILE} | grep -w DATAPROC_WAREHOUSE_BUCKET | cut -d '=' -f2`
echo `date` "DATAPROC_WAREHOUSE_BUCKET=${DATAPROC_WAREHOUSE_BUCKET}"

MY_SQL_INSATANCE_NAME=`cat ${PROP_FILE} | grep -w MY_SQL_INSATANCE_NAME | cut -d '=' -f2`
echo `date` "MY_SQL_INSATANCE_NAME=${MY_SQL_INSATANCE_NAME}"

DATAPROC_CLUSTER_NAME=`cat ${PROP_FILE} | grep -w DATAPROC_CLUSTER_NAME | cut -d '=' -f2`
echo `date` "DATAPROC_CLUSTER_NAME=${DATAPROC_CLUSTER_NAME}"

NETWORK=`cat ${PROP_FILE} | grep -w NETWORK | cut -d '=' -f2`
echo `date` "NETWORK=${NETWORK}"

SUBNET=`cat ${PROP_FILE} | grep -w SUBNET | cut -d '=' -f2`
echo `date` "SUBNET=${SUBNET}"

NETWORK_TAGS=`cat ${PROP_FILE} | grep -w NETWORK_TAGS | cut -d '=' -f2`
echo `date` "NETWORK_TAGS=${NETWORK_TAGS}"

MOUNTED_BUCKET_FOLDER=`cat ${PROP_FILE} | grep -w MOUNTED_BUCKET_FOLDER | cut -d '=' -f2`
echo `date` "MOUNTED_BUCKET_FOLDER=${MOUNTED_BUCKET_FOLDER}"

MYSQL_VERSION=`cat ${PROP_FILE} | grep -w MYSQL_VERSION | cut -d '=' -f2`
echo `date` "MYSQL_VERSION=${MYSQL_VERSION}"

MYSQL_ACTIVATION_POLICY=`cat ${PROP_FILE} | grep -w MYSQL_ACTIVATION_POLICY | cut -d '=' -f2`
echo `date` "MYSQL_ACTIVATION_POLICY=${MYSQL_ACTIVATION_POLICY}"

DATAPROC_NUM_MASTER_NODE=`cat ${PROP_FILE} | grep -w DATAPROC_NUM_MASTER_NODE | cut -d '=' -f2`
echo `date` "DATAPROC_NUM_MASTER_NODE=${DATAPROC_NUM_MASTER_NODE}"

DATAPROC_NUM_WORKER_NODE=`cat ${PROP_FILE} | grep -w DATAPROC_NUM_WORKER_NODE | cut -d '=' -f2`
echo `date` "DATAPROC_NUM_WORKER_NODE=${DATAPROC_NUM_WORKER_NODE}"

DATAPROC_MASTER_MACHINE_TYPE=`cat ${PROP_FILE} | grep -w DATAPROC_MASTER_MACHINE_TYPE | cut -d '=' -f2`
echo `date` "DATAPROC_MASTER_MACHINE_TYPE=${DATAPROC_MASTER_MACHINE_TYPE}"

DATAPROC_WORKER_MACHINE_TYPE=`cat ${PROP_FILE} | grep -w DATAPROC_WORKER_MACHINE_TYPE | cut -d '=' -f2`
echo `date` "DATAPROC_WORKER_MACHINE_TYPE=${DATAPROC_WORKER_MACHINE_TYPE}"

DATAPROC_WORKER_MACHINE_TYPE=`cat ${PROP_FILE} | grep -w DATAPROC_WORKER_MACHINE_TYPE | cut -d '=' -f2`
echo `date` "DATAPROC_WORKER_MACHINE_TYPE=${DATAPROC_WORKER_MACHINE_TYPE}"

DATAPROC_MASTER_BOOT_DISK_SIZE=`cat ${PROP_FILE} | grep -w DATAPROC_MASTER_BOOT_DISK_SIZE | cut -d '=' -f2`
echo `date` "DATAPROC_MASTER_BOOT_DISK_SIZE=${DATAPROC_MASTER_BOOT_DISK_SIZE}"

DATAPROC_WORKER_BOOT_DISK_SIZE=`cat ${PROP_FILE} | grep -w DATAPROC_WORKER_BOOT_DISK_SIZE | cut -d '=' -f2`
echo `date` "DATAPROC_WORKER_BOOT_DISK_SIZE=${DATAPROC_WORKER_BOOT_DISK_SIZE}"

DATAPROC_OS_IMG_VERSION=`cat ${PROP_FILE} | grep -w DATAPROC_OS_IMG_VERSION | cut -d '=' -f2`
echo `date` "DATAPROC_OS_IMG_VERSION=${DATAPROC_OS_IMG_VERSION}"

DATAPROC_HIVE_EXE_ENGN=`cat ${PROP_FILE} | grep -w DATAPROC_HIVE_EXE_ENGN | cut -d '=' -f2`
echo `date` "DATAPROC_HIVE_EXE_ENGN=${DATAPROC_HIVE_EXE_ENGN}"

SQOOP_VERSION=`cat ${PROP_FILE} | grep -w SQOOP_VERSION | cut -d '=' -f2`
echo `date` "SQOOP_VERSION=${SQOOP_VERSION}"

SQOOP_HADOOP_VERSION=`cat ${PROP_FILE} | grep -w SQOOP_HADOOP_VERSION | cut -d '=' -f2`
echo `date` "SQOOP_HADOOP_VERSION=${SQOOP_HADOOP_VERSION}"

OJDBC_VERSION=`cat ${PROP_FILE} | grep -w OJDBC_VERSION | cut -d '=' -f2`
echo `date` "OJDBC_VERSION=${OJDBC_VERSION}"

OJDBC_JAR=`cat ${PROP_FILE} | grep -w OJDBC_JAR | cut -d '=' -f2`
echo `date` "OJDBC_JAR=${OJDBC_JAR}"

PRESTO_VERSION=`cat ${PROP_FILE} | grep -w PRESTO_VERSION | cut -d '=' -f2`
echo `date` "PRESTO_VERSION=${PRESTO_VERSION}"

DEPLOYMENT_ENV=`cat ${PROP_FILE} | grep -w DEPLOYMENT_ENV | cut -d '=' -f2`
echo `date` "DEPLOYMENT_ENV=${DEPLOYMENT_ENV}"

DATAPROC_INSTALLER_HOME_DIR=`cat ${PROP_FILE} | grep -w DATAPROC_INSTALLER_HOME_DIR | cut -d '=' -f2`
echo `date` "DATAPROC_INSTALLER_HOME_DIR=${DATAPROC_INSTALLER_HOME_DIR}"