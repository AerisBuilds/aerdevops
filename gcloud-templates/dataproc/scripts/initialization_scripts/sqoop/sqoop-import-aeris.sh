#!/bin/bash

##################################################################
# Author         : jalaj agarwal
# Purpose        : Install sqoop import on master nodes
# Name           : sqoop-impoer-aeris.sh
# Type           : Script

#
# Mod Log
# Date                  By                Jira                    Description
# ----------    -----------------       ----------              ---------------
# 2019-07-31      Jalaj Agarwal          ANA-2364                Inception
###############################################################################

set -euxo pipefail

# Use Python from /usr/bin instead of /opt/conda.
export PATH=/usr/bin:$PATH

# Variables for running this script
readonly ROLE="$(/usr/share/google/get_metadata_value attributes/dataproc-role)"

readonly DATAPROC_ASSETS_BUCKET="$(/usr/share/google/get_metadata_value attributes/ngrs-dataproc-assets-bucket)"
readonly SQOOP_VERSION='1.4.7.bin__hadoop-2.6.0'
readonly OJDBC_VERSION='ojdbc6'

function install_sqoop(){
  # Download and unpack Presto server
  echo "Downloading sqoop import tar sqoop-${SQOOP_VERSION}.tar.gz"
  gsutil cp ${DATAPROC_ASSETS_BUCKET}/dataproc-init-scripts/sqoop/sqoop-${SQOOP_VERSION}.tar.gz .
  echo "Downloading ojdbc jar ${OJDBC_VERSION}.jar"
  gsutil cp ${DATAPROC_ASSETS_BUCKET}/dataproc-init-scripts/sqoop/${OJDBC_VERSION}.jar .
  #wget http://apachemirror.wuchna.com/sqoop/1.4.7/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
  mkdir -p /usr/lib/sqoop
  
  echo "uncompressing the sqoop import tar"
  tar -zxvf sqoop-${SQOOP_VERSION}.tar.gz  
  
  echo "moving sqoop import to /usr/lib/sqoop/"
  mv sqoop-${SQOOP_VERSION}/* /usr/lib/sqoop/
  
  echo "moving ojdbc jar /usr/lib/sqoop/"
  mv ${OJDBC_VERSION}.jar /usr/lib/sqoop/

  echo "setting environment variable for sqoop"
  cat>>/etc/environment<<EOF
PATH=$PATH:/usr/lib/sqoop/bin
EOF
  
  echo "sourcing environment variable"
  source /etc/environment
}



function main() {
  if [[ "${ROLE}" == 'Master' ]]; then
    echo "Since this is a master node so going to install Sqoop import"
    install_sqoop
  fi
}

main
