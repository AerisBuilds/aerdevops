#!/bin/bash

##################################################################
# Author         : jalaj agarwal
# Purpose        : Script to do general purpose housekeeping tasks
# Name           : general-task.sh
# Type           : Script

#
# Mod Log
# Date                  By                Jira                    Description
# ----------    -----------------       ----------              ---------------
# 2019-08-01      Jalaj Agarwal          ANA-2364                Inception
###############################################################################

set -euxo pipefail

# Use Python from /usr/bin instead of /opt/conda.
export PATH=/usr/bin:$PATH

# Variables for running this script
readonly ROLE="$(/usr/share/google/get_metadata_value attributes/dataproc-role)"

readonly DATAPROC_ASSETS_BUCKET="$(/usr/share/google/get_metadata_value attributes/ngrs-dataproc-assets-bucket)"
readonly DATAPROC_WAREHOUSE_BUCKET="$(/usr/share/google/get_metadata_value attributes/ngrs-dataproc-warehouse-bucket-name)"
readonly MOUNTED_FOLDER_FOR_BUCKET="$(/usr/share/google/get_metadata_value attributes/mounted-bucket-folder)"
readonly PROJECT_ZONE="$(/usr/share/google/get_metadata_value attributes/project-zone)"
readonly DEPLOYMENT_ENVIRONMENT="$(/usr/share/google/get_metadata_value attributes/deployment-environment)"

function install_gcfuse(){
  
  #Add the gcsfuse distribution URL as a package source and import its public key:
  export GCSFUSE_REPO=gcsfuse-`lsb_release -c -s`
  echo "deb http://packages.cloud.google.com/apt $GCSFUSE_REPO main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  
  #Update the list of packages available and install gcsfuse.
  sudo apt-get update
  
  echo "Installing gcsfuse"
  sudo apt-get install gcsfuse
  
  echo "updating the group to fuse"
  #sudo usermod -a -G fuse $USER
}

function mount_bucket(){
  echo "create a folder $MOUNTED_FOLDER_FOR_BUCKET where bucket needs to be mounted"
  mkdir -p $MOUNTED_FOLDER_FOR_BUCKET
  
  echo "Going to mount the bucket $DATAPROC_WAREHOUSE_BUCKET to folder $MOUNTED_FOLDER_FOR_BUCKET"
  gcsfuse $DATAPROC_WAREHOUSE_BUCKET $MOUNTED_FOLDER_FOR_BUCKET
  
  echo "checking if bucket is successfully mounted"
  bucket_mount=`df -h | grep $MOUNTED_FOLDER_FOR_BUCKET | wc -l`
  echo "bucket_mount count : $bucket_mount"
  if [ $bucket_mount -eq 0 ]
  then
        echo "$DATAPROC_WAREHOUSE_BUCKET doesn't seems to be mounted on $MOUNTED_FOLDER_FOR_BUCKET so retrying"
		mkdir -p /$MOUNTED_FOLDER_FOR_BUCKET
		gcsfuse $DATAPROC_WAREHOUSE_BUCKET $MOUNTED_FOLDER_FOR_BUCKET
		if [ $? -eq 0 ]
			then
				echo "$DATAPROC_WAREHOUSE_BUCKET bucket mounted successfully on $MOUNTED_FOLDER_FOR_BUCKET"
		else
		   echo "After 2 retries also, $DATAPROC_WAREHOUSE_BUCKET doesn't seems to be mounted on $MOUNTED_FOLDER_FOR_BUCKET"
		fi
   else
		echo "$DATAPROC_WAREHOUSE_BUCKET successfully mounted to folder $MOUNTED_FOLDER_FOR_BUCKET"
   fi
 
}

function prepare_directories(){
	echo "creating required directories"
	mkdir -p /u01/aeris/{data,tmp}
	cd /u01/aeris/data/
	mkdir -p {hlr,uscc,s3,aaa,voice,hss,neoaercloud,smsc,lte}
	cd /u01/aeris/data/hlr
	mkdir -p {cdma,gsm}
	cd /u01/aeris/data/hlr/cdma
	mkdir -p {unprocessed,downloads,filterData,listing,processed}
	cd /u01/aeris/data/hlr/cdma/downloads
	mkdir -p {hfe106,hfe001,hfe108,hfe002,hfe105,hfe107,hfe011,hfe009,hfe022,hfe019,hfe023,hfe012,hfe104,hfe024,hfe103,hfe102,hfe015,hfe018,hfe007,hfe008,hfe101,hfe021,hfe017,hfe010,hfe016,hfe020}
	cd /u01/aeris/data/hlr/cdma/listing
	mkdir -p {hfe106,hfe001,hfe108,hfe002,hfe105,hfe107,hfe011,hfe009,hfe022,hfe019,hfe023,hfe012,hfe104,hfe024,hfe103,hfe102,hfe015,hfe018,hfe007,hfe008,hfe101,hfe021,hfe017,hfe010,hfe016,hfe020}
	cd /u01/aeris/data/hlr/cdma/filterData
	mkdir -p {hfe106,hfe001,hfe108,hfe002,hfe105,hfe107,hfe011,hfe009,hfe022,hfe019,hfe023,hfe012,hfe104,hfe024,hfe103,hfe102,hfe015,hfe018,hfe007,hfe008,hfe101,hfe021,hfe017,hfe010,hfe016,hfe020}
	cd /u01/aeris/data/hlr/gsm
	mkdir -p {archived,downloads}
	cd /u01/aeris/data/uscc
	mkdir s3
	cd /u01/aeris/data/s3
	mkdir -p account_billing_summary/{backup,data}
	cd /u01/aeris/data/aaa
	mkdir -p {cdma,gsm}
	cd /u01/aeris/data/aaa/cdma 
	mkdir -p {unprocessed,downloads,concat,concatToMove,listing,processed,log}
	cd /u01/aeris/data/aaa/gsm
	mkdir -p {unprocessed,downloads,concat,concatToMove,listing,processed,log}
	cd /u01/aeris/data/aaa/gsm/listing
	mkdir -p {rad052,rad053,rad070,rad071,rad072,rad074,rad075,rad252,rad253}
	cd /u01/aeris/data/aaa/gsm/downloads
	mkdir -p {rad052,rad053,rad070,rad071,rad074,rad075,rad252,rad253}
	cd /u01/aeris/data/voice
	mkdir -p {csv,downloads,downloadsToMove}
	cd /u01/aeris/data/hss
	mkdir -p {csv,downloads,downloadsToMove}
	cd /u01/aeris/data/smsc
	mkdir -p {downloads,logFile,processed,topush}
	cd /u01/aeris/data/smsc/downloads
	mkdir -p {smsc-1,smsc-2}
	cd /u01/aeris/data/lte
	mkdir -p {downloads,downloadsToMove}
	cd /u01/aeris/data/neoaercloud
	mkdir -p {downloads,processed,topush}
	mkdir -p /var/deploy/export_pkg
	
	echo "creating folders in hdfs"
    hdfs dfs -mkdir -p /aeris/traffic/ip
    hdfs dfs -chmod -R 777 /aeris /hbase
	
	echo "changing permissions of directories"
	chmod -R 777 /u01 /var/deploy
}

function set_env_variables(){
  echo "setting deployment environment as variable"
  cat>>/etc/environment<<EOF
DEPLOYMENT_ENV=${DEPLOYMENT_ENVIRONMENT}
EOF
  
  echo "sourcing environment variable"
  source /etc/environment
}
function set_zone(){
     echo "setting zone as ${PROJECT_ZONE}"
     gcloud config set compute/zone ${PROJECT_ZONE}
}

function install_aws_client(){
    echo "Installing pip"
	apt install python3-pip -y 
	echo "Installing aws client"
	pip3 install awscli --upgrade
}

function main() {
  echo "Setting the environment variables"
	set_env_variables
	echo "Set zone for every node so that it is not asked everytime while ssh"
	set_zone
  if [[ "${ROLE}" == 'Master' ]]; then
    echo "Installing gcfuse"
    install_gcfuse
	echo "Mount bucket"
    mount_bucket
	echo "Preparing required directories"
	prepare_directories
    echo "Calling function to install aws client"
    install_aws_client
  fi
   echo "general-tasks.sh completed"	
}

main
