variable "project" {}
variable "environment" {}
variable "product_name" {}
variable "dataproc_service_account" {}
variable "dataproc_purpose" {}
variable "dataproc_subnetwork" {}
variable "dataproc_subnetwork_project" {}
variable "dataproc_image" {}
variable "dataproc_node_settings" {
  type = "map"
}
