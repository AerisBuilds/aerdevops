variable "project" {}
variable "productname" {}
variable "environment" {}
variable "dataproc_service_account" {}
variable "dataproc_purpose" {}
variable "dataproc_subnetwork" {}
variable "dataproc_subnetwork_project" {}
variable "dataproc_image" {}
variable "dataproc_node_settings" {
  type = "map"
}
