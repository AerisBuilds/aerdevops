module "dataproc" {
  source = "./components/dataproc"
  project = "${var.project}"
  productname = "${var.product_name}"
  environment = "${var.environment}"
  dataproc_node_settings = "${var.dataproc_node_settings}"
  dataproc_service_account = "${var.dataproc_service_account}"
  dataproc_purpose = "${var.dataproc_purpose}"
  dataproc_subnetwork = "${var.dataproc_subnetwork}"
  dataproc_subnetwork_project = "${var.dataproc_subnetwork_project}"
  
  dataproc_image = "${var.dataproc_image}"
}
