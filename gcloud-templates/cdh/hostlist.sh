#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#Set the GITREPO variable to the local directory where you have cloned this repository and create a file with SSH login and hostname.
#
GITREPO=$PWD
USER=ubuntu
CMSERVER=10.251.4.17
#
#
cat <<EOF >HOSTLIST
$USER@10.251.4.17
$USER@10.251.4.19
$USER@10.251.4.18
EOF

cat <<EOF >HOSTNAMES
cdh-ampsol-dev-masternode
cdh-ampsol-dev-slave-node1
cdh-ampsol-dev-slave-node2
EOF
