#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
echo "hiiiii its working fine now"





#if [ "$#" -ne 1 ]; then
#  echo "Usage: $0 GCS path of terraform output" >&2
#  exit 1
#fi
#GCS_PATH="$1"

echo "##########Prepare the Host Details from Terrafrom State###########"
# Read the gcs file name from argument
sh ./download-instance-metadata.sh gs://csb-ab-dev-tfstate-us-west1/test-cloudera/default.tfstate
#sh download-instance-metadata.sh $GCS_PATH

python3 ./parse-terraform-state.py

#echo "##########entry for ths hostlist###########"
#sh hostlist.sh
echo "#########Copy the scripts to all nodes#####"
sh ./copy-file.sh
echo "#######copied the scripts##################"
echo "###########configure hosts file############"
sh ./hostconfig.sh
echo "#############Formating the disk############"
sh ./format_disk.sh
echo "#########Formating completed###############"
echo "######Install the slave node#################"
echo "#########Installing the slave node##########"
sh ./install_slave.sh
echo "#############Install the master node########"
echo "#######Installing the master node###########"
sh ./install_master.sh
echo "#####Installation completed#########"