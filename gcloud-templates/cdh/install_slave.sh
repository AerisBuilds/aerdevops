#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#CMSERVER=ip-10-2-5-22.ec2.internal

sh execute_over_ssh.sh /tmp/setup-slave.sh
#
# for HOST in `cat HOSTNAMES`; do
#   echo "*** $HOST"
#     #ssh -i key/nrt-test.pem -o StrictHostKeyChecking=no -t $HOST "sudo bash $BOPT ./setup-slave.sh"
#     gcloud beta compute ssh $HOST --project  ampsolutions-dev-201903   --internal-ip --zone  us-west1-a --command "sudo bash $BOPT /tmp/setup-slave.sh"
#
#     done
#
# #
#
