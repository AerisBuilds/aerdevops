#!/usr/bin/env python3.6
import json
import csv
import collections


DEFAULT_OS_USER='ubuntu'

def parse_json(file):
    with open(file) as json_file:
        data = json.load(json_file)
        return data

def parse_ip(network_interfaces):
    if network_interfaces is not None:
         for interface in network_interfaces:
             if interface['name'] == 'nic0': # review if this is fixed always.
                 return interface['network_ip']

def build_instance_details(instances, os_user=DEFAULT_OS_USER):
    #print(instances)
    vm_list = []
    if instances is not None and instances['resources'] is not None:
        resources =  instances['resources']
        for resource in resources:
            if resource['type'] == 'google_compute_instance':
                vm_instances = resource['instances']
                for vm in vm_instances:
                    details = collections.OrderedDict()
                    attr = vm['attributes']
                    details['name']=attr['name']
                    ip = parse_ip(attr['network_interface'])
                    if attr['hostname'] is None or not attr['hostname'] :
                        raise ValueError('Hostname missing for Instance '+ ip)

                    details['hostname']=attr['hostname']
                    details['project']=attr['project']
                    details['zone']=attr['zone']
                    details['ip'] = ip
                    details['user'] = os_user
                    meta = attr['metadata']
                    role = meta.get('Role','')
                    if "cm-manager" in role:
                        role = "cm-manager"
                    details['role']=role
                    vm_list.append(details)
            else:
                continue
    return vm_list

def write_instance_details(details):
    keys = details[0].keys()
    # write as csv
    with open('instances.csv', 'w') as output:
        dict_writer = csv.DictWriter(output, keys)
        dict_writer.writeheader()
        dict_writer.writerows(details)

    with open('hostsentry.txt', 'w') as hostentry:
        for vm in details:
            hostentry.write(vm['ip'] + " "+ vm['hostname']+" "+vm['name']+"\n")


def main():
    instances = parse_json('instances.json')
    instance_details= build_instance_details(instances)
    write_instance_details(instance_details)

if __name__ == "__main__":
   main()
