#!/bin/bash
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
#Set the GITREPO variable to the local directory where you have cloned this repository and create a file with SSH login and hostname.
#

sh execute_over_ssh.sh /tmp/hosts.sh
#
# for HOST in `cat HOSTNAMES`; do
#  echo "*** $HOST"
#     #ssh -i key/nrt-test.pem -o StrictHostKeyChecking=no -t $HOST "sudo bash $BOPT ./hosts.sh"
#     gcloud beta compute ssh $HOST --project  ampsolutions-dev-201903   --internal-ip --zone  us-west1-a --command "sudo bash $BOPT /tmp/hosts.sh"
#
#     done
