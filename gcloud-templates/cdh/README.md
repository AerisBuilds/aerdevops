Cloudera Automatic Provisioning
===============================

These scripts deploy a Cloudera cluster, consisting of CDH and Cloudera Manager (CM). The cluster runs all of the available services in CDH, as well as the CM management services. You can use this as is to deploy a cluster or use it as a starting point to develop a more customized version that doesn't deploy all services or that uses different configurations.

Some limitations are as follows:

* It does not automatically validate the installation.
* The configurations used have not been extensively tested. So while the cluster comes up with all greens from a health check perspective, you should still validate that everything works.
* Not all management services are turned on, as some (ie Navigator) require a Cloudera Enterprise subscription.
* Accumulo is currently not installed.
* It currently works with CDH5.0. It can be modified to support CDH4.x, but it won't work without some modifications.

Prerequisites:
--------------
- http://www.cloudera.com/content/cloudera-content/cloudera-docs/CM5/latest/Cloudera-Manager-Installation-Guide/cm5ig_cm_requirements.html


Assumptions:
------------
- Running on a RHEL 6.x machine
- All yum repos are set up. This is for common things like MySQL, expect, etc. A Cloudera repo is downloaded for Cloudera specific packages.
- The clouderaconfig.ini file must contain settings applicable to a given environment (eg the proper hostnames, the ntp server, etc). 
- The cdh.parcel.version property in clouderaconfig.ini should match a version that is available in http://archive.cloudera.com/cdh5/parcels/latest/ or in a customer parcel repo that you set up via the remote.parcel.repo.urls property. You can use 'latest' for the cdh.parcel.version property to make it automatically determine the parcel version to use from the latest avaiable versions in the Cloudera parcel repo.


Steps To Deploy a Cluster:
--------------------------

1. Ensure that clouderaconfig.ini is updated with the hostnames for the cluster, the root data directory (note that the default is to have one data directory, which is not recommended for production use), etc.

2. Execute the following scripts at the same time. There are sleeps at the appropriate points to account for temporal dependencies amongst the slave and master processes. Something like Ansible could of course be used to do this in a less hackish way.
    * sudo ./setup-slave.sh #run on all of the slave nodes (ie every node other than the master).
    * sudo ./setup-master.sh #run on the CM master node.

3. Go to http://$CM_MASTER_HOSTNAME:7180/cmf and log in (see the clouderaconfig.ini file for the credentials) to view the cluster.

Steps to modifications:
-----------------------

Provide the Hostname and Internal IP in ths file hosts.sh script:

Provide the lists of Internal IP in the hostlist.sh script:

Provide the Ip and user of master install_master.sh script:

Define the Cluster's node details in a clouderaconfig file.

Start the script:

1. clone the bitbucket
2. provide the permission
  chmod +x *.sh
  dos2unix *
  chmod 400 key/pemfile
  ./startup.sh
