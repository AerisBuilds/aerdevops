#!/bin/bash

function upgrade_packages() {
    TRIES=60
EXITCODE=1
while [[ ${EXITCODE} -ne 0 ]]; do
apt-get update && apt-get upgrade -y
        EXITCODE=$?
        if [[ ${EXITCODE} -eq 0 ]]; then
apt-get autoremove -y
            log_message "upgrade_packages(): Done"
return 0
fi
        while [[ ${TRIES} -eq 0 ]]; do
log_message "upgrade_packages(): Error"
exit 1
done
log_message "upgrade_packages(): Try ${TRIES}, waiting..."
sleep 30
let TRIES=TRIES-1
    done
log_message "upgrade_packages(): Done"
}

function install-nginx () {

sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt install nginx-core -y

echo "Nginx installation complete"

#restarting nginx service

sudo service nginx restart
ps -ef|grep nginx

#Install custom nginx.conf
}

#################### MAIN SCRIPT ####################
upgrade_packages
install-nginx 

