#######################################################
#######################################################
#Created : Abhishek Ranjan                      #######
#Email : abhishek.ranjan@aeris.net              #######
#Description : Start NGINX as reverse proxy     #######
#Created : 15th Aug  2019                       #######
#######################################################

# Load stream module
load_module "modules/ngx_stream_module.so";

worker_processes  auto;
events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    access_log  /var/log/nginx/access.log;
    sendfile        on;

    # Allow big server_name has tables
    # https://nginx.org/en/docs/http/server_names.html
    server_names_hash_bucket_size 128;
    server_names_hash_max_size 4096;

    #Set resolver from which nginx will resolve the ip of proxy_pass
    resolver 8.8.8.8 ;
    resolver_timeout 10s ;
    keepalive_timeout  65;
    proxy_connect_timeout   300;
    proxy_send_timeout      300;
    proxy_read_timeout      300;
    send_timeout            300;

    # Define upstream for af2-rabbitmq-primary
    upstream af2-rabbitmq-primary {
        server 172.16.1.145:15672 max_fails=1 fail_timeout=10s;
    }

    # Define proxy rule for ports, send to respective upstreams
    server {
        server_name rabbitmq-primary.aerframe.aeris.com;
        set $upstream_endpoint rabbitmq-primary.aerframe.aeris.com;
        listen 15672;
        ignore_invalid_headers off;
        location / {
            proxy_pass http://af2-rabbitmq-primary;
            proxy_set_header Host $host:$server_port;
        }
    }

    # Define upstream for af2-rabbitmq-secondary
    upstream af2-rabbitmq-secondary {
        server 172.16.1.146:15672 max_fails=1 fail_timeout=10s;
    }

    # Define proxy rule for ports, send to respective upstreams
    server {
        server_name rabbitmq-secondary.aerframe.aeris.com;
        set $upstream_endpoint rabbitmq-secondary.aerframe.aeris.com;
        listen 15672;
        ignore_invalid_headers off;
        location / {
            proxy_pass http://af2-rabbitmq-secondary;
            proxy_set_header Host $host:$server_port;
        }
    }
}