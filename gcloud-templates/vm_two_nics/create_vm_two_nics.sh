#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bootstrap.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bootstrap.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi
## Load variables from vars file

NUM_OF_INSTANCES=1
#Create nginx instance with two NIC's
#while [ NUM_OF_INSTANCES -lt "${NUMBER_OF_INSTANCES}" ]
#do 
    #Print the values
    echo "${NUMBER_OF_INSTANCES}"

gcloud compute --project="${PROJECT}" instances create "${NGINX_VM_NAME}" \
--zone="${ZONE}" \
    --machine-type="${NGINX_MACHINE_TYPE}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=cloud-platform,compute-rw \
    --image="${NGINX_IMAGE}" \
    --image-project="${IMAGE_PROJECT}" \
    --boot-disk-size="${NGINX_BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${NGINX_BOOT_DISK_NAME}" \
    #--metadata-from-file startup-script=startup-script.sh \
    --tags="${NGINX_NETWORK_TAGS}" \
    --network-interface subnet=projects/${NGINX_GATEWAY_SHARED_VPC_PROJECT}/regions/${NGINX_GATEWAY_SHARED_VPC_REGION}/subnetworks/${NGINX_GATEWAY_SHARED_VPC_SUBNET},no-address\
    --network-interface subnet="${NGINX_SUBNET}",no-address

#gcloud compute --project="${PROJECT}" instances attach-disk "${VM_NAME}" \
#   --disk="${ATTACH_DISK_NAME}" --zone="${ZONE}"
#done
