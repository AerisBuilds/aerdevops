#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bootstrap.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bootstrap.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi
## Load variables from vars file
# Load variables from vars file

echo "Variables file path: $1"

source $1

NUM_OF_INSTANCES=0
#Create cassandra instance with two NIC's
while [ "${NUM_OF_INSTANCES}" -lt "${CASSANDRA_NUMBER_OF_INSTANCES}" ]
do
    #Print the values
    echo "${CASSANDRA_NUMBER_OF_INSTANCES}"

gcloud compute --project="${PROJECT}" instances create "${CASSANDRA_VM_NAME}"-"${NUM_OF_INSTANCES}" \
--zone="${ZONE}" \
    --machine-type="${CASSANDRA_MACHINE_TYPE}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=cloud-platform,compute-rw \
    --image="${CASSANDRA_IMAGE}" \
    --image-project="${CASSANDRA_IMAGE_PROJECT}" \
    --boot-disk-size="${CASSANDRA_BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${CASSANDRA_BOOT_DISK_NAME}" \
    --tags="${CASSANDRA_NETWORK_TAGS}" \
	--create-disk=mode=rw,size="${CASSANDRA_DISK_SIZE}",type=projects/${PROJECT}/zones/${ZONE}/diskTypes/pd-standard,name="${CASSANDRA_DISK_NAME}"-"${NUM_OF_INSTANCES}",device-name="${CASSANDRA_DISK_NAME}"-"${NUM_OF_INSTANCES}" \
    --network-interface subnet=projects/${CASSANDRA_GATEWAY_SHARED_VPC_PROJECT}/regions/${CASSANDRA_GATEWAY_SHARED_VPC_REGION}/subnetworks/${CASSANDRA_GATEWAY_SHARED_VPC_SUBNET},no-address\
    --network-interface subnet="${CASSANDRA_SUBNET}",no-address

#gcloud compute disks create "${CASSANDRA_DISK_NAME}"-"${NUM_OF_INSTANCES}" \
#--zone="${ZONE}" \
	#--disk-name="${CASSANDRA_DISK_NAME}"-"${NUM_OF_INSTANCES}" \
#	--size="${CASSANDRA_DISK_SIZE}" \
#	--type="pd-ssd" 
	
#gcloud compute instances attach-disk "${CASSANDRA_VM_NAME}"-"${NUM_OF_INSTANCES}" \
#    --disk="${CASSANDRA_DISK_NAME}"-"${NUM_OF_INSTANCES}" --zone="${ZONE}" \
	
	
   NUM_OF_INSTANCES=$(( NUM_OF_INSTANCES +1 ))
   
done
