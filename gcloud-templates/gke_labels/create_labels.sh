#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables files!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bootstrap.sh path_to_tf_file path_to_conf_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bootstrap.sh vars-product/product-env.tf vars-product/product-env.conf"
    echo -e "\033[0m"
    exit 1
fi

echo "Variables tf file path: $1"

export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENVIRONMENT=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NODE_POOL_TAGS=$(grep "^node_pool_tags=" $1 | sed 's/node_pool_tags=//;s/\[//;s/\]//;s/"//g;s/,/ /g' )
gcloud beta container clusters get-credentials $PRODUCT_NAME-$ENVIRONMENT-$REGION --region $REGION --project $PROJECT

kubectl get nodes | awk '{if(NR>1)print $1}' > nodes

while read p; do
  for node in ${NODE_POOL_TAGS[@]}
  do
    unset labels
    if [[ $node == *":"* ]]; then
      # split server name from sub-list
      tmpnode=(${node//:/ })
      node_name=${tmpnode[0]}
      labels=${tmpnode[1]}
      # make array from simple string
      labels=(${labels//;/ })
    fi
    if [[ $p == *"$node_name"* ]]; then
      echo kubectl label nodes "$p" --overwrite ${labels[@]}
      kubectl label nodes "$p" --overwrite ${labels[@]}
    fi
  done
done <nodes

