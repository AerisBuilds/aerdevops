#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_dns_zone.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_dns_zone vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

echo "Enable Google Cloud DNS API"
gcloud --project=${PROJECT} services enable dns.googleapis.com

echo "Create DNS Zone on project"
gcloud dns --project=${PROJECT} managed-zones create ${PROJECT_ZONE} --description="${PROJECT}" --dns-name=${PROJECT_DNS}

if [[ "$?" -eq 0 ]]; then
    echo "Get NS records from DNS Zone"
    export NS_OLD=$(gcloud dns record-sets list --project=${HOST_PROJECT} --zone=${HOST_ZONE} --name=${PROJECT_DNS} --type=NS --format="table[no-heading](rrdatas)" | sed 's/,/ /g')
    export NS_NEW=$(gcloud dns record-sets list --project=${PROJECT} --zone=${PROJECT_ZONE} --name=${PROJECT_DNS} --type=NS --format="table[no-heading](rrdatas)" | sed 's/,/ /g')
else
    echo "Error while creating DNS zone"
    exit 1
fi

if [[ ! -z "${NS_NEW}" ]]; then
    echo "Create DNS delegation in hosting zone"
    gcloud dns --project=${HOST_PROJECT} record-sets transaction start --zone=${HOST_ZONE}

    if [[ ! -z "${NS_OLD}" ]]; then
        echo "Remove existing DNS delegation"
        gcloud dns --project=${HOST_PROJECT} record-sets transaction remove ${NS_OLD} --name=${PROJECT_DNS} --ttl=60 --type=NS --zone=${HOST_ZONE}
    fi

    gcloud dns --project=${HOST_PROJECT} record-sets transaction add ${NS_NEW} --name=${PROJECT_DNS} --ttl=60 --type=NS --zone=${HOST_ZONE}
    gcloud dns --project=${HOST_PROJECT} record-sets transaction execute --zone=${HOST_ZONE}

    if [[ "$?" -eq 0 ]]; then
        echo "Done"
        exit 0
    else
        echo "Error"
        exit 1
    fi
else
    echo "Error"
    exit 1
fi
