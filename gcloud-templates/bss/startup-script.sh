#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

create_routes() {
    #Create required static routes
    ifconfig | grep ens5
    if [ $? -eq 0 ]
    then
        route add -v -net 172.16.0.0/16 gw 172.16.1.1 dev ens5
        route add -v -net 10.60.0.0/14 gw 172.16.1.1 dev ens5
        route add -v -net 10.64.0.0/20 gw 172.16.1.1 dev ens5
        /home/ubuntu/routes.sh
    fi
}


function upgrade_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        export DEBIAN_FRONTEND=noninteractive
        apt-get update && apt-get upgrade -y
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            apt-get autoremove -y
            log_message "upgrade_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "upgrade_packages(): Error"
            exit 1
        done

        log_message "upgrade_packages(): Try ${TRIES}, waiting..."
        sleep 15
        let TRIES=TRIES-1
    done

    log_message "upgrade_packages(): Done"
}

function install_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get install -y ${*}
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            log_message "install_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "install_packages(): Error"
            exit 1
        done

        log_message "install_packages(): Try ${TRIES}, waiting..."
        sleep 15
        let TRIES=TRIES-1
    done

    log_message "install_packages(): Done"
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_dependencies() {
    install_packages \
        jq \
        rsync \
        git \
        python \
        python-pip \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    log_message "install_dependencies(): Done"
}
function install_docker() {
    # Install docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    install_packages docker-ce docker-ce-cli containerd.io

    pip install docker-compose
    if [[ $? -ne 0 ]]; then
        log_message "install_docker(): Error trying to install docker-compose"
        exit 1
    fi

    # Enable Docker service
    systemctl enable docker
    systemctl start docker
    

    log_message "install_docker(): Done"
}

#function configure_docker_repo() {
#    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
#    DOCKER_USER=$(echo ${METADATA} | jq -r .docker_user)
#    DOCKER_PASS=$(echo ${METADATA} | jq -r .docker_pass)
#    DOCKER_REPO=$(echo ${METADATA} | jq -r .docker_repo)
#    docker login -u "${DOCKER_USER}" -p "${DOCKER_PASS}" "${DOCKER_REPO}"
#    if [[ $? -ne 0 ]]; then
#        log_message "configure_docker_repo(): Error"
#        exit 1
#    else
#        log_message "configure_docker_repo(): Done"
#    fi
#}

function disable_startup_scripts() {
    systemctl disable google-shutdown-scripts.service
}

function deploy_terraform() {
    # Create docker directory
    mkdir -p /opt/terraform-server/scripts

    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)

    # symlink to use docker-credential-gcloud to auth against gcr
    ln -s /snap/google-cloud-sdk/current/bin/docker-credential-gcloud /usr/local/bin
    gcloud auth configure-docker --quiet
    #docker pull ${DOCKER_IMAGE}
    

cat <<END > /opt/terraform-server/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: terraform-server
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - /opt/terraform-server/scripts/:/scripts/
      - /opt/terraform-server/home/:/root/
END

    # Start docker container
    cd /opt/terraform-server
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_terraform(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_terraform(): Done"
    fi
}

post_deployment_steps() {
    #Install helm gcs plugin on container
    docker exec terraform-server /usr/local/bin/helm init --client-only
    docker exec terraform-server /usr/local/bin/helm plugin install https://github.com/nouney/helm-gcs --version 0.2.0
    #Install kubectl on host
    snap install kubectl --classic
    #Install helm on host
    wget https://storage.googleapis.com/kubernetes-helm/helm-v2.12.1-linux-amd64.tar.gz
    tar -zxvf helm-v2.12.1-linux-amd64.tar.gz
    mv linux-amd64/helm /usr/local/bin/helm
    /usr/local/bin/helm init --client-only
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

create_routes

upgrade_packages

install_dependencies

configure_ulimits

install_gcloud

install_docker

#configure_docker_repo

deploy_terraform

post_deployment_steps
# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
