#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_bootstrap.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_bootstrap.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1



# Disable deletion protection
gcloud compute --project="${PROJECT}" instances update "${VM_NAME}" \
    --zone="${ZONE}" --no-deletion-protection

# Delete VM 
gcloud compute --project="${PROJECT}" instances delete "${VM_NAME}" \
    --zone="${ZONE}" -q

