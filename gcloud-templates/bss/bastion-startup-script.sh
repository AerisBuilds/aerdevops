#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

create_routes() {
    #Create required static routes
    ifconfig | grep ens5
    if [ $? -eq 0 ]
    then
        route add -v -net 172.16.0.0/16 gw 172.16.1.1 dev ens5
        route add -v -net 10.60.0.0/14 gw 172.16.1.1 dev ens5
        route add -v -net 10.64.0.0/20 gw 172.16.1.1 dev ens5
        /home/ubuntu/routes.sh
    fi
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function disable_startup_scripts() {
    systemctl disable google-shutdown-scripts.service
}

post_deployment_steps() {
    #Initialize helm on host
    /usr/local/bin/helm init --client-only
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

create_routes

configure_ulimits

post_deployment_steps
# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
