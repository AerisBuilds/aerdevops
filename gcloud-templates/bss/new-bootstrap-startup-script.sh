#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

create_routes() {
    #Create required static routes
    ifconfig | grep ens5
    if [ $? -eq 0 ]
    then
        route add -v -net 172.16.0.0/16 gw 172.16.1.1 dev ens5
        route add -v -net 10.60.0.0/14 gw 172.16.1.1 dev ens5
        route add -v -net 10.64.0.0/20 gw 172.16.1.1 dev ens5
        /home/ubuntu/routes.sh
    fi
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_docker() {
    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function disable_startup_scripts() {
    systemctl disable google-shutdown-scripts.service
}

function deploy_terraform() {
    # Create docker directory
    mkdir -p /opt/terraform-server/scripts

    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)

    # symlink to use docker-credential-gcloud to auth against gcr
    ln -s /snap/google-cloud-sdk/current/bin/docker-credential-gcloud /usr/local/bin
    gcloud auth configure-docker --quiet
    

cat <<END > /opt/terraform-server/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: terraform-server
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - /opt/terraform-server/scripts/:/scripts/
      - /opt/terraform-server/home/:/root/
END

    # Start docker container
    cd /opt/terraform-server
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_terraform(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_terraform(): Done"
    fi
}

post_deployment_steps() {
    #Install helm gcs plugin on container
    docker exec terraform-server /usr/local/bin/helm init --client-only
    docker exec terraform-server /usr/local/bin/helm plugin install https://github.com/nouney/helm-gcs --version 0.2.0
    #Initialize helm
    /usr/local/bin/helm init --client-only
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

create_routes

configure_ulimits

install_docker

deploy_terraform

post_deployment_steps
# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"
