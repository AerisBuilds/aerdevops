#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bastion.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bastion.sh vars-product/product-env.conf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1

    gcloud compute --project="${PROJECT}" instances create "${BASTION_VM_NAME}" \
    --zone="${ZONE}" \
    --machine-type="${BASTION_MACHINE_TYPE}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=cloud-platform,compute-rw \
    --image="${BASTION_IMAGE}" \
    --image-project="${IMAGE_PROJECT}" \
    --boot-disk-size="${BASTION_BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${BASTION_BOOT_DISK_NAME}" \
    --metadata block-project-ssh-keys=TRUE\
    --metadata-from-file startup-script=bastion-startup-script.sh \
    --tags="${BASTION_NETWORK_TAGS}" \
    --network-interface subnet=projects/${NAT_GATEWAY_SHARED_VPC_PROJECT}/regions/${NAT_GATEWAY_SHARED_VPC_REGION}/subnetworks/${NAT_GATEWAY_SHARED_VPC_SUBNET},no-address\
    --network-interface subnet="${SUBNET}",no-address\
    --deletion-protection \
    --can-ip-forward
