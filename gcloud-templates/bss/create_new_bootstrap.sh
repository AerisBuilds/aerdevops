#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_bootstrap.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_bootstrap.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1

if [[ -z "$SINGLE_NIC" ]] ; then

    # Create VM instance with two nics
    gcloud compute --project="${PROJECT}" instances create "${VM_NAME}" \
    --zone="${ZONE}" \
    --machine-type="${MACHINE_TYPE}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=cloud-platform,compute-rw \
    --image="${IMAGE}" \
    --image-project="${IMAGE_PROJECT}" \
    --boot-disk-size="${BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${BOOT_DISK_NAME}" \
    --metadata docker_image="${DOCKER_IMAGE}",block-project-ssh-keys=TRUE\
    --metadata-from-file startup-script=new-bootstrap-startup-script.sh \
    --tags="${NETWORK_TAGS}" \
    --network-interface subnet=projects/${NAT_GATEWAY_SHARED_VPC_PROJECT}/regions/${NAT_GATEWAY_SHARED_VPC_REGION}/subnetworks/${NAT_GATEWAY_SHARED_VPC_SUBNET},no-address\
    --network-interface subnet="${SUBNET}",no-address\
    --deletion-protection


else

    # Create VM instance with one nic
    gcloud compute --project="${PROJECT}" instances create "${VM_NAME}" \
    --zone="${ZONE}" \
    --machine-type="${MACHINE_TYPE}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=cloud-platform,compute-rw \
    --image="${IMAGE}" \
    --image-project="${IMAGE_PROJECT}" \
    --boot-disk-size="${BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${BOOT_DISK_NAME}" \
    --metadata docker_image="${DOCKER_IMAGE}",block-project-ssh-keys=TRUE\
    --metadata-from-file startup-script=new-bootstrap-startup-script.sh \
    --tags="${NETWORK_TAGS}" \
    --network-interface subnet="${SUBNET}"\
    --deletion-protection
fi

