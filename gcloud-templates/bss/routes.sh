#!/bin/bash
gw_ens4=$(ip addr show ens4  | grep "inet\b" | awk '{print $2}' | cut -d/ -f1 | sed 's:[^.]*$:1:')

ifconfig | grep ens5
if [ $? -eq 0 ]
then 
    #### ADD YOUR ROUTES HERE ###
    route add -v -net 172.30.100.0/24 gw $gw_ens4 dev ens4
    route add -v -net 172.30.113.0/24 gw $gw_ens4 dev ens4
    route add -v -net 10.233.1.0/24 gw $gw_ens4 dev ens4
    route add -v -net 172.31.107.32/27 gw $gw_ens4 dev ens4
    route add -v -net 172.31.115.0/27 gw $gw_ens4 dev ens4
    route add -v -net 172.31.116.0/24 gw $gw_ens4 dev ens4
    ### DONT MODIFY ANYTHING BELOW THIS LINE ###
    route add -v -net 10.0.0.0/8 gw $gw_ens4 dev ens4
    route add -v -net 0.0.0.0/0 gw 172.16.1.1 dev ens5
    route del -v -net 0.0.0.0/0 gw $gw_ens4 dev ens4
fi
