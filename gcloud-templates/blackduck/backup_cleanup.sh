#!/bin/sh

mkdir -p backups
echo "Get projects list and parse it:"
python3.7 hub-rest-api-python/examples/project_counts.py | sed 1,2d | tr -s " " | cut -d " " -f 3 > projects.txt

input="projects.txt"
while IFS= read -r line
do
  mkdir -p backups/"$line"
  echo "Getting project versions for $line :"
  python3.7 hub-rest-api-python/examples/get_project_versions.py "$line" --out_file=version.txt
  dos2unix -q version.txt
  sed -i s/^.*\,//g version.txt
  input2="version.txt"
  while IFS2= read -r line2
  do
    mkdir -p backups/"$line"/"$line2"
    echo "Download project scan $line/$line2 :"
    python3.7 hub-rest-api-python/examples/get_project_version_scan_info.py "$line" "$line2" | grep "IN_PROGRESS"
    if [ $? -eq 0 ]; then
      echo "SCAN IN PROGRESS, DON'T GENERATE BACKUP"
    else
      timeout 1h python3.7 hub-rest-api-python/examples/download_project_scans.py "$line" "$line2" --outputdir backups/"$line"/"$line2"/
      if [ $? -eq 0 ]; then
        echo "Delete project scan $line/$line2"
        python3.7 hub-rest-api-python/examples/delete_project_version_and_scans.py "$line" "$line2"
      else
        echo "Backup failed for scan $line/$line2"
      fi
    fi
  done < "$input2"
done < "$input"
echo "Upload scans to GCS"
gsutil cp -r backups gs://securityhub-aeris-reports
#echo "Remove empty scans"
#python3.7 hub-rest-api-python/examples/remove_empty_codelocations.py --dry-run FALSE
echo "Remove empty project and versions"
python3.7 hub-rest-api-python/examples/delete_empty_projects_and_versions.py
