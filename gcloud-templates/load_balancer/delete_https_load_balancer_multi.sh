#!/bin/bash

#################### FUNCTIONS ####################
delete_external_ip_address() {
    echo "Delete external IP address"
    gcloud compute addresses delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --global \
        -q
}

delete_health_check() {
    echo "Delete health check"
    gcloud compute health-checks delete "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        -q
}

delete_backend_service() {
    echo "Delete backend service"
    gcloud compute backend-services delete "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --global \
        -q
}

delete_url_map() {
    echo "Delete URL map"
    gcloud compute url-maps delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        -q
}

delete_target_https_proxy() {
    echo "Delete target https proxy"
    gcloud compute target-https-proxies delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        -q
}

delete_target_http_proxy() {
    echo "Delete target http proxy"
    gcloud compute target-http-proxies delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        -q
}

delete_forwarding_rule() {
    echo "Delete forwarding rule"
    gcloud compute forwarding-rules delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --global \
        -q
}

delete_http_forwarding_rule() {
    echo "Delete http forwarding rule"
    gcloud compute forwarding-rules delete "http-${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --global \
        -q
}

delete_firewall_rule() {
    echo "Delete firewall rule"
    gcloud compute firewall-rules delete "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        -q
}


#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_https_load_balancer_multi.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_https_load_balancer_multi.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

echo "Deleting ${MULTI_BACKEND_LOAD_BALANCER_NAME} load balancer"

IFS=','
read -ra LB_ID <<< "${MULTI_BACKEND_LOAD_BALANCER_IDS}"

URL_MAP_DELETED="false"

# Delete the global forwarding rules
delete_forwarding_rule

# Delete the target HTTPS proxy
delete_target_https_proxy

if [ -n "${HTTP_FRONTEND_ENABLED}" ];
then
  # Delete the global http forwarding rules
  delete_http_forwarding_rule

  # Delete the target HTTP proxy
  delete_target_http_proxy
fi

for i in "${LB_ID[@]}"; do
    LOAD_BALANCER_NAME="${i^^}_LOAD_BALANCER_NAME"

    if [ "${URL_MAP_DELETED}" == "false" ];
    then
      # Delete the URL map
      delete_url_map
      URL_MAP_DELETED="true"
    fi

    # Delete the backend service
    delete_backend_service

    # Delete the health check
    delete_health_check
done

# Delete the external IP address for the load balancer
delete_external_ip_address

# Delete the load balancer firewall rule
delete_firewall_rule

echo "${MULTI_BACKEND_LOAD_BALANCER_NAME} load balancer deleted"
