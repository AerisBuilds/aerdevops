#!/bin/bash

#################### FUNCTIONS ####################
create_firewall_rule() {
    echo "Create firewall rule"
    ALLOW_PORTS=""

    for i in "${!NP[@]}"; do
        ALLOW_PORTS+="${!PROTOCOL}:${NP[i]},"
    done

    ALLOW_PORTS=${ALLOW_PORTS%?}

    if [[ ${PROJECT} =~ ${!FIREWALL_RULE_PROJECT} ]];
    then
        gcloud compute firewall-rules create ${!LOAD_BALANCER_NAME} \
            --project ${PROJECT} \
            --network ${!FIREWALL_RULE_NETWORK} \
            --source-ranges 0.0.0.0/0 \
            --allow "${ALLOW_PORTS}"
    else
        echo "Create firewall rule in project ${!FIREWALL_RULE_PROJECT}"
        gcloud compute firewall-rules create ${!LOAD_BALANCER_NAME} \
            --project ${!FIREWALL_RULE_PROJECT} \
            --network ${!FIREWALL_RULE_NETWORK} \
            --source-ranges 0.0.0.0/0 \
            --allow "${ALLOW_PORTS}"
    fi
}

create_health_check() {
    echo "Create health check"
    gcloud compute health-checks create tcp "${!LOAD_BALANCER_NAME}" \
        --project=${PROJECT} \
        --port=${NP[0]}
}

create_backend_service() {
    echo "Create backend service"
    gcloud compute backend-services create ${!LOAD_BALANCER_NAME} \
        --project=${PROJECT} \
        --load-balancing-scheme=internal \
        --protocol="${!PROTOCOL^^}" \
        --region=${!REGION} \
        --health-checks=${!LOAD_BALANCER_NAME}
}

add_instance_group_to_backend_service() {
    echo "Add instance groups to backend service"
    IFS=','

    for j in "${!IG[@]}"; do
        gcloud compute backend-services add-backend ${!LOAD_BALANCER_NAME} \
            --project=${PROJECT} \
            --region=${!REGION} \
            --instance-group=${IG[j]} \
            --instance-group-zone=${IGZ[j]}
    done
}

create_forwarding_rule() {
    echo "Create forwarding rule"
    gcloud compute forwarding-rules create ${!LOAD_BALANCER_NAME} \
        --project=${PROJECT} \
        --region=${!REGION} \
        --load-balancing-scheme=internal \
        --network=${!NETWORK} \
        --subnet=${!SUBNET} \
        --ip-protocol="${!PROTOCOL^^}" \
        --ports="${!LOAD_BALANCER_NODE_PORT}" \
        --backend-service=${!LOAD_BALANCER_NAME} \
        --backend-service-region=${!REGION}
}


#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_internal_tcp_udp_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_internal_tcp_udp_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

IFS=','
read -ra LB_ID <<< "${INTERNAL_LOAD_BALANCER_IDS}"

for ILB in "${LB_ID[@]}"; do
    echo "Creating ${ILB} load balancer"

    LOAD_BALANCER_NAME="${ILB^^}_ILB_NAME"
    LOAD_BALANCER_NODE_PORT="${ILB^^}_ILB_NODE_PORT"
    PROTOCOL="${ILB^^}_ILB_PROTOCOL"
    INSTANCE_GROUP="${ILB^^}_ILB_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${ILB^^}_ILB_INSTANCE_GROUP_ZONE"
    REGION="${ILB^^}_ILB_REGION"
    NETWORK="${ILB^^}_ILB_NETWORK"
    SUBNET="${ILB^^}_ILB_SUBNET"
    FIREWALL_RULE_PROJECT="${ILB^^}_ILB_FIREWALL_RULE_PROJECT"
    FIREWALL_RULE_NETWORK="${ILB^^}_ILB_FIREWALL_RULE_NETWORK"

    read -ra NP <<< "${!LOAD_BALANCER_NODE_PORT}"
    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"

    # Create load balancer firewall rule
    create_firewall_rule

    # Create a HTTP health check to test TCP connectivity to the VMs
    create_health_check

    # Create the backend service for HTTP traffic
    create_backend_service

    # Add the instance groups to the backend services
    add_instance_group_to_backend_service

    # Create a forwarding rule for the backend service
    create_forwarding_rule

    echo "${ILB} internal ${!PROTOCOL} load balancer created"

done