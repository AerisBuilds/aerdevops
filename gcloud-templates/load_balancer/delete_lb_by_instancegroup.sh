#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_lb_by_instancegroup.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_lb_by_instancegroup.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"



export PRODUCT_NAME=$(grep "^product_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export ENV=$(grep "^environment=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export REGION=$(grep "^region=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')


INSTANCE_GROUPS_LIST=$(gcloud compute instance-groups list --project "${PROJECT}" --format=json | jq -r --arg NETWORK "$NETWORK" '.[] | select(.network | contains ($NETWORK)) | .name')
# TARGETPOOLS_LIST=$(gcloud compute target-pools list --project "${PROJECT}" --format=json)
BACKEND_SERVICES=$(gcloud compute backend-services list --project "${PROJECT}" --format=json)
FORWADING_RULES=$(gcloud compute forwarding-rules list --project "${PROJECT}" --format=json)
URL_MAPS=$(gcloud compute url-maps list --project "${PROJECT}" --format=json)
TARGET_HTTPS_PROXIES=$(gcloud compute target-https-proxies list --project "${PROJECT}" --format=json)
TARGET_HTTP_PROXIES=$(gcloud compute target-http-proxies list --project "${PROJECT}" --format=json)
TARGET_SSL_PROXIES=$(gcloud compute target-ssl-proxies list --project "${PROJECT}" --format=json)
ADDRRESSES=$(gcloud compute addresses list --project $PROJECT --format json)

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
INSTANCE_GROUPS=($INSTANCE_GROUPS_LIST) # split to array $names
IFS=$SAVEIFS 

BACKENDS_LIST=""
for INSTANCE_GROUP in "${INSTANCE_GROUPS[@]}"
do
   :

    BACKENDS_WITH_INSTANCE_GROUP=$(echo $BACKEND_SERVICES | jq --arg INSTANCE_GROUP "$INSTANCE_GROUP" '.[] | select(.backends[]?.group | contains($INSTANCE_GROUP)) | .name' | cut -d'"' -f2)
    if [[ "${BACKENDS_WITH_INSTANCE_GROUP}" != "" ]]; then
        printf -v c "$BACKENDS_WITH_INSTANCE_GROUP\n" 
        BACKENDS_LIST+=$c     
    fi

done



SAVEIFS=$IFS   # Save current IFS
IFS=$' '      # Change IFS to new line
BACKENDS_ARRAY=($BACKENDS_LIST) # split to array $names
IFS=$SAVEIFS 
BACKENDS_INVENTORY_RAW=$(echo "${BACKENDS_ARRAY[@]}" | sort | uniq )

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
TARGETPOOLS_INVENTORY=($TARGETPOOLS_INVENTORY_RAW)
BACKENDS_INVENTORY=($BACKENDS_INVENTORY_RAW)
IFS=$SAVEIFS


HEALTH_CKECKS_INVENTORY=""
for BACKEND in "${BACKENDS_INVENTORY[@]}"
do :

    HEALTH_CKECKS=$(echo $BACKEND_SERVICES |  jq --arg BACKEND "$BACKEND" '.[] | select (.selfLink | contains($BACKEND)) | .healthChecks[]' | cut -d '"' -f 2 | awk -F/ '{print $NF}') 
    if [[ "${HEALTH_CKECKS}" != "" ]]; then
        printf -v c "$HEALTH_CKECKS\n" 
        HEALTH_CKECKS_INVENTORY+=$c     
    fi   
    
    GLOBAL=$(echo $BACKEND_SERVICES | jq -r --arg BACKEND "$BACKEND" '.[] | select(.selfLink | contains($BACKEND)) | select(.selfLink | contains("global")) | .name')

    if [[ "${GLOBAL}" != "" ]]; then
                echo "global BACKEND: $BACKEND"
                
                URL_MAP=$(echo $URL_MAPS | jq --arg BACKEND "$BACKEND" '.[] | if .hostRules[]?.pathMatcher == null then select(.defaultService | contains($BACKEND)) else select((.defaultService | contains($BACKEND)) or (.hostRules[].pathMatcher | contains($BACKEND))) end | .name ' | cut -d'"' -f2 | awk 'NR==1{print $1}')    
                if [[ "${URL_MAP}" != "" ]]; then
                    TARGET_HTTPS_PROXIE=$(echo $TARGET_HTTPS_PROXIES |  jq --arg URL_MAP "$URL_MAP" '.[] | select(.urlMap | contains($URL_MAP)) |.name' | cut -d'"' -f2)    
                    TARGET_HTTP_PROXIE=$(echo $TARGET_HTTP_PROXIES |  jq --arg URL_MAP "$URL_MAP" '.[] | select(.urlMap | contains($URL_MAP)) |.name' | cut -d'"' -f2)    


                    if [[ "${TARGET_HTTPS_PROXIE}" != "" ]]; then
                        FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg TARGET_HTTPS_PROXIE "$TARGET_HTTPS_PROXIE" '.[] | select( .target != null) | select(.target | contains($TARGET_HTTPS_PROXIE)) | .name' | cut -d'"' -f2)
                        IP_FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg TARGET_HTTPS_PROXIE "$TARGET_HTTPS_PROXIE" '.[] | select( .target != null) | select(.target | contains($TARGET_HTTPS_PROXIE)) | .IPAddress' | cut -d'"' -f2 | awk 'NR==1{print $1}')
                        IP_NAME=$(echo $ADDRRESSES | jq --arg IP "$IP_FORWADING_RULE" '.[] | select (.address == $IP ) | .name' | cut -d'"' -f2)                        
                        echo "Deleting forwading rule: $FORWADING_RULE"
                        gcloud compute forwarding-rules delete $FORWADING_RULE --global -q --project "${PROJECT}"
                        echo "Deleting ip address: $IP_NAME"
                        gcloud compute addresses delete --project $PROJECT --global $IP_NAME -q                         
                        echo "Deleting https proxie: $TARGET_HTTPS_PROXIE"
                        gcloud compute target-https-proxies delete $TARGET_HTTPS_PROXIE -q --project "${PROJECT}"
                    fi
                    if [[ "${TARGET_HTTP_PROXIE}" != "" ]]; then
                        FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg TARGET_HTTP_PROXIE "$TARGET_HTTP_PROXIE" '.[] | select( .target != null) | select(.target | contains($TARGET_HTTP_PROXIE)) | .name' | cut -d'"' -f2)
                        IP_FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg TARGET_HTTP_PROXIE "$TARGET_HTTP_PROXIE" '.[] | select( .target != null) | select(.target | contains($TARGET_HTTP_PROXIE)) | .IPAddress' | cut -d'"' -f2 | awk 'NR==1{print $1}')
                        IP_NAME=$(echo $ADDRRESSES | jq --arg IP "$IP_FORWADING_RULE" '.[] | select (.address == $IP ) | .name' | cut -d'"' -f2)
                        echo "Deleting forwading rule: $FORWADING_RULE"
                        gcloud compute forwarding-rules delete $FORWADING_RULE --global -q --project "${PROJECT}"
                        echo "Deleting ip address: $IP_NAME"
                        gcloud compute addresses delete --project $PROJECT --global $IP_NAME -q
                        echo "Deleting http proxie: $TARGET_HTTP_PROXIE"
                        gcloud compute target-http-proxies delete $TARGET_HTTP_PROXIE -q --project "${PROJECT}"
                    fi
                    
                    echo "Deleting url map: $URL_MAP"
                    gcloud compute url-maps delete $URL_MAP -q --project "${PROJECT}"
                    echo "Deleting backend: $BACKEND"
                    gcloud compute backend-services delete --global $BACKEND -q --project "${PROJECT}"                
                
                else
                    TARGET_SSL_PROXIE=$(echo $TARGET_SSL_PROXIES | jq --arg BACKEND "$BACKEND" '.[] | select(.service | contains($BACKEND)) | .name' | cut -d'"' -f2)
                    FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg TARGET_SSL_PROXIE "$TARGET_SSL_PROXIE" '.[] | select( .target != null) | select(.target | contains($TARGET_SSL_PROXIE)) | .name' | cut -d'"' -f2)                    
                    echo "Deleting ssl proxie: $TARGET_SSL_PROXIE"
                    gcloud compute target-ssl-proxies delete $TARGET_SSL_PROXIE -q --project "${PROJECT}"
                    
                    echo "Deleting forwading rule: $FORWADING_RULE"
                    gcloud compute forwarding-rules delete $FORWADING_RULE --global -q --project "${PROJECT}"
                    echo "Deleting backend: $BACKEND"
                    gcloud compute backend-services delete --global $BACKEND -q --project "${PROJECT}"
                fi

 
    else
                    FORWADING_RULE=$(echo $FORWADING_RULES | jq --arg BACKEND "$BACKEND" '.[] | select( .backendService != null) | select(.backendService | contains($BACKEND)) | .name' | cut -d'"' -f2)
                    RULE_REGION=$(echo $FORWADING_RULES | jq --arg FORWADING_RULE $FORWADING_RULE '.[] | select(.name == $FORWADING_RULE)|select(.region != null) | .region' | cut -d '"' -f 2 | awk -F/ '{print $NF}')
                    echo "Deleting forwading rule: $FORWADING_RULE"
                    gcloud compute forwarding-rules  delete $FORWADING_RULE --project "${PROJECT}" --region ${RULE_REGION} -q
                    BACKEND_REGION=$(echo $BACKEND_SERVICES | jq --arg BACKEND $BACKEND '.[] | select(.name == $BACKEND)|select(.region != null) | .region' | cut -d '"' -f 2 | awk -F/ '{print $NF}')
                    echo "Deleting backend service: $BACKEND"  
                    gcloud compute backend-services delete --region $BACKEND_REGION $BACKEND -q --project "${PROJECT}"                   


    fi 

done




    HEALTH_CKECKS_INVENTORY_SORT=$(echo "$HEALTH_CKECKS_INVENTORY" | sort | uniq )

    SAVEIFS=$IFS   # Save current IFS
    IFS=$'\n'      # Change IFS to new line
    HEALTH_CKECKS=($HEALTH_CKECKS_INVENTORY_SORT) # split to array $names
    IFS=$SAVEIFS

    for HEALTH_CHECK in "${HEALTH_CKECKS[@]}"
    do :

        echo "deleting health check: $HEALTH_CHECK"

        gcloud compute health-checks delete $HEALTH_CHECK -q --project "${PROJECT}"
    done

