#!/bin/bash

#################### FUNCTIONS ######################
delete_target_pools() {
    echo "Delete target pools"

    gcloud compute target-pools delete "${TARGET_POOLS}" \
            --region "${REGION}" \
            --http-health-check "${HTTP-HEALTH-CHECK}" \
            --project "${PROJECT}" \
	    -q
}

delete_external_ip_address() {
    echo "Delete External IP address"
    gcloud compute addresses delete "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --region "${REGION}" \
	-q
}

delete_forwarding_rule() {
    echo "Delete forwarding rule"
    gcloud compute forwarding-rules delete "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --address "${!LOAD_BALANCER_NAME}" \
        --target-pool "${TARGET_POOLS}" \
        --ports "${CUSTOM_PORT}" \

}

#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_external_tcp_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_external_tcp_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi
# Load variables from vars file
echo "Variables file path: $1"
source $1

IFS=','
read -ra LB_ID <<< "${LOAD_BALANCER_IDS}"

for i in "${LB_ID[@]}"; do
    echo "Deleting ${i} load balancer"

    LOAD_BALANCER_NAME="${ELB^^}_ELB_NAME"
    LOAD_BALANCER_NODE_PORT="${ELB^^}_ELB_NODE_PORT"
    PROTOCOL="${ELB^^}_ELB_PROTOCOL"
    INSTANCE_GROUP="${ELB^^}_ELB_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${ELB^^}_ELB_INSTANCE_GROUP_ZONE"
    REGION="${ELB^^}_ELB_REGION"
    NETWORK="${ELB^^}_ELB_NETWORK"
    SUBNET="${ELB^^}_ELB_SUBNET"
    FIREWALL_RULE_PROJECT="${ELB^^}_ELB_FIREWALL_RULE_PROJECT"
    FIREWALL_RULE_NETWORK="${ELB^^}_ELB_FIREWALL_RULE_NETWORK"
    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"

    # Delete a health check
    #create_health_check

    # Delete global forwarding rules to route incoming requests to the proxy
    delete_forwarding_rule

    # Create an external IP address for the load balancer
    delete_external_ip_address
    #EXTERNAL_IP_ADDRESS="$(gcloud compute --project="${PROJECT}" addresses list | grep "${!LOAD_BALANCER_NAME}" | awk '{print $2}')"

    # Delete a target pool
    delete_target_pools

    # Create a backend service for the content provider
    #create_backend_service

    # Add the instance groups as backends to the backend services
    #add_instance_group_to_backend_service
    # Create a URL map that directs all incoming requests to the instances
    #create_url_map

    # Manage instance groups
    #manage_instance_groups

    # Create a target HTTPS proxy to route requests to the URL map
    # create_target_https_proxy

    # Create load balancer firewall rule
    #create_firewall_rule

    echo "${i} load Balancer deleted: https://${EXTERNAL_IP_ADDRESS}"

done	
