#!/bin/bash

#################### FUNCTIONS ####################
delete_external_ip_address() {
    echo "Delete external IP address"
    gcloud compute addresses delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        --global \
        -q
}


delete_health_check() {
    echo "Delete health check"
    gcloud compute health-checks delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        -q
}

delete_backend_service() {
    echo "Delete backend service"
    gcloud compute backend-services delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        --global \
        -q
}

# add_instance_group_to_backend_service() {
#     echo "Add instance groups to backend service"
#     IFS=','

#     for j in "${!IG[@]}"; do
#         gcloud compute backend-services add-backend ${!LOAD_BALANCER_NAME} \
#             --project ${PROJECT} \
#             --balancing-mode RATE \
#             --max-rate ${!MAX_RATE} \
#             --capacity-scaler 1 \
#             --instance-group ${IG[j]} \
#             --instance-group-zone ${IGZ[j]} \
#             --global
#     done
# }



delete_url_map() {
    echo "Delete URL map"
    gcloud compute url-maps delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        -q
}

delete_target_https_proxy() {
    echo "Delete target https proxy"
    gcloud compute target-https-proxies delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        -q
}

delete_forwarding_rule() {
    echo "Delete forwarding rule"
    gcloud compute forwarding-rules delete ${!LOAD_BALANCER_NAME} \
        --project ${PROJECT} \
        --global \
        -q
}

delete_firewall_rule() {
    echo "Delete firewall rule"

    if [[ ${PROJECT} =~ ${!FIREWALL_RULE_PROJECT} ]];
        then
            gcloud compute firewall-rules delete ${!LOAD_BALANCER_NAME} \
                --project ${PROJECT} -q
        else
            echo "Create firewall rule in project ${!FIREWALL_RULE_PROJECT}"
            gcloud compute firewall-rules delete ${!LOAD_BALANCER_NAME} \
                --project ${!FIREWALL_RULE_PROJECT} -q
    fi
}


#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

IFS=','
read -ra LB_ID <<< "${LOAD_BALANCER_IDS}"

for i in "${LB_ID[@]}"; do
    echo "Deleting ${i} load balancer"

    LOAD_BALANCER_NAME="${i^^}_LOAD_BALANCER_NAME"
    LOAD_BALANCER_NODE_PORT="${i^^}_LOAD_BALANCER_NODE_PORT"
    BACKEND_SERVICE_PORT_NAME="${i^^}_BACKEND_SERVICE_PORT_NAME"
    INSTANCE_GROUP="${i^^}_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${i^^}_INSTANCE_GROUP_ZONE"
    MAX_RATE="${i^^}_MAX_RATE"
    SSL_CERTIFICATES="${i^^}_SSL_CERTIFICATES"
    FIREWALL_RULE_PROJECT="${i^^}_FIREWALL_RULE_PROJECT"
    FIREWALL_RULE_NETWORK="${i^^}_FIREWALL_RULE_NETWORK"

    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"

    EXTERNAL_IP_ADDRESS="$(gcloud compute --project=${PROJECT} addresses list | grep ${!LOAD_BALANCER_NAME} | awk '{print $2}')"

    
    
    # Delete global forwarding rules to route incoming requests to the proxy
    delete_forwarding_rule
    
    # Delete a target HTTPS proxy to route requests to the URL map
    delete_target_https_proxy

    # Delete a URL map that directs all incoming requests to the instances
    delete_url_map

    # Delete a backend service for the content provider
    delete_backend_service

    # Delete a health check
    delete_health_check
    
    # Delete an external IP address for the load balancer
    delete_external_ip_address

    # Delete load balancer firewall rule
    delete_firewall_rule

    echo "${i} load Balancer deleted: ${!LOAD_BALANCER_NAME}"

done