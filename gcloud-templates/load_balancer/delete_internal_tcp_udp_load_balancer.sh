#!/bin/bash

#################### FUNCTIONS ####################
delete_firewall_rule() {
    echo "Delete firewall rule"
    ALLOW_PORTS=""

    for i in "${!NP[@]}"; do
        ALLOW_PORTS+="${!PROTOCOL}:${NP[i]},"
    done

    ALLOW_PORTS=${ALLOW_PORTS%?}

    if [[ ${PROJECT} =~ ${!FIREWALL_RULE_PROJECT} ]];
    then
        gcloud compute firewall-rules delete ${!LOAD_BALANCER_NAME} \
            --project ${PROJECT} \
            -q 
    else
        echo "Delete firewall rule in project ${!FIREWALL_RULE_PROJECT}"
        gcloud compute firewall-rules delete ${!LOAD_BALANCER_NAME} \
            --project ${!FIREWALL_RULE_PROJECT} \
            -q
    fi
}

delete_health_check() {
    echo "Delete health check"
    gcloud compute health-checks delete "${!LOAD_BALANCER_NAME}" \
        --project=${PROJECT} \
        -q
}

delete_backend_service() {
    echo "Delete backend service"
    gcloud compute backend-services delete ${!LOAD_BALANCER_NAME} \
        --project=${PROJECT} \
        --region=${!REGION} \
        -q
}


delete_forwarding_rule() {
    echo "Delete forwarding rule"
    gcloud compute forwarding-rules delete ${!LOAD_BALANCER_NAME} \
        --project=${PROJECT} \
        --region=${!REGION} \
        -q
}


#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./delete_internal_tcp_udp_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./delete_internal_tcp_udp_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

IFS=','
read -ra LB_ID <<< "${INTERNAL_LOAD_BALANCER_IDS}"

for ILB in "${LB_ID[@]}"; do
    echo "Deleating ${ILB} load balancer"

    LOAD_BALANCER_NAME="${ILB^^}_ILB_NAME"
    LOAD_BALANCER_NODE_PORT="${ILB^^}_ILB_NODE_PORT"
    PROTOCOL="${ILB^^}_ILB_PROTOCOL"
    INSTANCE_GROUP="${ILB^^}_ILB_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${ILB^^}_ILB_INSTANCE_GROUP_ZONE"
    REGION="${ILB^^}_ILB_REGION"
    NETWORK="${ILB^^}_ILB_NETWORK"
    SUBNET="${ILB^^}_ILB_SUBNET"
    FIREWALL_RULE_PROJECT="${ILB^^}_ILB_FIREWALL_RULE_PROJECT"
    FIREWALL_RULE_NETWORK="${ILB^^}_ILB_FIREWALL_RULE_NETWORK"

    read -ra NP <<< "${!LOAD_BALANCER_NODE_PORT}"
    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"

    # Delete load balancer firewall rule
    delete_firewall_rule

    # Delete a forwarding rule for the backend service
    delete_forwarding_rule

    # Delete the backend service for HTTP traffic
    delete_backend_service

    # Delete a HTTP health check to test TCP connectivity to the VMs
    delete_health_check





    echo "${ILB} internal ${!PROTOCOL} load balancer deleted"

done