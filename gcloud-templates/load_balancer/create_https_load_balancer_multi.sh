#!/bin/bash

#################### FUNCTIONS ####################
create_external_ip_address() {
    echo "Create external IP address"
    gcloud compute addresses create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --ip-version=IPV4 \
        --global
}

define_https_service_port_name() {
    echo "Define https service port name"
    IFS=','

    for j in "${!IG[@]}"; do
        NAMED_PORTS_OUTPUT="$(gcloud compute --project "${PROJECT}" instance-groups get-named-ports "${IG[j]}" --zone "${IGZ[j]}" | awk '{if(NR>1)print}')"
        NAMED_PORT="$(echo "${NAMED_PORTS_OUTPUT}" | grep "${!BACKEND_SERVICE_PORT_NAME}" | awk '{print $1}')"

        if [ -z "${NAMED_PORT}" ];
        then
            NAMED_PORTS=""
            IFS=$(echo -en "\n")

            while read -a NAMED_PORTS_OUTPUT_LINE;
            do
                NAME="$(echo "${NAMED_PORTS_OUTPUT_LINE}" | awk '{print $1}')"
                PORT="$(echo "${NAMED_PORTS_OUTPUT_LINE}" | awk '{print $2}')"

                if [[ -n "${NAME}" && -n "${PORT}" ]];
                then
                    NAMED_PORTS+="${NAME}:${PORT},"
                fi
            done <<< "${NAMED_PORTS_OUTPUT}"

            NAMED_PORTS+="${!BACKEND_SERVICE_PORT_NAME}:${!LOAD_BALANCER_NODE_PORT}"
            gcloud compute instance-groups set-named-ports "${IG[j]}" \
                --project "${PROJECT}" \
                --named-ports "${NAMED_PORTS}" \
                --zone "${IGZ[j]}"
        else
            echo "Named port ${NAMED_PORT} already defined in instance group ${IG[j]}"
        fi
    done
}

create_health_check() {
    echo "Create health check"
    gcloud compute health-checks create tcp "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --port "${!LOAD_BALANCER_NODE_PORT}"
}

create_backend_service() {
    echo "Create backend service"
    gcloud compute backend-services create "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --protocol HTTP \
        --health-checks "${!LOAD_BALANCER_NAME}" \
        --port-name "${!BACKEND_SERVICE_PORT_NAME}" \
        --session-affinity "${!SESSION_AFFINITY}" \
        --timeout "${!BACKEND_TIMEOUT}" \
        --connection-draining-timeout "${!DRAINING_TIMEOUT}" \
        --global
}

add_instance_group_to_backend_service() {
    echo "Add instance groups to backend service"
    IFS=','

    for j in "${!IG[@]}"; do
        gcloud compute backend-services add-backend "${!LOAD_BALANCER_NAME}" \
            --project "${PROJECT}" \
            --balancing-mode RATE \
            --max-rate "${!MAX_RATE}" \
            --capacity-scaler 1 \
            --instance-group "${IG[j]}" \
            --instance-group-zone "${IGZ[j]}" \
            --global
    done
}

create_url_map() {
    echo "Create URL map"
    gcloud compute url-maps create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --default-service "${!LOAD_BALANCER_NAME}"
}

add_url_path_rules() {
    echo "Add path matcher rules"
    gcloud compute url-maps add-path-matcher "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --path-matcher-name "${!LOAD_BALANCER_NAME}" \
        --default-service "${!LOAD_BALANCER_NAME}" \
        --backend-service-path-rules="/*=${!LOAD_BALANCER_NAME}" \
        --new-hosts="${!BACKEND_HOST}"
}

create_target_https_proxy() {
    echo "Create target https proxy"
    gcloud compute target-https-proxies create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --url-map "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --ssl-certificates "${!SSL_CERTIFICATES}"
}

create_target_http_proxy() {
    echo "Create target http proxy"
    gcloud compute target-http-proxies create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --url-map "${MULTI_BACKEND_LOAD_BALANCER_NAME}"
}

create_forwarding_rule() {
    echo "Create forwarding rule"
    gcloud compute forwarding-rules create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --address "${EXTERNAL_IP_ADDRESS}" \
        --target-https-proxy "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --global \
        --ports 443
}

create_http_forwarding_rule() {
    echo "Create http forwarding rule"
    gcloud compute forwarding-rules create "http-${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --address "${EXTERNAL_IP_ADDRESS}" \
        --target-http-proxy "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --global \
        --ports 80
}

create_firewall_rule() {
    echo "Create firewall rule"
    gcloud compute firewall-rules create "${MULTI_BACKEND_LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --network "${!FIREWALL_RULE_NETWORK}" \
        --source-ranges 0.0.0.0/0 \
        --allow "${ALLOW_PORTS}"
}


#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

echo "Creating ${MULTI_BACKEND_LOAD_BALANCER_NAME} load balancer"

# Create an external IP address for the load balancer
create_external_ip_address
EXTERNAL_IP_ADDRESS="$(gcloud compute --project="${PROJECT}" addresses list | grep "${MULTI_BACKEND_LOAD_BALANCER_NAME}" | awk '{print $2}')"

IFS=','
read -ra LB_ID <<< "${MULTI_BACKEND_LOAD_BALANCER_IDS}"

URL_MAP_CREATED="false"
ALLOW_PORTS=""

for i in "${LB_ID[@]}"; do

    LOAD_BALANCER_NAME="${i^^}_LOAD_BALANCER_NAME"
    LOAD_BALANCER_NODE_PORT="${i^^}_LOAD_BALANCER_NODE_PORT"
    BACKEND_SERVICE_PORT_NAME="${i^^}_BACKEND_SERVICE_PORT_NAME"
    INSTANCE_GROUP="${i^^}_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${i^^}_INSTANCE_GROUP_ZONE"
    MAX_RATE="${i^^}_MAX_RATE"
    SESSION_AFFINITY="${i^^}_SESSION_AFFINITY"
    BACKEND_TIMEOUT="${i^^}_BACKEND_TIMEOUT"
    DRAINING_TIMEOUT="${i^^}_DRAINING_TIMEOUT"
    SSL_CERTIFICATES="${i^^}_SSL_CERTIFICATES"
    BACKEND_HOST="${i^^}_BACKEND_HOST"
    FIREWALL_RULE_NETWORK="${i^^}_FIREWALL_RULE_NETWORK"

    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"

    # Define an HTTPS service and map a port name to the relevant port
    define_https_service_port_name

    # Create a health check
    create_health_check

    # Create a backend service for the content provider
    create_backend_service

    # Add the instance groups as backends to the backend services
    add_instance_group_to_backend_service

    if [ "${URL_MAP_CREATED}" == "false" ];
    then
      # Create a URL map that directs all incoming requests to the instances
      create_url_map
      URL_MAP_CREATED="true"
    fi

    # Add a path matcher to the URL map and define the request path mappings
    add_url_path_rules

    ALLOW_PORTS+="tcp:${!LOAD_BALANCER_NODE_PORT},"

done

ALLOW_PORTS=${ALLOW_PORTS%?}

# Create a target HTTPS proxy to route requests to the URL map
create_target_https_proxy

# Create global forwarding rules to route incoming requests to the proxy
create_forwarding_rule

if [ -n "${HTTP_FRONTEND_ENABLED}" ];
then
  # Create a target HTTP proxy to route requests to the URL map
  create_target_http_proxy

  # Create global http forwarding rules to route incoming requests to the proxy
  create_http_forwarding_rule
fi

# Create load balancer firewall rule
create_firewall_rule

echo "${MULTI_BACKEND_LOAD_BALANCER_NAME} load balancer created: https://${EXTERNAL_IP_ADDRESS}"
