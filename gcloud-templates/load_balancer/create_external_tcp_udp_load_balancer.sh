#!/bin/bash

#################### FUNCTIONS ####################
create_health_check() {
	echo "Create ${ETLB} Health Checks"
	gcloud  compute http-health-checks create "${!LOAD_BALANCER_NAME}" \
	--project "${PROJECT}" \
	--port=${NP[0]} \
	--request-path="${!REQUEST_PATH}"
}

create_target_pools() {
    echo "Create ${ETLB} target pools"
    gcloud compute target-pools create "${!LOAD_BALANCER_NAME}" \
	    --region "${!REGION}" \
        --http-health-check "${!LOAD_BALANCER_NAME}" \
	    --project "${PROJECT}"
}

create_firewall_rule() {
    echo "Create ${ETLB} Firewall Rules"
    IFS=","
    ALLOW_PORTS=""

    for i in "${!NP[@]}"; do
        ALLOW_PORTS+="${!PROTOCOL}:${NP[i]},"
    done

    ALLOW_PORTS=${ALLOW_PORTS%?}
		
	gcloud compute firewall-rules create "${!LOAD_BALANCER_NAME}" \
		--project "${!FIREWALL_RULE_PROJECT}" \
		--network "${!FIREWALL_RULE_NETWORK}" \
		--target-tags "${!INSTANCE_GROUP_TAG}" \
		--allow "${ALLOW_PORTS}"
}

set_target_pools_manage_instance_groups() {
    unset IFS
    tgp=`gcloud compute instance-groups managed describe "${IG}"   \
     --project "${PROJECT}" \
	 --region "${!REGION}"  \
	 --format='text' | grep -e ^targetPools | rev | cut -d'/' -f1 | rev | paste -d',' -s`
	 
	TARGET_POOLS=""
	if [[ -v $tgp ]];then
	TARGET_POOLS="${!LOAD_BALANCER_NAME}"
	else
	tgp=$(echo $tgp | sed -E "s|${!LOAD_BALANCER_NAME}||g")
	TARGET_POOLS="${tgp},${!LOAD_BALANCER_NAME}"
	TARGET_POOLS=$(echo $TARGET_POOLS |sed -E "s|,,|,|g" | sed -E "s|^,||")
	fi
	echo "TARGET_POOLS: $TARGET_POOLS"

    echo "Set target Pools For ${ETLB} In Instance Group"
    gcloud compute instance-groups managed set-target-pools "${IG}" \
      --target-pools "${TARGET_POOLS}" \
	  --project "${PROJECT}" \
	  --region "${!REGION}"
      IFS=","
}

create_external_ip_address() {
    echo "Create ${ETLB} External IP address"
    gcloud compute addresses create "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --region "${!REGION}" 
}

create_forwarding_rule() {
    echo "Create ${ETLB} forwarding rule"
    gcloud compute forwarding-rules create "${!LOAD_BALANCER_NAME}" \
        --project "${PROJECT}" \
        --address "${!LOAD_BALANCER_NAME}" \
        --target-pool "${!LOAD_BALANCER_NAME}" \
        --ports "${LOAD_BALANCER_PORT_RANGE}" \
        --region "${!REGION}"
}

#################### MAIN SCRIPT ####################

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_external_tcp_load_balancer.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_external_tcp_load_balancer.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1
IFS=','
read -ra LB_ID <<< "${EXTERNALTCP_LOAD_BALANCER_IDS}"
for ETLB in "${LB_ID[@]}"; do
    echo "Creating ${ETLB} load balancer"

    LOAD_BALANCER_NAME="${ETLB^^}_ETLB_NAME"
    LOAD_BALANCER_PORT="${ETLB^^}_ETLB_PORT"
    PROTOCOL="${ETLB^^}_ETLB_PROTOCOL"
    INSTANCE_GROUP="${ETLB^^}_ETLB_INSTANCE_GROUP"
    INSTANCE_GROUP_ZONE="${ETLB^^}_ETLB_INSTANCE_GROUP_ZONE"
    INSTANCE_GROUP_TAG="${ETLB^^}_ETLB_INSTANCE_GROUP_TAG"
    REGION="${ETLB^^}_ETLB_REGION"
    NETWORK="${ETLB^^}_ETLB_NETWORK"
    SUBNET="${ETLB^^}_ETLB_SUBNET"
    FIREWALL_RULE_PROJECT="${ETLB^^}_ETLB_FIREWALL_RULE_PROJECT"
    FIREWALL_RULE_NETWORK="${ETLB^^}_ETLB_FIREWALL_RULE_NETWORK"
	REQUEST_PATH="${ETLB^^}_ETLB_REQUEST_PATH"
	read -ra NP <<< "${!LOAD_BALANCER_PORT}"
    read -ra IG <<< "${!INSTANCE_GROUP}"
    read -ra IGZ <<< "${!INSTANCE_GROUP_ZONE}"
	end=`echo "${!LOAD_BALANCER_PORT}" | rev| cut -d',' -f1|rev`
	start=`echo "${!LOAD_BALANCER_PORT}" | cut -d',' -f1`
	LOAD_BALANCER_PORT_RANGE="$start-$end"

    # Create a health check
    create_health_check

    # Create load balancer firewall rule
    create_firewall_rule
	
    # Create an external IP address for the load balancer
    create_external_ip_address	
	
    # Create a target pool
    create_target_pools

    # Set targets pools: Manage instance groups
    set_target_pools_manage_instance_groups

    # Create global forwarding rules to route incoming requests to the proxy
    create_forwarding_rule
	
    EXTERNAL_IP_ADDRESS="$(gcloud compute --project="${PROJECT}" addresses list | grep "${!LOAD_BALANCER_NAME}" | awk '{print $2}')"
    echo "${ETLB} load Balancer created: https://${EXTERNAL_IP_ADDRESS}"

done
