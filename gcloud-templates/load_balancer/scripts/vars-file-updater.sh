#!/bin/bash

#################### MAIN ####################
if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./vars-file-updater.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./vars-file-updater.sh ../vars/vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM="$(gcloud --project=${PROJECT} compute instances describe ${NAT_GATEWAY_VM_NAME} --zone=${NAT_GATEWAY_ZONE} '--format=get(networkInterfaces[0].networkIP)')"
INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM="$(gcloud --project=${PROJECT} compute instances describe ${NAT_GATEWAY_VM_NAME} --zone=${NAT_GATEWAY_ZONE} '--format=get(networkInterfaces[1].networkIP)')"

if [[ -z "${INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM}" || -z "${INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM}" ]] ; then
  echo -e "\033[31m"
  echo -e "\033[31m Missing NAT Gateway VM IP's to update IPTABLES rules!"
  echo -e "\033[31m"
  echo -e "\033[0m"
  exit 1
fi

if grep -q "INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM" $1;
then
  echo "Replace current INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM"
  sed -i "s/.*INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM=.*/INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM=\"${INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM}\"/" "$1"
else
  echo "Adding INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM entry"
  echo "INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM=\"${INTERNAL_IP_NIC0_OF_NAT_GATEWAY_VM}\"" >> "$1"
fi

if grep -q "INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM" $1;
then
  echo "Replace current INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM"
  sed -i "s/.*INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM=.*/INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM=\"${INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM}\"/" "$1"
else
  echo "Adding INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM entry"
  echo "INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM=\"${INTERNAL_IP_NIC1_OF_NAT_GATEWAY_VM}\"" >> "$1"
fi

IFS=','
read -ra LB_ID <<< "${INTERNAL_LOAD_BALANCER_IDS}"

for ILB in "${LB_ID[@]}"; do
  LOAD_BALANCER_NAME="${ILB^^}_ILB_NAME"

  IP_LOAD_BALANCER=""
  AUX=1

  while [ -z "${IP_LOAD_BALANCER}" ]; do
    if [ "${AUX}" -gt 5 ]; then
      echo -e "Could not get the load balancer IP. Please verify if the load balancer was created properly"
      exit 1
    fi

    echo "Getting ${!LOAD_BALANCER_NAME} load balancer IP..."
    IP_LOAD_BALANCER="$(gcloud --project "${PROJECT}" compute forwarding-rules list | grep "${!LOAD_BALANCER_NAME}" | awk '{print $3}')"

    if [ -z "${IP_LOAD_BALANCER}" ]; then
      ((AUX+=1))
      sleep 60s
    fi
  done

  if grep -q "${ILB^^}_IP_LOAD_BALANCER" $1;
  then
    echo "Replace current LOAD BALANCER IP for ${!LOAD_BALANCER_NAME}"
    sed -i "s/.*${ILB^^}_IP_LOAD_BALANCER=.*/${ILB^^}_IP_LOAD_BALANCER=\"${IP_LOAD_BALANCER}\"/" "$1"
  else
    echo "Adding LOAD BALANCER IP entry for ${!LOAD_BALANCER_NAME}"
    echo "${ILB^^}_IP_LOAD_BALANCER=\"${IP_LOAD_BALANCER}\"" >> "$1"
  fi

  echo "vars file updated with ${!LOAD_BALANCER_NAME} IP ${IP_LOAD_BALANCER} entry"

done
