#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_service_account.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_service_account.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file
echo "Variables file path: $1"
source $1

# Create Service Account
echo "Creating service account roles"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_ID} --project=${PROJECT} --display-name="Terraform project ${PROJECT}"
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/viewer
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/storage.admin
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/dns.admin
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/container.clusterAdmin
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/serviceusage.serviceUsageAdmin
gcloud projects add-iam-policy-binding ${PROJECT_TOOLS} --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT}.iam.gserviceaccount.com --role roles/cloudbuild.builds.builder

gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SA_TOOLS} --role roles/container.admin

gcloud auth configure-docker --quiet
docker pull hello-world
docker tag hello-world gcr.io/${PROJECT_TOOLS}/hello-world
docker push gcr.io/${PROJECT_TOOLS}/hello-world

gsutil iam ch serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com:objectViewer gs://artifacts.${PROJECT_TOOLS}.appspot.com

echo "Service Account roles given!"
