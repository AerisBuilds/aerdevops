#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
    echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function upgrade_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get upgrade -y
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            apt-get autoremove -y
            log_message "upgrade_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "upgrade_packages(): Error"
            exit 1
        done

        log_message "upgrade_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "upgrade_packages(): Done"
}

function install_packages() {
    TRIES=60
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get install -y ${*}
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            log_message "install_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "install_packages(): Error"
            exit 1
        done

        log_message "install_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "install_packages(): Done"
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          65536
*                hard    nofile          65536
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function configure_kernel() {
cat <<EOF > /etc/sysctl.d/99-nginx.conf
#https://stackoverflow.com/questions/410616/increasing-the-maximum-number-of-tcp-ip-connections-in-linux
net.core.somaxconn=4096
net.core.netdev_max_backlog=2000
net.ipv4.tcp_max_syn_backlog=2048
EOF

# Load kernel parameters
sysctl -p /etc/sysctl.d/*.conf
if [[ $? -ne 0 ]]; then
    log_message "configure_kernel(): Error"
    exit 1
else
    log_message "configure_kernel(): Done"
fi
}

function install_dependencies() {
    install_packages \
        jq \
        rsync \
        git \
        python \
        python-pip \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    log_message "install_dependencies(): Done"
}

function install_docker() {
    # Install docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    install_packages docker-ce docker-ce-cli containerd.io

    pip install docker-compose
    if [[ $? -ne 0 ]]; then
        log_message "install_docker(): Error trying to install docker-compose"
        exit 1
    fi

    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function disable_startup_scripts() {
    systemctl disable google-startup-scripts.service
    systemctl disable google-shutdown-scripts.service
}

function download_nginx_config() {
    # Create nginx config directory
    mkdir -p /opt/waf/nginx/

    # Get instance metadata
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    WAF_BUCKET=$(echo ${METADATA} | jq -r .waf_bucket)

cat <<END > /opt/waf/update_waf_rules.sh
#!/bin/bash
function log_message() {
    echo "\$(date) - \${*}" | tee -a /var/log/update_waf_rules.log
}

function get_rules_from_bucket() {
    gsutil rsync -d -r gs://${WAF_BUCKET}/ /opt/waf/nginx/
    if [[ \$? -ne 0 ]]; then
        log_message "get_rules_from_bucket(): Error"
        exit 1
    else
        log_message "get_rules_from_bucket(): Done"
        if [[ \${DEPLOY_WAF} = "TRUE" ]]; then
            exit 0
        fi
    fi
}

function reload_waf() {
    docker exec waf nginx -t
    if [[ \$? -ne 0 ]]; then
        log_message "reload_waf(): Error, docker command returned an error"
        exit 1
    fi

    docker exec waf nginx -s reload
    if [[ \$? -ne 0 ]]; then
        log_message "reload_waf(): Error, unable to reload config"
        exit 1
    else
        log_message "reload_waf(): Done, reload config"
    fi
}
##### main
BEFORE_SYNC=\$(tar -cPf - /opt/waf/nginx/ | md5sum) # Get checksum of nginx config before sync
get_rules_from_bucket
AFTER_SYNC=\$(tar -cPf - /opt/waf/nginx/ | md5sum) # Get checksum of nginx config after sync

if [[ \${BEFORE_SYNC} != \${AFTER_SYNC} ]]; then
    reload_waf
else
    log_message "update_waf_rules(): Done"
fi
exit 0
END

    chmod u+x /opt/waf/update_waf_rules.sh
    export DEPLOY_WAF="TRUE"
    /opt/waf/update_waf_rules.sh deploy
    if [[ $? -ne 0 ]]; then
        log_message "download_nginx_config(): Error"
        exit 1
    else
        log_message "download_nginx_config(): Done"
    fi
}

function deploy_nginx() {
    # Create docker directory
    mkdir -p /opt/waf
    mkdir -p /opt/waf/html/_GCPWAF/RequestDenied/ /opt/waf/html/_GCPWAF/healthcheck/
    mkdir -p /opt/waf/ssl/

    # Get instance metadata
    IP=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip")
    PROJECT=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/project/project-id")
    METADATA=$(curl -s -H "Metadata-Flavor: Google" "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true")
    DOCKER_IMAGE=$(echo ${METADATA} | jq -r .docker_image)
    SSL_PASSWORD=$(echo ${METADATA} | jq -r .ssl_password)

echo ${SSL_PASSWORD} > /opt/waf/ssl_password

cat <<END > /opt/waf/docker-compose.yml
version: '3'
services:
  server:
    image: ${DOCKER_IMAGE}
    restart: unless-stopped
    container_name: waf
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    ports:
      - 80:80
      - 443:443
      - 6080:6080
      - 6443:6443
      - 7080:7080
      - 7443:7443
      - 8080:8080
      - 8443:8443
      - 9080:9080
      - 9443:9443
      - 10080:10080
    volumes:
      - /opt/waf/ssl_password:/etc/nginx/ssl_password:ro
      - /opt/waf/nginx/ssl/:/etc/nginx/ssl/
      - /opt/waf/nginx/conf.d/:/etc/nginx/conf.d/
      - /opt/waf/nginx/naxsi.d/:/etc/nginx/naxsi.d/
      - /opt/waf/nginx/nginx.conf:/etc/nginx/nginx.conf
      - /opt/waf/html/:/usr/share/nginx/html/
      - /opt/waf/log/:/var/log/nginx/
END

cat <<END > /opt/waf/html/_GCPWAF/RequestDenied/index.html
<!DOCTYPE html>
<html>
  <head>
    <title>Request Blocked</title>
  </head>
  <body>
    <div style="text-align: center">
      <h1>Malicious Request</h1>
      <hr>
      <p>This Request Has Been Blocked.</p>
    </div>
  </body>
</html>
END

cat <<END > /opt/waf/html/_GCPWAF/healthcheck/index.html
OK
END

    # Start docker container
    cd /opt/waf
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_nginx(): Error trying to start docker container"
        exit 1
    else
        log_message "deploy_nginx(): Container started"
    fi
    log_message "deploy_nginx(): Done"
}

function install_stackdriver_agent() {
    # Install stackdriver agent
    curl -sS https://dl.google.com/cloudagents/install-logging-agent.sh | sudo bash
    if [[ $? -ne 0 ]]; then
        log_message "install_stackdriver_agent(): Error trying to install"
        exit 1
    else
        log_message "install_stackdriver_agent(): Installed"
    fi

# Write stackdriver config file for WAF
cat << END > /etc/google-fluentd/config.d/waf.conf
<source>
  @type tail
  format none
  path /opt/waf/log/access.log
  pos_file /var/lib/google-fluentd/pos/waf-access.pos
  read_from_head true
  tag waf-access
</source>

<source>
  @type tail
  format none
  path /opt/waf/log/error.log
  pos_file /var/lib/google-fluentd/pos/waf-error.pos
  read_from_head true
  tag waf-error
</source>
END

# Write stackdriver config file for deployment
cat << END > /etc/google-fluentd/config.d/deployment.conf
<source>
  @type tail
  format none
  path /var/log/deployment.log
  pos_file /var/lib/google-fluentd/pos/deployment.pos
  read_from_head true
  tag deployment
</source>
END

# Write stackdriver config file for update_waf_rules
cat << END > /etc/google-fluentd/config.d/update_waf_rules.conf
<source>
  @type tail
  format none
  path /var/log/update_waf_rules.log
  pos_file /var/lib/google-fluentd/pos/update_waf_rules.pos
  read_from_head true
  tag deployment
</source>
END

    # Start enable and restart stackdriver agent
    systemctl enable google-fluentd
    systemctl restart google-fluentd

    log_message "install_stackdriver_agent(): Done"
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

upgrade_packages

install_dependencies

configure_ulimits

configure_kernel

install_docker

download_nginx_config

deploy_nginx

install_stackdriver_agent

# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH DEPLOYMENT"