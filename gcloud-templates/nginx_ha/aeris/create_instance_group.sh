#!/bin/bash
TIMESTAMP=$(date +%Y%m%d%H%M%S)
PROJECT="aeriscom-ap-prod-201907"
ENVIRONMENT="prod"
REGION="us-west1"
PURPOSE="aeris-nginx"
TEMPLATE_NAME="vm-ap-${ENVIRONMENT}-${PURPOSE}-${REGION}-${TIMESTAMP}"
INSTANCE_GROUP_NAME="vm-ap-${ENVIRONMENT}-${PURPOSE}-${REGION}"
HEALTHCHECK_NAME="nlb-ap-${ENVIRONMENT}-${PURPOSE}-${REGION}"
LOADBALANCER_NAME="nlb-ap-${ENVIRONMENT}-${PURPOSE}-${REGION}"
RULES="tcp:80,tcp:443,tcp:6080,tcp:6443,tcp:7080,tcp:7443,tcp:8080,tcp:8443,tcp:9080,tcp:9443,tcp:10080"
MACHINE_TYPE="n1-standard-2"
HOST_PROJECT="aeriscomm-hostproj-aer-201903"
SHARED_SUBNET="subnet-island-bss-nat-common-prod"
SUBNET="subnet-ap-prod-dmz-us-west1"
DOCKER_IMAGE="nginx:1.12.1-alpine"
SSL_PASSWORD="azPtIahRCJGrBO0OB6ygAdPkSiSHhUMW4U24jNncmdqv0Tivt9n6LN6iKwUn9KLAFgL827dKMFN7MPbDZiGz6lE4EcVnBl2r8ONdoxikLQa7OjCtalHDaiQDw55bNX5a"
PRODUCT_NAME="acpap"
WAF_BUCKET="csb-ap-prod-nginx-aeris-us-west1"
SERVICE_ACCOUNT="iam-ap-prod-gke-us-west1@aeriscom-ap-prod-201907.iam.gserviceaccount.com"
NETWORK="vpc-ap-prod-us-west1"
NETWORK_TAGS="aeris-nginx"
OS_IMAGE="ubuntu-1804-bionic-v20191021"
OS_IMAGE_PROJECT="ubuntu-os-cloud"
BOOT_DISK_SIZE="32GB"
INSTANCE_COUNT="2"
SSL_PASSWORD="azPtIahRCJGrBO0OB6ygAdPkSiSHhUMW4U24jNncmdqv0Tivt9n6LN6iKwUn9KLAFgL827dKMFN7MPbDZiGz6lE4EcVnBl2r8ONdoxikLQa7OjCtalHDaiQDw55bNX5a"

# Create Instance Template
gcloud beta compute instance-templates create ${TEMPLATE_NAME} \
 --project=${PROJECT} \
 --machine-type=${MACHINE_TYPE} \
 --network-interface subnet=projects/${HOST_PROJECT}/regions/${REGION}/subnetworks/${SHARED_SUBNET},no-address \
 --network-interface subnet=projects/${PROJECT}/regions/${REGION}/subnetworks/${SUBNET},no-address \
 --metadata=^,@^docker_image=${DOCKER_IMAGE},@environment=${ENVIRONMENT},@product_name=${PRODUCT_NAME},@waf_bucket=${WAF_BUCKET},@ssl_password=${SSL_PASSWORD} \
 --metadata-from-file startup-script=startup-script.sh \
 --maintenance-policy=MIGRATE \
 --service-account=${SERVICE_ACCOUNT} \
 --scopes=https://www.googleapis.com/auth/compute.readonly,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/userinfo.email \
 --region=${REGION} \
 --tags=${NETWORK_TAGS} \
 --image=${OS_IMAGE} \
 --image-project=${OS_IMAGE_PROJECT} \
 --boot-disk-size=${BOOT_DISK_SIZE} \
 --boot-disk-type=pd-standard \
 --boot-disk-device-name=vm-ap-${ENVIRONMENT}-${PURPOSE}-${REGION}-2 \
 --reservation-affinity=any \
  || echo "ERROR: Instance template creation was unsuccessful"

# Create healthcheck
gcloud compute health-checks create http ${HEALTHCHECK_NAME} \
  --project ${PROJECT} \
  --timeout "5" \
  --check-interval "10" \
  --unhealthy-threshold "3" \
  --healthy-threshold "2" \
  --port "10080" \
  --request-path "/_GCPWAF/healthcheck/" \
  || echo "ERROR: Healthcheck creation was unsuccessful"

# Create Instance Group
gcloud beta compute instance-groups managed create ${INSTANCE_GROUP_NAME} \
  --project=${PROJECT} \
  --base-instance-name=${INSTANCE_GROUP_NAME} \
  --template=${TEMPLATE_NAME} \
  --size=${INSTANCE_COUNT} \
  --zones=${REGION}-a,${REGION}-b,${REGION}-c \
  --instance-redistribution-type=PROACTIVE \
  --health-check=${HEALTHCHECK_NAME} \
  --initial-delay=300 \
  || echo "ERROR: Instance Group creation was unsuccessful"

# Create Firewall rule
gcloud compute firewall-rules create ${LOADBALANCER_NAME} \
  --project=${PROJECT} \
  --direction=INGRESS \
  --priority=1000 \
  --network=${NETWORK} \
  --action=ALLOW \
  --rules=${RULES} \
  --source-ranges=0.0.0.0/0 \
  --target-tags=${NETWORK_TAGS} \
  || echo "ERROR: Firewall rule creation was unsuccessful"
