#!/bin/bash



#################### FUNCTIONS ####################
create_bucket() {


    if [[ "${!BUCKET_REGION}" != "" ]]; then
        gsutil mb -l ${!BUCKET_REGION} -p ${PROJECT} "gs://${!BUCKET_NAME}"
    else
        gsutil mb -p ${PROJECT} "gs://${!BUCKET_NAME}"
    fi

    if [[ "${!BUCKET_VERSIONING}" == "TRUE" ]] || [[ "${!BUCKET_VERSIONING}" == "true" ]]
     then
        echo "seting verioning in bucket ${!BUCKET_NAME}"
        gsutil versioning set on "gs://${!BUCKET_NAME}"
    fi
}






if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1



IFS=','
read -ra BUCKETS_ID <<< "${BUCKET_IDS}"

for i in "${BUCKETS_ID[@]}"; do
    echo "Creating ${i} bucket"

    BUCKET_NAME="${i^^}_BUCKET_NAME"
    BUCKET_REGION="${i^^}_BUCKET_REGION"
    BUCKET_VERSIONING="${i^^}_BUCKET_VERSIONING"

    create_bucket

done

