#--------------------------------------------------
# Terraform ec2.tf created by Dockerizing
#------------------------------------------------#
resource "aws_instance" "noncore-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NONCORE-A" 
    Deployment ="PACE" 
    Component = "noncore-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "monitoring-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=monitoring' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-MONITORING-A" 
    Deployment ="PACE" 
    Component = "monitoring-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "cache-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=cache' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CACHE-A" 
    Deployment ="PACE" 
    Component = "cache-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ibus-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ibus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-IBUS-A" 
    Deployment ="PACE" 
    Component = "ibus-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "log-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=log' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-LOG-A" 
    Deployment ="PACE" 
    Component = "log-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "core-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CORE-A" 
    Deployment ="PACE" 
    Component = "core-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "kong-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kong' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-KONG-A" 
    Deployment ="PACE" 
    Component = "kong-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "safe-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=safe' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-SAFE-A" 
    Deployment ="PACE" 
    Component = "safe-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "web-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=web' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-WEB-A" 
    Deployment ="PACE" 
    Component = "web-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ebus-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-9774f9e0" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ebus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-EBUS-A" 
    Deployment ="PACE" 
    Component = "ebus-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "nginx-a" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-300d8047" 
associate_public_ip_address = true 
security_groups = ["sg-62dff41e"]
key_name = "PF-PACE-External" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NGINX-A" 
    Deployment ="PACE" 
    Component = "nginx-a" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "noncore-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NONCORE-B" 
    Deployment ="PACE" 
    Component = "noncore-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "monitoring-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=monitoring' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-MONITORING-B" 
    Deployment ="PACE" 
    Component = "monitoring-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "cache-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=cache' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CACHE-B" 
    Deployment ="PACE" 
    Component = "cache-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ibus-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ibus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-IBUS-B" 
    Deployment ="PACE" 
    Component = "ibus-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "log-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=log' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-LOG-B" 
    Deployment ="PACE" 
    Component = "log-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "core-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CORE-B" 
    Deployment ="PACE" 
    Component = "core-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "kong-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kong' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-KONG-B" 
    Deployment ="PACE" 
    Component = "kong-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "safe-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=safe' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-SAFE-B" 
    Deployment ="PACE" 
    Component = "safe-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "web-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=web' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-WEB-B" 
    Deployment ="PACE" 
    Component = "web-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ebus-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-e8513e8d" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ebus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-EBUS-B" 
    Deployment ="PACE" 
    Component = "ebus-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "nginx-b" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-05abc460" 
associate_public_ip_address = true 
security_groups = ["sg-62dff41e"]
key_name = "PF-PACE-External" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NGINX-B" 
    Deployment ="PACE" 
    Component = "nginx-b" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "noncore-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NONCORE-C" 
    Deployment ="PACE" 
    Component = "noncore-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "monitoring-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=monitoring' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-MONITORING-C" 
    Deployment ="PACE" 
    Component = "monitoring-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "cache-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=cache' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CACHE-C" 
    Deployment ="PACE" 
    Component = "cache-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ibus-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ibus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-IBUS-C" 
    Deployment ="PACE" 
    Component = "ibus-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "log-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=log' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "io1" 
    volume_size = "100" 
    encrypted = "true" 
    iops = "5000" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-LOG-C" 
    Deployment ="PACE" 
    Component = "log-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "core-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.4xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-CORE-C" 
    Deployment ="PACE" 
    Component = "core-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "kong-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=kong' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-KONG-C" 
    Deployment ="PACE" 
    Component = "kong-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "safe-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=safe' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-SAFE-C" 
    Deployment ="PACE" 
    Component = "safe-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "web-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=web' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-WEB-C" 
    Deployment ="PACE" 
    Component = "web-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "ebus-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-96f050cf" 
associate_public_ip_address = false 
security_groups = ["sg-07daf17b"]
key_name = "PF-PACE-Internal" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
           export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32`
           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
           sudo chkconfig docker on
           sudo service docker start
           sleep 3
           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ebus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.5 https://ranchernew.aerisatsp.com/v1/scripts/DB647E067386E9D826C5:1514678400000:6c68y4jQKtMt6VcPqd6vg8Rro4
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-EBUS-C" 
    Deployment ="PACE" 
    Component = "ebus-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------#
resource "aws_instance" "nginx-c" {
count = "1" 
ami = "ami-32cf7b4a" 
instance_type = "m4.2xlarge" 
subnet_id = "subnet-61f65638" 
associate_public_ip_address = true 
security_groups = ["sg-62dff41e"]
key_name = "PF-PACE-External" 

user_data = <<-EOF
           #!/bin/bash
           sudo yum -y update
           sudo echo -e "*     hard    nofile   1040000\n*     soft    nofile   1040000\nroot  hard    nofile   1040000\nroot  soft    nofile   1040000" >> /etc/security/limits.conf
           sudo echo -e "fs.file-max=1040000\nkernel.pid_max=1040000\nkernel.thread-max=1040000\nvm.max_map_count=1040000" >> /etc/sysctl.conf
           sleep 1
           mkfs -t ext4 /dev/xvdf
           mkdir /EBS
           mount -t ext4 -o noatime  /dev/xvdf /EBS
           sleep 1
           sudo echo -e "/dev/xvdf       /EBS  ext4    defaults,nofail      0     2" >> /etc/fstab
           sleep 1
           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker
           sleep 1
          EOF

  root_block_device {
    volume_type = "gp2" 
    volume_size = "40" 
    delete_on_termination = "true" 
  }
  ebs_block_device {
    device_name = "/dev/xvdf" 
    volume_type = "gp2" 
    volume_size = "100" 
    encrypted = "true" 
    delete_on_termination = "true" 
  }
  tags {
    Name = "PF-PACE-NGINX-C" 
    Deployment ="PACE" 
    Component = "nginx-c" 
    Project ="AMP" 
    Owner = "Pritpal.Singh@aeris.net" 
  }
  lifecycle {
    create_before_destroy = true
  }
}
#----------------------------------------------------#
# ec2.tf file creation is completed -----------------
