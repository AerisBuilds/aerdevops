# terraform template
Terraform files for deploying a Rancher HA cluster in AWS

## Usage

Clone this repo:

```
cd terraform-templates/rancher-ha

```

Edit the `variables.tf` file: "for some env vars, value have to be put on the fly, e.g. access and secret keys"

```

To create the cluster:

```

terraform apply

```

To destroy:

```

terraform destroy

```

###Reference
* https://Pritpal_DevOps@bitbucket.org/aeriscom/automotive-devops.git
