#------------------------------------------#
# ELB Security Group
#------------------------------------------#

resource "aws_security_group" "rancher_ha_elb" {
    name        = "${var.name_prefix}-elb-sg"
    description = "Rancher HA ELB"
    vpc_id      = "${var.aws_vpc_id}"
}

resource "aws_security_group_rule" "allow_https" {
    type              = "ingress"
    from_port         = 443
    to_port           = 443
    protocol          = "tcp"
    cidr_blocks       = ["10.0.0.0/8"]
    security_group_id = "${aws_security_group.rancher_ha_elb.id}"
}

resource "aws_security_group_rule" "allow_http" {
    type              = "ingress"
    from_port         = 80
    to_port           = 80
    protocol          = "tcp"
    cidr_blocks       = ["10.0.0.0/8"]
    security_group_id = "${aws_security_group.rancher_ha_elb.id}"
}

resource "aws_security_group_rule" "allow_all_out" {
    type              = "egress"
    from_port         = 0
    to_port           = 0
    protocol          = "-1"
    cidr_blocks       = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.rancher_ha_elb.id}"
}

#------------------------------------------#
# ELB Configuration
#------------------------------------------#

resource "aws_elb" "rancher_ha_http" {
    name     = "${var.name_prefix}-elb"
    internal = "${var.internal_elb}"
#    availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]
    subnets         = ["subnet-9774f9e0","subnet-e8513e8d","subnet-96f050cf"]
    security_groups = ["${aws_security_group.rancher_ha_elb.id}"]
    instances       = ["${aws_instance.rancher_ha.*.id}"]

    idle_timeout                = 400
    cross_zone_load_balancing   = true
    connection_draining         = true
    connection_draining_timeout = 400

    listener {
        instance_port     = 8080
        instance_protocol = "tcp"
        lb_port           = 443
        lb_protocol       = "ssl"
        ssl_certificate_id = "arn:aws:iam::248971149884:server-certificate/${var.certificate_name}"
    }

    listener {
        instance_port     = 8080
        instance_protocol = "tcp"
        lb_port           = 80
        lb_protocol       = "tcp"
    }

    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = 3
        target              = "HTTP:8080/login"
        interval            = 30
    }

    tags {
        Name = "${var.name_prefix}-elb-http"
        Project = "${var.project}"
        Component = "${var.component}"
        Deployment = "${var.deployment}"
        Owner = "${var.owner}"
    }
}

resource "aws_proxy_protocol_policy" "rancher_ha_http_proxy_policy" {
    load_balancer  = "${aws_elb.rancher_ha_http.name}"
    instance_ports = ["8080"]
}
