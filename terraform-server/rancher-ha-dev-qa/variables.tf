#------------------------------------------#
# AWS Environment Variables
#------------------------------------------#

variable "access_key" {}

variable "secret_key" {}

variable "region" {
default = "us-west-2"
description = "AWS region"
}

variable "zone_id" {
description = "Zone ID of route 53 hosted zone where A record will be created"
}

variable "elb_zone_id" {
description = "Zone ID of elb hosted zone where route 53 record point to elb dns"
}

variable "record_name" {
description = "URL for rancher server"
}

variable "certificate_name" {
description = "Name of the certificate, it should be same as in aws"
}

variable "count" {
    default     = "3"
    description = "Number of HA servers to deploy"
}

variable "name_prefix" {
    default     = "amp-rancher-server-dev-qa"
    description = "Prefix for all AWS resource names"
}

variable "ami" {
    default     = "ami-bf4193c7"
    description = "Instance AMI ID"
}

variable "key_name" {
    description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
    default     = "m4.large"
    description = "AWS Instance type"
}

variable "root_volume_size" {
    default     = "100"
    description = "Size in GB of the root volume for instances"
}

variable "aws_vpc_id" {
   default = "vpc-54907731"
   description = "VPC id"
}

variable "vpc_cidr" {
    default     = "10.6.0.0/16"
    description = "Subnet in CIDR format to assign to VPC"
}

variable "subnet_cidrs" {
    default     = ["10.6.51.0/24", "10.6.52.0/24", "10.6.53.0/24"]
    description = "Subnet ranges (requires 3 entries)"
}

variable "subnet_ec2ids" {
    type        = "string"
    default     = "subnet-9774f9e0, subnet-e8513e8d, subnet-96f050cf"
    description = "Subnet ranges (requires 3 entries)"
}


variable "availability_zones" {
    default     = ["us-west-2a", "us-wests-2b", "us-west-2c"]
    description = "Availability zones to place subnets"
}

variable "internal_elb" {
    default     = "true"
    description = "Force the ELB to be internal only"
}

variable "deployment" {
    default     = "DEV/QA"
    description = "Deployment tag value"
}

variable "component" {
    default     = "Rancher"
    description = "component tag value"
}

variable "project" {
    default     = "AMP"
    description = "project tag value"
}

variable "owner" {
    description = "Owner tag value, please put your email id here"
}

#------------------------------------------#
# Database Variables
#------------------------------------------#
variable "db_name" {
    default     = "rancher_terraform"
    description = "Name of the RDS DB"
}

variable "db_user" {
    default     = "rancher"
    description = "Username used to connect to the RDS database"
}

variable "db_pass" {
    description = "Password used to connect to the RDS database"
}

#------------------------------------------#
# Rancher Variables
#------------------------------------------#
variable "rancher_version" {
    default     = "stable"
    description = "Rancher version to deploy"
}
