#------------------------------------------#
# Route53 Configuration
#------------------------------------------#

#data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "rancher_route53_url" {
   zone_id = "${var.zone_id}"
   name = "${var.record_name}"
   type = "A"

   alias {
     name = "${aws_elb.rancher_ha_http.dns_name}"
     zone_id = "${var.elb_zone_id}"
     evaluate_target_health = false
   }
}
