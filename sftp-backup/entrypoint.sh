#!/bin/sh
/usr/sbin/crond && /config/s3fs-fuse/src/s3fs -d -f -o endpoint=us-west-2 -o use_cache=/tmp/cache -o enable_noobj_cache -o stat_cache_expire=60 -o enable_content_md5 -o use_rrs -o nonempty -o default_permissions -o allow_other -o bucket="$S3BUCKET" /opt/backup
