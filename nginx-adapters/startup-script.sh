#!/bin/bash

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function upgrade_packages() {
    TRIES=10
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get upgrade -y
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            apt-get autoremove -y
            log_message "upgrade_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "upgrade_packages(): Error"
            exit 1
        done

        log_message "upgrade_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "upgrade_packages(): Done"
}

function install_packages() {
    TRIES=10
    EXITCODE=1
    while [[ ${EXITCODE} -ne 0 ]]; do
        apt-get update && apt-get install -y ${*}
        EXITCODE=$?

        if [[ ${EXITCODE} -eq 0 ]]; then
            log_message "install_packages(): Done"
            return 0
        fi

        while [[ ${TRIES} -eq 0 ]]; do
            log_message "install_packages(): Error"
            exit 1
        done

        log_message "install_packages(): Try ${TRIES}, waiting..."
        sleep 30
        let TRIES=TRIES-1
    done

    log_message "install_packages(): Done"
}

function configure_ulimits() {
cat <<EOF > /etc/security/limits.d/20-nofile.conf
*                soft    nofile          32768
*                hard    nofile          32768
EOF

if [[ $? -ne 0 ]]; then
    log_message "configure_ulimits(): Error"
    exit 1
else
    log_message "configure_ulimits(): Done"
fi
}

function install_dependencies() {
    install_packages \
        jq \
        rsync \
        python \
        python-pip \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    log_message "install_dependencies(): Done"
}

function install_docker() {
    # Install docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    install_packages docker-ce docker-ce-cli containerd.io

    pip install docker-compose
    if [[ $? -ne 0 ]]; then
        log_message "install_docker(): Error trying to install docker-compose"
        exit 1
    fi

    # Enable Docker service
    systemctl enable docker
    systemctl start docker

    log_message "install_docker(): Done"
}

function set_base_nginx_conf {
    mkdir -p /opt/devops/nginx/conf

cat <<END > /opt/devops/nginx/conf/nginx-base.conf
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

worker_rlimit_nofile 65000;
events {
    worker_connections 65000;
    use epoll;
    multi_accept on;
}
http {
    keepalive_timeout 65;
    keepalive_requests 100000;
    sendfile         on;
    tcp_nopush       on;
    tcp_nodelay      on;

    client_body_buffer_size    128k;
    client_max_body_size       10m;
    client_header_buffer_size    1k;
    large_client_header_buffers  4 4k;
    output_buffers   1 32k;
    postpone_output  1460;

    client_header_timeout  3m;
    client_body_timeout    3m;
    send_timeout           3m;

    open_file_cache max=1000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 5;
    open_file_cache_errors off;

    gzip on;
    gzip_min_length  1000;
    gzip_buffers     4 4k;
    gzip_types       text/html application/x-javascript text/css application/javascript text/javascript text/plain text/xml application/json application/vnd.ms-fontobject application/x-font-opentype application/x-font-truetype application/x-font-ttf application/xml font/eot font/opentype font/otf image/svg+xml image/vnd.microsoft.icon;
    gzip_disable "MSIE [1-6]\.";
}
stream {
    upstream adapter_upstream {
        # Change the IP's to fit the environment
        UPSTREAM_SERVERS_IPS
    }
    server {
        listen NGINX_PROXY_PORT;
        proxy_pass adapter_upstream;
        proxy_responses 10;
        proxy_timeout 350s;
        error_log /var/log/nginx/adapters.log info;
    }
}

END

    log_message "set_base_nginx_conf(): Done"
}

function disable_startup_scripts() {
    systemctl disable google-startup-scripts.service
    systemctl disable google-shutdown-scripts.service
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START VM DEPLOYMENT"

upgrade_packages

install_dependencies

configure_ulimits

install_docker

set_base_nginx_conf

# Disable Google startup-script service
disable_startup_scripts

log_message "FINISH VM DEPLOYMENT"
#!/usr/bin/env bash