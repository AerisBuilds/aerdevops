#!/bin/bash

ADAPTER=$1
PORT=$2
UPSTREAM_SERVERS=$3

#################### FUNCTIONS ####################
function log_message() {
echo "$(date) - ${*}" | tee -a /var/log/${ADAPTER}-deployment.log
}

function horizontal_rule() {
    log_message "========================================================="
}

function deploy_nginx_conf() {
    # Create nginx.conf for the adapter
    mkdir -p /opt/nginx/${ADAPTER}/
    cp /opt/devops/nginx/conf/nginx-base.conf /opt/nginx/${ADAPTER}/nginx.conf

    IFS=',' read -ra SERVER <<< "${UPSTREAM_SERVERS}"

    str=""
    for i in "${SERVER[@]}"
    do
        aux="server $i;\n\t"
        str+="$aux"
    done

    sed -i "s/UPSTREAM_SERVERS_IPS/$str/g" /opt/nginx/${ADAPTER}/nginx.conf
    sed -i "s/NGINX_PROXY_PORT/$PORT/g" /opt/nginx/${ADAPTER}/nginx.conf

    # Create log file for the nginx adapter
    mkdir -p /var/log/nginx/
    touch /var/log/nginx/${ADAPTER}-adapter.log

    log_message "deploy_nginx_conf(): Done"
}

function deploy_nginx_docker_compose() {
    # Create docker-compose directory
    mkdir -p /opt/devops/${ADAPTER}/

cat <<END > /opt/devops/${ADAPTER}/docker-compose.yml
version: '3'
services:
  server:
    image: nginx:latest
    restart: unless-stopped
    container_name: ${ADAPTER}-adapter
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - /opt/nginx/${ADAPTER}/nginx.conf:/etc/nginx/nginx.conf
      - /var/log/nginx/${ADAPTER}-adapter.log:/var/log/nginx/adapters.log
    ports:
      - "${PORT}:41234"
END

    log_message "deploy_nginx_docker_compose(): Done"
}

function start_docker_container() {
    # Start docker container
    cd /opt/devops/${ADAPTER}
    docker-compose up -d
    if [[ $? -ne 0 ]]; then
        log_message "deploy_nginx(): Error trying to start docker container"
        exit 1
    else
        log_message "start_docker_container(): Done"
    fi
}

#################### MAIN SCRIPT ####################

horizontal_rule

log_message "START DEPLOYMENT"

deploy_nginx_conf

deploy_nginx_docker_compose

start_docker_container

log_message "FINISH DEPLOYMENT"
#!/usr/bin/env bash