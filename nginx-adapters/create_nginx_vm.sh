#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create_nginx_vm.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create_nginx_vm.sh vars-product/product-env.conf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"

source $1

# Create VM instance
gcloud compute --project="${PROJECT}" instances create "${VM_NAME}" \
    --zone="${ZONE}" \
    --machine-type="${MACHINE_TYPE}" \
    --subnet="${SUBNET}" \
    --network-tier=PREMIUM \
    --maintenance-policy=MIGRATE \
    --service-account="${SERVICE_ACCOUNT}" \
    --scopes=https://www.googleapis.com/auth/cloud-platform \
    --image="${IMAGE}" \
    --image-project="${IMAGE_PROJECT}" \
    --boot-disk-size="${BOOT_DISK_SIZE}" \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name="${BOOT_DISK_NAME}" \
    --metadata-from-file startup-script=startup-script.sh
