sleep 1
CONF_SOURCE_FILE="/opt/jenkins/workspace/terraform-parameterized/terraform/ec2_info.cfg"
EC2_TF_TARGET_FILE="/opt/jenkins/workspace/terraform-parameterized/terraform/ec2.tf"
echo "----paramter file : $parameter_file "
cp -rp /opt/jenkins/workspace/terraform-parameterized/terraform/parameter_files/$parameter_file /opt/jenkins/workspace/terraform-parameterized/terraform/ec2_info.cfg
#rm -rf /go/0.9.6/parameter_files


if [ -e $CONF_SOURCE_FILE ]
then
   if [ -e $EC2_TF_TARGET_FILE ]
   then
      rm -f $EC2_TF_TARGET_FILE
   fi

   echo "----start ec2.tf creation --------"
   echo "#--------------------------------------------------"  > $EC2_TF_TARGET_FILE
   echo "# Terraform ec2.tf created by Dockerizing"           >> $EC2_TF_TARGET_FILE

   exec < $CONF_SOURCE_FILE
   while read line
   do
      component=`echo $line |awk '{print $1}'`
      if [ ${component:0:1} = "#" ]
      then
         continue
      elif [ ${component:0:4} = "http" ]
      then
         rancher_domain=`echo $line |awk '{print $1}'`
         rancher_id=`echo $line |awk '{print $2}'`
         deployment=`echo $line |awk '{print $3}'`
         project=`echo $line    |awk '{print $4}'`
         owner=`echo $line      |awk '{print $5}'`

         continue
      fi

      component=`echo $line |awk '{print $1}'`

      temp=${component//-/ }
      label=`echo $temp |awk '{print $1}'`

      count=`echo $line |awk '{print $2}'`
      ami=`echo $line |awk '{print $3}'`
      type=`echo $line |awk '{print $4}'`
      subnet=`echo $line |awk '{print $5}'`
      security_group=`echo $line |awk '{print $6}'`
      key_name=`echo $line |awk '{print $7}'`
      root_volume_type=`echo $line |awk '{print $8}'`
      root_volume_size=`echo $line |awk '{print $9}'`
      root_iops=`echo $line |awk '{print $10}'`
      ebs_volume_type=`echo $line |awk '{print $11}'`
      ebs_volume_size=`echo $line |awk '{print $12}'`
      ebs_iops=`echo $line |awk '{print $13}'`
      public_ip_address=`echo $line |awk '{print $14}'`
      rancher_host=`echo $line |awk '{print $15}'`
      name=`echo $line |awk '{print $16}'`


      echo "#------------------------------------------------#"                         >> $EC2_TF_TARGET_FILE
      echo "resource \"aws_instance\" \"$component\" {"                                 >> $EC2_TF_TARGET_FILE
      echo "count = \"$count\" "                                                        >> $EC2_TF_TARGET_FILE
      echo "ami = \"$ami\" "                                                            >> $EC2_TF_TARGET_FILE
      echo "instance_type = \"$type\" "                                                 >> $EC2_TF_TARGET_FILE
      echo "subnet_id = \"$subnet\" "                                                   >> $EC2_TF_TARGET_FILE
      echo "associate_public_ip_address = $public_ip_address "                          >> $EC2_TF_TARGET_FILE
      echo "security_groups = [\"$security_group\"]"                                    >> $EC2_TF_TARGET_FILE
      echo "key_name = \"$key_name\" "                                                  >> $EC2_TF_TARGET_FILE
      echo ""                                                                           >> $EC2_TF_TARGET_FILE

      echo "user_data = <<-EOF"                                                         >> $EC2_TF_TARGET_FILE
      echo "           #!/bin/bash"                                                     >> $EC2_TF_TARGET_FILE
      echo "           sudo yum -y update"                                              >> $EC2_TF_TARGET_FILE
      echo "           sudo echo -e \"*     hard    nofile   1040000\\n*     soft    nofile   1040000\\nroot  hard    nofile   1040000\\nroot  soft    nofile   1040000\" >> /etc/security/limits.conf"  >> $EC2_TF_TARGET_FILE
      echo "           sudo echo -e \"fs.file-max=1040000\\nkernel.pid_max=1040000\\nkernel.thread-max=1040000\\nvm.max_map_count=1040000\" >> /etc/sysctl.conf"                                         >> $EC2_TF_TARGET_FILE

      echo "           sleep 1"                                                                                      >> $EC2_TF_TARGET_FILE
      echo "           mkfs -t ext4 /dev/xvdf"                                                                       >> $EC2_TF_TARGET_FILE
      echo "           mkdir /EBS"                                                                                   >> $EC2_TF_TARGET_FILE
      echo "           mount -t ext4 -o noatime  /dev/xvdf /EBS"                                                     >> $EC2_TF_TARGET_FILE
      echo "           sleep 1"                                                                                      >> $EC2_TF_TARGET_FILE
      echo "           sudo echo -e \"/dev/xvdf       /EBS  ext4    defaults,nofail      0     2\" >> /etc/fstab"    >> $EC2_TF_TARGET_FILE
      echo "           sleep 1"                                                                                      >> $EC2_TF_TARGET_FILE
      echo "           mkdir /EBS/docker; chmod -R 777 /EBS/docker; ln -s /EBS/docker /var/lib/docker"               >> $EC2_TF_TARGET_FILE
      echo "           sleep 1"                                                                                      >> $EC2_TF_TARGET_FILE

      if [ "$rancher_host" == "yes" ] || [ "$rancher_host" == "Yes" ] || [ "$rancher_host" == "YES" ]
      then
            echo "           export HOSTIP=\`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-32\`"       >> $EC2_TF_TARGET_FILE
            #echo "           sudo yum -y install docker-18.03.1ce-2.amzn2.x86_64"                        >> $EC2_TF_TARGET_FILE
            echo "           sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09"          >> $EC2_TF_TARGET_FILE
            echo "           sudo chkconfig docker on"                                                   >> $EC2_TF_TARGET_FILE
            echo "           sudo service docker start"                                                  >> $EC2_TF_TARGET_FILE
            echo "           sleep 3"                                                                    >> $EC2_TF_TARGET_FILE
            echo "           sudo docker run --rm --privileged -e CATTLE_AGENT_IP=\$HOSTIP -e CATTLE_HOST_LABELS='services=$label' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher  rancher/agent:v1.2.7 $rancher_domain/v1/scripts/$rancher_id"     >> $EC2_TF_TARGET_FILE
            echo "           sleep 10" 
      fi

      echo "          EOF"                                                                               >> $EC2_TF_TARGET_FILE


      echo ""                                                                           >> $EC2_TF_TARGET_FILE
      echo "  root_block_device {"                                                      >> $EC2_TF_TARGET_FILE
      echo "    volume_type = \"$root_volume_type\" "                                   >> $EC2_TF_TARGET_FILE
      echo "    volume_size = \"$root_volume_size\" "                                   >> $EC2_TF_TARGET_FILE

      if [ "$root_iops" != "N/A" ]
      then
         echo "    iops = \"$root_iops\" "                                              >> $EC2_TF_TARGET_FILE
      fi

      echo "    delete_on_termination = \"true\" "                                      >> $EC2_TF_TARGET_FILE
      echo "  }"                                                                        >> $EC2_TF_TARGET_FILE

      echo "  ebs_block_device {"                                                       >> $EC2_TF_TARGET_FILE
      echo "    device_name = \"/dev/xvdf\" "                                           >> $EC2_TF_TARGET_FILE
      echo "    volume_type = \"$ebs_volume_type\" "                                    >> $EC2_TF_TARGET_FILE
      echo "    volume_size = \"$ebs_volume_size\" "                                    >> $EC2_TF_TARGET_FILE
      echo "    encrypted = \"true\" "                                                  >> $EC2_TF_TARGET_FILE

      if [ "$ebs_iops" != "N/A" ]
      then
         echo "    iops = \"$ebs_iops\" "                                               >> $EC2_TF_TARGET_FILE
      fi

      echo "    delete_on_termination = \"true\" "                                      >> $EC2_TF_TARGET_FILE
      echo "  }"                                                                        >> $EC2_TF_TARGET_FILE

      echo "  tags {"                                                                   >> $EC2_TF_TARGET_FILE
      echo "    Name = \"$name\" "                                                      >> $EC2_TF_TARGET_FILE
      echo "    Deployment =\"$deployment\" "                                           >> $EC2_TF_TARGET_FILE
      echo "    Component = \"$component\" "                                            >> $EC2_TF_TARGET_FILE
      echo "    Project =\"$project\" "                                                 >> $EC2_TF_TARGET_FILE
      echo "    Owner = \"$owner\" "                                                    >> $EC2_TF_TARGET_FILE
      echo "  }"                                                                        >> $EC2_TF_TARGET_FILE
      echo "  lifecycle {"                                                              >> $EC2_TF_TARGET_FILE
      echo "    create_before_destroy = true"                                           >> $EC2_TF_TARGET_FILE
      echo "  }"                                                                        >> $EC2_TF_TARGET_FILE
      echo "}"                                                                          >> $EC2_TF_TARGET_FILE



  done
  echo ""                                                                               >> $EC2_TF_TARGET_FILE
  echo ""                                                                               >> $EC2_TF_TARGET_FILE
  echo "#----------------------------------------------------#"                         >> $EC2_TF_TARGET_FILE
  echo "# ec2.tf file creation is completed -----------------"                          >> $EC2_TF_TARGET_FILE
  echo "----end ec2.tf creation --------"

  sleep 1

else
  echo "----parameter file is not exist  ex) $CONF_SOURCE_FILE "

fi