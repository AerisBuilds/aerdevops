#------------------------------------------#
# AWS Provider Configuration
#------------------------------------------#

provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "${var.region}"

}

#------------------------------------------#
# WS Environment Values
#------------------------------------------#
variable "access_key" {
    description = "AWS account access key ID"
}

variable "secret_key" {
    description = "AWS account secret access key"
}

variable "region" {
  description = "EC2 Region for the VPC"
}