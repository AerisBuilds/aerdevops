resource "aws_instance" "Core" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-3bb13d41"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=core' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.7 http://ranchernew.aerisatsp.com/v1/scripts/4D0F999252B8D7A5D270:1514678400000:290MP5tjPJcgvlI0yewP7jIVWY
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-DEV1-CORE"
        Deployment = "DEV"
        Component = "Application"
        Project = "AMP"
		Owner = "abhay.rao@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_instance" "NonCore" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-3bb13d41"]
  key_name = "ampdeveloper"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=noncore' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.7 http://ranchernew.aerisatsp.com/v1/scripts/4D0F999252B8D7A5D270:1514678400000:290MP5tjPJcgvlI0yewP7jIVWY
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-DEV1-NONCORE"
        Deployment = "DEV"
        Component = "Application"
        Project = "AMP"
		Owner = "abhay.rao@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}



resource "aws_instance" "Ebus" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=ebus' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.7 http://ranchernew.aerisatsp.com/v1/scripts/4D0F999252B8D7A5D270:1514678400000:290MP5tjPJcgvlI0yewP7jIVWY
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-DEV1-MQTT"
        Deployment = "DEV"
        Component = "Application"
        Project = "AMP"
		Owner = "abhay.rao@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_instance" "Log" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "m4.2xlarge"
  subnet_id="subnet-9774f9e0"
  security_groups = ["sg-3bb13d41"]
  key_name = "automotive-devops"

  user_data = <<-EOF
              #!/bin/bash
			  #Update pakage
			  sudo yum -y update
			  sudo echo -e "*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" >> /etc/security/limits.conf
			  export HOSTIP=`ifconfig eth0 | grep -i 'inet addr' | cut -c 21-31`
	          #Installing Docker
              sudo yum -y install docker-1.12.6-2.19.amzn1 --releasever=2016.09
              sudo chkconfig docker on
	          sudo service docker start
              sleep 3
              #install rancher agent
			  sudo docker run --rm --privileged -e CATTLE_AGENT_IP=$HOSTIP -e CATTLE_HOST_LABELS='services=log' -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.7 http://ranchernew.aerisatsp.com/v1/scripts/4D0F999252B8D7A5D270:1514678400000:290MP5tjPJcgvlI0yewP7jIVWY
              EOF

  root_block_device {
    volume_type = "gp2"
    volume_size = "200"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-DEV1-LOG"
        Deployment = "DEV"
        Component = "Application"
        Project = "AMP"
		Owner = "abhay.rao@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}



#----------------Public Subnet--------------#
resource "aws_instance" "Nginx" {
  count = "1"
  ami = "ami-6df1e514"
  instance_type = "c4.xlarge"
  subnet_id="subnet-300d8047"
  associate_public_ip_address = true
  security_groups = ["sg-5448c42e"]
  key_name = "automotive-devops"

    root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    delete_on_termination = "true"
   }
      tags {
        Name = "PF-DEV1-Nginx-VM"
        Deployment = "DEV"
        Component = "Application"
        Project = "AMP"
		Owner = "abhay.rao@aeris.net"
    }
  lifecycle {
    create_before_destroy = true
  }
}