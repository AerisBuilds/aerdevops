#!/bin/bash

# Set new destination directly toward a tcp adapter 

sudo iptables -A PREROUTING -t nat -p tcp -d 172.31.105.133 --dport 41234 -j DNAT --to-destination 13.126.236.160:41234

# Set source address so that packets come back here
sudo iptables -A POSTROUTING -t nat -p tcp -d 13.126.236.160 --dport 41234 -j SNAT --to-source 172.31.105.133


