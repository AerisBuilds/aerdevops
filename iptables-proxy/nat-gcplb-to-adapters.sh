#!/bin/bash

# Set new destination directly toward the tcp load balancer adapter

sudo iptables -A PREROUTING -t nat -p tcp -d 35.244.51.227 --dport 41234 \
         -m statistic --mode nth --every 2 --packet 0              \
         -j DNAT --to-destination 35.154.33.191:41234

sudo iptables -A PREROUTING -t nat -p tcp -d 35.244.51.227 --dport 41234 \
         -m statistic --mode nth --every 1 --packet 0              \
         -j DNAT --to-destination 13.126.114.254:41234

# Set source address so that packets come back here
sudo iptables -A POSTROUTING -t nat -p tcp -d 35.154.33.191 --dport 41234 -j SNAT --to-source 172.31.105.133

sudo iptables -A POSTROUTING -t nat -p tcp -d 13.126.114.254 --dport 41234 -j SNAT --to-source 172.31.105.133

