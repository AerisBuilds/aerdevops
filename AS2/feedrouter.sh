#!/bin/bash
cd /opt/mendelson/as2/wd/messages/Aeris/inbox/MME/
find . -type f > ../raw_file.txt
sed 's/^..//' ../raw_file.txt > ../file.txt
sed 's/^/\/opt\/mendelson\/as2\/wd\/messages\/Aeris\/inbox\/MME\//' ../file.txt > ../mme.txt
for file in `cat /opt/mendelson/as2/wd/messages/Aeris/inbox/mme.txt`; do
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
shipping="/opt/mendelson/as2/wd/messages/Aeris/shipping"
registration="/opt/mendelson/as2/wd/messages/Aeris/registration"
if [  -f "$file" ]
then
    if grep -q VMT101H1 "$file"; then
    mv $file $shipping/shipping_$current_time.p7m
    else
    grep -q VMT102H1 "$file"
    mv $file $registration/registration_$current_time.p7m
    fi

fi
done
rm -f /opt/mendelson/as2/wd/messages/Aeris/inbox/file.txt
rm -f  /opt/mendelson/as2/wd/messages/Aeris/inbox/mme.txt
rm -f /opt/mendelson/as2/wd/messages/Aeris/inbox/raw_file.txt
