#!/bin/bash

kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n app
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n app
kubectl create service externalname um-server --external-name um-server.spc.svc.cluster.local -n app
kubectl create service externalname ota --external-name ota.dp.svc.cluster.local -n app
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n auth
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n auth
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n dp 
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n dp
kubectl create service externalname keycloak --external-name keycloak.auth.svc.cluster.local -n dp
kubectl create service externalname mts --external-name mts.spc.svc.cluster.local -n dp
kubectl create service externalname nm --external-name nm.spc.svc.cluster.local -n dp
##############################  For v2 changes ###########################################
#kubectl create service externalname otabus --external-name infra-otabus.infra.svc.cluster.local -n dp
kubectl create service externalname otabus --external-name infra-otabus-finch.infra.svc.cluster.local -n dp
##############################  v2 changes     ###########################################
kubectl create service externalname pay --external-name pay.spc.svc.cluster.local -n dp
kubectl create service externalname payment --external-name payment.spc.svc.cluster.local -n dp
kubectl create service externalname redis-master --external-name infra-redis-ha.infra.svc.cluster.local -n dp
kubectl create service externalname redis-sentinel --external-name infra-redis-ha.infra.svc.cluster.local -n dp
kubectl create service externalname sync --external-name sync.spc.svc.cluster.local -n dp
kubectl create service externalname zk --external-name infra-kafka-zookeeper.infra.svc.cluster.local -n dp
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n dpbase
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n dpbase
kubectl create service externalname a2 --external-name a2.dp.svc.cluster.local -n spc
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n spc
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n spc
kubectl create service externalname nm --external-name nm.spr.svc.cluster.local -n spc
kubectl create service externalname pa --external-name pa.dp.svc.cluster.local -n spc
kubectl create service externalname payment --external-name pay.spc.svc.cluster.local -n spc
kubectl create service externalname ota --external-name ota.dp.svc.cluster.local -n spc
kubectl create service externalname azs --external-name azs.dp.svc.cluster.local -n spc
kubectl create service externalname feed-reporting --external-name feed-reporting.spr.svc.cluster.local -n spc
kubectl create service externalname admin-keycloak --external-name keycloak.auth.svc.cluster.local -n spc
kubectl create service externalname a2 --external-name a2.dp.svc.cluster.local -n spr
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n spr
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n spr
kubectl create service externalname mts --external-name mts.spc.svc.cluster.local -n spr
kubectl create service externalname ota --external-name ota.dp.svc.cluster.local -n spr
kubectl create service externalname pa --external-name pa.dp.svc.cluster.local -n spr
kubectl create service externalname payment --external-name pay.spc.svc.cluster.local -n spr
kubectl create service externalname pc --external-name pc.spc.svc.cluster.local -n spr
kubectl create service externalname redis-master --external-name infra-redis-ha.infra.svc.cluster.local -n spr
kubectl create service externalname redis-sentinel --external-name infra-redis-ha.infra.svc.cluster.local -n spr
kubectl create service externalname sftp --external-name app-internal-ftp-finch.app.svc.cluster.local -n spr
kubectl create service externalname tspmonitor --external-name tspmonitor.test.svc.cluster.local -n spr
kubectl create service externalname ehb --external-name ehb.dp.svc.cluster.local -n spr
kubectl create service externalname um-server --external-name um-server.spc.svc.cluster.local -n spr
################################################### only for usprod ###################################
kubectl create service externalname sftp --external-name app-sftp-server-finch.app.svc.cluster.local -n spr
################################################### only for usprod ###################################
kubectl create service externalname ebus --external-name dpbase-rabbitmq.dpbase.svc.cluster.local -n test
kubectl create service externalname ibus --external-name infra-kafka.infra.svc.cluster.local -n test
kubectl create service externalname mts --external-name mts.spc.svc.cluster.local -n test
kubectl create service externalname redis-master --external-name infra-redis-ha.infra.svc.cluster.local -n test