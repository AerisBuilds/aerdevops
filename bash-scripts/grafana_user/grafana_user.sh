#!/bin/bash

function create_guest {
curl -X POST -H "Content-Type: application/json" -d '{
  "name":"guest",
  "email":"guest@localhost.com",
  "login":"guest",
  "password":"guest"
}' -u admin:prom-operator $COL2/api/admin/users

curl -X POST -H "Content-Type: application/json" -d '{
   "id": 1,
    "folderId": -1,
    "userId": 0,
    "teamId": 0,
    "role": "Viewer",
    "permission": 1,
    "permissionName": "View",
    "uid": "nErXDvCkzz",
    "isFolder": false,
  },
  {
    "id": 2,
    "dashboardId": -1,
    "userId": 0,
    "teamId": 0,
    "team": "",
    "role": "Editor",
    "permission": 2,
    "permissionName": "Edit",
    "isFolder": false,
  }' -u admin:prom-operator $COL2/api/admin/users/folders
  
 }
  exec < ./endpoint.list
        while read line
                do
                  whole_line=`echo $line`
                  if [ ${whole_line:0:1} = "#" ]
                  then
                         continue
                  fi

                  COL1=`echo $line |awk '{print $1}'`
                  COL2=`echo $line |awk '{print $2}'`

                  if [ "$COL1" == "endpoint" ]
                     then
                            create_guest
                  fi
        done
