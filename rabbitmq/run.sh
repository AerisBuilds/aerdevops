#!/bin/bash

set -e

exec /usr/local/bin/docker-entrypoint.sh rabbitmq-server &
sleep 10
rabbitmq-plugins enable --online rabbitmq_mqtt
sleep 5
rabbitmqctl add_user $APPLICATIONUSER $APPLICATIONPASS &
sleep 5
rabbitmqctl set_user_tags $APPLICATIONUSER monitoring &
sleep 5
rabbitmqctl set_permissions -p / $APPLICATIONUSER ".*" ".*" ".*" &
tail -f /dev/null

