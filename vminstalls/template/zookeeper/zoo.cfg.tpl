# the directory where the snapshot is stored.
dataDir=<zk_dataDir>

# The number of snapshots to retain in dataDir
autopurge.snapRetainCount=<zk_auto_purge_snap_retain_count>

# Set to "0" to disable auto purge feature
autopurge.purgeInterval=<zk_auto_purge_interval>

# The number of milliseconds of each tick
tickTime=2000

# The number of ticks that the initial 
# synchronization phase can take
initLimit=5

# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
syncLimit=2

# the port at which the clients will connect
clientPort=<zk_clientPort>

# the maximum number of client connections.
# increase this if you need to handle more clients
maxClientCnxns=60
#################################################################################