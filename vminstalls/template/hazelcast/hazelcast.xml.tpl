<?xml version="1.0" encoding="UTF-8"?>
<hazelcast xmlns="http://www.hazelcast.com/schema/config"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://www.hazelcast.com/schema/config
           http://www.hazelcast.com/schema/config/hazelcast-config-3.11.xsd">
           
	<group>
		<name>dev</name>
		<password>dev-pass</password>
	</group>
	<network>
		<port auto-increment="true">5701</port>
		<join>
			<multicast enabled="false">
				<multicast-group>224.2.2.3</multicast-group>
				<multicast-port>54327</multicast-port>
			</multicast>
			<tcp-ip enabled="true">
				slaveNodes
            </tcp-ip>
			<!-- <interface>192.168.41.*</interface> -->
		</join>
		<interfaces enabled="false">
			<interface>10.3.1.*</interface>
		</interfaces>
	</network>
	<executor-service>
		 <pool-size>16</pool-size>
	</executor-service>

	<properties>
                <property name="hazelcast.executor.client.thread.count">200</property>
                <property name="hazelcast.jmx">true</property>
		<property name="hazelcast.partition.count">1013</property>
        </properties>


<serialization>
	<serializers>
                         <serializer type-class="com.aeris.core.util.cache.hazelCast.component.DeviceCache" class-name="com.aeris.core.util.cache.serializer.DeviceCacheSerializer"/>
                         <serializer type-class="com.aeris.core.util.cache.alerts.CurrentAlertThresholdInfo" class-name="com.aeris.core.util.cache.serializer.CurrentThresholdInfoSerializer"/>

	</serializers>
</serialization>

	<map name="SMSAlertThresholdMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>


	<map name="PacketAlertThresholdMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
	<map name="VoiceAlertThresholdMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
	<map name="CurrentValueMap">
		 <map-store enabled="true">
			<class-name>com.aeris.core.util.cache.hazelCast.CurrentValueMapPersistenceProvider
			</class-name>
			<write-delay-seconds>120</write-delay-seconds>
		</map-store>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>

		<eviction-policy>NONE</eviction-policy>
	</map>

	<map name="PacketDedupEntriesMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>3600</time-to-live-seconds>
	</map>

	<map name="SMSDedupEntriesMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>3600</time-to-live-seconds>
	</map>

	<map name="VoiceDedupEntriesMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>3600</time-to-live-seconds>
	</map>
	
	<map name="AAASessionLevelCounterMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>86400</time-to-live-seconds>
	</map>
	  
	<map name="AlertsCountMap">
		<map-store enabled="true">
			<class-name>com.aeris.core.util.cache.hazelCast.AlertsCountMapPersistenceProvider
			</class-name>
			<write-delay-seconds>10</write-delay-seconds>
		</map-store>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>7200</time-to-live-seconds>
	</map>
	 
	<map name="RegNotAlertThresholdMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
	<map name="AerCloudApiKeyMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>0</time-to-live-seconds>
	</map>

	
	<map name="PointCodeCarrierMap">
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<eviction-policy>NONE</eviction-policy>
		<time-to-live-seconds>0</time-to-live-seconds>
	</map>
	
	<map name="RegNotEventNotificationMap">
                <near-cache>
                        <time-to-live-seconds>0</time-to-live-seconds>
                        <max-idle-seconds>60</max-idle-seconds>
                        <eviction-policy>NONE</eviction-policy>
                        <max-size>1000000</max-size>
                        <invalidate-on-change>true</invalidate-on-change>
                </near-cache>
                <backup-count>0</backup-count>
                <async-backup-count>2</async-backup-count>
                <read-backup-data>true</read-backup-data>
                <eviction-policy>NONE</eviction-policy>
        </map>
	  
<map name="HexMeidMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
<map name="ImsiMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
	  <map name="MsisdnMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
<map name="PMinMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="SubIdMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="countryCodeMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="ratePlanMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
<map name="CountryOperatorIDToZoneListCacheMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="MCCToCounOpIDCacheMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="NasIpCacheMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="RatePlanToZoneIDCacheMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>

<map name="SubIdListMap">
		<near-cache>
			<time-to-live-seconds>0</time-to-live-seconds>
			<max-idle-seconds>60</max-idle-seconds>
			<eviction-policy>NONE</eviction-policy>
			<max-size>1000000</max-size>
			<invalidate-on-change>true</invalidate-on-change>
		</near-cache>
		<backup-count>0</backup-count>
		<async-backup-count>2</async-backup-count>
		<read-backup-data>true</read-backup-data>
		<eviction-policy>NONE</eviction-policy>
	</map>
</hazelcast>
