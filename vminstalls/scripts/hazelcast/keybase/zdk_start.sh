#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
######Command to start the zookeeper service########
sudo mkdir -p /tmp/zookeeper
sudo echo "Create zookeeper directory"
sudo mkdir -p /usr/local/zookeeper
sudo tar -xvf /opt/zookeeper-* -C /usr/local/zookeeper/
sudo echo "Rename the zookeeper config file"
sudo mv /usr/local/zookeeper/zookeeper-3.4.14/conf/zoo_sample.cfg /usr/local/zookeeper/zookeeper-3.4.14/conf/zoo.cfg_blk
sudo cp -r zoo.cfg /usr/local/zookeeper/zookeeper-3.4.14/conf/ 
sudo -bE /usr/local/zookeeper/zookeeper-3.4.14/bin/zkServer.sh start