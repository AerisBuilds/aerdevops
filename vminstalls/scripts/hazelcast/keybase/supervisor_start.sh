#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#Command to start supervisor
sudo echo "Create storm home directory"
sudo mkdir -p /usr/local/storm
sudo echo "Untar the storm package"
sudo tar -xvf /opt/apache-storm-* -C /usr/local/storm/
sudo echo "copy the storm yaml file"
sudo cp -r storm.yaml /usr/local/storm/apache-storm-0.9.3/conf/
sudo -bE /usr/local/storm/apache-storm-0.9.3/bin/storm supervisor
