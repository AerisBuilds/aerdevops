#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : hz_config.sh
#-- Type           : Shell Script
#-- Purpose        : Configure Hazelcast on GCP
#-- Description    : 
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga		  Install storm components on GCP         
#-----------------------------------------------------------------------------------------------

echo "Starting Hazelcast Installation"

#Step 1: create Hz directory to configure hazelcast
sudo mkdir -p /usr/local/aeris_lib/resources
sudo chmod 777 /usr/local/aeris_lib/resources
cd /usr/local/aeris_lib/resources
