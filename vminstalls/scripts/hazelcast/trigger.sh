#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : start.sh
#-- Type           : Shell Script
#-- Purpose        : Configure storm on on all the nodes
#-- Description    : 
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga		  Configure storm components on GCP         
#-----------------------------------------------------------------------------------------------

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
sudo chmod +x ${THIS_DIR}/../read-vartf.sh
HAZELCAST_NODES=`${THIS_DIR}/../read-vartf.sh ${1} hazelcast`
echo `date` "HAZELCAST_NODES=$HAZELCAST_NODES"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

source ${THIS_DIR}/PropertiesReader.sh $env

MEMORY_USAGE=$(getMemoryUsage)
VERSION=${2}
echo "Memory: ${MEMORY_USAGE}"
echo "VERSION: ${VERSION}"

IFS=',' read -r -a nodesArr <<< "${HAZELCAST_NODES}"

for i in "${!nodesArr[@]}"
do
	HZ_NODES+="<member>${nodesArr[i]}</member>""\n"
    sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}" --command "$(cat $THIS_DIR/hz_install.sh)"
done

sudo chmod +x ${THIS_DIR}/hz_config.sh
sudo ${THIS_DIR}/hz_config.sh ${HZ_NODES}

sudo wget --user "aerdeployer" --password "AerDeployer4" "https://repo.aeriscloud.com/repository/aeris-release/com/aeris/aerboss/real-time-analytics-core/2.0.3/real-time-analytics-core-2.0.3-jar-with-dependencies.jar"
	 sudo chmod 777 ./real-time-analytics-core-2.0.3-jar-with-dependencies.jar
	 echo "Downloaded core jar"

for i in "${!nodesArr[@]}"
do
	echo "Starting Hazelcast installation"
	 sudo /snap/bin/gcloud compute scp ${THIS_DIR}/../../template/hazelcast/hazelcast.xml --project "${project}" --zone "${zone}" --internal-ip ${nodesArr[i]}:~/
	 sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo mv ~/hazelcast.xml /usr/local/aeris_lib/resources/"
	 sudo gcloud compute scp ${THIS_DIR}/cache-server-log4j.properties --project "${project}" --zone "${zone}" --internal-ip ${nodesArr[i]}:/usr/local/aeris_lib/resources/
	 sudo /snap/bin/gcloud compute scp ./real-time-analytics-core-2.0.3-jar-with-dependencies.jar --project "${project}" --zone "${zone}" --internal-ip ${nodesArr[i]}:~/
	 echo "Copied core jar"
	 sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo mv ~/real-time-analytics-core-2.0.3-jar-with-dependencies.jar /usr/local/aeris_lib/"
	 sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo java -Xmx25600m -XX:+UseConcMarkSweepGC -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 -XX:MaxHeapFreeRatio=25 -XX:MinHeapFreeRatio=15 -cp /usr/local/aeris_lib/real-time-analytics-core-2.0.3-jar-with-dependencies.jar -Dhazelcast.config=/usr/local/aeris_lib/resources/hazelcast.xml -Dhazelcast.logging.type=log4j -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1010 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dhazelcast.jmx=true -Dlog4j.configuration=file:///usr/local/aeris_lib/resources/cache-server-log4j.properties com.aeris.core.util.cache.StartCacheInstance&" &     
	 echo "Started Hazelcast node:${nodesArr[i]}"
done




