#!/bin/bash
################################################################
# Name           : PropertiesReader.sh
# Type           : Script
# Purpose        : A utility which read the properties from 'Application.properties' file.

#This directory will get the directory where this script is residing, not from where it is called or not PWD/current working directory.
THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo ${THIS_DIR}

env=${1}
PROP_FILE=${THIS_DIR}/../../conf/conf-ab/storm/ab-${env}.conf

echo "properties file path is : ${PROP_FILE}"

function getMemoryUsage() {
	echo `cat ${PROP_FILE} | grep -w memory_usage | cut -d '=' -f2`
}

#function getSlaveNodes() {
#	echo `cat ${PROP_FILE} | grep -w slave_nodes | cut -d '=' -f2`
#}