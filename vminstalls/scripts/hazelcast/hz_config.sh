#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : hz_config.sh
#-- Type           : Shell Script
#-- Purpose        : Configure Hazelcast on GCP
#-- Description    : 
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga		  Install storm components on GCP         
#-----------------------------------------------------------------------------------------------

#Step 1: copy hazelcast.xml
echo "Copying config file"
THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "THIS_DIR ${THIS_DIR}"

HAZELCAST_NODES=${1}

#cd ${THIS_DIR}/../../template/hazelcast
hz_tpl=${THIS_DIR}/../../template/hazelcast/hazelcast.xml.tpl
sudo chmod 777 $hz_tpl
hz_conf=${THIS_DIR}/../../template/hazelcast/hazelcast.xml
rm -rf $hz_conf

#sudo cp hazelcast.xml.tpl hazelcast.xml

sudo cat $hz_tpl | sed "s#slaveNodes#${HAZELCAST_NODES}#g; " > $hz_conf
sudo chmod 777 $hz_conf
