#!/bin/bash
cd "$(dirname "$0")"

if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./prerequisites_destroy.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./prerequisites_destroy.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi

# Load variables from vars file

echo "Variables file path: $1"


export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export NETWORK=$(grep "^network=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

RAW_VM_NAMES=$(gcloud compute instances list --project=$PROJECT --format=json | jq --arg NETWORK "$NETWORK" '.[] | select(.networkInterfaces[].network | contains($NETWORK)) | .name' | cut -d'"' -f2)


SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
VM_NAMES=($RAW_VM_NAMES) # split to array $names
IFS=$SAVEIFS 

for VM_NAME in "${VM_NAMES[@]}"
do
   : 
     echo "vm name: $VM_NAME"


    ZONE=$(gcloud compute instances list --project=${PROJECT} --filter="name=('$VM_NAME')" | awk 'NR == 2 {print $2}')

      if [[ $VM_NAME == *"bss-${ZONE}"* ]]; then
         echo "bss instance, delete after all instances were removed"

      else
            gcloud compute --project="${PROJECT}" instances update "${VM_NAME}" \
                --zone="${ZONE}" --no-deletion-protection

            # Delete VM 
            gcloud compute --project="${PROJECT}" instances delete "${VM_NAME}" \
                --zone="${ZONE}" -q        
      fi

done
