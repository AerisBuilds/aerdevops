#!/bin/bash
# Script to Register DNS Names
# Created by Jins on 08/03/2019

DNS_TTL=300
DNS_ZONE=internal-aeris-com

# $1 - PROJECT_ID
# $2 - DNS_NAME
# $3 - DNS_TARGET
# $4 - DNS_TTL
# $5 - DNS_TYPE
# $6 - DNS_ZONE
delete_dns_record() {
  gcloud dns --project=$1 record-sets transaction start --zone=$6
  gcloud dns --project=$1 record-sets transaction remove --zone=$6 --name=$2 --ttl=$4 --type=$5 "$3"
  gcloud dns --project=$1 record-sets transaction execute --zone=$6
  status=$?
  if [ $status -ne 0 ]; then
    rm transaction.yaml
  fi
}

# $1 - PROJECT_ID
# $2 - DNS_NAME
# $3 - DNS_TARGET
# $4 - DNS_TTL
# $5 - DNS_TYPE
# $6 - DNS_ZONE
create_dns_record() {
  gcloud dns --project=$1 record-sets transaction start --zone=$6
  gcloud dns --project=$1 record-sets transaction add "$3" --name=$2 --ttl=$4  --type=$5 --zone=$6
  gcloud dns --project=$1 record-sets transaction execute --zone=$6
  status=$?
  if [ $status -ne 0 ]; then
    rm transaction.yaml
  fi
}

for DNS_ENTRY in `cat dnsentry.txt`; do
  echo "*** $DNS_ENTRY"
  DNS_NAME=`echo $DNS_ENTRY|cut -d',' -f 1`
  DNS_TARGET=`echo $DNS_ENTRY|cut -d',' -f 2`
  DNS_TYPE=`echo $DNS_ENTRY|cut -d',' -f 3`
  PROJECT_ID=`echo $DNS_ENTRY|cut -d',' -f 4`

  # Delete the DNS record if exists. Gcloud fails the transacion if a record is not found. So deletion and creation are
  # executed as separate transaction.
  delete_dns_record $PROJECT_ID $DNS_NAME $DNS_TARGET $DNS_TTL $DNS_TYPE $DNS_ZONE
  create_dns_record $PROJECT_ID $DNS_NAME $DNS_TARGET $DNS_TTL $DNS_TYPE $DNS_ZONE

done
