#!/bin/bash
# Usage : To prepare and setup Cloudera master
# Checked on OS : Ubuntu 14.04
# Dated : 25-Jun-2019
# Owner : DevOps
# Note : This should be run on the host on which the CM server is to run.

# Set up some vars
#config_file=/tmp/clouderaconfig.ini
config_file=/tmp/cdh-service-config.conf
cm_server_host=$(grep cm.host ${config_file} | awk -F'=' '{print $2}')
ntp_server=$(grep ntp.server ${config_file} | awk -F'=' '{print $2}')
# This should be set to whatever HIVE_HMS_HOST is set to in deploycloudera.py
hive_metastore_host=$(grep hive.metastore.host ${config_file} | awk -F'=' '{print $2}')
hive_metastore_password=$(grep hive.metastore.password ${config_file} | awk -F'=' '{print $2}')

# Prep Cloudera repo
sudo apt-get -y install wget
#wget http://archive.cloudera.com/cm5/redhat/6/x86_64/cm/cloudera-manager.repo
#sudo mv cloudera-manager.repo /etc/apt-get.repos.d/

# Turn off firewall
sudo service iptables stop

# Turn off SELINUX
echo 0 | sudo tee /selinux/enforce > /dev/null

# Set up NTP
sudo apt-get -y install ntp
sudo chkconfig ntp on
sudo ntpdate "${ntp_server}"
sudo /etc/init.d/ntp start

# Set up python
#sudo rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
sudo apt-get -y install python-pip
sudo pip install cm_api
sudo pip install pyhocon

# Set up metastoredb for Hive
hive_metastore_password=hivepass
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${hive_metastore_password}"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${hive_metastore_password}"
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server
#mysql -uroot -p"${hive_metastore_password}" --execute="CREATE DATABASE metastore; USE metastore; SOURCE ./hive-0.12.0-cdh5.0.0/scripts/metastore/upgrade/mysql/hive-schema-0.12.0.mysql.sql;"
mysql -uroot -p"${hive_metastore_password}" --execute="CREATE DATABASE metastore; USE metastore; SOURCE ./hive-1.1.0-cdh5.15.2/scripts/metastore/upgrade/mysql/hive-schema-0.14.0.mysql.sql;"
#mysql -uroot -p"${hive_metastore_password}" --execute="CREATE USER 'hive'@'${hive_metastore_host}' IDENTIFIED BY '${hive_metastore_password}';"
mysql -uroot -p"${hive_metastore_password}" --execute="grant all privileges on metastore.* to 'hive'@'localhost' identified by '${hive_metastore_password}';"
mysql -uroot -p"${hive_metastore_password}" --execute="GRANT ALL ON metastore.* TO 'hive'@'${hive_metastore_host}' IDENTIFIED BY '${hive_metastore_password}';"
mysql -uroot -p"${hive_metastore_password}" --execute="GRANT ALL ON metastore.* TO 'hive'@'%' IDENTIFIED BY '${hive_metastore_password}';"
#mysql -uroot -p"${hive_metastore_password}" --execute="REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'hive'@'${hive_metastore_host}';"
mysql -uroot -p"${hive_metastore_password}" --execute="GRANT SELECT,INSERT,UPDATE,DELETE,LOCK TABLES,EXECUTE ON metastore.* TO 'hive'@'${hive_metastore_host}';"
mysql -uroot -p"${hive_metastore_password}" --execute="FLUSH PRIVILEGES;"
#mysql -uroot -p"${hive_metastore_password}" --execute="create database oozie; grant all privileges on oozie.* to 'oozie'@'localhost' identified by '${hive_metastore_password}'; grant #all privileges on oozie.* to 'oozie'@'%' identified by '${hive_metastore_password}';"

# Make sure DNS is set up properly so all nodes can find all other nodes

# For master
sudo apt-get update -y
sudo apt-get -y install cloudera-manager-agent cloudera-manager-daemons cloudera-manager-server cloudera-manager-server-db-2
sudo service cloudera-scm-server-db start
sudo sed -i 's/INFO,LOGFILE/DEBUG,LOGFILE/g' /usr/sbin/cmf-server
sudo service cloudera-scm-server start
sudo sed -i.bak -e"s%server_host=localhost%server_host=${cm_server_host}%" /etc/cloudera-scm-agent/config.ini
sudo service cloudera-scm-agent start
sudo apt-get install dos2unix -y
sudo apt-get install zip
sudo apt-get install unzip

# Prep work before calling the Cloudera provisioning script.
firehostdbpassword=$(grep com.cloudera.cmf.ACTIVITYMONITOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
navigatordbpassword=$(grep com.cloudera.cmf.NAVIGATOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
headlampdbpassword=$(grep com.cloudera.cmf.REPORTSMANAGER.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')

# Sleep for a while to give the agents enough time to check in with the master.
# Or better yet, make a dependency so that the slave setup scripts don't start until now and the rest of this script doesn't finish until the slaves finish.
sleep_time=20
echo "Sleeping for ${sleep_time} seconds so managed cluster nodes can get set up."
sleep ${sleep_time}
echo "Done sleeping. Deploying cluster now."

# Execute script to deploy Cloudera cluster
sudo python /tmp/deploycloudera.py -i"${hive_metastore_password}" -f"${firehostdbpassword}" -n"${navigatordbpassword}" -r"${headlampdbpassword}"
#sudo python /tmp/deploycloudera.py  -i"${hive_metastore_password}" -f"${firehostdbpassword}" -n"${navigatordbpassword}"

# Now stop the cluster gracefully if necessary; ie if all servers are automatically rebooted at the end of the provisioning process
#sudo python stopcloudera.py

