#!/bin/bash

input=${1}
data=""
startBracket=0

while IFS= read -r line
do
        ## check if it is the starting line of settings
        if [[ $line == $2_node_settings={* ]]
        then
        startBracket=$(( startBracket + 1 ))
        fi

        ## use if it is greater than 0
        if [ ${startBracket} -gt 0 ]
        then
                if [[ $line == *} ]]
                then
                        startBracket=$(( startBracket - 1 ))
                else
                        if [[ $line == *{ ]]
                        then
                                if [[ $line != $2_node_settings={* ]]
                                then
                                        startBracket=$(( startBracket + 1 ))
                                fi
                        fi
                        if [[ $line =~ "name=" ]]
                        then
                                host=`echo ${line} | grep -w name | cut -d '=' -f2`
                                if [[ $host != "" ]]
                                then
                                        data=$data','`echo $host | sed -e 's/^"//' -e 's/"$//'`
                                fi
                        fi
                fi
        fi
done < "$input"
data=`echo $data | sed -e 's/^,//'`
echo $data
