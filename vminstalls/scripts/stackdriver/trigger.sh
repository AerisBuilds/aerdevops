#!/bin/bash
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Date:August-2019
# Owner: DevOps
#
#
# This script will do the following things:
#     1. Copy configuration files, install-agent script & set-up script from Bootstrap Server to GCP Virtual Machines.
#     2. Run the install-agent script.
#     3. Modify the configuration & restarting agent so that we can start getting proper logs.
#
#
#
#
#
#
######MASTER######
gcloud beta compute --project "aeriscom-ampsol-dev-201908" scp --internal-ip --zone "us-west1-a" /opt/terraform-server/scripts/aerdevops/vminstalls/conf/conf-ampsol/stackdriver/cdhmaster/*.conf dev-dpp-cdhmaster:~/.
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" scp --internal-ip --zone "us-west1-a" /opt/terraform-server/scripts/aerdevops/vminstalls/scripts/stackdriver/stackdriver-logging-agent-install.sh dev-dpp-cdhmaster:~/.
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "ls -ltr"
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "pwd"
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "whoami"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "bash stackdriver-logging-agent-install.sh"
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "rm -f clouderascm.conf"
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "cp *.conf /etc/google-fluentd/config.d/."
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "service google-fluentd restart"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "rm -f stackdriver-logging-agent-install.sh"
gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-a" "dev-dpp-cdhmaster" --command "rm -f *.conf"
######END######
######SLAVE######
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" scp --internal-ip --zone "us-west1-b" /opt/terraform-server/scripts/aerdevops/vminstalls/conf/conf-ampsol/stackdriver/cdhslave/*.conf dev-dpp-cdhslave01:~/.
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" scp --internal-ip --zone "us-west1-b" /opt/terraform-server/scripts/aerdevops/vminstalls/scripts/stackdriver/stackdriver-logging-agent-install.sh dev-dpp-cdhslave01:~/.
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "ls -ltr"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "pwd"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "whoami"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "bash stackdriver-logging-agent-install.sh"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "cp *.conf /etc/google-fluentd/config.d/."
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "service google-fluentd restart"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "rm -f stackdriver-logging-agent-install.sh"
#gcloud beta compute --project "aeriscom-ampsol-dev-201908" ssh --internal-ip --zone "us-west1-b" "dev-dpp-cdhslave01" --command "rm -f *.conf"
######END######
