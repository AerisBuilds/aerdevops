#!/bin/bash

# Simple script to audit the major GCP project components - jeff.howe@aeris.net
# Script is run from project bootstrap server and will create a log file with the project details

# Once script is started, specify the log file name needed.  The log will be saved to the home directory of the currect bss user.

#Zone & Region Array
ZONE=(us-west1-a us-west1-b us-west1-c asia-south1-a asia-south1-b asia-south1-c asia-southeast1-a asia-southeast1-b asia-southeast1-c)
REGION=(us-west1 asia-south1 asia-southeast1)

# clear screen and get logfile name from user and retrieve users home directory
echo -e '\0033\0143'

echo "This script will install audit the key GCP components for this project. The findings"
echo "will be available in a log file in the home directory of the current user."
echo ""
#echo "Please enter the log file name - ie ampsol-dev.log"
echo ""
#read logfile

user="$(whoami)"
sudo rm -rf /home/$user/projectdetails.sh
path="/home/$user/projectdetails.sh"

# Get subnet & VPC details
echo "Subnet/VPC Resources:" >> "$path"

subnets="$(gcloud compute networks subnets list | awk '{if (NR!=1) {print $1" "$2" "$3" "$4}}')"
ln=0
echo "$subnets" | while IFS= read -r line;
do
    ln=`expr $ln + 1`
    subnetname=$(echo $line | cut -f1 -d ' ')
    region=$(echo $line | cut -f2 -d ' ')
    vpc=$(echo $line | cut -f3 -d ' ')
    range=$(echo $line | cut -f4 -d ' ')
    echo "$ln) Subnet_Name=$subnetname  Region=$region  VPC=$vpc  IP_Range=$range" >> "$path"
done

echo " " >> "$path"

# get vm resources
echo "Running Compute VM Resources:" >> "$path"


vm="$(gcloud compute instances list | awk '{if (NR!=1) {print $1" "$2" "$3" "$4" "$6}}')"
ln=0
#for instance in $(gcloud compute instances list --format="value(name)" --filter="zone:$i status:running" --quiet)
echo "$vm" | while IFS= read -r line;
do
    ln=`expr $ln + 1`
    vmname=$(echo $line | cut -f1 -d ' ')
    region=$(echo $line | cut -f2 -d ' ')
    type=$(echo $line | cut -f3 -d ' ')
    ip=$(echo $line | cut -f4 -d ' ')
    status=$(echo $line | cut -f5 -d ' ')
echo "$ln) Vm_Name=$vmname  Region=$region  TYPE=$type  Private_ip=$ip" Status=$status >> "$path"
done

##for i in "${ZONE[@]}"
##do
##  ln=0
##  echo -e "$i" >> "$path"
##for instance in $(gcloud compute instances list --format="value(name)" --filter="zone:$i status:running" --quiet)
##  do
#    echo $i
#    echo "${instance}"
    #free_memory=$(gcloud compute ssh $instance --zone=$ZONE --internal-ip --command="free -m | awk '/^Mem:/{print \$4}'" --quiet)
##    ulimit=$(gcloud compute ssh $instance --zone=$i --internal-ip --quiet)
#    echo "${ulimit}"
##    ln=`expr $ln + 1`
##    echo "$ln) VM_Name=${instance}  Zone=$i Ulimit=${ulimit}" >> "$path"
##  done
##  echo " " >> "$path"
##done


# get CSQL resources and details
echo " " >> "$path"
echo "CSQL Resources:" >> "$path"
csql="$(gcloud sql instances list | awk '{if (NR!=1) {print $1" "$2" "$3" "$4" "$6}}')"
ln=0
echo "$csql" | while IFS= read -r line;
do
    ln=`expr $ln + 1`
    sqlname=$(echo $line | cut -f1 -d ' ')
    version=$(echo $line | cut -f2 -d ' ')
    zone=$(echo $line | cut -f3 -d ' ')
    type=$(echo $line | cut -f4 -d ' ')
    ip=$(echo $line | cut -f5 -d ' ')
    echo "$ln)  CSQL_Name=$sqlname  Version=$version  Zone=$zone  CSQL_Type=$type  Private_IP=$ip" >> "$path"
done

#get Redis (memorystore) resources and details
echo " " >> "$path"
echo "Redis Memorystore Resources:" >> "$path"
for i in "${REGION[@]}"
do
  ln=0
  echo -e "$i" >> "$path"
  redis="$(gcloud redis instances list --region=$i | awk '{if (NR!=1) {print $1" "$2" "$3" "$4" "$5" "$6}}')"
  echo "$redis" | while IFS= read -r line;
    do
      ln=`expr $ln + 1`
      redisname=$(echo $line | cut -f1 -d ' ')
      version=$(echo $line | cut -f2 -d ' ')
      region=$(echo $line | cut -f3 -d ' ')
      tier=$(echo $line | cut -f4 -d ' ')
      gbmemory=$(echo $line | cut -f5 -d ' ')
      ip=$(echo $line | cut -f6 -d ' ')
        if [ ! -z "$redisname" ]; then
          echo "$ln)  Redis_Name=$redisname  Version=$version  Region=$region  Tier=$tier  Private_IP=$ip" >> "$path"
        fi
    done
    echo " " >> "$path"
done

# get CloudDNS resources
echo " " >> "$path"
echo "CloudDNS Resources:" >> "$path"

cdns="$(gcloud dns managed-zones list  | awk '{if (NR!=1) {print $1" "$2" "$3}}')"
ln=0
echo "$cdns" | while IFS= read -r line;
do
    ln=`expr $ln + 1`
    dnsname=$(echo $line | cut -f1 -d ' ')
    dnszone=$(echo $line | cut -f2 -d ' ')
    visibility=$(echo $line | cut -f3 -d ' ')
    echo "$ln) Name=$dnsname  DNS_Zone=$dnszone  Visibility=$visibility" >> "$path"
    recordsets="$(gcloud dns record-sets list --zone=$dnsname | awk '{if (NR!=1) {print $1" "$2" "$3" "$4}}')"
    echo "$recordsets" | while IFS= read -r line;
    do
      rsname=$(echo $line | cut -f1 -d ' ')
      type=$(echo $line | cut -f2 -d ' ')
      ttl=$(echo $line | cut -f3 -d ' ')
      record=$(echo $line | cut -f4 -d ' ')
      echo "Recordset_Name=$rsname  TTL=$ttl  Type=$type  Record=$record" >> "$path"
    done
done

# get Firewall rules
echo " " >> "$path"
echo " " >> "$path"
echo "Firewall rules:" >> "$path"

gcloud compute firewall-rules list >> "$path"

# get Load-balancers - Internal ILB's
echo " " >> "$path"
echo " " >> "$path"
echo "Internal ILB load-balancers:" >> "$path"

gcloud compute forwarding-rules list --filter="loadBalancingScheme=INTERNAL" >> "$path"

# get Load-balancers - External HLB & NLB's
echo " " >> "$path"
echo " " >> "$path"
echo "External HLB & NLB load-balancers:" >> "$path"

gcloud compute forwarding-rules list --filter="loadBalancingScheme=EXTERNAL" >> "$path"

# get Bucket List
echo " " >> "$path"
echo " " >> "$path"
echo "Bucket List:" >> "$path"

gsutil ls >> "$path"


# get compute addresses
echo " " >> "$path"
echo " " >> "$path"
echo "compute addresses list:" >> "$path"

gcloud compute addresses list >> "$path"