#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
zk_nodes=`${THIS_DIR}/../read-vartf.sh ${1} zookeeper`
echo `date` "zk_nodes=$zk_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

sudo chmod +x ${THIS_DIR}/propertiesReader.sh
source ${THIS_DIR}/propertiesReader.sh $env

sudo chmod +x ${THIS_DIR}/install.sh
install_script=$THIS_DIR/install.sh
install_script_modified=$THIS_DIR/install_modified.sh

sudo chmod +x ${THIS_DIR}/start.sh
start_script=$THIS_DIR/start.sh
start_script_modified=$THIS_DIR/start_modified.sh

rm -rf $install_script_modified
rm -rf $start_script_modified

sudo chmod 777 -R ${THIS_DIR}
cat $install_script | sed "s#<zk_version>#${zk_version}#g;s#<zk_home>#${zk_home}#g;" > $install_script_modified
cat $start_script | sed "s#<zk_version>#${zk_version}#g;s#<zk_home>#${zk_home}#g;s#<zk_log4jVersion>#${zk_log4jVersion}#g;s#<zk_slf4jVersion>#${zk_slf4jVersion}#g;s#<zk_slf4jApiVersion>#${zk_slf4jApiVersion}#g;" > $start_script_modified

sudo chmod 777 $install_script_modified $start_script_modified

IFS=',' read -r -a nodesArr <<< "${zk_nodes}"


	for i in "${!nodesArr[@]}"
	do
 		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $install_script_modified)"
	done

	# configuration
	sudo chmod +x configure.sh
	$THIS_DIR/configure.sh ${zk_nodes} ${project} ${zone} ${env}

	# start zookeeper
	for i in "${!nodesArr[@]}"
	do
 		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}" --internal-ip --command "$(cat $start_script_modified)"
	done
	
	echo `date` "Zookeeper Successfully Installed in all cluster nodes"