#!/bin/bash

echo `date` "checking if there is already running zookeeper exist.."
zookeeper_pid=`ps -ef | grep org.apache.zookeeper.server.quorum.QuorumPeerMain | grep -v grep | awk '{print $2}'`

if [[ ! -z "$zookeeper_pid" ]]; then
	echo `date` "ERROR: Since a zookeeper is already running in this node hence exiting this installation process."
	exit -1
fi

echo `date` "downloading zookeeper archive..."
cd /<zk_home>/ && sudo wget https://archive.apache.org/dist/zookeeper/<zk_version>/<zk_version>.tar.gz

echo `date` "un-tar downloaded archive..."
sudo tar -zxf <zk_version>.tar.gz

echo `date` "creating data directory and then removing tar file, which is no longer required"
cd <zk_version> && sudo mkdir -p data && sudo rm -rf /<zk_home>/<zk_version>.tar.gz