#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
zk_nodes=${1}
project=${2}
zone=${3}
env=${4}

sudo chmod +x ${THIS_DIR}/propertiesReader.sh
source ${THIS_DIR}/propertiesReader.sh $env

sudo chmod 777 -R $THIS_DIR/../../template/zookeeper 
zookeeper_conf_tpl=$THIS_DIR/../../template/zookeeper/zoo.cfg.tpl
zookeeper_conf=$THIS_DIR/../../template/zookeeper/zoo.cfg

myid=$THIS_DIR/../../template/zookeeper/myid
rm -rf $zookeeper_conf

cat $zookeeper_conf_tpl | sed "s#<zk_dataDir>#${zk_dataDir}#g; s#<zk_clientPort>#${zk_clientPort}#g; s#<zk_auto_purge_snap_retain_count>#${zk_auto_purge_snap_retain_count}#g; s#<zk_auto_purge_interval>#${zk_auto_purge_interval}#g;" >> $zookeeper_conf

chmod 777 $zookeeper_conf
echo "#" >> $zookeeper_conf

IFS=',' read -r -a nodesArr <<< "${zk_nodes}"

for i in "${!nodesArr[@]}"
do
	serverCount=$(( $i + 1 ))
	echo "server.$serverCount=${nodesArr[i]}:2888:3888" >> $zookeeper_conf
done 

for i in "${!nodesArr[@]}"
do
	echo "copying configuration file $zookeeper_conf into ${nodesArr[i]} "
		touch $myid
		echo $(( $i + 1 )) >  $myid
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}" --internal-ip  --command "sudo mkdir -p ${zk_dataDir} ${zk_logDir} ~/zookeeper/ && sudo chmod 777 -R ${zk_dataDir}  ${zk_logDir} ~/zookeeper/"
		sudo gcloud  compute scp --project "${project}"  --zone "${zone}" --internal-ip $zookeeper_conf ${nodesArr[i]}:~/zookeeper/
		sudo gcloud  compute scp --project "${project}"  --zone "${zone}" --internal-ip $myid ${nodesArr[i]}:~/zookeeper/
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}" --internal-ip  --command "sudo mv ~/zookeeper/zoo.cfg /${zk_home}/${zk_version}/conf/ && sudo chown root:root /${zk_home}/${zk_version}/conf/zoo.cfg &&  sudo chmod 664 /${zk_home}/${zk_version}/conf/zoo.cfg"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}" --internal-ip  --command "sudo mv ~/zookeeper/myid ${zk_dataDir}/ && sudo chown root:root /${zk_home}/${zk_version}/conf/zoo.cfg &&  sudo chmod 664 /${zk_home}/${zk_version}/conf/zoo.cfg"
done