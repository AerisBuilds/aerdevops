#!/bin/bash

cd /<zk_home>/<zk_version>

sudo java -cp <zk_version>.jar:lib/<zk_log4jVersion>.jar:lib/<zk_slf4jVersion>.jar:lib/<zk_slf4jApiVersion>.jar:conf org.apache.zookeeper.server.quorum.QuorumPeerMain conf/zoo.cfg >> logs/zookeeper.log & 