#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

env=${1}
PROP_FILE=${THIS_DIR}/../../conf/conf-ab/zookeeper/ab-${env}.conf

zk_dataDir=`cat ${PROP_FILE} | grep -w zk_dataDir | cut -d '=' -f2`
echo `date` "zk_dataDir=${zk_dataDir}"

zk_logDir=`cat ${PROP_FILE} | grep -w zk_logDir | cut -d '=' -f2`
echo `date` "zk_logDir=${zk_logDir}"

zk_clientPort=`cat ${PROP_FILE} | grep -w zk_clientPort | cut -d '=' -f2`
echo `date` "zk_clientPort=${zk_clientPort}"

zk_auto_purge_interval=`cat ${PROP_FILE} | grep -w zk_auto_purge_interval | cut -d '=' -f2`
echo `date` "zk_auto_purge_interval=${zk_auto_purge_interval}"

zk_auto_purge_snap_retain_count=`cat ${PROP_FILE} | grep -w zk_auto_purge_snap_retain_count | cut -d '=' -f2`
echo `date` "zk_auto_purge_snap_retain_count=${zk_auto_purge_snap_retain_count}"

zk_home=`cat ${PROP_FILE} | grep -w zk_home | cut -d '=' -f2`
echo `date` "zk_home=${zk_home}"

zk_version=`cat ${PROP_FILE} | grep -w zk_version | cut -d '=' -f2`
echo `date` "zk_version=${zk_version}"

zk_log4jVersion=`cat ${PROP_FILE} | grep -w zk_log4jVersion | cut -d '=' -f2`
echo `date` "zk_log4jVersion=${zk_log4jVersion}"

zk_slf4jVersion=`cat ${PROP_FILE} | grep -w zk_slf4jVersion | cut -d '=' -f2`
echo `date` "zk_slf4jVersion=${zk_slf4jVersion}"

zk_slf4jApiVersion=`cat ${PROP_FILE} | grep -w zk_slf4jApiVersion | cut -d '=' -f2`
echo `date` "zk_slf4jApiVersion=${zk_slf4jApiVersion}"