#!/bin/bash

if [ -d "/<zk_home>/<zk_version>" ]
then
	echo `date` "Stoping already running zookeeper.."
	zookeeper_pid=`ps -ef | grep org.apache.zookeeper.server.quorum.QuorumPeerMain | grep -v grep | awk '{print $2}'`
	sudo kill -9 $zookeeper_pid
	echo `date`"removing any previous installation of zookeeper"
	sudo rm -rf /<zk_home>/<zk_version>
fi