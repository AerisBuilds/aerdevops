#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
nifi_nodes=`${THIS_DIR}/../read-vartf.sh ${1} nifi`
echo `date` "nifi_nodes=$nifi_nodes"
zookeeper_nodes=`${THIS_DIR}/../read-vartf.sh ${1} zookeeper`
echo `date` "zookeeper_nodes=$zookeeper_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

if [[ -z "$nifi_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit -1;
fi


IFS=',' read -r -a nodesArr <<< "${nifi_nodes}"

	for i in "${!nodesArr[@]}"
	do
	 sudo /snap/bin/gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $THIS_DIR/uninstall.sh)"
	sudo /snap/bin/gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $THIS_DIR/install.sh)"
	echo "${nodesArr[i]}"
	done	
	
#   perform configuration
	sudo chmod +x $THIS_DIR/configure.sh
	$THIS_DIR/configure.sh ${nifi_nodes} ${project} ${zone} ${env} ${zookeeper_nodes}


#   start the cluster
	sudo chmod +x $THIS_DIR/startNifi.sh
	$THIS_DIR/startNifi.sh ${nifi_nodes} ${project} ${zone} ${env}

