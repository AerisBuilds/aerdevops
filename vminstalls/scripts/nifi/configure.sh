#!/bin/bash


echo "Initiating Nifi configuration..."

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
nifi_nodes=${1}
project=${2}
zone=${3}
env=${4}
zookeeper_nodes=${5}

sudo chmod +x ${THIS_DIR}/propertiesReader.sh
source ${THIS_DIR}/propertiesReader.sh $env

IFS=',' read -r -a nodesArrZk <<< "${zookeeper_nodes}"
nifi_zookeeper_connect_string=""
for i in "${!nodesArrZk[@]}"
do	
	nifi_zookeeper_connect_string+=${nodesArrZk[i]}":2181,"	
done

nifi_zookeeper_connect_string=${nifi_zookeeper_connect_string::-1}

echo "nifi_zookeeper_connect_string=$nifi_zookeeper_connect_string"

IFS=',' read -r -a nodesArr <<< "${nifi_nodes}"

nifi_properties_tpl=$THIS_DIR/../../template/nifi/nifi.properties.tpl
bootstrap_conf_tpl=$THIS_DIR/../../template/nifi/bootstrap.conf.tpl
state_mgmnt_tpl=$THIS_DIR/../../template/nifi/state-management.xml.tpl

# prepare configuration files from the above templates to /usr/local/nifi/nifi-1.4.0/conf/ on each node.

for i in "${!nodesArr[@]}"
do	
	nifi_props_file="nifi.properties"
	touch $nifi_props_file
	cat $nifi_properties_tpl | sed "s#<nifi_node_name>#${nodesArr[i]}#g; s#<nifi_zookeeper_connect_string>#${nifi_zookeeper_connect_string}#g" >> "$nifi_props_file"
	chmod 777 $nifi_props_file
	echo "Prepared $nifi_props_file. Copying to ${nodesArr[i]}"
	sudo /snap/bin/gcloud compute scp --project "${project}"  --zone "${zone}" --internal-ip  "$nifi_props_file" ${nodesArr[i]}:~/
	sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo mv ~/nifi.properties /usr/local/nifi/nifi-1.4.0/conf/"
	rm $nifi_props_file
	
	bootstrap_conf="bootstrap.conf"
	touch $bootstrap_conf
	cat $bootstrap_conf_tpl >> "$bootstrap_conf"
	echo "Prepared $bootstrap_conf. Copying into ${nodesArr[i]}"
	sudo /snap/bin/gcloud compute scp --project "${project}"  --zone "${zone}" --internal-ip  "$bootstrap_conf" ${nodesArr[i]}:~/
	sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo mv ~/bootstrap.conf /usr/local/nifi/nifi-1.4.0/conf/"
	rm $bootstrap_conf

	state_mgmnt_conf="state-management.xml"
	touch $state_mgmnt_conf
	cat $state_mgmnt_tpl | sed "s#<nifi_zookeeper_connect_string>#${nifi_zookeeper_connect_string}#g" >> "$state_mgmnt_conf"
	chmod 777 $state_mgmnt_conf
	echo "Prepared $state_mgmnt_conf. Copying to ${nodesArr[i]}"
	sudo /snap/bin/gcloud compute scp --project "${project}"  --zone "${zone}" --internal-ip  "$state_mgmnt_conf" ${nodesArr[i]}:~/
	sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${nodesArr[i]}"  --command "sudo mv ~/state-management.xml /usr/local/nifi/nifi-1.4.0/conf/"
	rm "$state_mgmnt_conf"
 
done
