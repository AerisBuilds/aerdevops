#!/bin/bash


echo "Going to start Nifi cluster.."

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
nifi_nodes=${1}
project=${2}
zone=${3}
env=${4}

sudo chmod +x ${THIS_DIR}/propertiesReader.sh
source ${THIS_DIR}/propertiesReader.sh $env

IFS=',' read -r -a nodesArr <<< "${nifi_nodes}"

for i in "${!nodesArr[@]}"
do	
	
	sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip  "${nodesArr[i]}"  --command "sudo /usr/local/nifi/nifi-1.4.0/bin/nifi.sh start" 
 
done
