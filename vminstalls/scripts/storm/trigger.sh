#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : start.sh
#-- Type           : Shell Script
#-- Purpose        : Configure storm on on all the nodes
#-- Description    :
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga    Configure storm components on GCP
#-----------------------------------------------------------------------------------------------

THIS_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
STORM_NODES=$(${THIS_DIR}/../read-vartf.sh ${1} storm)
echo $(date) "STORM_NODES=$STORM_NODES"
ZOOKEEPER_SERVERS=$(${THIS_DIR}/../read-vartf.sh ${1} zookeeper)
echo $(date) "ZOOKEEPER_SERVERS=$ZOOKEEPER_SERVERS"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

NIMBUS_NODE="$(cut -d ',' -f 1 <<<$STORM_NODES)"
SUPERVISOR_NODES="$(cut -d ',' -f 2- <<<$STORM_NODES)"
echo "NIMBUS ${NIMBUS_NODE}"
echo "SP ${SUPERVISOR_NODES}"

IFS=',' read -r -a nodesArr <<<"${STORM_NODES}"

for i in "${!nodesArr[@]}"; do
  sudo /snap/bin/gcloud compute ssh --project "${project}" --zone "${zone}" --internal-ip "${nodesArr[i]}" --command "$(cat $THIS_DIR/storm_installation.sh)"
done

echo "starting configuration script"
sudo chmod +x ${THIS_DIR}/configuration.sh
${THIS_DIR}/configuration.sh ${ZOOKEEPER_SERVERS} ${NIMBUS_NODE} $env

echo "copying storm.yaml file"
for i in "${!nodesArr[@]}"; do
  sudo gcloud compute scp ${THIS_DIR}/../../template/storm/storm.yaml --project "${project}" --zone "${zone}" --internal-ip ${nodesArr[i]}:~/storm/storm.yaml
  sudo /snap/bin/gcloud compute ssh --project "${project}" --zone "${zone}" "${nodesArr[i]}" --internal-ip --command "sudo mv ~/storm/storm.yaml /usr/local/storm/apache-storm-0.9.3/conf"
done

echo "starting nimbus node"
sudo /snap/bin/gcloud compute ssh --project "${project}" --zone "${zone}" --internal-ip "${NIMBUS_NODE}" --command "sudo /usr/local/storm/apache-storm-0.9.3/bin/storm nimbus&" &
echo "Nimbus started"
#sudo gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${NIMBUS_NODE}" --command "sudo -bE /usr/local/storm/apache-storm-0.9.3/bin/storm ui;"

IFS=',' read -r -a nodesSupArr <<<"${SUPERVISOR_NODES}"

echo "Sleeping for 20 seconds" 
sleep 20
echo "starting supervisor nodes"
for i in "${!nodesSupArr[@]}"; do
  echo "Node: ${nodesSupArr[i]}"
  sudo /snap/bin/gcloud compute ssh --project "${project}" --zone "${zone}" --internal-ip "${nodesSupArr[i]}" --command "sudo /usr/local/storm/apache-storm-0.9.3/bin/storm supervisor&" &
  echo "SP:${nodesSupArr[i]} started"
done
echo "All Supervisor started"

sudo /snap/bin/gcloud compute --project "${project}" ssh --zone "${zone}" --internal-ip "${NIMBUS_NODE}" --command "sudo /usr/local/storm/apache-storm-0.9.3/bin/storm ui&" &
echo "Finished"