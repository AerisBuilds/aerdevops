#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : configuration.sh
#-- Type           : Shell Script
#-- Purpose        : Configure storm on GCP
#-- Description    :
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga		  Configure storm components on GCP
#-----------------------------------------------------------------------------------------------

echo "Configuring Storm"

THIS_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
echo "THIS_DIR ${THIS_DIR}"
env=${3}
source ${THIS_DIR}/PropertiesReader.sh $env
#Reading the values that needs to be set in the config file
ZOOKEEPER_SERVERS=${1}
PORT=$(getPort)
LOCAL_DIR=$(getLocalDir)
NIMBUS_HOST=${2}
TIMEOUT_SECS=$(getTopologyTimeoutSecs)
TOPOLOGY_DEBUG=$(getTopologyDebug)
TOPOLOGY_STATS=$(getTopologyStats)
SUPERVISOR_PORTS=$(getSupervisorPorts)
SUPERVISOR_CHILDOPTS=$(getSupervisorChildopts)
CHILDOPTS=$(getChildopts)
NIMBUS_CHILDOPTS=$(getNimbusChildopts)
UI_PORT=$(getUIPort)

sudo chmod 777 ${THIS_DIR}/../../template/storm
cd ${THIS_DIR}/../../template/storm

st_tmp=${THIS_DIR}/../../template/storm/storm.yaml.tmp
sudo chmod 777 $st_tmp
st_conf=${THIS_DIR}/../../template/storm/storm.yaml
rm -rf $st_conf

#sudo cp storm.yaml.tmp storm.yaml


IFS=',' read -r -a nodesArr <<<"${ZOOKEEPER_SERVERS}"
ZK_SERVERS="\n"
for server in "${nodesArr[@]}"; do
  ZK_SERVERS+="- "\"${server}\""\n"
done
echo -e ${ZK_SERVERS}
IFS=',' read -r -a portsArr <<<"${SUPERVISOR_PORTS}"
PORTS="\n"
for p in "${portsArr[@]}"; do
  PORTS+="- "${p}"\n"
done
echo -e ${PORTS}
sudo cat $st_tmp | sed "s#getZookeeperServers#${ZK_SERVERS}#g; s#getPort#${PORT}#g; s#getLocalDir#${LOCAL_DIR}#g; s#getNimbusHost#${NIMBUS_HOST}#g; s#getTopologyTimeoutSecs#${TIMEOUT_SECS}#g; s#getTopologyDebug#${TOPOLOGY_DEBUG}#g; s#getTopologyStats#${TOPOLOGY_STATS}#g; s#getSupervisorPorts#${PORTS}#g; s#getSupervisorChildopts#${SUPERVISOR_CHILDOPTS}#g; s#getWorkerChildopts#${CHILDOPTS}#g; s#getNimbusChildOpts#${NIMBUS_CHILDOPTS}#g; s#getUiPort#${UI_PORT}#g;" >$st_conf

sudo chmod 777 $st_conf
echo "configuration finished"