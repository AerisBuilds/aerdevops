#!/bin/bash
################################################################
# Name           : PropertiesReader.sh
# Type           : Script
# Purpose        : A utility which read the properties from 'Application.properties' file.

#This directory will get the directory where this script is residing, not from where it is called or not PWD/current working directory.

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo ${THIS_DIR}

env=${1}
PROP_FILE=${THIS_DIR}/../../conf/conf-ab/storm/ab-${env}.conf
function getPort () {
	echo `cat ${PROP_FILE} | grep -w port | cut -d '=' -f2`
}

function getLocalDir () {
	echo `cat ${PROP_FILE} | grep -w local-dir | cut -d '=' -f2`
}

function getTopologyTimeoutSecs () {
	echo `cat ${PROP_FILE} | grep -w topology-timeoutsecs | cut -d '=' -f2`
}

function getTopologyDebug () {
	echo `cat ${PROP_FILE} | grep -w topology-debug | cut -d '=' -f2`
}

function getTopologyStats () {
	echo `cat ${PROP_FILE} | grep -w topology-stats | cut -d '=' -f2`
}

function getSupervisorPorts () {
    echo `cat ${PROP_FILE} | grep -w supervisor-slotsports | cut -d '=' -f2`
}

function getSupervisorChildopts () {
	echo `cat ${PROP_FILE} | grep -w supervisor-childopts | cut -d '=' -f2,3`
}

function getNimbusChildopts () {
	echo `cat ${PROP_FILE} | grep -w nimbus.childopts | cut -d '=' -f2,3`
}

function getChildopts () {
	echo `cat ${PROP_FILE} | grep -w worker-childopts | cut -d '=' -f2,3`
}

function getUIPort () {
	echo `cat ${PROP_FILE} | grep -w uiport | cut -d '=' -f2`
}

