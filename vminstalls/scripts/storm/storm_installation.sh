#!/bin/bash
#-----------------------------------------------------------------------------------------------
#-- Name           : storm_installation.sh
#-- Type           : Shell Script
#-- Purpose        : Install storm on GCP
#-- Description    : 
#-- Mod Log
#-- Date        By              Jira       Description
#-- ----------  --------------  ---------- -----------------------------------------------------
#   2019-07-24 Timsy Jagga		  Install storm components on GCP         
#-----------------------------------------------------------------------------------------------

echo "Starting Storm Installation"

#Step 1: create storm directory to install storm
sudo mkdir storm
sudo chmod 777 storm
sudo mkdir -p /usr/local/storm
cd /usr/local/storm
echo "removing already present storm file"
sudo rm -rf *
#Step 2: download apache storm and extract it

echo "downloading storm"
sudo wget "https://archive.apache.org/dist/storm/apache-storm-0.9.3/apache-storm-0.9.3.tar.gz"
sudo tar -zxf apache-storm-0.9.3.tar.gz

#Step 3: Calling the storm configuration script
cd apache-storm-0.9.3/
sudo mkdir data
cd bin
sudo sed -i 's/print("Running:/#print("Running:/g' storm