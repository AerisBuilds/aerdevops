#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#Set the GITREPO variable to the local directory where you have cloned this repository and create a file with SSH login and hostname.
#
#
######Configuration file for storm#############
ssh-keygen -f "/root/.ssh/known_hosts" -R localhost
#
#
##########################################################
sudo echo "################NIMBUS NODE###################" >> /etc/hosts
sudo echo "10.4.18.55 storm-dci-nimbus.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-nimbus                    " >> /etc/hosts
#
sudo echo "###########SUPERVISOR NODE####################" >> /etc/hosts
sudo echo "10.4.18.5 storm-dci-supervisor-node1.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node1" >> /etc/hosts
sudo echo "10.4.18.31 storm-dci-supervisor-node2.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node2" >> /etc/hosts
sudo echo "10.4.18.36 storm-dci-supervisor-node3.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node3" >> /etc/hosts
sudo echo "10.4.18.16 storm-dci-supervisor-node4.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node4" >> /etc/hosts
sudo echo "10.4.18.32 storm-dci-supervisor-node5.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
sudo echo "10.4.18.47 storm-dci-supervisor-node7.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
sudo echo "10.4.18.50 storm-dci-supervisor-node7.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
#
sudo echo "#############ZOOKEEPER NODE######################" >> /etc/hosts
sudo echo "10.4.18.26 zookeeper-dci-node1.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
sudo echo "10.4.18.4 zookeeper-dci-node2.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
sudo echo "10.4.18.58 zookeeper-dci-node3.us-west1-a.c.aeriscom-ab-dci-201905.internal storm-dci-supervisor-node5" >> /etc/hosts
