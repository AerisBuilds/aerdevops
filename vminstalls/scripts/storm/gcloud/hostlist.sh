#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#Set the GITREPO variable to the local directory where you have cloned this repository and create a file with SSH login and hostname.
#
GITREPO=$PWD
PROJECT_ID=aeriscom-ab-dci-201905
ZONE=us-west1-a
#USER=ubuntu
#
#
#
#cat <<EOF >HOSTLIST
#$USER@10.4.18.55
#$USER@10.4.18.5
#$USER@10.4.18.31
#$USER@10.4.18.36
#$USER@10.4.18.16
#$USER@10.4.18.32
#$USER@10.4.18.47
#$USER@10.4.18.50
#$USER@10.4.18.26
#$USER@10.4.18.4
#$USER@10.4.18.58
#EOF


cat <<EOF >HOSTNAMES
storm-dci-nimbus
storm-dci-supervisor-node1
storm-dci-supervisor-node2
storm-dci-supervisor-node3
storm-dci-supervisor-node4
storm-dci-supervisor-node5
storm-dci-supervisor-node6
storm-dci-supervisor-node7
zookeeper-dci-node1
zookeeper-dci-node2
zookeeper-dci-node3
EOF

