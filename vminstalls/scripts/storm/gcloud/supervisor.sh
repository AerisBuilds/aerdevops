#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
########List of Supervisor server#####
#supervisor=10.4.18.5,10.4.18.31,10.4.18.36,10.4.18.16,10.4.18.32,10.4.18.47,10.4.18.50
#USER=ubuntu
##
##
#######Start the supervisor server###########
##
##
# ssh -i key/nrt-test.pem -o StrictHostKeyChecking=no -t $USER@$supervisor "sudo bash $BOPT ./supervisor_start.sh"
#done
##
##Provide the supervisor nodes details
SUPERVISOR_NODE=storm-dci-supervisor-node1,storm-dci-supervisor-node2,storm-dci-supervisor-node3,storm-dci-supervisor-node4,storm-dci-supervisor-node5,storm-dci-supervisor-node6,storm-dci-supervisor-node7
#
# 
gcloud compute ssh $SUPERVISOR_NODE --project $PROJECT_ID --internal-ip --command --zone $ZONE "sudo sh /tmp/supervisor_start.sh"
done