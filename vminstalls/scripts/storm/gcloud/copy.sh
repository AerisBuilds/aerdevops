#! /bin/bash
#for HOST in `cat HOSTLIST`; do
#echo "*** $HOST"
#scp -i key/nrt-test.pem -o StrictHostKeyChecking=no -p \
#${GITREPO}conf.sh* \
#${GITREPO}config.sh* \
#${GITREPO}hostlist.sh* \
#${GITREPO}nimbus.sh* \
#${GITREPO}nimbus_start.sh* \
#${GITREPO}storm.yaml \
#${GITREPO}supervisor.sh* \
#${GITREPO}supervisor_start.sh* \
#${GITREPO}zdk_start.sh* \
#${GITREPO}zkserver.sh* \
#${GITREPO}zoo.cfg \
#$HOST:
#done


#Provide the project and zone details
PROJECT_ID=aeriscom-ab-dci-201905
ZONE=us-west1-a
##Command to copy the files
for HOST in `cat HOSTNAMES`; do
echo "*** $HOST"
gcloud beta compute scp \
--project $PROJECT_ID \
--internal-ip --zone $ZONE \
${GITREPO}conf.sh* \
${GITREPO}config.sh* \
${GITREPO}hostlist.sh* \
${GITREPO}nimbus.sh* \
${GITREPO}nimbus_start.sh* \
${GITREPO}storm.yaml \
${GITREPO}supervisor.sh* \
${GITREPO}supervisor_start.sh* \
${GITREPO}zdk_start.sh* \
${GITREPO}zkserver.sh* \
${GITREPO}zoo.cfg \
$HOST:/tmp/
done