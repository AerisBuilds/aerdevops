#!/bin/bash
# Owner: DevOps
#Date:26/07/2019
# Script to download the instance metadata file from google storage.

echo ""

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 GCS path" >&2
  exit 1
fi

GCS_PATH="$1"

#gs://csb-ampsolutions-dev-tfstate-us-west1/cloudera/default.tfstate
gsutil cp $GCS_PATH instances.json
