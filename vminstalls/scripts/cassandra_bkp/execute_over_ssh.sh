#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Owner: DevOps
#Date:July-2019
#
COMMAND="$1"
for HOST in `cat instances.csv | awk '{if(NR>1)print}'`; do
  echo "*** $HOST"
  HOST_NAME=`echo $HOST|cut -d',' -f 1`
  PROJECT_ID=`echo $HOST|cut -d',' -f 3`
  ZONE=`echo $HOST|cut -d',' -f 4`
  USER=`echo $HOST|cut -d',' -f 5`

  gcloud beta compute ssh $USER@$HOST_NAME --project  $PROJECT_ID   --internal-ip --zone  $ZONE --command "sudo bash $COMMAND"
done
