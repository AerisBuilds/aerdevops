#! /bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#Date:Jully-2019
#Owner: DevOps
#
#
THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cassandra_nodes=`${THIS_DIR}/../../../../read-vartf.sh ${1} cassandra`
echo `date` "cassandra_nodes=$cassandra_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g'`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g'`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g'`
echo `date` "env=$env"

if [[ -z "$cassandra_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit 1;
fi

#TF_CONFIG_FILE=$1
#sh hostlist.sh
echo "Calling copy.sh"

sh copy.sh "$cassandra_nodes" "$project" "$zone"
sh disk_format.sh
#sh config.sh
sh cassandra.sh "$cassandra_nodes" "$project" "$zone" "$env"
