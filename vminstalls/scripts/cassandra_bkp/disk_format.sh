#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#Owner: DevOps
#Date:July-2019
#Formatting And Mounting Extra Disk On VM Instance
#
#we should format the disk to ext4 using the following command. In the below command we are mentioning /dev/sdb as that is the extra disk available.
sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb

#Next, create a mount directory on the instance as shown below. You can replace the /demo-mount with a custom name and path you prefer.
sudo mkdir -p /mnt

 #Now, mount the disk to the directory we created using the following command.
 sudo mount -o discard,defaults /dev/sdb /mnt

 #If you want write permissions to this disk for all the users, you can execute the following command. Or, based on the privileges you need for the disk, you can apply the user permissions.
 sudo chmod a+w /mnt

 #Check the mounted disk using the following command.
 df -h

 #Automount Disk On Reboot
 #1. First, back up the fstab file.
 sudo cp /etc/fstab /etc/fstab.backup

 #2. Execute the following command to make a fstab entry with the UUID of the disk.
 sudo echo UUID=`sudo blkid -s UUID -o value /dev/sdb` /mnt ext4 discard,defaults,nofail  2 | sudo tee -a /etc/fstab

 #2. Check the UUID of the extra disk
 sudo blkid -s UUID -o value /dev/sdb

 #3. Open fstab file and check for the new entry for the UUID of the extra disk
 sudo cat /etc/fstab

 sudo echo "Now, on every reboot, the disk will automatically mount to the defined folder based on the fstab entry."
