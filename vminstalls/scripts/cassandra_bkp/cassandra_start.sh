#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Owner: DevOps
#Date:July-2019
#
#
source /tmp/value.txt
#cassandra configuration
sudo echo "Create cassandra home directory"
sudo mkdir -p /usr/local/cassandra/
sudo chown -R ubuntu:ubuntu /usr/local/cassandra/
#
#Cassandra data directory
sudo mkdir -p /mnt/cassandra/data
sudo chown -R ubuntu:ubuntu /mnt/cassandra/data
#
#commitlog_directory
sudo mkdir -p /mnt/cassandra/commitlog
sudo chown -R ubuntu:ubuntu /mnt/cassandra/commitlog
#
#Caches directory
sudo mkdir -p /mnt/cassandra/saved_caches
sudo chown -R ubuntu:ubuntu /mnt/cassandra/saved_caches
#
#Untar the cassandra package
sudo echo "Untar the cassandra package"
sudo tar -xvf /opt/apache-cassandra-* -C /usr/local/cassandra/
#
#Cassandra home directory
cs_dir=/usr/local/cassandra/apache-cassandra-3.11.0
#
#Replace the content of template.yaml to the cassandra.yaml
sudo cp -r /tmp/template.yaml $cs_dir/conf/cassandra.yaml
#
#Replace the Cluster name as provide in the value.txt file
sudo sed -i "s/CN/$CN/g" $cs_dir/conf/cassandra.yaml
#
#Replace the seed node details as per provided thw value in value.txt
sudo sed -i "s/seed_address/$seed_address/g" $cs_dir/conf/cassandra.yaml
#
#Get the IP of the host
IP=$(ifconfig ens4 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
#
## Append the IP of the host to the listen_address and RPC
sudo sed -i "s/localhost/$IP/g" $cs_dir/conf/cassandra.yaml
#
sudo chown -R ubuntu:ubuntu /usr/local/cassandra/
#Start the cassandra server.
sudo su - ubuntu $cs_dir/bin/cassandra &
#
