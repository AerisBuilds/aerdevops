#! /bin/bash
#
## shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#Date:Jully-2019
#Owner: DevOps
#This script will copy the following mentions file to the remote hosts
#
#
GITREPO=$(pwd)
  PROJECT_ID=$2
     ZONE=$3

IFS=","

for HOST in $1; do
     echo "*** $HOST running"
    # HOST_NAME=`echo $HOST|cut -d',' -f 1`

   #  USER=`echo $HOST|cut -d',' -f 5`
	 
   # gcloud beta compute scpi \
   # ${GITREPO}/conf.sh* \
   # ${GITREPO}/config.sh* \
   # ${GITREPO}/copy.sh* \
   # ${GITREPO}/hostlist.sh* \
#	${GITREPO}/template.yaml* \
 #   ${GITREPO}/cassandra.sh* \
 #   ${GITREPO}/cassandra_start.sh* \
 #   ${GITREPO}/startup.sh* \
 #   $HOST:/tmp \
 #   --project $PROJECT_ID \
 #   --internal-ip --zone $ZONE

   gcloud beta compute scp \
    ${GITREPO}/conf.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/config.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/copy.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/hostlist.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/template.yaml* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/cassandra.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/cassandra_start.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

gcloud beta compute scp \
    ${GITREPO}/startup.sh* \
    $HOST:/tmp \
    --project $PROJECT_ID \
    --internal-ip --zone $ZONE

done
