#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#Date:Jully-2019
#Owner: DevOps
#
# 
User=ubuntu
 PROJECT_ID=$2
  ZONE=$3
  ENV=$4

IFS=","

for HOST in $1; do
echo "*** $HOST"
gcloud compute scp --project $PROJECT_ID --internal-ip  --zone $ZONE ./value-${4}.txt $HOST:/tmp/value.txt
#gcloud compute ssh $HOST --project $PROJECT_ID --internal-ip  --zone $ZONE --command "sudo sh /tmp/cassandra_start.sh"
gcloud compute ssh $HOST --project $PROJECT_ID --internal-ip  --zone $ZONE --command "$(cat cassandra_start.sh)"
done
