#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
cassandra_nodes=`${THIS_DIR}/../read-vartf.sh ${1} cassandra`
echo `date` "cassandra_nodes=$cassandra_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

if [[ -z "$cassandra_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit -1;
fi

IFS=',' read -r -a nodesArr <<< "${cassandra_nodes}"

	for i in "${!nodesArr[@]}"
	do
		echo `date` "Stopping and cleaing cassandra on node $i"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "sudo pkill -f cassandra"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "sudo rm -rf apache-cassandra-3.11.0-bin* && sudo rm -rf /usr/local/cassandra/ && sudo rm -rf /mnt/cassandra"
	done
	
echo `date` "Successfully stopped and cleaned up cassandra cluster"