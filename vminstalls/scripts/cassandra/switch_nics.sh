#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
cassandra_nodes=`${THIS_DIR}/../read-vartf.sh ${1} cassandra`
echo `date` "cassandra_nodes=$cassandra_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

if [[ -z "$cassandra_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit -1;
fi

cassandra_conf=/usr/local/cassandra/apache-cassandra-3.11.0/conf/cassandra.yaml
cassandra_conf_nic=/usr/local/cassandra/apache-cassandra-3.11.0/conf/cassandra.yaml_nic
cassandra_conf_tmp=/usr/local/cassandra/apache-cassandra-3.11.0/conf/cassandra.yaml_tmp

IFS=',' read -r -a nodesArr <<< "${cassandra_nodes}"

	for i in "${!nodesArr[@]}"
	do
		echo `date` "Stopping cassandra on node $i"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "sudo pkill -f cassandra"
	done 

	for i in "${!nodesArr[@]}"
	do
		echo `date` "switching cassandra.yaml and cassandra.yaml_nic file in node $i"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "sudo mv $cassandra_conf $cassandra_conf_tmp &&  sudo mv $cassandra_conf_nic $cassandra_conf &&  mv $cassandra_conf_tmp  $cassandra_conf_nic"
	done
	
	# start cassandra on all nodes
	for i in "${!nodesArr[@]}"
	do
		echo `date` "starting cassandra on node $i"
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "/usr/local/cassandra/apache-cassandra-3.11.0/bin/cassandra &"
	done
	
echo `date` "nic switch configuration completed"