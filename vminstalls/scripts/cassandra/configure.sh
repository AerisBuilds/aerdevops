#!/bin/bash


echo "Initiating Cassandra configuration..."

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cassandra_nodes=${1}
project=${2}
zone=${3}
env=${4}


# from first three cassandra nodes, we are creating seed address in cassandra.yaml
#cassandra_node1=`echo $cassandra_nodes | cut -d ',' -f1`
#cassandra_node2=`echo $cassandra_nodes | cut -d ',' -f2`
#cassandra_node3=`echo $cassandra_nodes | cut -d ',' -f3`

for i in 1 2 3
do
echo `date` "fetching nics for cassandra node $i"
cassandra_node=`echo $cassandra_nodes | cut -d ',' -f$i`
nics=`sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${cassandra_node}" --internal-ip --command "ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"`
nic0=`echo $nics | cut -d ' ' -f1`
nic1=`echo $nics | cut -d ' ' -f2`
	if [[ ! -z  ${seed_address_nic0} ]]; then
		seed_address_nic0=${seed_address_nic0},$nic0
	else
		seed_address_nic0=$nic0
	fi
	
	if [[ ! -z  ${seed_address_nic1} ]]; then
		seed_address_nic1=${seed_address_nic1},$nic1
	else
		seed_address_nic1=$nic1
	fi
done
echo `date` "seed_address for nic0=${seed_address_nic0}"
echo `date` "seed_address for nic1=${seed_address_nic1}"


# create conf file from tpl file and replace values in the placeholders
sudo chmod 777 -R $THIS_DIR/../../template/cassandra
cassandra_conf_tpl=$THIS_DIR/../../template/cassandra/cassandra.yaml.tpl
sudo chmod +x $cassandra_conf_tpl
cassandra_conf=$THIS_DIR/../../template/cassandra/cassandra.yaml
cassandra_conf_nic=$THIS_DIR/../../template/cassandra/cassandra.yaml_nic
jvm_option_conf=$THIS_DIR/../../template/cassandra/jvm.options
rm -rf $cassandra_conf
rm -rf $cassandra_conf_nic

IFS=',' read -r -a nodesArr <<< "${cassandra_nodes}"

env_uppper=`echo $env | tr a-z A-Z`

for i in "${!nodesArr[@]}"
do
	echo ""
	nics=`sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}" --internal-ip --command "ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"`
	nic0=`echo $nics | cut -d ' ' -f1`
	nic1=`echo $nics | cut -d ' ' -f2`
	sudo cat $cassandra_conf_tpl | sed "s#<CN>#NRT $env_uppper#g;s#<seed_address>#${seed_address_nic0}#g;s#<listen_address>#${nic0}#g;s#<broadcast_address>#${nic0}#g;s#<broadcast_rpc_address>#${nic0}#g;" > $cassandra_conf
	sudo cat $cassandra_conf_tpl | sed "s#<CN>#NRT $env_uppper#g;s#<seed_address>#${seed_address_nic1}#g;s#<listen_address>#${nic1}#g;s#<broadcast_address>#${nic1}#g;s#<broadcast_rpc_address>#${nic1}#g;" > $cassandra_conf_nic
	
	echo `date` "copying configuration file $cassandra_conf and $cassandra_conf_nic into ${nodesArr[i]} "
	
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "mkdir -p ~/cassandra/ && rm -rf ~/cassandra/cassandra.yaml && sudo rm -rf ~/cassandra/cassandra.yaml_nic"
		sudo gcloud  compute scp --project "${project}" --zone "${zone}"  --internal-ip $cassandra_conf ${nodesArr[i]}:~/cassandra/
		sudo gcloud  compute scp --project "${project}" --zone "${zone}"  --internal-ip $cassandra_conf_nic ${nodesArr[i]}:~/cassandra/
		sudo gcloud  compute scp --project "${project}" --zone "${zone}"  --internal-ip $jvm_option_conf ${nodesArr[i]}:~/cassandra/
		
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "sudo mv ~/cassandra/cassandra.yaml /usr/local/cassandra/apache-cassandra-3.11.0/conf/ && sudo mv ~/cassandra/cassandra.yaml_nic /usr/local/cassandra/apache-cassandra-3.11.0/conf/ && sudo mv ~/cassandra/jvm.options /usr/local/cassandra/apache-cassandra-3.11.0/conf/  && sudo chmod 777 -R /usr/local/cassandra && sudo chown root:root -R /usr/local/cassandra"

done

# move conf file to /etc/rabbitmq to each node.




# start cassandra on all nodes
for i in "${!nodesArr[@]}"
do
	echo `date` "starting cassandra in ${nodesArr[i]}"
	sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "/usr/local/cassandra/apache-cassandra-3.11.0/bin/cassandra &"
done

echo `date` "successfully Installed and started cassandra"