#!/bin/bash

# downloand cassandra
sudo wget http://archive.apache.org/dist/cassandra/3.11.0/apache-cassandra-3.11.0-bin.tar.gz

# untar and place it /usr/local/cassandra
sudo tar -xvf apache-cassandra-3.11.0-bin.tar.gz && sudo mkdir -p /usr/local/cassandra && sudo mv apache-cassandra-3.11.0 /usr/local/cassandra/

#create mandatory directories
sudo mkdir -p /mnt/cassandra/data && sudo mkdir -p /mnt/cassandra/commitlog && sudo mkdir -p /mnt/cassandra/hints && sudo mkdir -p /mnt/cassandra/saved_caches && sudo chmod 777 -R /mnt/cassandra