#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
cassandra_nodes=`${THIS_DIR}/../read-vartf.sh ${1} cassandra`
echo `date` "cassandra_nodes=$cassandra_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

if [[ -z "$cassandra_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit -1;
fi

IFS=',' read -r -a nodesArr <<< "${cassandra_nodes}"

	for i in "${!nodesArr[@]}"
	do
		echo `date` "starting cassandra installation and drive mounting process into ${nodesArr[i]}"
	 	sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $THIS_DIR/install.sh)"
	    sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $THIS_DIR/disk-mount.sh)"
	done	
	
#   perform configuration
	sudo chmod +x $THIS_DIR/configure.sh
	$THIS_DIR/configure.sh ${cassandra_nodes} ${project} ${zone} ${env}