#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


for HOST in `cat instances.csv | awk '{if(NR>1)print}'`; do
     echo "*** $HOST"
     HOST_NAME=`echo $HOST|cut -d',' -f 1`
     PROJECT_ID=`echo $HOST|cut -d',' -f 3`
     ZONE=`echo $HOST|cut -d',' -f 4`
     USER=`echo $HOST|cut -d',' -f 5`

     # gcloud beta compute scp \
     #  --project  $PROJECT_ID \
     #  --internal-ip --zone  $ZONE   \
     #  ${GITREPO}cloudera-scripts.tar.gz  \
     #  $USER@$HOST_NAME:/tmp

      #bash execute_over_ssh.sh "cd /tmp && tar -xzf /tmp/cloudera-scripts.tar.gz"



   gcloud beta compute scp \
    --project  $PROJECT_ID \
    --internal-ip --zone  $ZONE   \
    ${GITREPO}disk_format.sh    \
    ${GITREPO}deploycloudera.py   \
    ${GITREPO}restartcloudera.py     \
    ${GITREPO}setup-master.sh     \
    ${GITREPO}setup-slave.sh    \
    ${GITREPO}cleanup-master.sh     \
    ${GITREPO}stopcloudera.py  \
    ${GITREPO}hosts.sh  \
    ${GITREPO}hostsentry.txt  \
    ${GITREPO}cdh-service-config.conf  \
    ${GITREPO}cm_db_ddl.sql \
    ${GITREPO}mysql-custom.cnf \
    $USER@$HOST_NAME:/tmp
 done
