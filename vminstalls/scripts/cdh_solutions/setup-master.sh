#!/bin/bash
# Usage : To prepare and setup Cloudera master
# Checked on OS : Ubuntu 14.04
# Dated : 25-Jun-2019
# Owner : DevOps
# Note : This should be run on the host on which the CM server is to run.

# Set up some vars.
config_file=/tmp/cdh-service-config.conf
cm_server_host=$(grep cm_host ${config_file} |head -1 | awk -F':' '{print $2}'|awk '{$1=$1};1')
deploy_external_cm_db=$(grep deploy_external_cm_db ${config_file} |head -1 | awk -F':' '{print $2}'|awk '{$1=$1};1')

# This should be set to whatever HIVE_HMS_HOST is set to in deploycloudera.py
#hive_metastore_host=$(grep hive.metastore.host ${config_file} | awk -F'=' '{print $2}')
#hive_metastore_password=$(grep hive.metastore.password ${config_file} | awk -F'=' '{print $2}')

# Prep Cloudera repo
sudo apt-get -y install wget
#wget http://archive.cloudera.com/cm5/redhat/6/x86_64/cm/cloudera-manager.repo
#sudo mv cloudera-manager.repo /etc/apt-get.repos.d/

# Turn off firewall
sudo service iptables stop

# Turn off SELINUX
echo 0 | sudo tee /selinux/enforce > /dev/null

# Set up NTP
#sudo apt-get -y install ntp
#udo chkconfig ntp on
#sudo ntpdate "${ntp_server}"
#sudo /etc/init.d/ntp start

# Set up python
#sudo rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
sudo apt-get -y install python-pip
sudo pip install cm_api
sudo pip install pyhocon

# Set up MySQL
#wget http://archive.cloudera.com/cdh5/cdh/5/hive-0.12.0-cdh5.0.0.tar.gz
#tar zxf hive-0.12.0-cdh5.0.0.tar.gz
#sudo apt-get -y install mysql-server expect
#sudo service mysql restart
##sudo /sbin/chkconfig mysqld on
#/bin/echo -e "\nY\n${hive_metastore_password}\n${hive_metastore_password}\nY\nn\nY\nY\n" | sudo tee /tmp/answers > /dev/null
#sudo cat /tmp/answers | sudo /usr/bin/mysql_secure_installation
#sudo rm /tmp/answers
#mysql -uroot -p"${hive_metastore_password}" --execute="CREATE DATABASE metastore; USE metastore; SOURCE ./hive-0.12.0-cdh5.0.0/scripts/metastore/upgrade/mysql/hive-schema-0.12.0.mysql.sql;"
#mysql -uroot -p"${hive_metastore_password}" --execute="CREATE USER 'hive'@'${hive_metastore_host}' IDENTIFIED BY '${hive_metastore_password}';"
#mysql -uroot -p"${hive_metastore_password}" --execute="REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'hive'@'${hive_metastore_host}';"
#mysql -uroot -p"${hive_metastore_password}" --execute="GRANT SELECT,INSERT,UPDATE,DELETE,LOCK TABLES,EXECUTE ON metastore.* TO 'hive'@'${hive_metastore_host}';"
#mysql -uroot -p"${hive_metastore_password}" --execute="FLUSH PRIVILEGES;"
#mysql -uroot -p"${hive_metastore_password}" --execute="create database oozie; grant all privileges on oozie.* to 'oozie'@'localhost' identified by '${hive_metastore_password}'; grant #all privileges on oozie.* to 'oozie'@'%' identified by '${hive_metastore_password}';"

# Make sure DNS is set up properly so all nodes can find all other nodes

# For master
sudo apt-get update -y
sudo apt-get -y install cloudera-manager-agent cloudera-manager-daemons cloudera-manager-server

if [ $deploy_external_cm_db = true ]; then
  echo "Installing local external MySQL DB for Cloudera"
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
  sudo apt-get -y install mysql-server libmysql-java
  sudo service mysql stop
  sudo cp /tmp/mysql-custom.cnf /etc/mysql/conf.d/
  sudo sed -i.bak -e"s%bind-address%#bind-address%" /etc/mysql/my.cnf
  sudo service mysql start
  sudo mysql -uroot -proot < /tmp/cm_db_ddl.sql
  # Work around to set the JAVA home for script.
  sudo ln -s /usr/lib/jvm/jdk1.8.0_201 /usr/lib/jvm/java-8-oracle
  sudo /usr/share/cmf/schema/scm_prepare_database.sh mysql -h $cm_server_host scm scm scm
  sudo unlink /usr/lib/jvm/java-8-oracle
  sudo rm /etc/cloudera-scm-server/db.mgmt.properties

else
    sudo apt-get -y install cloudera-manager-server-db-2
    sudo service cloudera-scm-server-db start
fi

sudo sed -i 's/INFO,LOGFILE/DEBUG,LOGFILE/g' /usr/sbin/cmf-server
sudo service cloudera-scm-server start
sudo sed -i.bak -e"s%server_host=localhost%server_host=${cm_server_host}%" /etc/cloudera-scm-agent/config.ini
sudo service cloudera-scm-agent start

# Prep work before calling the Cloudera provisioning script.
#firehostdbpassword=$(grep com.cloudera.cmf.ACTIVITYMONITOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
#navigatordbpassword=$(grep com.cloudera.cmf.NAVIGATOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
#headlampdbpassword=$(grep com.cloudera.cmf.REPORTSMANAGER.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')

# TEMP - read this from file.
firehostdbpassword=amon
navigatordbpassword=nav
headlampdbpassword=rman


# Sleep for a while to give the agents enough time to check in with the master.
# Or better yet, make a dependency so that the slave setup scripts don't start until now and the rest of this script doesn't finish until the slaves finish.
sleep_time=240
echo "Sleeping for ${sleep_time} seconds so managed cluster nodes can get set up."
sleep ${sleep_time}
echo "Done sleeping. Deploying cluster now."

# Execute script to deploy Cloudera cluster
#sudo python deploycloudera.py -i"${hive_metastore_password}" -f"${firehostdbpassword}" -n"${navigatordbpassword}" -r"${headlampdbpassword}"
sudo python /tmp/deploycloudera.py  -f"${firehostdbpassword}" -n"${navigatordbpassword}"

# Now stop the cluster gracefully if necessary; ie if all servers are automatically rebooted at the end of the provisioning process
#sudo python stopcloudera.py
