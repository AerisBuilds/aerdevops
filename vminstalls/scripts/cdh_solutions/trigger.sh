#!/bin/bash
cd "$(dirname "$0")"



if [[ $# -eq 0 ]] ; then
    echo -e "\033[31m"
    echo -e "\033[31m Missing path to variables file!"
    echo -e "\033[31m"
    echo -e "\033[31m Usage:"
    echo -e "\033[31m ./create.sh path_to_variables_file"
    echo -e "\033[31m"
    echo -e "\033[31m Example:"
    echo -e "\033[31m ./create.sh vars-product/product-env.tf"
    echo -e "\033[0m"
    exit 1
fi



# Load variables from vars file
echo "Variables file path: $1"
#export PROJECT=$(grep "^project=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
#export PURPOSE=$(grep "^project_name=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET=$(grep "^bucket=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')
export BUCKET_PURPOSE=$(grep "^cloudera_purpose=" $1 |sed 's/"//g' |awk -F'=' '{print $2}')

sudo chmod 777 /opt/terraform-server/scripts/aerdevops/vminstalls/scripts/cdh_solutions/startup.sh

#/opt/terraform-server/scripts/aerdevops/gcloud-templates/cdh/startup.sh gs://${BUCKET}/cloudera/default.tfstate
/opt/terraform-server/scripts/aerdevops/vminstalls/scripts/cdh_solutions/startup.sh  gs://${BUCKET}/${BUCKET_PURPOSE}/default.tfstate
