#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#


if [ "$#" -ne 1 ]; then
  echo "Usage: $0 GCS path of terraform output" >&2
  exit 1
fi
GCS_PATH="$1"

echo "Copying ENV Specific Config file"

sudo cp /opt/terraform-server/scripts/aerdevops/vminstalls/conf/conf-${DEPLOY_PRODUCT}/cdh/cdh-service-config-${DEPLOY_ENV}.conf cdh-service-config.conf

echo "##########Prepare the Host Details from Terrafrom State###########"
# Read the gcs file name from argument
#sh download-instance-metadata.sh gs://csb-ampsolutions-dev-tfstate-us-west1/cloudera/default.tfstate
sudo bash download-instance-metadata.sh $GCS_PATH

sudo python3.6 parse-terraform-state.py

# TODO find out the path in BSS server and use it instead of .
#tar -czvf cloudera-scripts.tar.gz .

#echo "##########entry for ths hostlist###########"
#sh hostlist.sh
echo "#########Copy the scripts to all nodes#####"
sudo bash copy-file.sh
echo "#######copied the scripts##################"
echo "###########configure hosts file############"
sudo bash hostconfig.sh
echo "#############Formating the disk############"
sudo bash format_disk.sh
echo "#########Formating completed###############"
echo "#########Installing the slave node##########"
sudo bash install_slave.sh
echo "#######Installing the master node###########"
sudo bash install_master.sh
echo "#######Installing the master node###########"
sudo bash register-dns.sh
echo "#####Installation completed#########"
