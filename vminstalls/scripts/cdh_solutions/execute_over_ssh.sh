#!/bin/bash

COMMAND="$1"
for HOST in `cat instances.csv | awk '{if(NR>1)print}'`; do
  echo "*** $HOST"
  HOST_NAME=`echo $HOST|cut -d',' -f 1`
  PROJECT_ID=`echo $HOST|cut -d',' -f 3`
  ZONE=`echo $HOST|cut -d',' -f 4`
  USER=`echo $HOST|cut -d',' -f 5`

  gcloud beta compute ssh $USER@$HOST_NAME --project  $PROJECT_ID   --internal-ip --zone  $ZONE --command "sudo $COMMAND"
done
