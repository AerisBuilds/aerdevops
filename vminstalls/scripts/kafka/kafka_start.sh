#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#Kafka configuration
sudo echo "Create kafka home directory"
sudo mkdir -p /usr/local/
#download kafka
sudo wget https://archive.apache.org/dist/kafka/0.9.0.0/kafka_2.11-0.9.0.0.tgz -O /opt/kafka_2.11-0.9.0.0.tgz
#Untar the kafka package
sudo tar -xvf /opt/kafka* -C /usr/local/
#Kafka home directory
kf_dir=/usr/local/kafka_2.11-0.9.0.0
#kafka log directory
sudo echo "create kafka log directory"
sudo mkdir -p $kf_dir/kafka_logs
#Replace the server.properties file with the template
sudo cp -r /tmp/template.server.properties $kf_dir/config/server.properties

##Provide the IP of the server
IP=$(ifconfig ens4 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
BID=B_PH
sudo sed -i "s/localhost/$IP/g" $kf_dir/config/server.properties
echo "replaced localhost"
sudo sed -i "s/brokerId/$BID/g" $kf_dir/config/server.properties
echo "replaced brokerId"
#
#Start the kafka server.
sudo -bE $kf_dir/bin/kafka-server-start.sh -daemon $kf_dir/config/server.properties &
sleep 5
echo "checking if kafka is running or not"
processes=$(ps -ef | grep kafka | wc -l)

if [ $processes -gt 0 ]; then
  echo "kafka started successfully"
else
  echo "kafka is not running. please run it manually"
fi
