#!/bin/bash
# shellcheck disable=SC1090
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
########List of Supervisor server#####
echo "kafka.sh : parameters passed to this script : $*"

brokerId=0
PROJECT_ID=$2
ZONE=$3
ZOOKEEPER_SERVERS=$5

IFS=',' read -r -a nodesArr <<<"${ZOOKEEPER_SERVERS}"
ZK_SERVERS=""
for server in "${nodesArr[@]}"; do
  ZK_SERVERS+="${server}:2181,"
done
ZK_SERVERS=${ZK_SERVERS::-1}
echo -e "zookeeper servers : ${ZK_SERVERS}"
THIS_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
PROP_FILE=/tmp/template.server.properties
sudo chmod +x $THIS_DIR/../../template/kafka/template.server.tpl

IFS=","

for KF in $1; do

echo "***Performing operations on $KF node"
sudo /snap/bin/gcloud compute scp $THIS_DIR/../../template/kafka/template.server.tpl $KF:$PROP_FILE --project $PROJECT_ID --internal-ip --zone $ZONE
sudo /snap/bin/gcloud compute ssh $KF --project $PROJECT_ID --internal-ip --zone $ZONE -- "sudo chmod 777 $PROP_FILE; sudo sed -i 's/ZOO_KEEPER_PLACEHOLDER/$ZK_SERVERS/g' $PROP_FILE"
sudo /snap/bin/gcloud compute ssh $KF --project $PROJECT_ID --internal-ip --zone $ZONE -- "$(cat $THIS_DIR/kafka_start.sh | sed s/B_PH/$brokerId/g)"
brokerId=$(($brokerId + 1))

done
