#!/bin/bash

THIS_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
echo "this_dir=$THIS_DIR"
sudo chmod +x ${THIS_DIR}/../read-vartf.sh
KAFKA_NODES=$(${THIS_DIR}/../read-vartf.sh ${1} 'kafka')
echo $(date) "kafka nodes=$KAFKA_NODES"
PROJECT=$(cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1)
echo $(date) "project=$PROJECT"
ZONE=$(cat ${1} | grep -w zone -m 1 | cut -d '=' -f2 | sed 's/\"//g' | head -1)
echo $(date) "zone=$ZONE"
ENV=$(cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1)
echo $(date) "env=$ENV"

ZOOKEEPER_SERVERS=$(${THIS_DIR}/../read-vartf.sh ${1} zookeeper)
echo $(date) "ZOOKEEPER_SERVERS=$ZOOKEEPER_SERVERS"

if [ -z "$KAFKA_NODES" ]; then
  echo $(date) "ERROR: KAFKA_NODES information not provided. Hence Exiting installation.. "
  exit 1
fi

if [ -z "$ZOOKEEPER_SERVERS" ]; then
  echo $(date) "ERROR: ZOOKEEPER_SERVERS information not provided. Hence Exiting installation.. "
  exit 1
fi

echo "calling kafka.sh"
sudo chmod +x $THIS_DIR/kafka.sh
sudo chmod +x $THIS_DIR/kafka_start.sh
#sh copy.sh $KAFKA_NODES $PROJECT $ZONE
$THIS_DIR/kafka.sh "$KAFKA_NODES" "$PROJECT" "$ZONE" "$ENV" "$ZOOKEEPER_SERVERS"

echo "done"
