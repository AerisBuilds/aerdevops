#!/bin/bash
# Usage : To prepare and setup Cloudera master
# Checked on OS : Ubuntu 14.04 and  Ubuntu 16.04.6 
# Dated : 25-Jun-2019
# Owner : DevOps
# Note : This should be run on the host on which the CM server is to run.

# Set up some vars
config_file=/tmp/cdh-service-config.conf
cm_server_host=$(grep cm.host ${config_file} | awk -F'=' '{print $2}')
#ntp_server=$(grep ntp.server ${config_file} | awk -F'=' '{print $2}')

# This should be set to whatever HIVE_HMS_HOST is set to in deploycloudera.py
#hive_metastore_host=$(grep hive.metastore.host ${config_file} | awk -F'=' '{print $2}')
#hive_metastore_password=$(grep hive.metastore.password ${config_file} | awk -F'=' '{print $2}')

# Prep Cloudera repo
sudo apt-get -y install wget

# Turn off firewall
#sudo service iptables stop

# Turn off SELINUX
echo 0 | sudo tee /selinux/enforce > /dev/null

# Set up NTP
#sudo apt-get -y install ntp
#sudo chkconfig ntp on
#sudo ntpdate "${ntp_server}"
#sudo /etc/init.d/ntp start

# Set up python
sudo apt-get -y install python-pip
sudo pip install cm_api
sudo pip install pyhocon


# Make sure DNS is set up properly so all nodes can find all other nodes

# For master
sudo apt-get update -y
sudo apt-get -y install cloudera-manager-agent cloudera-manager-daemons cloudera-manager-server

#If Cloudera Manager is configured by external database then enable the below line otherwise comment iit.
#deploy_external_cm_db=$(grep deploy_external_cm_db ${config_file} |head -1 | awk -F':' '{print $2}'|awk '{$1=$1};1')
#Set true if you have enabled the deploy_external_cm_db in the above line and it will use exteranl database for the cloudera manager.
if [ $deploy_external_cm_db = false ]; then
  echo "Installing local external MySQL DB for Cloudera"
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
  sudo apt-get -y install mysql-server libmysql-java
  sudo service mysql stop
  sudo cp /tmp/mysql-custom.cnf /etc/mysql/conf.d/
  sudo sed -i.bak -e"s%bind-address%#bind-address%" /etc/mysql/my.cnf
  sudo service mysql start
  sudo mysql -uroot -proot < /tmp/cm_db_ddl.sql
  # Work around to set the JAVA home for script.
  sudo ln -s /usr/lib/jvm/jdk1.8.0_201 /usr/lib/jvm/java-8-oracle
  sudo /usr/share/cmf/schema/scm_prepare_database.sh mysql -h $cm_server_host scm scm scm
  sudo unlink /usr/lib/jvm/java-8-oracle
  sudo rm /etc/cloudera-scm-server/db.mgmt.properties

else
    sudo apt-get -y install cloudera-manager-server-db-2
    sudo service cloudera-scm-server-db start
fi
#
#Mysql database setup for hive metastore_db
#Uncomment the below line if you need the Hive metastore otherwise comment it.
deploy_metastore_db=$(grep deploy_metastore_db ${config_file} |head -1 | awk -F':' '{print $2}'|awk '{$1=$1};1')
#Set true if you need Hive Metastore for the hive server and make sure you have uncomment in th abouve line
if [ $deploy_metastore_db = true ]; then
  root_password=root
  hive_metastore_password=hivepass
  sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${root_password}"
  sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${root_password}"
  sudo DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server
  mysql -uroot -p"${root_password}" --execute="CREATE DATABASE metastore; USE metastore;"
  mysql -uroot -p"${root_password}" --execute="grant all privileges on metastore.* to 'hive'@'localhost' identified by '${hive_metastore_password}';"
  mysql -uroot -p"${root_password}" --execute="GRANT ALL ON metastore.* TO 'hive'@'${hive_metastore_host}' IDENTIFIED BY '${hive_metastore_password}';"
  mysql -uroot -p"${root_password}" --execute="GRANT ALL ON metastore.* TO 'hive'@'%' IDENTIFIED BY '${hive_metastore_password}';"
  mysql -uroot -p"${root_password}" --execute="GRANT SELECT,INSERT,UPDATE,DELETE,LOCK TABLES,EXECUTE ON metastore.* TO 'hive'@'${hive_metastore_host}';"
  mysql -uroot -p"${root_password}" --execute="FLUSH PRIVILEGES;"

else
	echo "none"
fi


sudo sed -i 's/INFO,LOGFILE/DEBUG,LOGFILE/g' /usr/sbin/cmf-server
sudo service cloudera-scm-server start
sudo sed -i.bak -e"s%server_host=localhost%server_host=${cm_server_host}%" /etc/cloudera-scm-agent/config.ini
sudo service cloudera-scm-agent start
sudo apt-get install dos2unix zip unzip -y

# Prep work before calling the Cloudera provisioning script.
#firehostdbpassword=$(grep com.cloudera.cmf.ACTIVITYMONITOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
#navigatordbpassword=$(grep com.cloudera.cmf.NAVIGATOR.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')
#headlampdbpassword=$(grep com.cloudera.cmf.REPORTSMANAGER.db.password /etc/cloudera-scm-server/db.mgmt.properties | awk -F'=' '{print $2}')

# TEMP - read this from file.
#firehostdbpassword=amon
#navigatordbpassword=nav
#headlampdbpassword=rman
firehostdbpassword=qCTTgdj4w2
navigatordbpassword=VggUNjHRMs
headlampdbpassword=0XBGiLktUG

# Sleep for a while to give the agents enough time to check in with the master.
# Or better yet, make a dependency so that the slave setup scripts don't start until now and the rest of this script doesn't finish until the slaves finish.
sleep_time=200
echo "Sleeping for ${sleep_time} seconds so managed cluster nodes can get set up."
sleep ${sleep_time}
echo "Done sleeping. Deploying cluster now."

# Execute script to deploy Cloudera cluster as per the requirement of the service
#For connectivty unable the below line.
sudo python /tmp/deploycloudera.py -i"${hive_metastore_password}" -f"${firehostdbpassword}" -n"${navigatordbpassword}" -r"${headlampdbpassword}"
#For solutions uncomment the below line
#sudo python /tmp/deploycloudera.py  -f"${firehostdbpassword}" -n"${navigatordbpassword}"

# Now stop the cluster gracefully if necessary; ie if all servers are automatically rebooted at the end of the provisioning process
#sudo python /tmp/stopcloudera.py

# Now start the cluster gracefully if necessary; ie if all servers are automatically rebooted at the end of the provisioning process
#sudo python /tmp/startcloudera.py
