#!/bin/bash
# Usage : To prepare and setup Cloudera Slave nodes
# Checked on OS : Ubuntu 14.04 and  Ubuntu 16.04.6 
# Dated : 25-Jun-2018
# Owner : DevOps
# Note : This should be run on the host on which the CM slaves roles are running.

# This is to be run on all nodes that will be managed by CM. This includes all Hadoop nodes,
# as well as nodes that run CM management services (if those aren't running on the CM server node).

# Set up some vars
config_file=/tmp/cdh-service-config.conf
cm_server_host=$(grep cm_host ${config_file} |head -1 | awk -F':' '{print $2}'|awk '{$1=$1};1')
#ntp_server=$(grep ntp.server ${config_file} | awk -F'=' '{print $2}')

# Prep Cloudera repo
sudo apt-get -y install wget
#apt-get install dos2unix -y
#wget http://archive.cloudera.com/cm5/redhat/6/x86_64/cm/cloudera-manager.repo
#sudo mv cloudera-manager.repo /etc/apt-get.repos.d/

# Turn off firewall
#sudo service iptables stop

# Turn off SELINUX
#echo 0 | tee /selinux/enforce > /dev/null

#Set up NTP
#sudo apt-get -y install ntp
#sudo chkconfig ntp on
#sudo ntpdate "${ntp_server}"
#sudo /etc/init.d/ntp start

# Make the mysql driver available to hive
#sudo apt-get -y install mysql-connector-java
##sudo mkdir -p /usr/lib/hive/lib/
#sudo ln -s /usr/share/java/mysql-connector-java.jar /usr/lib/hive/lib/mysql-connector-java.jar
apt-get install libmysql-java -y

# Make sure DNS is set up properly so all nodes can find all other nodes

# For slaves
sudo apt-get update -y
sudo apt-get -y install cloudera-manager-agent cloudera-manager-daemons
sudo sed -i.bak -e"s%server_host=localhost%server_host=${cm_server_host}%" /etc/cloudera-scm-agent/config.ini

# Sleep a while so the CM server can come up
sleep_time=10
echo "Sleeping for ${sleep_time} seconds so CM server can start up."
sleep ${sleep_time}
echo "Done sleeping. Starting CM agent now."

sudo service cloudera-scm-agent start
