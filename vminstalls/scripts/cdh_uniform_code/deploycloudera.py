#!/usr/bin/env python
# Licensed to Cloudera, Inc. under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  Cloudera, Inc. licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Deploys a CDH cluster and configures CM management services.

import socket, sys, time, ConfigParser, csv, pprint, urllib2
from subprocess import Popen, PIPE, STDOUT
from math import log as ln
from cm_api.api_client import ApiResource
from cm_api.endpoints.services import ApiService
from cm_api.endpoints.services import ApiServiceSetupInfo
from optparse import OptionParser
from pyhocon import ConfigFactory
from pyhocon import HOCONConverter
import traceback


### Do some initial prep work ###

#Use the following argument parser as per your requirement for Connectivity we use Hive metastore so uncomment for the connectivity. However for solutions Hive doesn't require keep comment the Hive Metastore parser.
# Prep command line argument parser
parser = OptionParser()
parser.add_option('-i', '--hmsdbpassword', dest='HIVE_METASTORE_PASSWORD', default='', help='The password for the Hive MetaStore DB, which should have DB name "metastore" and user "???".')
parser.add_option('-f', '--firehosedbpassword', dest='FIREHOSE_DATABASE_PASSWORD', default='', help='The password for the Firehose (Activity Monitor) DB, which should have DB name "amon" and user "amon".')
parser.add_option('-n', '--navigatordbpassword', dest='NAVIGATOR_DATABASE_PASSWORD', default='', help='The password for the Navigator DB, which should have DB name "nav" and user "nav".')
parser.add_option('-r', '--headlampdbpassword', dest='HEADLAMP_DATABASE_PASSWORD', default='', help='The password for the Headlamp (Report Manager) DB, which should have DB name "rman" and user "rman".')
(options, args) = parser.parse_args()

# Prep pretty printer ###
PRETTY_PRINT = pprint.PrettyPrinter(indent=4)


CONF=ConfigFactory.parse_file('/tmp/cdh-service-config.conf')
print("############################################################")
print(HOCONConverter.convert(CONF, 'hocon'))
print("############################################################")

### Set up environment-specific vars ###

# This is the host that the Cloudera Manager server is running on
#CM_HOST = CONFIG.get("CM", "cm.host")
CM_HOST = CONF.cloudera_manager.host

# Hadoop config parameters
#Uncomment the Hive Metastore password options if not using Hive. For solutions comment the line and for connectivity uncomment.
# This is the password for the Hive Metastore database
HIVE_METASTORE_PASSWORD = options.HIVE_METASTORE_PASSWORD

# These are passwords for the embedded postgres database that the Cloudera Manager management services use
FIREHOSE_DATABASE_PASSWORD = options.FIREHOSE_DATABASE_PASSWORD
NAVIGATOR_DATABASE_PASSWORD = options.NAVIGATOR_DATABASE_PASSWORD
HEADLAMP_DATABASE_PASSWORD = options.HEADLAMP_DATABASE_PASSWORD


### CM Definition ###
CMD_TIMEOUT = 180

#### Cluster Definition #####

### Parcels ###
PARCEL_VERSION = CONF.cdh_parcel_version
if PARCEL_VERSION.lower() == "latest":
   # Get list of parcels from the cloudera repo to see what the latest version is. Then to parse:
   # find first item that starts with CDH-
   # strip off leading CDH-
   # strip off everything after the last - in that item, including the -
   LATEST_PARCEL_URL = 'http://archive.cloudera.com/cdh5/parcels/latest/'
   PARCEL_PREFIX = 'CDH-'
   dir_list = urllib2.urlopen(LATEST_PARCEL_URL).read()
   dir_list = dir_list[dir_list.index(PARCEL_PREFIX)+len(PARCEL_PREFIX):]
   dir_list = dir_list[:dir_list.index('"')]
   PARCEL_VERSION = dir_list[:dir_list.rfind('-')]


PARCELS = [
   { 'name' : "CDH", 'version' : PARCEL_VERSION }
]
#The below two line is for the additional parcels. Uncomment the below two line if any additional parcels is using. For solutions uncomment the belwo two line and for connectivity comment as not using any additonal parcels packages.
#for p in CONF.cloudera_manager.additional_parcels:
#    PARCELS.append({'name': p.name, 'version': p.version})


### Hive ###
#HIVE_SERVICE_NAME = "HIVE"
#HIVE_SERVICE_CONFIG = {
#  'hive_metastore_database_host': CM_HOST,
#  'hive_metastore_database_name': 'metastore',
#  'hive_metastore_database_password': HIVE_METASTORE_PASSWORD,
#  'hive_metastore_database_port': 3306,
#  'hive_metastore_database_type': 'mysql',
#  'mapreduce_yarn_service': MAPRED_SERVICE_NAME,
#  'zookeeper_service': ZOOKEEPER_SERVICE_NAME,
#  'mapreduce_yarn_service': YARN_SERVICE_NAME,
#}
#HIVE_HMS_HOST = CLUSTER_HOSTS[0]
#HIVE_HMS_CONFIG = {
#  'hive_metastore_java_heapsize': 85306784,
#}
#HIVE_HS2_HOST = CLUSTER_HOSTS[0]
#HIVE_HS2_CONFIG = { }
#HIVE_WHC_HOST = CLUSTER_HOSTS[0]
#HIVE_WHC_CONFIG = { }
#HIVE_GW_HOSTS = list(CLUSTER_HOSTS)
#HIVE_GW_CONFIG = { }
#
#### Impala ###
#IMPALA_SERVICE_NAME = "IMPALA"
#IMPALA_SERVICE_CONFIG = {
#  'hdfs_service': HDFS_SERVICE_NAME,
#  'hbase_service': HBASE_SERVICE_NAME,
#  'hive_service': HIVE_SERVICE_NAME,
#}
#IMPALA_SS_HOST = CLUSTER_HOSTS[0]
#IMPALA_SS_CONFIG = { }
#IMPALA_CS_HOST = CLUSTER_HOSTS[0]
#IMPALA_CS_CONFIG = { }
#IMPALA_ID_HOSTS = list(CLUSTER_HOSTS)
#IMPALA_ID_CONFIG = { }

### Search ###
#SEARCH_SERVICE_NAME = "SEARCH"
#SEARCH_SERVICE_CONFIG = {
#  'hdfs_service': HDFS_SERVICE_NAME,
#  'zookeeper_service': ZOOKEEPER_SERVICE_NAME,
#}
#SEARCH_SOLR_HOST = CLUSTER_HOSTS[0]
#SEARCH_SOLR_CONFIG = { }
#SEARCH_GW_HOSTS = list(CLUSTER_HOSTS)
#SEARCH_GW_CONFIG = { }
#
#### Flume ###
#FLUME_SERVICE_NAME = "FLUME"
#FLUME_SERVICE_CONFIG = {
#  'hdfs_service': HDFS_SERVICE_NAME,
#  'hbase_service': HBASE_SERVICE_NAME,
#  'solr_service': SEARCH_SERVICE_NAME,
#}
#FLUME_AGENT_HOSTS = list(CLUSTER_HOSTS)
#FLUME_AGENT_CONFIG = { }
#
#### Oozie ###
#OOZIE_SERVICE_NAME = "OOZIE"
#OOZIE_SERVICE_CONFIG = {
#  'mapreduce_yarn_service': YARN_SERVICE_NAME,
#}
#OOZIE_SERVER_HOST = CLUSTER_HOSTS[0]
#OOZIE_SERVER_CONFIG = {
#   'oozie_java_heapsize': 207881018,
#   'oozie_database_host': CM_HOST,
#   'oozie_database_name': 'oozie',
#   'oozie_database_password': HIVE_METASTORE_PASSWORD,
#   'oozie_database_type': 'mysql',
#   'oozie_database_user': 'oozie',
#}

### Sqoop ###
#SQOOP_SERVICE_NAME = "SQOOP"
#SQOOP_SERVICE_CONFIG = {
#  'mapreduce_yarn_service': YARN_SERVICE_NAME,
#}
#SQOOP_SERVER_HOST = CLUSTER_HOSTS[0]
#SQOOP_SERVER_CONFIG = {
#   'sqoop_java_heapsize': 207881018,
#}

#### HUE ###
#HUE_SERVICE_NAME = "HUE"
#HUE_SERVICE_CONFIG = {
#  'hive_service': HIVE_SERVICE_NAME,
#  'hbase_service': HBASE_SERVICE_NAME,
#  'impala_service': IMPALA_SERVICE_NAME,
#  'oozie_service': OOZIE_SERVICE_NAME,
#  'sqoop_service': SQOOP_SERVICE_NAME,
#  'hue_webhdfs': HDFS_SERVICE_NAME + "-" + HDFS_NAMENODE_SERVICE_NAME,
#  'hue_hbase_thrift': HBASE_SERVICE_NAME + "-" + HBASE_THRIFTSERVER_SERVICE_NAME,
#}
#HUE_SERVER_HOST = CLUSTER_HOSTS[0]
#HUE_SERVER_CONFIG = {
#   'hue_server_hue_safety_valve': '[search]\r\n## URL of the Solr Server\r\nsolr_url=http://' + SEARCH_SOLR_HOST + ':8983/solr',
#}
#HUE_KTR_HOST = CLUSTER_HOSTS[0]
#HUE_KTR_CONFIG = { }
#
### Accumulo ###
#ACCUMULO_SERVICE_NAME = "accumulo"
#ACCUMULO_SERVICE_CONFIG = {
##   'accumulo_instance_name' : "accumulo",
#  'mapreduce_service' : MAPRED_SERVICE_NAME,
#  'zookeeper_service' : ZOOKEEPER_SERVICE_NAME,
#}
#ACCUMULO_MASTER_HOSTS = [ CLUSTER_HOSTS[0] ]
#ACCUMULO_MASTER_CONFIG = { }
#3ACCUMULO_TRACER_HOSTS = list(CLUSTER_HOSTS)
#3ACCUMULO_TRACER_CONFIG = { }
#ACCUMULO_TSERVER_HOSTS = list(CLUSTER_HOSTS)
#ACCUMULO_TSERVER_CONFIG = { }
#ACCUMULO_LOGGER_HOSTS = list(CLUSTER_HOSTS)
#ACCUMULO_LOGGER_CONFIG = { }
#ACCUMULO_GATEWAY_HOSTS = list(CLUSTER_HOSTS)
#ACCUMULO_GATEWAY_CONFIG = { }
#ACCUMULO_MONITOR_HOST = CLUSTER_HOSTS[0]
#ACCUMULO_MONITOR_CONFIG = { }
#ACCUMULO_GC_HOST = CLUSTER_HOSTS[0]
#ACCUMULO_GC_CONFIG = { }

#Jins will mentions the details of the part what the following code will do?.
##################################################		
#def translate_config(conf):		
#    output={}		
#    for key in conf:		
#        output[key.strip('\"')]=conf.get(key)		
#    return output		
###################################################
### Deployment/Initialization Functions ###

# Creates the cluster and adds hosts
def init_cluster(api, cluster_name, cdh_version, hosts):
   cluster = api.create_cluster(cluster_name, cdh_version)
   # Add the CM host to the list of hosts to add in the cluster so it can run the management services
   all_hosts = list(hosts)
   cluster.add_hosts(all_hosts)
   return cluster

# Downloads and distributes parcels
def deploy_parcels(cluster, parcels):
   for parcel in parcels:
      p = cluster.get_parcel(parcel['name'], parcel['version'])
      p.start_download()
      while True:
         p = cluster.get_parcel(parcel['name'], parcel['version'])
         if p.stage == "DOWNLOADED":
            break
         if p.state.errors:
            raise Exception(str(p.state.errors))
         print "Downloading %s: %s / %s" % (parcel['name'], p.state.progress, p.state.totalProgress)
         time.sleep(20)
      print "Downloaded %s" % (parcel['name'])
      p.start_distribution()
      while True:
         p = cluster.get_parcel(parcel['name'], parcel['version'])
         if p.stage == "DISTRIBUTED":
            break
         if p.state.errors:
            raise Exception(str(p.state.errors))
         print "Distributing %s: %s / %s" % (parcel['name'], p.state.progress, p.state.totalProgress)
         time.sleep(20)
      print "Distributed %s" % (parcel['name'])
      p.activate()

# Deploys management services. Not all of these are currently turned on because some require a license.
# This function also starts the services.
def deploy_management(manager, mgmt_service):
   mgmt = manager.create_mgmt_service(ApiServiceSetupInfo())

   # create roles. Note that host id may be different from host name (especially in CM 5). Look it it up in /api/v5/hosts
   mgmt.create_role(mgmt_service.amon.role_name + "-1", "ACTIVITYMONITOR", CM_HOST)
   mgmt.create_role(mgmt_service.alert_publisher.role_name + "-1", "ALERTPUBLISHER", CM_HOST)
   mgmt.create_role(mgmt_service.event_server.role_name + "-1", "EVENTSERVER", CM_HOST)
   mgmt.create_role(mgmt_service.host_monitor.role_name + "-1", "HOSTMONITOR", CM_HOST)
   mgmt.create_role(mgmt_service.service_monitor.role_name + "-1", "SERVICEMONITOR", CM_HOST)
   #mgmt.create_role(nav_role_name + "-1", "NAVIGATOR", CM_HOST)
#   mgmt.create_role(navms_role_name + "-1", "NAVIGATORMETADATASERVER", CM_HOST)
   #mgmt.create_role(rman_role_name + "-1", "REPORTSMANAGER", CM_HOST)

   # now configure each role
   for group in mgmt.get_all_role_config_groups():
       if group.roleType == "ACTIVITYMONITOR":
           amon_conf=mgmt_service.amon.role_config
           amon_conf['firehose_database_password']=CONF.activitymonitor.db.password
           group.update_config(amon_conf)
       elif group.roleType == "ALERTPUBLISHER":
           group.update_config(mgmt_service.alert_publisher.role_config)
       elif group.roleType == "EVENTSERVER":
           group.update_config(mgmt_service.event_server.role_config)
       elif group.roleType == "HOSTMONITOR":
           group.update_config(mgmt_service.host_monitor.role_config)
       elif group.roleType == "SERVICEMONITOR":
           group.update_config(mgmt_service.service_monitor.role_config)
#       elif group.roleType == "NAVIGATOR":
#           group.update_config(nav_role_conf)
#      elif group.roleType == "NAVIGATORMETADATASERVER":
#           group.update_config(navms_role_conf)
#       elif group.roleType == "REPORTSMANAGER":
#           group.update_config(rman_role_conf)

   # now start the management service
   mgmt.start().wait()

   return mgmt


# Deploys and initializes ZooKeeper
def deploy_zookeeper(cluster, zk_service):
   zk = cluster.create_service(zk_service.service_name, "ZOOKEEPER")
   zk.update_config(zk_service.service_config)

   zk_id = 0
   zk_role_conf=zk_service.role_config
   for zk_host in zk_service.hosts:
      zk_id += 1
      zk_role_conf['serverId'] = zk_id
      role = zk.create_role(zk_service.service_name + "-" + str(zk_id), "SERVER", zk_host)
      role.update_config(zk_role_conf)

   zk.init_zookeeper()

   return zk


# Deploys HDFS - NN, DNs, SNN, gateways.
# This does not yet support HA yet.
def deploy_hdfs(cluster, conf):
   hdfs_service_name=conf.service_name
   hdfs_service = cluster.create_service(hdfs_service_name, "HDFS")
   hdfs_service.update_config(conf.service_config)

   nn_role_group = hdfs_service.get_role_config_group("{0}-NAMENODE-BASE".format(hdfs_service_name))
   nn_role_group.update_config(conf.namenode.role_config)
   nn_service_pattern = "{0}-" + conf.namenode.service_name
   hdfs_service.create_role(nn_service_pattern.format(hdfs_service_name), "NAMENODE", conf.namenode.host)

   #snn_role_group = hdfs_service.get_role_config_group("{0}-NAMENODE-BASE".format(hdfs_service_name))
   #snn_role_group.update_config(hdfs_snn_config)
   hdfs_service.create_role("{0}-snn".format(hdfs_service_name), "NAMENODE", conf.secondary_namenode.host)
   #TODO: Kapil, Why Secondary name node config is commented ?
   # to make hdfs in ha, we need two namenode - one on namenode.host and other on secondary_namenode.host. Enable method deploy_hdfs_ha

   jn_role_group = hdfs_service.get_role_config_group("{0}-JOURNALNODE-BASE".format(hdfs_service_name))
   jn_role_group.update_config(conf.journalnode.role_config)
   dn_role_group = hdfs_service.get_role_config_group("{0}-DATANODE-BASE".format(hdfs_service_name))
   dn_role_group.update_config(conf.datanodes.role_config)

   gw_role_group = hdfs_service.get_role_config_group("{0}-GATEWAY-BASE".format(hdfs_service_name))
   gw_role_group.update_config(conf.hdfs_gateway.role_config)

   journalnode = 0
   for host in conf.journalnode.hosts:
      journalnode += 1
      hdfs_service.create_role("{0}-jn-".format(hdfs_service_name) + str(journalnode), "JOURNALNODE", host)

   datanode = 0
   for host in conf.datanodes.hosts:
      datanode += 1
      hdfs_service.create_role("{0}-dn-".format(hdfs_service_name) + str(datanode), "DATANODE", host)

   gateway = 0
   for host in conf.hdfs_gateway.hosts:
      gateway += 1
      hdfs_service.create_role("{0}-gw-".format(hdfs_service_name) + str(gateway), "GATEWAY", host)

   return hdfs_service

#copy of deploy hdfs
# def deploy_hdfs_copy(cluster, hdfs_service_name, hdfs_config, hdfs_nn_service_name, hdfs_nn_host, hdfs_nn_config, hdfs_snn_host, hdfs_snn_config, hdfs_dn_hosts, hdfs_dn_config, hdfs_gw_hosts, hdfs_gw_config):
#    hdfs_service = cluster.create_service(hdfs_service_name, "HDFS")
#    hdfs_service.update_config(hdfs_config)

#    nn_role_group = hdfs_service.get_role_config_group("{0}-NAMENODE-BASE".format(hdfs_service_name))
#    nn_role_group.update_config(hdfs_nn_config)
#    nn_service_pattern = "{0}-" + hdfs_nn_service_name
#    hdfs_service.create_role(nn_service_pattern.format(hdfs_service_name), "NAMENODE", hdfs_nn_host)

#    snn_role_group = hdfs_service.get_role_config_group("{0}-SECONDARYNAMENODE-BASE".format(hdfs_service_name))
#    snn_role_group.update_config(hdfs_snn_config)
#    hdfs_service.create_role("{0}-snn".format(hdfs_service_name), "SECONDARYNAMENODE", hdfs_snn_host)

#    dn_role_group = hdfs_service.get_role_config_group("{0}-DATANODE-BASE".format(hdfs_service_name))
#    dn_role_group.update_config(hdfs_dn_config)

#    gw_role_group = hdfs_service.get_role_config_group("{0}-GATEWAY-BASE".format(hdfs_service_name))
#    gw_role_group.update_config(hdfs_gw_config)

#    datanode = 0
#    for host in hdfs_dn_hosts:
#       datanode += 1
#       hdfs_service.create_role("{0}-dn-".format(hdfs_service_name) + str(datanode), "DATANODE", host)

#    gateway = 0
#    for host in hdfs_gw_hosts:
#       gateway += 1
#       hdfs_service.create_role("{0}-gw-".format(hdfs_service_name) + str(gateway), "GATEWAY", host)

#    return hdfs_service



# enable_hdfs_ha
def enable_hdfs_ha(cluster, hdfs_service, timeout):
   for role in hdfs_service.get_all_roles():
    if role.name == 'HDFS-nn':
     active_nn_name = role.name
    elif role.name == 'HDFS-snn':
     standby_name = role.name

   print "active nn --> "+  active_nn_name
   print "secondary nn ---> "+ standby_name
   active_shared_path ="/hadoop" + "/namenode"
   standby_shared_path ="/hadoop" +"/namesecondary"
   nameservice ="nameservice1"

   cmd = hdfs_service.enable_hdfs_ha(active_nn_name, active_shared_path, standby_name, standby_shared_path, nameservice)
   if not cmd.wait(timeout).success:
     print "WARNING: Failed to namenode HA, attempting to continue with the setup"

# Initializes HDFS - format the file system
def init_hdfs(hdfs_service, hdfs_name, timeout):
   cmd = hdfs_service.format_hdfs("{0}-nn".format(hdfs_name))[0]
   if not cmd.wait(timeout).success:
      print "WARNING: Failed to format HDFS, attempting to continue with the setup"

# Deploys MapReduce - JT, TTs, gateways.
# This does not yet support HA yet.
# This shouldn't be run if YARN is deployed.
def deploy_mapreduce(cluster, mapred_service_name, mapred_service_config, mapred_jt_host, mapred_jt_config, mapred_tt_hosts, mapred_tt_config, mapred_gw_hosts, mapred_gw_config ):
   mapred_service = cluster.create_service(mapred_service_name, "MAPREDUCE")
   mapred_service.update_config(mapred_service_config)

   jt = mapred_service.get_role_config_group("{0}-JOBTRACKER-BASE".format(mapred_service_name))
   jt.update_config(mapred_jt_config)
   mapred_service.create_role("{0}-jt".format(mapred_service_name), "JOBTRACKER", mapred_jt_host)

   tt = mapred_service.get_role_config_group("{0}-TASKTRACKER-BASE".format(mapred_service_name))
   tt.update_config(mapred_tt_config)

   gw = mapred_service.get_role_config_group("{0}-GATEWAY-BASE".format(mapred_service_name))
   gw.update_config(mapred_gw_config)

   tasktracker = 0
   for host in mapred_tt_hosts:
      tasktracker += 1
      mapred_service.create_role("{0}-tt-".format(mapred_service_name) + str(tasktracker), "TASKTRACKER", host)

   gateway = 0
   for host in mapred_gw_hosts:
      gateway += 1
      mapred_service.create_role("{0}-gw-".format(mapred_service_name) + str(gateway), "GATEWAY", host)

   return mapred_service


# Deploys YARN - RM, JobHistoryServer, NMs, gateways
# This shouldn't be run if MapReduce is deployed.
def deploy_yarn(cluster, conf):
   yarn_service_name=conf.service_name
   yarn_service = cluster.create_service(yarn_service_name, "YARN")
   yarn_service.update_config(conf.service_config)

   rm = yarn_service.get_role_config_group("{0}-RESOURCEMANAGER-BASE".format(yarn_service_name))
   rm.update_config(conf.resource_manager.role_config)
   yarn_service.create_role("{0}-rm".format(yarn_service_name), "RESOURCEMANAGER", conf.resource_manager.host)

   jhs = yarn_service.get_role_config_group("{0}-JOBHISTORY-BASE".format(yarn_service_name))
   jhs.update_config(conf.job_history_server.role_config)
   yarn_service.create_role("{0}-jhs".format(yarn_service_name), "JOBHISTORY", conf.job_history_server.host)

   nm = yarn_service.get_role_config_group("{0}-NODEMANAGER-BASE".format(yarn_service_name))
   nm.update_config(conf.node_manager.role_config)

   nodemanager = 0
   for host in conf.node_manager.hosts:
      nodemanager += 1
      yarn_service.create_role("{0}-nm-".format(yarn_service_name) + str(nodemanager), "NODEMANAGER", host)

   gw = yarn_service.get_role_config_group("{0}-GATEWAY-BASE".format(yarn_service_name))
   gw.update_config(conf.gateway.role_config)

   gateway = 0
   for host in conf.gateway.hosts:
      gateway += 1
      yarn_service.create_role("{0}-gw-".format(yarn_service_name) + str(gateway), "GATEWAY", host)

   #TODO need api version 6 for these, but I think they are done automatically?
   #yarn_service.create_yarn_job_history_dir()
   #yarn_service.create_yarn_node_manager_remote_app_log_dir()

   return yarn_service


# Deploys spark - master, workers, gateways
def deploy_spark(cluster, conf):
   spark_service_name = conf.service_name
   spark_service = cluster.create_service(spark_service_name, "SPARK")
   spark_service.update_config(conf.service_config)

   sm = spark_service.get_role_config_group("{0}-SPARK_MASTER-BASE".format(spark_service_name))
   sm.update_config(conf.master.role_config)
   spark_service.create_role("{0}-sm".format(spark_service_name), "SPARK_MASTER", conf.master.host)

   sw = spark_service.get_role_config_group("{0}-SPARK_WORKER-BASE".format(spark_service_name))
   sw.update_config(conf.workers.role_config)

   worker = 0
   for host in conf.workers.hosts:
      worker += 1
      spark_service.create_role("{0}-sw-".format(spark_service_name) + str(worker), "SPARK_WORKER", host)

   gw = spark_service.get_role_config_group("{0}-GATEWAY-BASE".format(spark_service_name))
   gw.update_config(spark.gateway.role_config)

   gateway = 0
   for host in conf.gateway.hosts:
      gateway += 1
      spark_service.create_role("{0}-gw-".format(spark_service_name) + str(gateway), "GATEWAY", host)

   #TODO - CreateSparkUserDirCommand, SparkUploadJarServiceCommand???

   return spark_service


# Deploys HBase - HMaster, RSes, HBase Thrift Server, gateways
def deploy_hbase(cluster, conf):
   hbase_service_name = conf.service_name
   hbase_service = cluster.create_service(hbase_service_name, "HBASE")
   hbase_service.update_config(conf.service_config)

   hm = hbase_service.get_role_config_group("{0}-MASTER-BASE".format(hbase_service_name))
   hm.update_config(conf.master.role_config)
   hbase_service.create_role("{0}-hm".format(hbase_service_name), "MASTER", conf.master.host)
   hbase_service.create_role("{0}-hmb".format(hbase_service_name), "MASTER", conf.backup_master.host)

#   hmb_service_pattern = "{0}-" + hbase_service_name
#   hbase_service.create_role(hmb_service_pattern.format(hbase_service_name), "MASTER", hbase_hm_host)

   rs = hbase_service.get_role_config_group("{0}-REGIONSERVER-BASE".format(hbase_service_name))
   rs.update_config(conf.region_servers.role_config)

   ts = hbase_service.get_role_config_group("{0}-HBASETHRIFTSERVER-BASE".format(hbase_service_name))
   ts.update_config(conf.thriftserver.role_config)
   ts_name_pattern = "{0}-" + conf.thriftserver.name
   hbase_service.create_role(ts_name_pattern.format(hbase_service_name), "HBASETHRIFTSERVER", conf.thriftserver.host)

   gw = hbase_service.get_role_config_group("{0}-GATEWAY-BASE".format(hbase_service_name))
   gw.update_config(conf.gateway.role_config)

   regionserver = 0
   for host in conf.region_servers.hosts:
      regionserver += 1
      hbase_service.create_role("{0}-rs-".format(hbase_service_name) + str(regionserver), "REGIONSERVER", host)

   gateway = 0
   for host in conf.gateway.hosts:
      gateway += 1
      hbase_service.create_role("{0}-gw-".format(hbase_service_name) + str(gateway), "GATEWAY", host)

   hbase_service.create_hbase_root()

   return hbase_service


# Deploys Hive - hive metastore, hiveserver2, webhcat, gateways
def deploy_hive(cluster, conf):
   hive_service_name = conf.service_name
   hive_service = cluster.create_service(hive_service_name, "HIVE")
   hive_service.update_config(conf.service_config)

   hms = hive_service.get_role_config_group("{0}-HIVEMETASTORE-BASE".format(hive_service_name))
   hms.update_config(conf.metastore.role_config)
   hive_service.create_role("{0}-hms".format(hive_service_name), "HIVEMETASTORE", conf.metastore.host)

   hs2 = hive_service.get_role_config_group("{0}-HIVESERVER2-BASE".format(hive_service_name))
   hs2.update_config(conf.hiveserver2.role_config)
   hive_service.create_role("{0}-hs2".format(hive_service_name), "HIVESERVER2", conf.hiveserver2.host)

   whc = hive_service.get_role_config_group("{0}-WEBHCAT-BASE".format(hive_service_name))
   whc.update_config(conf.webhcat.role_config)
   hive_service.create_role("{0}-whc".format(hive_service_name), "WEBHCAT", conf.webhcat.host)

   gw = hive_service.get_role_config_group("{0}-GATEWAY-BASE".format(hive_service_name))
   gw.update_config(conf.gateway.role_config)

   gateway = 0
   for host in conf.gateway.hosts:
      gateway += 1
      hive_service.create_role("{0}-gw-".format(hive_service_name) + str(gateway), "GATEWAY", host)

   return hive_service


# Initialized hive
def init_hive(hive_service):
   hive_service.create_hive_metastore_database()
   hive_service.create_hive_metastore_tables()
   hive_service.create_hive_warehouse()
   #don't think that the create_hive_userdir call is needed as the create_hive_warehouse already creates it
   #hive_service.create_hive_userdir()


# Deploys Impala - statestore, catalogserver, impalads
def deploy_impala(cluster, conf):
   impala_service_name = conf.service_name
   impala_service = cluster.create_service(impala_service_name, "IMPALA")
   impala_service.update_config(conf.service_config)

   ss = impala_service.get_role_config_group("{0}-STATESTORE-BASE".format(impala_service_name))
   ss.update_config(conf.statestore.role_config)
   impala_service.create_role("{0}-ss".format(impala_service_name), "STATESTORE", conf.statestore.host)

   cs = impala_service.get_role_config_group("{0}-CATALOGSERVER-BASE".format(impala_service_name))
   cs.update_config(conf.catalogserver.role_config)
   impala_service.create_role("{0}-cs".format(impala_service_name), "CATALOGSERVER", conf.catalogserver.host)

   id = impala_service.get_role_config_group("{0}-IMPALAD-BASE".format(impala_service_name))
   id.update_config(conf.impalad.role_config)

   impalad = 0
   for host in conf.impalad.hosts:
      impalad += 1
      impala_service.create_role("{0}-id-".format(impala_service_name) + str(impalad), "IMPALAD", host)
   impala_service.start()

   # Don't think we need these at the end:
   #impala_service.create_impala_catalog_database()
   #impala_service.create_impala_catalog_database_tables()
   #impala_service.create_impala_user_dir()

   return impala_service

# Deploys Search - solr server, gateways
#def deploy_search(cluster, search_service_name, search_service_config, search_solr_host, search_solr_config, search_gw_hosts, search_gw_config):
#   search_service = cluster.create_service(search_service_name, "SOLR")
#   search_service.update_config(search_service_config)
#
#   solr = search_service.get_role_config_group("{0}-SOLR_SERVER-BASE".format(search_service_name))
#   solr.update_config(search_solr_config)
#   search_service.create_role("{0}-solr".format(search_service_name), "SOLR_SERVER", search_solr_host)
#
#   gw = search_service.get_role_config_group("{0}-GATEWAY-BASE".format(search_service_name))
#   gw.update_config(search_gw_config)
#
#   gateway = 0
#   for host in search_gw_hosts:
#      gateway += 1
#      search_service.create_role("{0}-gw-".format(search_service_name) + str(gateway), "GATEWAY", host)
#
#   #solrctl init
#   zk_ensemble = ZOOKEEPER_HOSTS[0] + ":2181," + ZOOKEEPER_HOSTS[1] + ":2181," + ZOOKEEPER_HOSTS[2] + ":2181/solr"
#   shell_command = ["export SOLR_ZK_ENSEMBLE=" + zk_ensemble + "; solrctl init"]
#   solrctl_init_output = Popen(shell_command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True).stdout.read()
#
#   return search_service


# Deploys Flume - agents
#def deploy_flume(cluster, flume_service_name, flume_service_config, flume_agent_hosts, flume_agent_config):
#   flume_service = cluster.create_service(flume_service_name, "FLUME")
#   flume_service.update_config(flume_service_config)
#
#   gw = flume_service.get_role_config_group("{0}-AGENT-BASE".format(flume_service_name))
#   gw.update_config(flume_agent_config)
#
#   agent = 0
#   for host in flume_agent_hosts:
#      agent += 1
#      flume_service.create_role("{0}-agent-".format(flume_service_name) + str(agent), "AGENT", host)
#
#   return flume_service


# Deploys Oozie - oozie server.
# This does not support HA yet.
#def deploy_oozie(cluster, oozie_service_name, oozie_service_config, oozie_server_host, oozie_server_config):
#   oozie_service = cluster.create_service(oozie_service_name, "OOZIE")
#   oozie_service.update_config(oozie_service_config)
#
#   oozie_server = oozie_service.get_role_config_group("{0}-OOZIE_SERVER-BASE".format(oozie_service_name))
#   oozie_server.update_config(oozie_server_config)
#   oozie_service.create_role("{0}-server".format(oozie_service_name), "OOZIE_SERVER", oozie_server_host)
#
#   oozie_service.install_oozie_sharelib()
#
#   return oozie_service


# Deploys Sqoop - sqoop server
def deploy_sqoop(cluster, conf):
   sqoop_service_name = conf.service_name
#   sqoop_service = cluster.create_service(sqoop_service_name, "SQOOP")
#   sqoop_service.update_config(sqoop_service_config)


#   oozie_server = sqoop_service.get_role_config_group("{0}-SQOOP_SERVER-BASE".format(sqoop_service_name))
#   oozie_server.update_config(sqoop_server_config)
#   sqoop_service.create_role("{0}-server".format(sqoop_service_name), "SQOOP_SERVER", sqoop_server_host)
   sqoop_service = cluster.create_service(sqoop_service_name, "SQOOP_CLIENT")
   sqoop_service.update_config(conf.client.role_config)
   sqoop_service.create_role("{0}-client".format(sqoop_service_name), "GATEWAY", conf.client.host)

   return sqoop_service

## Deploys Kafka - kafka server
def deploy_kafka(cluster, conf):
   kafka_service_name = conf.service_name
   kafka_service = cluster.create_service(kafka_service_name, "KAFKA")
   kafka_service.update_config(translate_config(conf.service_config))

   kafka = kafka_service.get_role_config_group("{0}-KAFKA_BROKER-BASE".format(kafka_service_name))
   kafka.update_config(translate_config(conf.brokers.role_config))
   kafkabroker=0
   for host in conf.brokers.hosts:
      kafkabroker += 1
      kafka_service.create_role("{0}-KAFKA_BROKER".format(kafka_service_name)+str(kafkabroker), "KAFKA_BROKER", host)
   return kafka_service


# Deploys HUE - hue server
#def deploy_hue(cluster, hue_service_name, hue_service_config, hue_server_host, hue_server_config, hue_ktr_host, hue_ktr_config):
#   hue_service = cluster.create_service(hue_service_name, "HUE")
#   hue_service.update_config(hue_service_config)
#
#   hue_server = hue_service.get_role_config_group("{0}-HUE_SERVER-BASE".format(hue_service_name))
#   hue_server.update_config(hue_server_config)
#   hue_service.create_role("{0}-server".format(hue_service_name), "HUE_SERVER", hue_server_host)
#
#   #ktr = hue_service.get_role_config_group("{0}-KT_RENEWER-BASE".format(hue_service_name))
#   #ktr.update_config(hue_ktr_config)
#   #hue_service.create_role("{0}-ktr".format(hue_service_name), "KT_RENEWER", hue_ktr_host)
#
#   return hue_service


# Deploys Accumulo - masters, tracers, tservers, loggers, gateways, monitor, GC
def deploy_accumulo(cluster, service_name, service_config, master_hosts, master_config, tracer_hosts, tracer_config, tserver_hosts, tserver_config, logger_hosts, logger_config, monitor_host, monitor_config, gc_host, gc_config, gw_hosts, gw_config):

   accumulo = cluster.create_service(service_name, "csd_accumulo")
   accumulo.update_config(service_config)

   accumulo_masters_group = accumulo.get_role_config_group("{0}-MASTER-BASE".format(service_name))
   accumulo_masters_group.update_config(master_config)

   accumulo_tracers_group = accumulo.get_role_config_group("{0}-TRACER-BASE".format(service_name))
   accumulo_tracers_group.update_config(tracer_config)

   accumulo_tservers_group = accumulo.get_role_config_group("{0}-TSERVER-BASE".format(service_name))
   accumulo_tservers_group.update_config(tserver_config)

   accumulo_loggers_group = accumulo.get_role_config_group("{0}-LOGGER-BASE".format(service_name))
   accumulo_loggers_group.update_config(logger_config)

   accumulo_gateways_group = accumulo.get_role_config_group("{0}-GATEWAY-BASE".format(service_name))
   accumulo_gateways_group.update_config(gw_config)

   accumulo_monitor_group = accumulo.get_role_config_group("{0}-MONITOR-BASE".format(service_name))
   accumulo_monitor_group.update_config(monitor_config)

   accumulo_gc_group = accumulo.get_role_config_group("{0}-GC-BASE".format(service_name))
   accumulo_gc_group.update_config(gc_config)

   #Create Accumulo Masters
   count = 0
   for host in master_hosts:
      count += 1
      accumulo.create_role("{0}-master".format(service_name) + str(count), "csd_accumulo_master", host)

   #Create Accumulo Loggers
   count = 0
   for host in logger_hosts:
      count += 1
      accumulo.create_role("{0}-logger".format(service_name) + str(count), "csd_accumulo_logger", host)

   #Create Accumulo TServers
   count = 0
   for host in tserver_hosts:
      count += 1
      accumulo.create_role("{0}-tserver".format(service_name) + str(count), "csd_accumulo_tserver", host)

   #Create Accumulo Gateways
   count = 0
   for host in gw_hosts:
      count += 1
      accumulo.create_role("{0}-gateway".format(service_name) + str(count), "GATEWAY", host)

   #Create Accumulo Tracers
   count = 0
   for host in tracer_hosts:
      count += 1
      accumulo.create_role("{0}-tracer".format(service_name) + str(count), "csd_accumulo_tracer", host)

   #Create Accumulo GC
   accumulo.create_role("{0}-gc".format(service_name), "csd_accumulo_gc", gc_host)

   #Create Accumulo Monitor
   accumulo.create_role("{0}-monitor".format(service_name), "csd_accumulo_monitor", monitor_host)

   return accumulo

# Executes steps that need to be done after the final startup once everything is deployed and running.
def post_startup(cluster, hdfs_service):
   # Create HDFS temp dir
   hdfs_service.create_hdfs_tmp()
   hive_service_config=CONF.services.hive
   hive_service_name=hive_service_config.service_name

   # Create hive warehouse dir
   shell_command = ['curl -i -H "Content-Type: application/json" -X POST -u "' + CONF.cloudera_manager.user  + ':' + CONF.cloudera_manager.password + '" -d "serviceName=' + hive_service_name + ';clusterName=' + CONF.cluster_name + '" http://' + CM_HOST + ':7180/api/v5/clusters/' + CONF.cluster_name + '/services/' + hive_service_name + '/commands/hiveCreateHiveWarehouse']
   create_hive_warehouse_output = Popen(shell_command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True).stdout.read()

#   # Create oozie database
#   oozie_service.stop().wait()
#   shell_command = ['curl -i -H "Content-Type: application/json" -X POST -u "' + ADMIN_USER + ':' + ADMIN_PASS + '" -d "serviceName=' + OOZIE_SERVICE_NAME + ';clusterName=' + CLUSTER_NAME + '" http://' + CM_HOST + ':7180/api/v5/clusters/' + CLUSTER_NAME + '/services/' + OOZIE_SERVICE_NAME + '/commands/createOozieDb']
#   create_oozie_db_output = Popen(shell_command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True).stdout.read()
#   # give the create db command time to complete
#   time.sleep(30)
#   oozie_service.start().wait()

   # Deploy client configs to all necessary hosts
   cmd = cluster.deploy_client_config()
   if not cmd.wait(CMD_TIMEOUT).success:
      print "Failed to deploy client configs for {0}".format(cluster.name)

   # Noe change permissions on the /user dir so YARN will work
   shell_command = ['sudo -u hdfs hadoop fs -chmod 775 /user']
   user_chmod_output = Popen(shell_command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True).stdout.read()



### Main function ###
def main():
   API = ApiResource(CONF.cloudera_manager.host, version=5, username=CONF.cloudera_manager.user, password=CONF.cloudera_manager.password)
   MANAGER = API.get_cloudera_manager()
   MANAGER.update_config(CONF.cloudera_manager.config)
   print "Connected to CM host on " + CM_HOST + " and updated CM configuration"

   all_nodes=CONF.slavenodes
   all_nodes.append(CONF.masternode)
   if CONF.masternode != CONF.cm_host:
       all_nodes.append(CONF.cm_host)
	   
   CLUSTER = init_cluster(API, CONF.cluster_name, CONF.cdh_version, all_nodes)
   print "Initialized cluster " + CONF.cluster_name + " which uses CDH version " + CONF.cdh_version

   deploy_management(MANAGER, CONF.cloudera_manager.mgmt_service)
   print "Deployed CM management service " + CONF.cloudera_manager.mgmt_service.service_name + " to run on " + CM_HOST
   #if ADDITIONAL_PARCELS != '':
   # setup_additional_parcels(MANAGER,ADDITIONAL_PARCELS)

   # Added poor mans  re-try logic for  parcel distribution.
   retry_count=5
   for i in range(retry_count+1):
        try:
            print "Deploy Parcel try count - "+ str(i)
            deploy_parcels(CLUSTER, PARCELS)
            print "Downloaded and distributed parcels: "
            PRETTY_PRINT.pprint(PARCELS)
            break
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())
            if i < retry_count:
                time.sleep(10)
                continue
            raise

   zk_service_conf=CONF.services.zookeeper
   if zk_service_conf.enabled:
        zookeeper_service = deploy_zookeeper(CLUSTER,zk_service_conf)
        print "Deployed ZooKeeper " + zk_service_conf.service_name + " to run on: "
        PRETTY_PRINT.pprint(zk_service_conf.hosts)

   hdfs_service_conf=CONF.services.hdfs
   if hdfs_service_conf.enabled:
       hdfs_service = deploy_hdfs(CLUSTER,hdfs_service_conf)
       print "Deployed HDFS service " + hdfs_service_conf.service_name + " using NameNode on " + hdfs_service_conf.namenode.host + ", SecondaryNameNode on " +  hdfs_service_conf.secondary_namenode.host + ", and DataNodes running on: "
       PRETTY_PRINT.pprint(hdfs_service_conf.datanodes.hosts)

       init_hdfs(hdfs_service, hdfs_service_conf.service_name, CMD_TIMEOUT)
       print "Initialized HDFS service"
       enable_hdfs_ha(CLUSTER, hdfs_service, CMD_TIMEOUT)
       print "Enabled NameNode Successfully"

   yarn_service_conf=CONF.services.yarn
   if yarn_service_conf.enabled:
       yarn_service = deploy_yarn(CLUSTER, yarn_service_conf)
       print "Deployed YARN service " + yarn_service_conf.service_name + " using ResourceManager on " + yarn_service_conf.resource_manager.host + ", JobHistoryServer on " + yarn_service_conf.job_history_server.host + ", and NodeManagers on "
       PRETTY_PRINT.pprint(yarn_service_conf.node_manager.hosts)

   spark_service_conf=CONF.services.spark
   if spark_service_conf.enabled:
       spark_service = deploy_spark(CLUSTER, spark_service_conf)
       print "Deployed SPARK service " + spark_service_conf.service_name + " using SparkMaster on " + spark_service_conf.master.host + " and SparkWorkers on "
       PRETTY_PRINT.pprint(spark_service_conf.workers.hosts)

   hbase_service_config=CONF.services.hbase
   if hbase_service_config.enabled :
       deploy_hbase(CLUSTER, hbase_service_config)
       print "Deployed HBase service " + hbase_service_config.service_name + " using HMaster on " + hbase_service_config.master.host + " and RegionServers on "
       PRETTY_PRINT.pprint(hbase_service_config.region_servers.hosts)

   hive_service_config=CONF.services.hive
   if hive_service_config.enabled :
       hive_service = deploy_hive(CLUSTER, hive_service_config)
#       print "Deployed Hive service " + hive_service_config.service_name + " using HiveMetastoreServer on " + hive_service_config.master.host + " and HiveServer2 on " + hive_service_config.gateway.host + "and HiveGateway on"
#       PRETTY_PRINT.pprint(hive_service_config.gateway.hosts)
       init_hive(hive_service)
       print "Initialized Hive service"
   
   impala_service_config=CONF.services.impala
#   print "impala service flag" + impala_service_config.enabled 
   if impala_service_config.enabled :
       impala_service = deploy_impala(CLUSTER, impala_service_config)
       print "Deployed Impala service " + impala_service_config.service_name + " using StateStore on " + impala_service_config.statestore.host + ", CatalogServer on " + impala_service_config.catalogserver.host + ", and ImpalaDaemons on "
       PRETTY_PRINT.pprint(impala_service_config.impalad.hosts)

   #Need to start the cluster now as subsequent services need the cluster to be runnign
   #TODO can we just start ZK, and maybe HDFS, instead of everything? It's just needed for the search service
   print "About to restart cluster"
   CLUSTER.stop().wait()
   CLUSTER.start().wait()
   print "Done restarting cluster"

#    enable_hdfs_ha(CLUSTER, hdfs_service, CMD_TIMEOUT)
#    print "Enabled NameNode Successfully"

#   search_service = deploy_search(CLUSTER, SEARCH_SERVICE_NAME, SEARCH_SERVICE_CONFIG, SEARCH_SOLR_HOST, SEARCH_SOLR_CONFIG, SEARCH_GW_HOSTS, SEARCH_GW_CONFIG)
#   print "Deployed Search service " + SEARCH_SERVICE_NAME + " using SOLRHost " + SEARCH_SOLR_HOST
#
#   flume_service = deploy_flume(CLUSTER, FLUME_SERVICE_NAME, FLUME_SERVICE_CONFIG, FLUME_AGENT_HOSTS, FLUME_AGENT_CONFIG)
#   print "Deployed Flume service " + FLUME_SERVICE_NAME + " using FlumeAgents on "
#   PRETTY_PRINT.pprint(FLUME_AGENT_HOSTS)
#
#   oozie_service = deploy_oozie(CLUSTER, OOZIE_SERVICE_NAME, OOZIE_SERVICE_CONFIG, OOZIE_SERVER_HOST, OOZIE_SERVER_CONFIG)
#   print "Deployed Oozie service " + OOZIE_SERVICE_NAME + " using OozieServer on " + OOZIE_SERVER_HOST

   sqoop_service_config=CONF.services.sqoop
   if sqoop_service_config.enabled :
       sqoop_service = deploy_sqoop(CLUSTER, sqoop_service_config)
       print "Deployed Sqoop service " + sqoop_service_config.service_name + " using SqoopServer on " + sqoop_service_config.client.host
   
   kafka_service_conf = CONF.services.kafka
   if kafka_service_conf.enabled :
       kafka_service = deploy_kafka(CLUSTER, kafka_service_conf)
       print "Deployed Kafka service " + kafka_service_conf.service_name + " using KafkaServer on "
       PRETTY_PRINT.pprint(kafka_service_conf.brokers.hosts)
       kafka_service.start()
#   hue_service = deploy_hue(CLUSTER, HUE_SERVICE_NAME, HUE_SERVICE_CONFIG, HUE_SERVER_HOST, HUE_SERVER_CONFIG, HUE_KTR_HOST, HUE_KTR_CONFIG)
#   print "Deployed HUE service " + HUE_SERVICE_NAME + " using HueServer on " + HUE_SERVER_HOST

#   #deploy_accumulo(CLUSTER, ACCUMULO_SERVICE_NAME, ACCUMULO_SERVICE_CONFIG, ACCUMULO_MASTER_HOSTS, ACCUMULO_MASTER_CONFIG, ACCUMULO_TRACER_HOSTS, #ACCUMULO_TRACER_CONFIG, ACCUMULO_TSERVER_HOSTS, ACCUMULO_TSERVER_CONFIG, ACCUMULO_LOGGER_HOSTS, ACCUMULO_LOGGER_CONFIG, ACCUMULO_MONITOR_HOST, ACCUMULO_MONITOR_CONFIG, ACCUMULO_GC_HOST, ACCUMULO_GC_CONFIG, ACCUMULO_GATEWAY_HOSTS, ACCUMULO_GATEWAY_CONFIG)

   #print "About to restart cluster."
   #CLUSTER.stop().wait()
   #CLUSTER.start().wait()
   #print "Done restarting cluster."

   post_startup(CLUSTER, hdfs_service)

   print "Finished deploying Cloudera cluster. Go to http://" + CM_HOST + ":7180 to administer the cluster."
   print "If the Oozie service (and therefore the HUE service as well, which depends on it) did not start properly, go to the Oozie service, stop it, click on the Actions button and choose 'Create Database', then start it."
   print "If there are any other services not running, restart them now."


if __name__ == "__main__":
   main()
