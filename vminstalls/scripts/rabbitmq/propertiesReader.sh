#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

env=${1}
PROP_FILE=${THIS_DIR}/../../conf/conf-ab/rabbitmq/ab-${env}.conf

rmq_loopback_users_guest=`cat ${PROP_FILE} | grep -w rmq_loopback_users_guest | cut -d '=' -f2`
echo `date` "rmq_loopback_users_guest=${rmq_loopback_users_guest}"

rmq_vm_memory_high_watermark_relative=`cat ${PROP_FILE} | grep -w rmq_vm_memory_high_watermark_relative | cut -d '=' -f2`
echo `date` "rmq_vm_memory_high_watermark_relative=${rmq_vm_memory_high_watermark_relative}"