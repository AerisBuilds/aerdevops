#!/bin/bash


echo "Initiating RMQ configuration..."

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
rmq_nodes=${1}
project=${2}
zone=${3}
env=${4}

sudo chmod +x ${THIS_DIR}/propertiesReader.sh
source ${THIS_DIR}/propertiesReader.sh $env

# copy erlang.cookie value from first to rest of the node to make all nodes to be part of cluster.
rmq_leadingnode=`echo $rmq_nodes | cut -d ',' -f1`

erlang_val=`sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${rmq_leadingnode}" --internal-ip --command "sudo cat /var/lib/rabbitmq/.erlang.cookie"`
echo "value .erlang.cookie of first node of cluster = $erlang_val"

IFS=',' read -r -a nodesArr <<< "${rmq_nodes}"
for i in "${!nodesArr[@]}"
do
	if [ $i != 0 ]
	then
		echo "copying .erlang.cookie of first node ${rmq_leadingnode} into ${nodesArr[i]} "
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "sudo chmod 777 /var/lib/rabbitmq/.erlang.cookie && echo $erlang_val > /var/lib/rabbitmq/.erlang.cookie && sudo chmod 400 /var/lib/rabbitmq/.erlang.cookie"
	fi
done

# create conf file from tpl file and replace values in the placeholders
sudo chmod 777 -R $THIS_DIR/../../template/rabbitmq
rabbit_mq_conf_tpl=$THIS_DIR/../../template/rabbitmq/rabbitmq.conf.tpl
sudo chmod +x $rabbit_mq_conf_tpl
rabbit_mq_conf=$THIS_DIR/../../template/rabbitmq/rabbitmq.conf
rm -rf $rabbit_mq_conf

sudo cat $rabbit_mq_conf_tpl | sed "s#<rmq_loopback_users_guest>#${rmq_loopback_users_guest}#g; s#<rmq_vm_memory_high_watermark_relative>#${rmq_vm_memory_high_watermark_relative}#g;" > $rabbit_mq_conf

sudo chmod 777 $rabbit_mq_conf


# move conf file to /etc/rabbitmq to each node.

for i in "${!nodesArr[@]}"
do
		echo "copying configuration file $rabbit_mq_conf into ${nodesArr[i]} "
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "mkdir -p ~/rabbitmq/ && rm -rf ~/rabbitmq/rabbitmq.conf && sudo rm -rf /etc/rabbitmq/rabbitmq.conf"
		sudo gcloud  compute scp --project "${project}" --zone "${zone}"  --internal-ip $rabbit_mq_conf ${nodesArr[i]}:~/rabbitmq/
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "sudo mv ~/rabbitmq/rabbitmq.conf /etc/rabbitmq/ && sudo chown root:rabbitmq /etc/rabbitmq/rabbitmq.conf &&  sudo chmod 644 /etc/rabbitmq/rabbitmq.conf && rm -rf ~/esl-erlang_22.0.5-1~ubuntu~xenial_amd64.deb ~/rabbitmq-server_3.7.15-1_all.deb"
done



# restart all nodes using sudo systemctl restart rabbitmq-server.service
for i in "${!nodesArr[@]}"
do
	sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "sudo systemctl restart rabbitmq-server.service"
done


# except first node  (leading node) run below commands in rest of the nodes.

for i in "${!nodesArr[@]}"
do
	if [ $i != 0 ]
	then 
		sudo gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip  --command "sudo rabbitmqctl stop_app && sudo rabbitmqctl join_cluster rabbit@${rmq_leadingnode} && sudo rabbitmqctl start_app"
	fi	
done