#!/bin/bash

# 1) download erlang package
echo `date` "dowloading erlang package..."
wget https://packages.erlang-solutions.com/erlang/debian/pool/esl-erlang_22.0.5-1~ubuntu~xenial_amd64.deb

# 2) dpkg deb file
echo `date` "dpkg downloaded debian file..."
sudo dpkg -i esl-erlang_22.0.5-1~ubuntu~xenial_amd64.deb

# 3) dpkg program is not able to resolve dependencies of esl-erlang hence execute
echo `date` "dpkg program is not able to resolve dependencies of esl-erlang hence executing sudo apt-get install -y -f..."
sudo apt-get install -y -f

# 4) Install RMQ
echo `date` "Now starting rabbit MQ Installation process..."

	#remove any past apt-update for rabbitmq
	sudo rm -rf /etc/apt/sources.list.d/bintray.rabbitmq.list && sudo apt-get install socat
    # a) for auto update in future run
    wget https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.7.15/rabbitmq-server_3.7.15-1_all.deb
    #echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list 
   
    # b) getting public key of above package directory 
    #wget -O- https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc | sudo apt-key add -
   
    sudo dpkg -i rabbitmq-server_3.7.15-1_all.deb
    # c) and then we need to update list of packages and install rabbitmq-server package in common way:
   
    #sudo apt-get update && sudo apt-get install socat
    
	sudo apt-get install rabbitmq-server -y
	
	# d) check the status of sudo rabbitmqctl status if it exit with 0 then rabbitmq successfully installed.
	sudo rabbitmqctl status
	installation_sts=$?
	if [[ $tar_sts -eq 0 ]]; then
		echo `date` "Successfully installed RabbitMQ.."
	else
		echo `date` "Some Problem in RabbitMQ installation please check.."
		# we can call uninstall.sh script here to remove all junk folder created during installation process.
		./uninstall.sh &
		exit
	fi
	
# 5) setting up administrator panel
	sudo rabbitmq-plugins enable rabbitmq_management