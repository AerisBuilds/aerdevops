#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

sudo chmod +x ${THIS_DIR}/../read-vartf.sh
rmq_nodes=`${THIS_DIR}/../read-vartf.sh ${1} rabbitmq`
echo `date` "rmq_nodes=$rmq_nodes"
project=`cat ${1} | grep -w project | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "project=$project"
zone=`cat ${1} | grep -w zone | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "zone=$zone"
env=`cat ${1} | grep -w environment | cut -d '=' -f2 | sed 's/\"//g' | head -1`
echo `date` "env=$env"

if [[ -z "$rmq_nodes" ]]; then
	echo `date` "ERROR: Nodes information not provided. Hence Exiting installation.. "
	exit -1;
fi

IFS=',' read -r -a nodesArr <<< "${rmq_nodes}"

	for i in "${!nodesArr[@]}"
	do
	 	sudo /snap/bin/gcloud  compute --project "${project}" ssh --zone "${zone}" "${nodesArr[i]}"  --internal-ip --command "$(cat $THIS_DIR/install.sh)"
	done	
	
#   perform configuration
	sudo chmod +x $THIS_DIR/configure.sh
	$THIS_DIR/configure.sh ${rmq_nodes} ${project} ${zone} ${env}